import { NavigationContainer } from '@react-navigation/native';
import React, { useState, useEffect } from 'react';
import 'react-native-gesture-handler';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from './carebox/redux/store';
import { navigationRef } from './carebox/navigation/NavigationService';
import MainStackNavigator from './carebox/navigation/MainStackNavigator';
import MainContextProvider from './carebox/store/MainProvider';
import { save } from './carebox/utils/storage';
import { loginActions } from './carebox/redux/slices/LoginSlice';
import { AUTH_TOKEN } from './carebox/utils/constants';
import { SheetProvider } from 'react-native-actions-sheet';
import { profileActions } from './carebox/redux/slices/profile-slice';
const CareBox = (props) => {

  const user = props.user;
  console.log(user, "CareBoxUser")
  const saveAccessToken = async () => {
    try {
      await save(AUTH_TOKEN, user.accessToken)
    } catch (err) {
      console.error(err)
    }
  }

  useEffect(() => {
    saveAccessToken();
    store.dispatch(profileActions.getProfileSuccess(user?.userInfo));
    store.dispatch(loginActions.loginSuccess(user?.accessToken));
    store.dispatch(profileActions.saveRoles(user?.roles));
  }, [])

  return (
    <SheetProvider>
      <MainContextProvider>
        <SafeAreaProvider>
          <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
              <NavigationContainer ref={navigationRef} independent={true}>
                <MainStackNavigator />
              </NavigationContainer>
            </PersistGate>
          </Provider>
        </SafeAreaProvider>
      </MainContextProvider>
    </SheetProvider>

  );
};

export default CareBox;
