
# react-native-care-box

## Getting started

`$ npm install react-native-care-box --save`

### Mostly automatic installation

`$ react-native link react-native-care-box`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-care-box` and add `RNCareBox.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNCareBox.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.RNCareBoxPackage;` to the imports at the top of the file
  - Add `new RNCareBoxPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-care-box'
  	project(':react-native-care-box').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-care-box/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-care-box')
  	```

#### Windows
[Read it! :D](https://github.com/ReactWindows/react-native)

1. In Visual Studio add the `RNCareBox.sln` in `node_modules/react-native-care-box/windows/RNCareBox.sln` folder to their solution, reference from their app.
2. Open up your `MainPage.cs` app
  - Add `using Care.Box.RNCareBox;` to the usings at the top of the file
  - Add `new RNCareBoxPackage()` to the `List<IReactPackage>` returned by the `Packages` method


## Usage
```javascript
import RNCareBox from 'react-native-care-box';

// TODO: What to do with the module?
RNCareBox;
```
  