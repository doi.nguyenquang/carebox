Doc ghi chú tích hợp app carebox vào app tổng Hưng Thịnh
# Lưu ý: thêm independent={true} vào NavigationContainer vì do dùng chung 2 navigation
```js
 <NavigationContainer ref={navigationRef} independent={true}>
        <Stack.Navigator headerMode="none" initialRouteName="Login">
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="CareBox" component={CareBox} />
        </Stack.Navigator>
      </NavigationContainer>
```
## Add souce vào dự án
1. add link git vào package.json
```json
{
  "react-native-care-box": "https://gitlab.com/hungthinhcorp/mobile/healthy-carebox.git",
}

```

2. Chuyển màn hình vào carebox và config thông tin user
- Tham khảo màn hình src/carebox/index.js
```js
import React, {useContext, useEffect} from 'react';
import CareBox from 'react-native-care-box';

const CareBoxView = (props) => {
console.log(props, 'CareBoxView');
  return (
    <CareBox
      user = {props.route.params.data}
    />
  );
};

export default CareBoxView;

```

## Config Native
1. Android:
1.1. AndroidManifest.xml
```xml
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    package="com.hungthinh.carebox">

    
    <uses-feature android:name="android.hardware.camera" />
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-feature android:name="android.hardware.camera.autofocus" />

    <uses-permission android:name="android.permission.CAMERA" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    <uses-permission android:name="android.permission.CHANGE_NETWORK_STATE" />

    <uses-permission android:name="android.permission.SYSTEM_ALERT_WINDOW" />

    <uses-permission
        android:name="android.permission.BIND_TELECOM_CONNECTION_SERVICE"
        tools:ignore="ProtectedPermissions" />
    
    <uses-permission android:name="android.permission.CALL_PHONE" />

    <!-- OPTIONAL PERMISSIONS, REMOVE WHATEVER YOU DO NOT NEED -->
    <uses-permission
        android:name="android.permission.MANAGE_DOCUMENTS"
        tools:ignore="ProtectedPermissions" />
    <uses-permission android:name="android.permission.READ_INTERNAL_STORAGE" />
    <uses-permission android:name="android.permission.READ_PHONE_STATE" />
    <uses-permission android:name="android.permission.MODIFY_AUDIO_SETTINGS" />

    <!-- These require runtime permissions on M -->
    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
    <uses-permission android:name="android.permission.RECORD_AUDIO" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
    <uses-permission
        android:name="android.permission.WRITE_SETTINGS"
        tools:ignore="ProtectedPermissions" />

    <uses-permission android:name="android.permission.USE_FULL_SCREEN_INTENT" />
    <uses-permission android:name="android.permission.VIBRATE" />
    <uses-permission android:name="android.permission.FOREGROUND_SERVICE" />
    <uses-permission android:name="android.permission.WAKE_LOCK" />
    <uses-permission android:name="android.permission.DISABLE_KEYGUARD" />

    <application
        android:name=".MainApplication"
        android:allowBackup="false"
        android:icon="@mipmap/ic_launcher"
        android:label="@string/app_name"
        android:roundIcon="@mipmap/ic_launcher_round"
        android:theme="@style/AppTheme"
        android:usesCleartextTraffic="true">
        <activity
            android:name=".MainActivity"
            android:configChanges="keyboard|keyboardHidden|orientation|screenLayout|screenSize|smallestScreenSize|uiMode"
            android:turnScreenOn="true"
            android:showWhenLocked="true"
            android:exported="true"
            android:label="@string/app_name"
            android:launchMode="singleInstance"
            android:windowSoftInputMode="adjustResize">
            <intent-filter>
                <action android:name="android.intent.action.MAIN" />
                <category android:name="android.intent.category.LAUNCHER" />
                <action android:name="android.intent.action.DOWNLOAD_COMPLETE"/>
            </intent-filter>
        </activity>
        <!-- Thêm phần config path -->
        <provider
            android:name="androidx.core.content.FileProvider"
            android:authorities="${applicationId}.provider"
            android:exported="false"
            android:grantUriPermissions="true">
            <meta-data
                android:name="android.support.FILE_PROVIDER_PATHS"
                android:resource="@xml/provider_paths" /> 
        </provider>

    </application>
</manifest>
```
1.2. nội dung android/app/src/res/xml/provider_paths.xml
```xml
<?xml version="1.0" encoding="utf-8"?>
<paths xmlns:android="http://schemas.android.com/apk/res/android">
    <external-path name="external_files" path="."/>
</paths>
```
1.3. Thêm audio vào app:
- copy 2 file iphone.mp3 và waiting.mp3 từ foler android/app/src/res/raw vào dự án
- copy folder android/app/src/res/assets/fonts vào dự án

2. IOS:
2.1. mở xcode và kéo các file sau vào iphone.mp3, waiting.mp3 và các file fonts: SVN...
2.2. Info.plist thêm các thẻ sau

```plist
<key>NSAppleMusicUsageDescription</key>
	<string>Music!</string>
	<key>NSBluetoothAlwaysUsageDescription</key>
	<string>motion</string>
	<key>NSBluetoothPeripheralUsageDescription</key>
	<string>motion</string>
	<key>NSCalendarsUsageDescription</key>
	<string>Calendars</string>
	<key>NSCameraUsageDescription</key>
	<string>Bạn có thể dùng camera để lấy ảnh, tạo bản đăng và thay đổi cấu hình, wallpicture ...</string>
	<key>NSContactsUsageDescription</key>
	<string>contacts</string>
	<key>NSFaceIDUsageDescription</key>
	<string>Why is my app authenticating using face id?</string>
	<key>NSLocationAlwaysAndWhenInUseUsageDescription</key>
	<string>Always and when in use!</string>
	<key>NSLocationAlwaysUsageDescription</key>
	<string>Can I have location always?</string>
	<key>NSLocationUsageDescription</key>
	<string>Older devices need location.</string>
	<key>NSLocationWhenInUseUsageDescription</key>
	<string>Need location when in use</string>
	<key>NSMicrophoneUsageDescription</key>
	<string>Vui lòng cấp quyền cho microphone để sử dụng giọng nói cho video call hoặc chat</string>
	<key>NSMotionUsageDescription</key>
	<string>motion</string>
	<key>NSPhotoLibraryAddUsageDescription</key>
	<string>Image storage, report data</string>
	<key>NSPhotoLibraryUsageDescription</key>
	<string>Điều này cho phép bạn gửi hoặc đăng ảnh và video từ thư viện và lưu những người bạn chụp</string>
	<key>NSRemindersUsageDescription</key>
	<string>reminders</string>
	<key>NSSpeechRecognitionUsageDescription</key>
	<string>Vui lòng cấp quyền cho microphone để sử dụng tìm kiếm bằng giọng nói</string>
	<key>UIAppFonts</key>
	<array>
		<string>SVN-Gilroy-Black.otf</string>
		<string>SVN-Gilroy-Bold.otf</string>
		<string>SVN-Gilroy-Heavy.otf</string>
		<string>SVN-Gilroy-Light.otf</string>
		<string>SVN-Gilroy-Medium.otf</string>
		<string>SVN-Gilroy-Regular.otf</string>
		<string>SVN-Gilroy-SemiBold.otf</string>
		<string>SVN-Gilroy-Thin.otf</string>
		<string>SVN-Gilroy-XBold.otf</string>
		<string>SVN-Gilroy-Xlight.otf</string>
	</array>
	<key>UIBackgroundModes</key>
	<array>
		<string>audio</string>
		<string>fetch</string>
		<string>processing</string>
		<string>remote-notification</string>
		<string>voip</string>
	</array>
```

## Config url server: tải git và change https://gitlab.com/hungthinhcorp/mobile/healthy-carebox.git
Có 2 url cần thay đổi trong app. 
Search và replace url tương ứng: https://carebox.vantaidvt.com.vn và host peerjs: 167.179.72.201