import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    backgroundImage: {
        backgroundColor: 'white'
    },
    loginContainer: {
        flex: 1,
        backgroundColor: 'white'
    },
    emptyView: {
        height: 20
    },
    loginViewStyles: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 16,
        backgroundColor: 'white'
    },
    loginTitleStyles: {
    },
    loginContentStyles: {
        color: 'black',
        marginTop: 8,
    },
    inputContainer: {
        width: '100%',
        height: 60,
        borderRadius: 12,
        justifyContent: 'center',
        borderWidth: 1,
    },
    inputStyle: {
        height: '100%',
        paddingHorizontal: 16
    },
    vButton: {
        paddingHorizontal: 16,
        paddingBottom: 20,
        backgroundColor: 'white'
    },
    loginBtnStyles: {
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderRadius: 12,
    },
    btnTextStyles: {
        lineHeight: 24,
    }
})