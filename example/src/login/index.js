import React, { useState } from 'react';
import {
  View,
  Text,
  StatusBar,
  SafeAreaView,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
} from 'react-native';
import { isEmpty } from 'lodash';
import { styles } from './styles'
import * as api from '../../src/services/Request'
import ApiConfig from '../../src/services/api_config'

const LoginPage = ({navigation}) => {
  // const navigation = useNavigation()
  const [username, setUserName] = useState('chau.tranhoangphuong')
  const [password, setPassword] = useState('12345678')

  const onLoginPress = async () => {
    if (!isEmpty(username) && !isEmpty(password)) {
      const bodyData = {
        username: username,
        password: password,
      }
      // dispatch(loginActions.loginRequest({ bodyData: bodyData }))
      const res = await api.methodPost({ api: ApiConfig.loginAdmin, body: bodyData  });
      if (res['success']) {
          navigation.navigate('CareBox', {data: res['data']});
      }
      
    }
  }


  return (
    <View style={styles.container}>
      <StatusBar translucent backgroundColor="transparent" barStyle={"light-content"} />
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior={Platform.OS === 'ios' ? 'padding' : undefined}>
        <SafeAreaView style={styles.loginContainer}>
          <View style={styles.loginViewStyles}>
            <Text style={styles.loginTitleStyles}>CareBOX chào bạn</Text>
            <Text style={styles.loginContentStyles}>Vui lòng đăng nhập để tiếp tục</Text>
            <View style={styles.emptyView} />
            <View style={styles.inputContainer}>
              <TextInput
                value={username}
                onChangeText={(text) => setUserName(text)}
                style={styles.inputStyle}
                keyboardType={'email-address'}
                placeholder={"Username của bạn"}
              />
            </View>
            <View style={styles.emptyView} />
            <View style={styles.inputContainer}>
              <TextInput
                value={password}
                passwordReveal={true}
                autoCapitalize="none"
                autoCorrect={false}
                secureTextEntry={true}
                style={styles.inputStyle}
                placeholder={"Mật khẩu"}
                onChangeText={(text) => setPassword(text)}
              />
            </View>
          </View>
          <View style={styles.vButton}>
            <TouchableOpacity style={styles.loginBtnStyles} onPress={onLoginPress}>
              <Text style={styles.btnTextStyles}>ĐĂNG NHẬP</Text>
            </TouchableOpacity>
          </View>
        </SafeAreaView>
       
      </KeyboardAvoidingView>
    </View>
  )
}

export default LoginPage;
