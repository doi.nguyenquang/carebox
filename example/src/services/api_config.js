import {Platform} from 'react-native';

export default class ApiConfig {
  constructor() {}

  static isDebug = true;
  static isLog = true;

  static baseUrl = Platform.OS === 'ios' ? 'https://carebox.vantaidvt.com.vn' : 'http://167.179.72.201:3690';

  // static baseUrl = this.isDebug
  //     ? Platform.OS === 'ios'
  //         ? 'http://localhost:3690'
  //         : 'http://10.0.2.2:3690'
  //     : 'http://167.179.72.201:3690';
  // static  baseUrlSocket = baseUrl;

  //user
  static login = '/user/login';
  static logout = '/user/logout';
  static register = '/user/register';
  static loginLocal = '/user/local-auth';
  static postLocal = '/user-devices';
  static forgot = '/user/forgot';
  static resetPw = '/user/resetPw';
  static refreshToken = 'ac';
  static changeWallPage = '/user/changeWallpage';
  static userEdit = '/user/edit';
  static userCategory = '/user-category';
  static department = '/department';
  static changeAvatar = '/user/changeAvatar';
  static getUserCode = '/user/getUserCode';
  static changePw = '/user/changePw';
  static notification = '/noti/getAll';
  static staffs = '/user/search';
  static userProfile = '/user/profile';

  static getStaff = '/admin/doctors';
  static loginAdmin = '/admin/login';
  static getStaffDetail = '/admin/doctor-detail';
  //Chat.
  static chat = '/chat';

  //Notification.
  static settingNotification = '/setting-notification';
  static getAllNoti = '/noti';

  //category
  static categoryAll = '/category/getAll';
  static book = '/book';
  static bookCancel = '/book/reject-active';

  //Frequently Questions.
  static question = '/question';

  //news
  static news = '/news';
  static newsView = '/news/view';
  static newsFavorite = '/news-favorites';
  static newsDetail = '/news/detail';
  static newsAll = '/news/all';

  //post
  static postCreate = '/post/create';
  static post = '/post';
  static postDel = '/post/del';
  static like = '/like';
  static comment = '/comment';

  //course
  static course = '/course';
  static courseDetail = '/course/detail';
  static discussion = '/discussion';
  static commentDiscussion = '/comment-discussion';
  static likeDiscussion = '/like-discussion';
  static userCourseList = '/user-course-list';
  static courseComplete = '/user-course-complete';

  //quiz
  static quiz = '/quiz';
  static quizTest = '/quiz-test';
  static quizResult = '/quiz-result';
  static quizPecent = '/quiz-result/percent';

  //mini test
  static test = '/test';
  static testResult = '/test-result';

  //mailbox
  static getallMailbox = '/mail-box';
  static getMail = '/mail';

  //home
  static homePractise = '/home/practise';
  static home = '/home';

  //checkin

  static checkin = '/checkin';
  static checkinUser = '/user-checkin';

  //

  static bookMarket = '/news-favorites';
  static more = '/more';
  static userDevices = '/user-devices/dsDevices';

  static host = 'demo.cloudwebrtc.com';
  static notiRegular = '/noti-regulation';
  static call = '/noti/call';
  static userAccess = '/user-access';

  //'0: new, 1: course và quiz', 2 bản tin tâm lý
  static courseQuiz = '1';
  static cateNEW = '2';
  static hotline = '5';
  static privacy = '1';
  static happy = '3';
  static term = '0';

  //hungthing
  static cateH = `/news/cateMenu`;
  static getByCategory = `/news/getByCategory`;
  static isAdmin = `HEALTH_CARE_ADMIN`;
  static isDoctor = `SucKhoe_BacSi`;
}
