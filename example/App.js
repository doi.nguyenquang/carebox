import 'react-native-gesture-handler';
import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {navigationRef} from './src/helpers/RootNavigation';
import Login from './src/login';
import CareBox from './src/carebox';

const Stack = createStackNavigator();

export default function App() {
  return (
      <NavigationContainer ref={navigationRef} independent={true}>
        <Stack.Navigator headerMode="none" initialRouteName="Login">
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="CareBox" component={CareBox} />
        </Stack.Navigator>
      </NavigationContainer>
  );
}
