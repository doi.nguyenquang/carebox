/**
 * @format
 */

import {AppRegistry} from 'react-native';
// import CareBox from 'react-native-care-box';
import App from './App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
