export const EVENTS = {
    // News
    FILTER_DONE: 'FILTER_DONE',
    // Posts
    CREATE_SUCCESS: 'CREATE_SUCCESS',
    // Comment post success
    COMMENT_SUCCESS: 'COMMENT_SUCCESS',
}
