import { store } from '../../carebox/redux/store';
import axios from 'axios';
import ApiConfig from '../../carebox/services/api_config'
import { load } from '../../carebox/utils/storage';
import { isEmpty } from 'lodash';
import { AUTH_TOKEN } from '../../carebox/utils/constants';

const FormData = global.FormData;
// import https from 'https';

export const Request = axios.create({
  baseURL: ApiConfig.baseUrl,
  timeout: 20000,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json; charset=utf-8',
    // 'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
    // 'X-User-ID': 'tien.nguyennhat',
  },
  // httpsAgent: new https.Agent({
  //   rejectUnauthorized: false
  // })
});

// const AUTH_TOKEN = 'Bearer ' + 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxOSwidXNlcl9uYW1lIjoiY2FsbDFAZ21haWwuY29tIiwidXNlcl9mdWxsX25hbWUiOiJOZ3V54buFbiBWxINuIGNhbGwxIiwidXNlcl9lbWFpbCI6ImNhbGwxQGdtYWlsLmNvbSIsInVzZXJfcGhvbmUiOiJbXCIwOTQzNTc0NTU2XCIsXCJcIl0iLCJ1c2VyX2F2YXRhciI6Ii9wdWJsaWMvYXZhdGFycy82NjY4YTEyNC0yZmJiLTRjOTEtYjJjNy0wYTdiYjMzMzc3MGYuanBnIiwidXNlcl93YWxscGFnZSI6Ii9wdWJsaWMvaW1hZ2VzL2ZjYTNhYzUyLWE3Y2ItNDIzYS1iNmNmLTMzMGMyNThjMWViMy5qcGciLCJ1c2VyX2RlcGFydG1lbnQiOm51bGwsInVzZXJfYWRkcmVzcyI6ImRvdmFuZHU5eDAxQGdtYWlsLmNvbSIsImlkZW50aXR5X251bWJlciI6IjAxMjk5MzU3OCIsInVzZXJfcm9sZSI6IjMiLCJ1c2VyX3R5cGUiOiIxIiwidXNlcl9tYXJyaWFnZSI6IjEiLCJkZXBhcnRtZW50X2lkIjo1LCJ1c2VyX2NvZGUiOiI1OTEwNTEiLCJkZXMiOm51bGwsImNyZWF0ZWRfYXQiOiIyMDIyLTA0LTEwVDEzOjQxOjEwLjAwMFoiLCJ1cGRhdGVkX2F0IjoiMjAyMi0wNy0zMFQwODozMjo1Ni4wMDBaIiwiaWF0IjoxNjY2NDEwNzIyLCJleHAiOjE5ODE3NzA3MjJ9.VSL7s05IdroN083fRAQdtZXTUr2l_RQaf62VzFZCvBw';
// Request.defaults.headers.common['Authorization'] = AUTH_TOKEN;

Request.interceptors.request.use(
  async (config) => {
    let accessToken = await load(AUTH_TOKEN)
      .then((token) => token)
      .catch((err) => {
        console.debug(err);
      });
    console.log('accessToken ', accessToken);
    // if (store.getState()['feature/user'].isLogin) {
    //   //add token
    // }
    if (!isEmpty(accessToken)) {
      config.headers.common.Authorization = 'Bearer ' + accessToken;
    }
    return config;
  },
  function (error) {
    //handle error refresh token
    console.log('Requesterr', error)
    return Promise.reject(error);
  },
);

Request.interceptors.response.use(
  function (response) {
    // console.log('response', response)
    return response;
  },
  async function (err) {
    console.log('Requestresponseerr', err)
    return Promise.reject(err);
  },
);

export const _ApiRequest = async (method, url, { data, params, isFormData }) => {
  try {
    var body;
    var headers = {
      'user_id': store.getState().profileReducer?.profileData?.id,
      'X-User-ID': store.getState().profileReducer?.profileData?.preferredUsername
    };
    if (data != null) {
      if (isFormData) {
        if (ApiConfig.isLog) {
          console.log('data_ApiRequest', data)
        }

        body = data;
        headers = {
          ...headers,
          'Content-Type': 'multipart/form-data; charset=utf-8; boundary="another cool boundary";'
        }
      } else {
        if (ApiConfig.isLog) {
          console.log('data_ApiRequest', data)
        }
        body = data;
      }

    }
    if (params != null && ApiConfig.isLog) {
      console.log('paramsApiRequest', params)
    }
    const res = await Request.request({
      method: method,
      url: url,
      data: body,
      params: params,
      headers: headers
    })
    if (ApiConfig.isLog) {
      console.log('res----', res.data)
    }

    return res.data;
  } catch (err) {
    console.log('Request', err);
    return {
      'success': false,
      'message': err.message
    }
  }

};

export const _ApiRequestFormData = async (url, body) => {
  try {
    console.log(body, '_ApiRequestFormData');
    const res = await fetch(`${ApiConfig.baseUrl}${url}`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
        'user_id': store.getState().profileReducer?.profileData?.id,
        'X-User-ID': store.getState().profileReducer?.profileData?.preferredUsername,
      },
      body,
    });
    if (ApiConfig.isLog) {
      console.log('res----', res)
    }
    const resJson = await res.json();
    console.log(resJson);
    return resJson;
  } catch (err) {
    console.log('Request', err);
    return {
      'success': false,
      'message': err.message
    }
  }
};


export const methodGet = ({ api, queries }) => _ApiRequest('get', api, { params: queries });
export const methodPost = ({ api, body, isFormData }) => _ApiRequest('post', api, { data: body, isFormData });
export const methodPostFormData = ({ api, body }) => _ApiRequestFormData( api, body);
export const methodPut = ({ api, body }) => _ApiRequest('put', api, { data: body });
export const methodDel = ({ api, query }) => _ApiRequest('delete', api, { params: query });
