import axios from 'axios'
import ConfigUrl from '../config'
import DeviceInfo from 'react-native-device-info'
import { Platform } from 'react-native'
import Constants, { NAME_SCREEN } from '../constants'
import * as Bluebird from 'bluebird'
import { showModal, stopLoad } from '../modules/root/actions'
import { store } from '../../App'
import md5 from 'md5'
import constants from '../constants'
import AsyncStorage from '@react-native-community/async-storage'
import * as Navigation from '../navigation'
import { dateCodePush } from '@/constants/dateCodePush'
import ErrorHandle from './ErrorUtil'
import Utilities from './Utilities'
import { messages } from '@/constants/messages'
import { GlobalModalManager } from '~/Components'

class UtilApi {
  fetch = async (
    url,
    config = {
      // cacheKey?: '',
      method: '',
    },
    isLogin = false,
    contentType = 'application/json',
  ) => {
    try {
      const headerDevice = {
        deviceId: DeviceInfo.getUniqueId(),
        platform: Platform.OS,
        osVersion: Platform.Version,
        appVersion: ConfigUrl.BASE_URL === ConfigUrl.apiUrl_LIVE ?
          `${DeviceInfo.getVersion()}(${DeviceInfo.getBuildNumber()})` : dateCodePush,
        AuthenKey: md5(DeviceInfo.getUniqueId() + Constants.AuthenKeyPass),
      }
      config['method'] = config.method ? config.method : 'POST'
      config['timeout'] = 90000
      if (!config.headers) {
        config.headers = {}
      }
      config.validateStatus = function (status) {
        return status
      }
      config.headers['Content-Type'] = contentType
      if (isLogin) {
        config.headers['Accept'] = contentType
        config.headers['header'] = JSON.stringify(headerDevice)
      } else {
        const token = store.getState().get('root').get('token')
        if (token) {
          config.headers['Authorization'] = `Bearer ${token}`
          config.headers['DeviceId'] = DeviceInfo.getUniqueId()
        } else {
          Navigation.navigate(NAME_SCREEN.INPUT_PIN, { flag: Constants.INPUT_PIN_EXIT })
          return Bluebird.reject('')
        }
      }
      // set branch code
      const profile = store.getState().get('main').get('profileMe')
      const branches = profile?.BranchInfors || []
      const mainBranch = branches?.find((e) => e?.IsConcurrent === '0') || branches[0]
      const branchCode = await AsyncStorage.getItem('BRANCH_CODE' + profile?.LoginName)
      if (branchCode) {
        config.headers['branchcode'] = branchCode
      } else {
        if (mainBranch) {
          config.headers['branchcode'] = mainBranch?.BranchCode
          AsyncStorage.setItem('BRANCH_CODE' + profile?.LoginName, mainBranch?.BranchCode)
        }
      }
      config.url = ConfigUrl.BASE_URL + url
      let responseCache = null
      if (config.cacheKey) {
        responseCache = await AsyncStorage.getItem(`${config.cacheKey}_${config.url}`)
      }
      // console.log('responseCache', responseCache)
      const responseCacheData = JSON.parse(responseCache)
      if (config.cacheKey && await this.checkIsNeedGetCache(config.cacheKey)
        && responseCacheData && !Utilities.isArrayEmpty(responseCacheData?.Data || [])) {
        return Bluebird.resolve(responseCacheData)
      } else {
        console.log('config ', config)
        let response = await axios(config)
        console.log('response ' + url + ': ', response)
        store.dispatch(stopLoad())
        if (response && response.status == 200) {
          if (config.cacheKey && !Utilities.isArrayEmpty(response.data?.Data || [])) {
            await AsyncStorage.setItem(`${config.cacheKey}_${config.url}`, JSON.stringify(response.data))
          }
          return Bluebird.resolve(response.data)
        } else {
          switch (response.status) {
            case 401:
              let typeLogin = await AsyncStorage.getItem(constants.TYPE_LOGIN)
              const user = store.getState().get('main').get('profileMe')
              Navigation.push(NAME_SCREEN.INPUT_PIN_EXPIRED_TOKEN, { flag: typeLogin ? Utilities.getFlagScreen(typeLogin) : constants.INPUT_PIN_EXIT, userName: user.LoginName, isExpired: true })
              break
            case 403:
              GlobalModalManager.getRef().showFailedModal(messages.ERROR_SERVER_TITLE, messages.ACCESS_DENIED + ErrorHandle.checkIsShowLogError(response))
              break
            case 413:
              return response;
            default:
              GlobalModalManager.getRef().showFailedModal(messages.ERROR_SERVER_TITLE, messages.ERROR_SERVER + ErrorHandle.checkIsShowLogError(response))
              break
          }
        }
      }
    } catch (err) {
      store.dispatch(stopLoad())
      if (err.status === 401) {
        let typeLogin = await AsyncStorage.getItem(constants.TYPE_LOGIN)
        const user = store.getState().get('main').get('profileMe')
        Navigation.push(NAME_SCREEN.INPUT_PIN_EXPIRED_TOKEN, { flag: typeLogin ? Utilities.getFlagScreen(typeLogin) : constants.INPUT_PIN_EXIT, userName: user.LoginName, isExpired: true })
      }
      else if (axios.isCancel(err)) {
        console.log('cancel request')
      } else {
        const logError = '\n' + ' Util API : ' + err.toString() + '\n' + ' url : ' + url
        // GlobalModalManager.getRef().showFailedModal(messages.ERROR_SERVER_TITLE, messages.ERROR_SERVER + ErrorHandle.checkIsShowLogError(logError))
        return Bluebird.reject(err)
      }

    }
  }

  checkIsNeedGetCache = async (key) => {
    const configItem = Utilities.checkConfigCommon(store, key)
    const cacheLocalJson = await AsyncStorage.getItem(constants.CACHE_VERSION)
    const cacheLocal = JSON.parse(cacheLocalJson)
    let cacheLocalVersion = null
    if (cacheLocal) {
      cacheLocalVersion = cacheLocal?.find(item => item.NAME == key) || {}
    }
    if (parseInt(configItem?.VALUES_DATA || '0') <= parseInt(cacheLocalVersion?.VALUES_DATA || '0')) {
      return true
    } else {
      await AsyncStorage.setItem(constants.CACHE_VERSION, JSON.stringify(store.getState().get('root').get('configComon')))
      const allKey = await AsyncStorage.getAllKeys()
      const resultKey = allKey.filter(_key => _key.startsWith(key))
      console.log('resultKey', resultKey)
      await AsyncStorage.multiRemove(resultKey)
      return false
    }
  }
}

export default new UtilApi()
