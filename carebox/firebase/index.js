import firebase from '@react-native-firebase/app';
import { Platform } from 'react-native';

// Your secondary Firebase project credentials for Android...
const androidCredentials = {
    appId: '1:941014089874:android:2154a0cc1d163a575d264b',
    apiKey: 'AIzaSyCE5M5TQrsCYEmE3v3foZqzCOIV60jgqJA',
    projectId: 'carebox-9d9e8',
    messagingSenderId: '941014089874',
    authDomain: 'carebox-9d9e8.firebaseapp.com',
    androidClientId:
        '941014089874-gu4mjo7ccqd8tf15r3bl84pb51borsec.apps.googleusercontent.com',
};

// Your secondary Firebase project credentials for iOS...
const iosCredentials = {
    apiKey: 'AIzaSyCE5M5TQrsCYEmE3v3foZqzCOIV60jgqJA',
    appId: '1:941014089874:ios:403d74493d94e3865d264b',
    messagingSenderId: '941014089874',
    projectId: 'carebox-9d9e8',
    authDomain: 'carebox-9d9e8.firebaseapp.com',
    iosBundleId: 'com.example.carebox',
    iosClientId:
        '941014089874-n76spajt0cidhgdm55it939d7c7opq35.apps.googleusercontent.com',
    databaseURL: 'https://react-native-firebase-testing.firebaseio.com',
};

// Select the relevant credentials
const credentials = Platform.select({
    android: androidCredentials,
    ios: iosCredentials,
});

const config = {
    name: 'SECONDARY_APP',
};

await firebase.initializeApp(credentials, config);