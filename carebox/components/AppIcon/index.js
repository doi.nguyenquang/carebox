import theme from '../../../carebox/theme';
import React from 'react';
import {View} from 'react-native';

const AppIcon= ({
  color = theme.colors.base90,
  style,
  icon,
  size,
}) => {
  const viewStyle = {
    width: size ?? 24,
    height: size ?? 24,
    justifyContent: 'center',
    alignItems: 'center',
  };
  return <View style={[viewStyle, style]}>{icon({fill: color})}</View>;
};

export default AppIcon;
