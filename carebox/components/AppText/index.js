import theme from '../../../carebox/theme';
import {fontPixel} from '../../../carebox/utils/sizes';
import React from 'react';
import {Text, StyleSheet, View, TouchableOpacity} from 'react-native';
import { setFontFamily } from '../../../carebox/utils/functions';

const AppText = props => {
  const {
    title,
    numberOfLines,
    style,
    allowFontScaling,
    type = 'text',
    size,
    color,
    containerStyle,
    onPress
  } = props;
  if (!title) {
    return null;
  }
  const textStyle = {
    color: color ?? theme.colors.black,
    fontSize: type === 'text' ? fontPixel(size ?? 14) : fontPixel(size ?? 16),
  };
  return (
    <TouchableOpacity style={containerStyle} onPress={onPress} disabled={!onPress}>
    <Text
      style={[
        type === 'title' ? styles.defaultTitle : styles.defaultText,
        textStyle,
        style,
      ]}
      numberOfLines={numberOfLines ?? 1}
      allowFontScaling={allowFontScaling ?? false}
      {...props}>
      {title}
    </Text>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  defaultText: {
    flexShrink: 1,
    fontSize: fontPixel(14),
    color: theme.colors.black,
    fontFamily: setFontFamily('GilroyRegular'),
  },
  defaultTitle: {
    flexShrink: 1,
    fontSize: fontPixel(16),
    color: theme.colors.black,
    fontWeight: 'bold',
    fontFamily: setFontFamily('GilroyRegular'),
  },
});
export default AppText;
