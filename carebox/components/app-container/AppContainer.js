import {COLORS} from '../../../carebox/utils/colors';
import React from 'react';
import {StyleSheet} from 'react-native';
import {View} from 'react-native';

const AppContainer = ({children, style}) => {
  return <View style={[styles.container, style]}>{children}</View>;
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.white,
  },
});
export default AppContainer;
