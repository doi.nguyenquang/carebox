import React, {useState} from 'react';
import {TextInput} from 'react-native';
import styles from './styles';

const AppTextInput = ({
  messages,
  onChangeText,
  placeholder,
  style,
  maxLength,
}) => {
  const [isFocused, setIsFocused] = useState(false);
  const onBlur = () => {
    if (isFocused) {
      setIsFocused(false);
    }
  };
  const onFocus = () => {
    if (!isFocused) {
      setIsFocused(true);
    }
  };
  return (
    <TextInput
      onBlur={onBlur}
      onFocus={onFocus}
      maxLength={maxLength || 100}
      style={[styles.textInput, style]}
      placeholder={placeholder || 'Soạn tin nhắn...'}
      value={messages}
      onChangeText={onChangeText}
    />
  );
};

export default AppTextInput;
