import {COLORS} from '../../../carebox/utils/colors';

const {StyleSheet} = require('react-native');

const styles = StyleSheet.create({
  viewTextInput: {
    borderRadius: 10,
    borderWidth: 1,
    paddingVertical: 8,
    paddingHorizontal: 16,
    marginHorizontal: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  viewTextInputNotActive: {
    borderColor: COLORS.gray1,
  },
  viewTextInputActive: {
    borderColor: COLORS.green2,
  },
  textInput: {
    width: '90%',
    paddingVertical: 0,
  },
});
export default styles;
