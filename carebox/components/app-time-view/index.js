import React from 'react';
import { View, StyleSheet, Image, Text } from 'react-native';
import { COLORS } from '../../../carebox/utils/colors';
import { IconLock } from '../../../carebox/assets/icons'
import { toHHMMSS } from '../../../carebox/utils/utils'

const AppTimeView = (props) => {
  const { value , style} = props;
  console.log('AppTimeView', value);
  if (value) {
    const time = toHHMMSS(value);
    return (
      <View style={[styles.target, style]}>
        <Image style={styles.icTarget} source={IconLock} />
        <Text style={styles.targetValue}>{time}</Text>
      </View>
    );
  }
  return <View/>;
};

const styles = StyleSheet.create({
  target: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    width: '100%',
    // justifyContent: 'center',
  },
  targetValue: {
    color: COLORS.black303030,
    flexWrap: 'wrap'
  },
  icTarget: {
    color: COLORS.orangeF58220,
    width: 18,
    height: 18,
    marginRight: 10,
  },
});

export default AppTimeView;
