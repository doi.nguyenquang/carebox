import {COLORS} from '../../../carebox/utils/colors';
import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  loading: {
    backgroundColor: COLORS.black,
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    justifyContent: 'center',
    alignContent: 'center',
  },
  error: {
    backgroundColor: COLORS.black,
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    justifyContent: 'center',
    alignContent: 'center',
  },
  errorText: {
    color: COLORS.white,
    textAlign: 'center',
  },
});
export default styles;
