import React from 'react';
import {Image} from 'react-native';
import PropTypes from 'deprecated-react-native-prop-types';
import {StyleSheet} from 'react-native';
import {fontPixel} from '../../../carebox/utils/sizes';

const IconImage = ({source, style}) => {
  return <Image 
  resizeMode = "contain"
  source={source}
  style={[styles.container, style]} />;
};
IconImage.prototype = {
  source: PropTypes.node,
  style: PropTypes.object,
};

IconImage.defaultProps = {
  source: require('../../../carebox/assets/icons/ic_voice_chat.png'),
  style: {},
};
const styles = StyleSheet.create({
  container: {
    width: fontPixel(24),
    height: fontPixel(24),
  },
});
export default IconImage;
