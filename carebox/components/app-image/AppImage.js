import {COLORS} from '../../../carebox/utils/colors';
import React, {useState} from 'react';
import {ActivityIndicator, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import styles from './styles';

export default function AppImage({uri, style, resizeMode}) {
  const [loading, setLoading] = useState(false);
  const [err, setError] = useState(false);
  return (
    <View style={style}>
      <FastImage
        style={style}
        onLoadStart={() => {
          setLoading(true);
        }}
        onError={() => {
          setLoading(false);
          setError(true);
        }}
        onLoadEnd={() => {
          setLoading(false);
        }}
        source={{
          uri: uri,
        }}
        resizeMode={resizeMode}
      />
      {!loading && err && <View style={[style, styles.error]} />}
      {loading && (
        <View style={[style, styles.loading]}>
          <ActivityIndicator size="small" color={COLORS.loading} />
        </View>
      )}
    </View>
  );
}
