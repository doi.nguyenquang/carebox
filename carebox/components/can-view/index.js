import React from 'react'
import { StyleProp, View, ViewStyle } from 'react-native'

const CanView = ({ condition, children, isReturnNull, style = {}, fallbackComponent, ...rest }) => {
  if (!condition && fallbackComponent) {
    return fallbackComponent()
  }

  if (!condition && isReturnNull) {
    return null
  }
  return (
    <View style={[{ display: !!condition ? 'flex' : 'none' }, style]} {...rest}>
      {children}
    </View>
  )
}

export default CanView
