import React from 'react';
import { View, StyleSheet, Image, Text } from 'react-native';
import { COLORS } from '../../../carebox/utils/colors';
import { IconLock } from '../../../carebox/assets/icons'
import { toHHMMSS } from '../../../carebox/utils/utils'
import { CategoryModel } from '../../../carebox/models/category_model'
import { setFontFamily } from '../../../carebox/utils/functions';

const AppCateView = (props) => {
  const { value } = props;
  const model: CategoryModel = value;
  console.log('AppCateView', model);
  if(model){
    return (
      <View style={styles.target}>
        <Text style={styles.targetValue}>{model.categoryTitle}</Text>
      </View>
    );
  }
  return null;
 
};

const styles = StyleSheet.create({
  target: {
    backgroundColor: COLORS.blue54BADE,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    alignItems:'center',
    alignSelf: 'center',
    justifyContent: 'center',
    paddingHorizontal: 15,
    paddingVertical:3
  },
  targetValue: {
    color: COLORS.white,
    width: '100%',
    fontSize: 12,
    fontFamily: setFontFamily('GilroyRegular'),
  },
  
});

export default AppCateView;
