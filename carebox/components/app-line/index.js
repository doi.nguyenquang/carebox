import React from 'react';
import {View, StyleSheet} from 'react-native';
import { COLORS } from '../../../carebox/utils/colors';

const AppLine = (props) => {
  const {style} = props;
  return (
    <View  style={[styles.line, style ]}   />
  );
};

const styles = StyleSheet.create({
  line: {
    width: 'auto',
    height:1,
    backgroundColor: COLORS.greyE9E9E9,
  }
});

export default AppLine;
