import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { COLORS } from '../../../carebox/utils/colors';
import { fontPixel } from '../../../carebox/utils/sizes';
import { setFontFamily } from '../../../carebox/utils/functions';
import { IconMore } from '../../../carebox/assets/icons'
import { IconImage } from '../../../carebox/components/app-image';

export const TitleCardComponent = (props) => {
  const { titleText, titleStyle, onPress, containerStyles, iconStyles, more } = props
  const moreValue = more ?? true;
  return (
    <View style={styles.vList}>
      <Text
        numberOfLines={1}
        style={[styles.titleStyles, titleStyle]}>{titleText}</Text>
      {moreValue && <TouchableOpacity onPress={onPress}>
        <IconImage
          source={IconMore}
          style={[styles.nextIconStyles, iconStyles]}
        />
      </TouchableOpacity>}
    </View>
  )
}

const styles = StyleSheet.create({
  vList: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
  },
  titleStyles: {
    flex: 1,
    color: COLORS.black303030,
    fontWeight: 'bold',
    lineHeight: 24,
    fontSize: fontPixel(16),
    fontFamily: setFontFamily('GilroyBold')
  },
  nextIconStyles: {
    marginLeft: 10
  }
})