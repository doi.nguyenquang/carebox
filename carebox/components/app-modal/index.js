import React from 'react';
import {StyleSheet} from 'react-native';
import {KeyboardAvoidingView, Platform} from 'react-native';
import Modal from 'react-native-modal';

const AppModal = props => {
  const {isVisible, hide, children, backdropOpacity, style, onModalHide} = props;
  return (
    <Modal
      isVisible={isVisible}
      onBackButtonPress={hide}
      backdropOpacity={backdropOpacity ?? 0.2}
      onModalHide={onModalHide ? onModalHide : () => {}}
      style={[styles.container, style]}>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : undefined}>
        {children}
      </KeyboardAvoidingView>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-end',
    margin: 0,
  },
});

export default AppModal;
