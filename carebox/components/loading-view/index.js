import React from 'react';
import { ActivityIndicator, View, StyleSheet } from 'react-native';
import { COLORS } from '../../../carebox/utils/colors';
import AppText from '../app-text';
import { fontPixel, heightPixel } from '../../../carebox/utils/sizes';
import { setFontFamily } from '../../../carebox/utils/functions';

const LoadingView = (props) => {
  const { title } = props;
  return (
    <View style={styles.view}>
      <ActivityIndicator color={COLORS.loading} size={'small'} />
      <AppText style={styles.textTitle}>{title ?? 'Đang tải...'}</AppText>
    </View>
  );
};

const styles = StyleSheet.create({
  textTitle: {
    fontSize: fontPixel(14),
    color: COLORS.black,
    fontWeight: '400',
    fontFamily: setFontFamily('GilroyRegular'),
  },
  view: {
    height: heightPixel(96),
    borderRadius: 14,
    paddingVertical: heightPixel(14),
    alignItems: 'center',
    justifyContent: 'center',
    
  },
});

export default LoadingView;
