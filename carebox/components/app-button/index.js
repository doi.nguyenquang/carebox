import React from 'react';
import {
  GestureResponderEvent,
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import _ from 'lodash';
// import {AppButtonProps} from './types';
import { fontPixel } from '../../../carebox/utils/sizes';
import { COLORS } from '../../../carebox/utils/colors';
import { setFontFamily } from '../../../carebox/utils/functions';

const AppButton = (props: AppButtonProps) => {
  const { children, onPress, title, style, styleBtn } = props;
  const handlePress = (e: GestureResponderEvent) => {
    if (!onPress) {
      return;
    } else {
      var throt_fun = _.throttle(onPress, 2000);
      throt_fun(e);
    }
  };
  return (
    <TouchableOpacity
      style={styleBtn}
      hitSlop={{
        bottom: fontPixel(10),
        top: fontPixel(10),
        right: fontPixel(10),
        left: fontPixel(10),
      }}
      activeOpacity={0.9}
      {...props}
      onPress={handlePress}>
      {title && <View style={styles.btnTitle}>
        <Text style={[styles.title]}>{title}</Text>
      </View>}
      {children}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  title: {
    textAlign: 'center',
    color: 'white',
    textTransform: 'uppercase',
    fontFamily: setFontFamily('GilroyMedium')
  },
  btnTitle: {
    width: 'auto',
    borderRadius: 10,
    paddingVertical: 10,
    backgroundColor: COLORS.main,
    paddingHorizontal: 15,
  }
});

export default AppButton;
