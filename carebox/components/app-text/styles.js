import {COLORS} from '../../../carebox/utils/colors';
import {fontPixel} from '../../../carebox/utils/sizes';
import {StyleSheet} from 'react-native';
import { setFontFamily } from '../../../carebox/utils/functions';

const styles = StyleSheet.create({
  text: {
    flexShrink: 1,
    fontSize: fontPixel(16),
    color: COLORS.black,
    fontFamily: setFontFamily('GilroyRegular'),
  },
});
export default styles;
