import React from 'react';
import {Text} from 'react-native';
import styles from './styles';

const AppText = props => {
  const {title, numberOfLines, style, allowFontScaling} = props;
  if (!title) {
    return null;
  }
  return (
    <Text
      style={[styles.text, style]}
      numberOfLines={numberOfLines ?? 1}
      allowFontScaling={allowFontScaling ?? false}
      {...props}>
      {title}
    </Text>
  );
};

export default AppText;
