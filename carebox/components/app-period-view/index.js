import React from 'react';
import { View, StyleSheet, Image, Text } from 'react-native';
import { COLORS } from '../../../carebox/utils/colors';
import { IconLesson } from '../../../carebox/assets/icons'
import { fontPixel } from '../../../carebox/utils/sizes';
import { setFontFamily } from '../../../carebox/utils/functions';

const AppPeriodView = (props) => {
  const { value , style} = props;
  if (value) {
    return (
      <View style={[styles.target, style]}>
        <Image style={styles.icTarget} source={IconLesson} />
        <Text style={styles.targetValue}>{value} tiết</Text>
      </View>
    );
  }
  return <View/>;
};

const styles = StyleSheet.create({
  target: {
    flex: 1,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  targetValue: {
    width: '100%',
    flexShrink: 1,
    color: COLORS.black303030,
    fontSize: fontPixel(14),
    fontFamily: setFontFamily('GilroyMedium')
  },
  icTarget: {
    color: COLORS.orangeF58220,
    width: 24,
    height: 24,
    marginRight: 10,
  },
});

export default AppPeriodView;
