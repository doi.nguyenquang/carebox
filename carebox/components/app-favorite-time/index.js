import React from 'react';
import { View, StyleSheet, Image, Text } from 'react-native';
import { COLORS } from '../../../carebox/utils/colors';
import { IconFavorite } from '../../../carebox/assets/icons'
import AppTimeView from '../../../carebox/components/app-time-view';
import AppFavoriteView from '../../../carebox/components/app-favorite';

const FavoriteTimeView = (props) => {
  const { favorite, time, style } = props;
  return (
    <View style={[styles.target, style]}>
      <AppFavoriteView value={favorite}/>
      <AppTimeView value={time} />
    </View>
  );
};

const styles = StyleSheet.create({
  target: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    width: '100%',
  },
  targetValue: {
    color: COLORS.black303030,
  },
 
});

export default FavoriteTimeView;
