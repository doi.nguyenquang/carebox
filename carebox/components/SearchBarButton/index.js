import React, {memo} from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import theme from '../../../carebox/theme';
import AppIcon from '../AppIcon';
import {FilterIcon, SearchIcon} from '../../../carebox/assets/svg';
import {TextInput} from 'react-native-gesture-handler';

const SearchBarButton = ({
  onPressLeft,
  onPressRight,
  style,
  rightIconColor,
}) => {
  return (
    <View style={[styles.container, style]}>
      <TouchableOpacity
        style={styles.searchContainer}
        activeOpacity={1}
        onPress={onPressLeft}>
        <View style={styles.searchIcon}>
          <AppIcon icon={SearchIcon} />
        </View>
        <TextInput
          pointerEvents="none"
          autoCompleteType={'off'}
          returnKeyType="search"
          autoCorrect={false}
          editable={false}
          // style={{backgroundColor: theme.colors.white}}
          placeholder={'Nhập nội dung cần tìm'}
          keyboardType={'default'}
        />
      </TouchableOpacity>
      <TouchableOpacity
        onPress={onPressRight}
        style={styles.buttonRight}
        activeOpacity={1}>
        <AppIcon
          icon={FilterIcon}
          color={rightIconColor}
          style={styles.filterIcon}
        />
      </TouchableOpacity>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: theme.colors.transparent,
    flexDirection: 'row',
  },
  searchContainer: {
    flex: 1,
    backgroundColor: theme.colors.white,
    flexDirection: 'row',
    borderWidth: 1,
    borderRadius: 12,
    borderColor: '#E5E5E5',
    zIndex: 999,
  },
  searchIcon: {
    marginHorizontal: 16,
    marginVertical: 8,
  },
  filterIcon: {
    margin: 8,
  },
  buttonRight: {
    marginLeft: 16,
    backgroundColor: theme.colors.white,
    borderWidth: 1,
    borderRadius: 12,
    borderColor: '#E5E5E5',
    alignItems: 'center',
    alignContent: 'center',
  },
});

export default memo(SearchBarButton);
