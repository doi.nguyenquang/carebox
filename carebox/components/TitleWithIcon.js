import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import theme from '../../carebox/theme';
import AppText from './AppText';
import AppIcon from './AppIcon';

export const TitleWithIcon = ({
  card,
  leftIcon,
  rightIcon,
  title,
  style,
  onPress,
  titleStyle,
  leftIconColor,
  rightIconColor,
}) => {
  return (
    <TouchableOpacity
      style={[card ? styles.containerCard : styles.container, style]}
      disabled={!onPress}
      onPress={onPress}>
      {leftIcon && <AppIcon icon={leftIcon} color={leftIconColor} />}
      <AppText style={[styles.text, titleStyle]} title={title} />
      {rightIcon && <AppIcon icon={rightIcon} color={rightIconColor} />}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  containerCard: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  text: {
    // fontSize: 16,
    // fontWeight: '600',
    color: theme.colors.base90,
    marginLeft: 8,
  },
});
