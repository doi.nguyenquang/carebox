import React, {memo} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import theme from '../../../carebox/theme';
import AppIcon from '../AppIcon';
import {BackIcon, SettingIcon, SearchIcon} from '../../../carebox/assets/svg';
import { setFontFamily } from '../../../carebox/utils/functions';

const HeaderBase = ({
  searchBar,
  onPressLeft,
  title,
  leftIcon,
  leftTitle,
  fontSizeTitle,
  leftIconColor,
  rightTitle,
  rightIcon,
  onPressRight,
  isNotShowBack,
  containerStyle,
  txtRightStyle,
  rightIconColor,
  colorTitle,
  backgroundColor,
  statusBar = true,
  barStyle,
}) => {
  const navigation = useNavigation();

  const onPress = () => {
    if (onPressLeft) {
      onPressLeft();
    } else {
      navigation.goBack();
    }
  };

  const titleTxtColor = {
    color: colorTitle ? colorTitle : theme.colors.base90,
    fontSize: fontSizeTitle || 16,
  };

  const renderLeftContainer = () => {
    return (
      <>
        {isNotShowBack ? (
          leftTitle && (
            <TouchableOpacity style={styles.buttonLeft} onPress={onPressLeft}>
              <Text numberOfLines={1} style={[styles.rightText, txtRightStyle]}>
                {leftTitle}
              </Text>
            </TouchableOpacity>
          )
        ) : (
          <TouchableOpacity onPress={onPress} style={[styles.buttonLeft]}>
            <AppIcon icon={leftIcon ?? BackIcon} color={leftIconColor} />
          </TouchableOpacity>
        )}
      </>
    );
  };
  const renderContentHeader = () => {
    return (
      <>
        {searchBar ? (
          <View style={styles.searchContainer}>
            <View style={{marginHorizontal: 16, marginVertical: 8}}>
              <AppIcon icon={SearchIcon} />
            </View>
            <TextInput
              // autoFocus
              autoCompleteType={'off'}
              returnKeyType="search"
              autoCorrect={false}
              // value={'adsasdas'}
              placeholder={'Nhập nội dung cần tìm'}
              keyboardType={'default'}
            />
          </View>
        ) : (
          title && (
            <View style={styles.titleView}>
              <Text
                style={[styles.titleStyle, titleTxtColor]}
                numberOfLines={1}>
                {title}
              </Text>
            </View>
          )
        )}
      </>
    );
  };

  const renderRightContainer = () => {
    return (
      <>
        {rightTitle ? (
          <TouchableOpacity style={styles.buttonRight} onPress={onPressRight}>
            <Text numberOfLines={1} style={[styles.rightText, txtRightStyle]}>
              {rightTitle}
            </Text>
          </TouchableOpacity>
        ) : (
          rightIcon && (
            <TouchableOpacity onPress={onPressRight} style={styles.buttonRight}>
              <AppIcon icon={rightIcon ?? SettingIcon} color={rightIconColor} />
            </TouchableOpacity>
          )
        )}
      </>
    );
  };

  return (
    <>
      <View style={[styles.container, containerStyle]}>
        {renderLeftContainer()}
        {renderContentHeader()}
        {!searchBar && renderRightContainer()}
      </View>
    </>
  );
};
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: theme.colors.transparence,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: Platform.OS === 'android' ? 8 : 0,
    width: theme.dimensions.widthScreen,
  },
  searchContainer: {
    flex: 1,
    backgroundColor: theme.colors.white,
    flexDirection: 'row',
    borderWidth: 1,
    borderRadius: 12,
    borderColor: '#E5E5E5',
    marginHorizontal: 16,
  },
  buttonLeft: {
    paddingLeft: 16,
    paddingVertical: 8,
    zIndex: 1,
  },
  buttonRight: {
    alignItems: 'flex-end',
    paddingRight: 16,
    paddingVertical: 8,
    zIndex: 1,
  },
  hitSlop: {
    bottom: 16,
    left: 16,
    right: 16,
    top: 16,
  },
  iconStyle: {
    height: 24,
    tintColor: theme.colors.base90,
    width: 24,
  },
  rightIcon: {
    alignItems: 'flex-end',
    paddingRight: 16,
    paddingVertical: 8,
  },
  rightText: {
    color: theme.colors.white,
    fontSize: 16,
    fontWeight: '400',
    fontFamily: setFontFamily('GilroyRegular'),
  },
  titleStyle: {
    color: theme.colors.base90,
    fontSize: 16,
    fontWeight: '500',
    paddingVertical: 8,
    textAlign: 'center',
    fontFamily: setFontFamily('GilroyRegular'),
  },
  titleView: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    flex: 1,
    width: '100%',
    position: 'absolute',
  },
});

export default memo(HeaderBase);
