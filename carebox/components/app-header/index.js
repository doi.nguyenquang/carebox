import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Image, Text, TextInput } from 'react-native';
import { COLORS } from '../../../carebox/utils/colors';
import { IconBack, IconSearch, IconClose } from '../../../carebox/assets/icons'
import AppButton from '../../../carebox/components/app-button'
import { NavigationService } from '../../../carebox/navigation/NavigationService';
import { setFontFamily } from '../../../carebox/utils/functions';

const AppHeader = (props) => {
  const {
    title,
    children,
    style,
    onBack,
    search,
    onChangeText,
    onSubmitEditing,
    showBack = true,
    searchStyle,
  } = props;

  const [isShowClear, setShowClear] = useState(false);
  const [val, setVal] = React.useState('');

  const onBackPress = () => {
    if (onBack) {
      onBack();
    } else {
      NavigationService.goBack();
    }
  }

  const _onChangeText = (text) => {
    if (onChangeText) {
      onChangeText(text);
    }
    if (!isShowClear) {
      setShowClear(true)
    }
    setVal(text);
  }

  const _onClose = () => {
    setShowClear(false);
    setVal('');
    onSubmitEditing(null);
  }

  return (
    <View style={[styles.target, style]}>
      {showBack && <AppButton
        onPress={onBackPress}>
        <Image style={styles.icTarget} source={IconBack} />
      </AppButton>}
      {title && <View style={styles.vTitle}>
        <Text style={styles.title} numberOfLines={1}>{title}</Text>
      </View>}
      {search && <View style={[styles.vsearch, searchStyle]}>
        <AppButton
          onPress={() => onSubmitEditing(val)}>
          <Image
            source={IconSearch}
            style={styles.imageStyle}
          />
        </AppButton>

        <TextInput
          style={styles.input}
          onChangeText={_onChangeText}
          value={val}
          placeholder="Tìm kiếm..."
          underlineColorAndroid="transparent"
          returnKeyType='search'
          returnKeyLabel='search'
          onSubmitEditing={() => onSubmitEditing(val)}
        />
        {isShowClear && <AppButton
          onPress={_onClose}>
          <Image
            source={IconClose}
            style={styles.imageStyle}
          />
        </AppButton>}

      </View>}
      {children}
    </View>
  );
}

const styles = StyleSheet.create({
  target: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    height: 56,
  },
  vTitle: {
    position: 'absolute',
    top: 0,
    left: 46,
    right: 46,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    fontFamily: setFontFamily('GilroySemiBold'),
  },
  title: {
    color: COLORS.black303030,
    fontSize: 18,
    fontWeight: 'bold',
    fontFamily: setFontFamily('GilroySemiBold'),
  },
  icTarget: {
    width: 36,
    height: 36,
  },
  input: {
    flex: 1,
    fontFamily: setFontFamily('GilroyMedium'),
  },
  vsearch: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    height: 40,
    borderRadius: 10,
    flex: 1,
    marginRight: 20,
    marginLeft: 10,
  },
  imageStyle: {
    padding: 10,
    margin: 10,
    height: 25,
    width: 25,
    resizeMode: 'stretch',
    alignItems: 'center',
  },

});

export default AppHeader;
// import React, {Component} from 'react';
// import {
//   StyleSheet,
//   View,
//   StatusBar,
//   Platform,
//   SafeAreaView,
// } from 'react-native';

// const MyStatusBar = ({backgroundColor, ...props}) => (
//   <View style={[styles.statusBar, {backgroundColor}]}>
//     <SafeAreaView>
//       <StatusBar translucent backgroundColor={backgroundColor} {...props} />
//     </SafeAreaView>
//   </View>
// );

// class DarkTheme extends Component {
//   render() {
//     return (
//       <View style={styles.container}>
//         <MyStatusBar backgroundColor="#5E8D48" barStyle="light-content" />
//         <View style={styles.appBar} />
//         <View style={styles.content} />
//       </View>
//     );
//   }
// }

// const STATUSBAR_HEIGHT = StatusBar.currentHeight;
// const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
//   statusBar: {
//     height: STATUSBAR_HEIGHT,
//   },
//   appBar: {
//     backgroundColor: '#79B45D',
//     height: APPBAR_HEIGHT,
//   },
//   content: {
//     flex: 1,
//     backgroundColor: '#33373B',
//   },
// });
