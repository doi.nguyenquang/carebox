import React from 'react';
import { View, StyleSheet, Image, Text } from 'react-native';
import { COLORS } from '../../../carebox/utils/colors';
import { IconFavorite } from '../../../carebox/assets/icons'

const FavoriteView = (props) => {
  const { value, style } = props;
  return (
    <View style={[styles.target, style]}>
      <Image style={styles.icTarget} source={IconFavorite} />
      <Text style={styles.targetValue}>{value}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  target: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 15,
  },
  targetValue: {
    color: COLORS.black303030,
  },
  icTarget: {
    color: COLORS.orangeF58220,
    width: 18,
    height: 18,
    marginRight: 10,
  },
});

export default FavoriteView;
