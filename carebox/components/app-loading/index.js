import React from 'react';
import {ActivityIndicator, View} from 'react-native';
import Modal from 'react-native-modal';
import styles from './styles';
import {COLORS} from '../../../carebox/utils/colors';
import AppText from '../app-text';
import { UIActivityIndicator } from 'react-native-indicators';
const AppLoading = (props) => {
  const {isVisible, backdropOpacity, animationOutTiming, title, onModalHide = () => {}} = props;
  return (
    <Modal
      isVisible={isVisible}
      onModalHide={onModalHide}
      backdropOpacity={backdropOpacity ?? 0.2}
      animationOutTiming={animationOutTiming ?? 300}
      animationIn={'fadeIn'}
      animationOut={'fadeOut'}
      style={styles.container}>
      <View style={styles.view}>
        <UIActivityIndicator color={COLORS.loading} />
        <AppText style={styles.textTitle}>{title ?? 'Đang tải...'}</AppText>
      </View>
    </Modal>
  );
};

export default AppLoading;
