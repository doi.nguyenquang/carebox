import {COLORS} from '../../../carebox/utils/colors';
import {fontPixel, heightPixel} from '../../../carebox/utils/sizes';
import {StyleSheet} from 'react-native';
import { setFontFamily } from '../../../carebox/utils/functions';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textTitle: {
    fontSize: fontPixel(14),
    color: COLORS.black,
    fontWeight: '400',
    fontFamily: setFontFamily('GilroyRegular'),
  },
  view: {
    height: heightPixel(96),
    width: heightPixel(96),
    backgroundColor: COLORS.white,
    borderRadius: 14,
    paddingVertical: heightPixel(14),
    alignItems: 'center',
    justifyContent: 'center',
  },
});
export default styles;
