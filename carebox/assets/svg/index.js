import DeleteIcon from './ic_delete.svg';
import BackIcon from './ic_back.svg';
import ReloadIcon from './ic_reload.svg';
import SettingIcon from './ic_setting.svg';
import ClockIcon from './ic_clock.svg';
import HeartIcon from './ic_heart.svg';
import SearchIcon from './ic_search.svg';
import FilterIcon from './ic_filter.svg';
import ArrowRightOutlinedIcon from './ic_arrow_right_outlined.svg';

export {
  BackIcon,
  DeleteIcon,
  ReloadIcon,
  SettingIcon,
  ClockIcon,
  HeartIcon,
  SearchIcon,
  FilterIcon,
  ArrowRightOutlinedIcon
};
