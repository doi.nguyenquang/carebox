import React from 'react';
import Svg, {G, Path} from 'react-native-svg';

const IconAward = props => {
  const {fill, size} = props;
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width={size || 24}
      height={size || 24}
      {...props}>
      <G fill={fill}>
        <Path d="M12.12 13.53h-.17a3.99 3.99 0 1 1 .2 0ZM12 6.97a2.531 2.531 0 0 0-.1 5.06.84.84 0 0 1 .23 0A2.532 2.532 0 0 0 12 6.97Z" />
        <Path
          data-name="Vector"
          d="M12 22.75a10.71 10.71 0 0 1-7.25-2.82.755.755 0 0 1-.243-.635 4.408 4.408 0 0 1 2.1-3.12 10.524 10.524 0 0 1 10.78 0 4.428 4.428 0 0 1 2.1 3.12.716.716 0 0 1-.24.63A10.71 10.71 0 0 1 12 22.75ZM6.08 19.1A9.208 9.208 0 0 0 12 21.25a9.208 9.208 0 0 0 5.92-2.15 3.251 3.251 0 0 0-1.37-1.68 9.049 9.049 0 0 0-9.11 0 3.227 3.227 0 0 0-1.36 1.68Z"
        />
        <Path
          data-name="Vector"
          d="M12 22.75A10.75 10.75 0 1 1 22.75 12 10.759 10.759 0 0 1 12 22.75Zm0-20A9.25 9.25 0 1 0 21.25 12 9.261 9.261 0 0 0 12 2.75Z"
        />
      </G>
    </Svg>
  );
};

export default IconAward;
