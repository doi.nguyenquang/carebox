import React from 'react';
import Svg, {Path} from 'react-native-svg';
const IconHome = props => {
  const {fill, size} = props;
  return (
    <Svg
      xmlns="http://www.w3.org/2000/svg"
      width={size || 24}
      height={size || 24}
      {...props}>
      <Path
        d="m20.83 8.01-6.55-5.24a3.765 3.765 0 0 0-4.55-.01L3.18 8.01a3.917 3.917 0 0 0-1.31 3.43l1.26 7.54A3.716 3.716 0 0 0 6.7 22h10.6a3.773 3.773 0 0 0 3.58-3.03l1.26-7.54a4.018 4.018 0 0 0-1.31-3.42ZM12.75 18a.75.75 0 0 1-1.5 0v-3a.75.75 0 0 1 1.5 0Z"
        fill={fill}
        data-name="vuesax/bold/home"
      />
    </Svg>
  );
};

export default IconHome;
