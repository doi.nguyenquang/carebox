import {Platform} from 'react-native';

export const EnumFontFamily = {
  GilroyThin: 'GilroyThin',
  GilroyXLight: 'GilroyXLight',
  GilroyLight: 'GilroyLight',
  GilroyMedium: 'GilroyMedium',
  GilroySemiBold: 'GilroySemiBold',
  GilroyBold: 'GilroyBold',
  GilroyXBold: 'GilroyXBold',
  GilroyHeavy: 'GilroyHeavy',
  GilroyBlack: 'GilroyBlack',
  GilroyRegular: 'GilroyRegular',
};
// export type TypeFontFamily =
//   | 'GilroyThin'
//   | 'GilroyXLight'
//   | 'GilroyLight'
//   | 'GilroyMedium'
//   | 'GilroySemiBold'
//   | 'GilroyBold'
//   | 'GilroyXBold'
//   | 'GilroyHeavy'
//   | 'GilroyBlack'
//   | 'GilroyRegular';

export const setFontFamily = type => {
  const isIOS = Platform.OS === 'ios';
  switch (type) {
    case EnumFontFamily.GilroyThin:
      return isIOS ? 'SVN-GilroyThin' : 'SVN-Gilroy-Thin';
    case EnumFontFamily.GilroyXLight:
      return isIOS ? 'SVN-GilroyXLight' : 'SVN-Gilroy-XLight';
    case EnumFontFamily.GilroyLight:
      return isIOS ? 'SVN-GilroyLight' : 'SVN-Gilroy-Light';
    case EnumFontFamily.GilroyMedium:
      return isIOS ? 'SVN-GilroyMedium' : 'SVN-Gilroy-Medium';
    case EnumFontFamily.GilroySemiBold:
      return isIOS ? 'SVN-GilroySemiBold' : 'SVN-Gilroy-SemiBold';
    case EnumFontFamily.GilroyBold:
      return isIOS ? 'SVN-GilroyBold' : 'SVN-Gilroy-Bold';
    case EnumFontFamily.GilroyXBold:
      return isIOS ? 'SVN-GilroyXBold' : 'SVN-Gilroy-XBold';
    case EnumFontFamily.GilroyHeavy:
      return isIOS ? 'SVN-GilroyHeavy' : 'SVN-Gilroy-Heavy';
    case EnumFontFamily.GilroyBlack:
      return isIOS ? 'SVN-GilroyBlack' : 'SVN-Gilroy-Black';
    case EnumFontFamily.GilroyRegular:
      return isIOS ? 'SVN-Gilroy' : 'SVN-Gilroy-Regular';
    default:
      break;
  }
};
