import moment from 'moment';
import Toast from "react-native-root-toast";
import RNFetchBlob from 'rn-fetch-blob';
import { PermissionsAndroid, Alert, Platform } from 'react-native';
import { COLORS } from './colors';
import RNFS from 'react-native-fs';

export const toHHMMSS = (secs) => {
  var sec_num = parseInt(secs, 10)
  var hours = Math.floor(sec_num / 3600)
  var minutes = Math.floor(sec_num / 60) % 60
  var seconds = sec_num % 60

  const value = [hours, minutes, seconds]
    .map(v => v < 10 ? "0" + v : v)
    .filter((v, i) => v !== "00" || i > 0)
    .join(":")

  if (hours || minutes) {
    return value + ' phút'
  }
  return null;
}

export const convertDayVi = (date) => {
  const day = moment(date).format('dddd');
  switch (day) {
    case "Monday":
      return "Thứ 2, "
    case "Tuesday":
      return "Thứ 3"
    case "Wednesday":
      return "Thứ 4"
    case "Thursday":
      return "Thứ 5"
    case "Friday":
      return "Thứ 6"
    case "Saturday":
      return "Thứ 7"
    default: return "Chủ nhật"
  }
}

export const getGreetingText = () => {
  const currentDate = new Date();
  const hrs = currentDate.getHours();
  if (hrs >= 5 && hrs <= 11) {
    return "Chào buổi sáng";
  } else if (hrs >= 12 && hrs <= 18) {
    return "Chào buổi chiều";
  } else if (hrs >= 19 || hrs <= 4) {
    return "Chào buổi tối";
  }
  return "Xin chào"
}

export const showToastMessage = (message) => {
  Toast.show(message, {
    duration: Toast.durations.SHORT,
    position: 0,
    shadow: false,
    animation: true,
    hideOnPress: true,
    backgroundColor: COLORS.black303030,
    textColor: COLORS.white,
  })
}

export const downLoadAndSaveFile = async (fileUrl) => {
  if (Platform.OS === 'ios') {
    const data = await downloadFile(fileUrl);
    if (data) {
      return new Promise(resolve => {
        resolve({ success: true, data });
      });
    }
  } else {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'Storage Permission Required',
          message: 'Application needs access to your storage to download File',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        const data = await downloadFile(fileUrl);
        if (data) {
          return new Promise(resolve => {
            resolve({ success: true, data });
          });
        }
      } else {
        Alert.alert('Error', 'Storage Permission Not Granted');
      }
    } catch (err) {
      console.log('++++' + err);
    }
  }
};

const getFileExtention = (fileUrl) => {
  // To get the file extension
  return /[.]/.exec(fileUrl) ?
    /[^.]+$/.exec(fileUrl) : undefined;
};

export const downloadFile = async (fileUrl) => {

  let date = new Date();
  // File URL which we want to download
  let FILE_URL = fileUrl;
  // Function to get extention of the file url
  let file_ext = getFileExtention(FILE_URL);

  file_ext = '.' + file_ext[0];

  // config: To get response by passing the downloading related options
  // fs: Root directory path to download
  const { config, fs } = RNFetchBlob;
  let RootDir = Platform.OS === 'android' ?  RNFS.ExternalDirectoryPath : fs.dirs.DocumentDir;
  let options = {
    fileCache: true,
    path: `${RootDir}/Carebox_${moment().unix()}${file_ext}`,
    addAndroidDownloads: {
      path: `${RootDir}/Carebox_${moment().unix()}${file_ext}`,
      description: 'downloading file...',
      notification: true,
      // useDownloadManager works with Android only
      useDownloadManager: true,
    },
  };
  const responseDL = await config(options).fetch('GET', FILE_URL);
  return responseDL.path();
};

export const getFirstDayOfMonth = () => {
  const now = new Date();
  const firstDay =
    moment(new Date(now.getFullYear(), now.getMonth(), 1)).format(
      'YYYY-MM-DD',
    ) + ' 00:00:00';
  return firstDay;
};

export const getLastDayOfMonth = () => {
  const now = new Date();
  const lastDay =
    moment(new Date(now.getFullYear(), now.getMonth() + 1, 0)).format(
      'YYYY-MM-DD',
    ) + ' 23:59:59';
  return lastDay;
};
