import {Dimensions, PixelRatio,  Platform, StatusBar } from 'react-native';
const {width: SCREEN_WIDTH, height: SCREEN_HEIGHT} = Dimensions.get('window');
type BaseType = 'width' | 'height';
const BASE_WIDTH = 375;
const BASE_HEIGHT = 812;
const widthBaseScale = SCREEN_WIDTH / BASE_WIDTH;
const heightBaseScale = SCREEN_HEIGHT / BASE_HEIGHT;
const normalize = (size: number, based: BaseType = 'width') => {
  const newSize =
    based === 'height' ? size * heightBaseScale : size * widthBaseScale;
  return Math.round(PixelRatio.roundToNearestPixel(newSize));
};

export const widthPixel = (size: number) => {
  return normalize(size, 'width');
};
export const heightPixel = (size: number) => {
  return normalize(size, 'height');
};
export const fontPixel = (size: number) => {
  return heightPixel(size);
};
export const SIZE = {
  SCREEN_WIDTH,
  SCREEN_HEIGHT,
};

export function isIphoneX() {
  const dimen = Dimensions.get('window');
  return (
      Platform.OS === 'ios' &&
      !Platform.isPad &&
      !Platform.isTVOS &&
      ((dimen.height === 780 || dimen.width === 780)
        || (dimen.height === 812 || dimen.width === 812)
        || (dimen.height === 844 || dimen.width === 844)
        || (dimen.height === 896 || dimen.width === 896)
        || (dimen.height === 926 || dimen.width === 926))
  );
}

export function ifIphoneX(iphoneXStyle, regularStyle) {
  if (isIphoneX()) {
      return iphoneXStyle;
  }
  return regularStyle;
}

export function getStatusBarHeight(safe) {
  return Platform.select({
      ios: ifIphoneX(safe ? 44 : 30, 20),
      android: StatusBar.currentHeight,
      default: 0
  });
}

export function getBottomSpace() {
  return isIphoneX() ? 34 : 0;
}