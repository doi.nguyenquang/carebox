export const AUTH_TOKEN = "AUTH_TOKEN"

const userRole = {
  admin: 1,
  staff: 2,
  user: 3,
  accountant: 4
}

const categoryType = {
  new: 0,
  courseAndQuiz: 1,
  newsMentality: 2
}

const consultMethods = ['Mes247', 'VideoCall', 'Live']

export {
  userRole,
  categoryType,
  consultMethods
}