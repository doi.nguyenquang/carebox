import {TransitionPresets} from '@react-navigation/stack';

export const screenOptions = {
  ...TransitionPresets.SlideFromRightIOS,
  gestureEnabled: true,
  headerShown: false,
};
