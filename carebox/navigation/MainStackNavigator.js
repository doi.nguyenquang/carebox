import React from 'react';
import { ROUTER_NAME } from './RouterName';
import { screenOptions } from './NavigationConfig';
import { createStackNavigator } from '@react-navigation/stack';
import QuizDetailScreen from '../../carebox/screens/quiz/detail/quiz_detail_screen';
import QuizTestScreen from '../../carebox/screens/quiz/test/quiz_test_screen';
import QuizScreen from '../../carebox/screens/quiz/quiz_screen';
import CourseScreen from '../../carebox/screens/course/course_screen';
import CourseDetailScreen from '../../carebox/screens/course/detail/course_detail_screen';
import TabBottomNavigator from './TabBottomNavigator';
import ChatScreen from '../../carebox/screens/chat';
import NewsScreen from '../../carebox/screens/news';
import ConsultScheduleScreen from '../../carebox/screens/consultation/schedule';
import FavoriteScreen from '../../carebox/screens/favorite';
import SetupConsultScreen from '../../carebox/screens/consultation/setup';
import CreatePostScreen from '../../carebox/pages/news/create';
import CmtScreen from '../../carebox/pages/news/cmt';
import QuestionScreen from '../../carebox/screens/questions';
import DetailQuestionScreen from '../../carebox/screens/questions/detail';
import PrivacyScreen from '../../carebox/pages/account/privacy';
import FilterNewsScreen from '../../carebox/pages/news/filter';
import QuizTestResultScreen from '../../carebox/screens/quiz/test/quiz_test_result.screen';
import CheckInScreen from '../../carebox/screens/check-in';
import CallScreen from '../../carebox/screens/call';

const Stack = createStackNavigator();

const MainStackNavigator = () => {

  return (
    <Stack.Navigator screenOptions={screenOptions}>
      <Stack.Screen
        name={ROUTER_NAME.MAIN_TAB}
        component={TabBottomNavigator}
      />
      <Stack.Screen
        name={ROUTER_NAME.QuizDetailScreen}
        component={QuizDetailScreen}
      />
      <Stack.Screen
        name={ROUTER_NAME.QuizTestScreen}
        component={QuizTestScreen}
      />
      <Stack.Screen name={ROUTER_NAME.QuizScreen} component={QuizScreen} />
      <Stack.Screen name={ROUTER_NAME.CourseScreen} component={CourseScreen} />
      <Stack.Screen
        name={ROUTER_NAME.CourseDetailScreen}
        component={CourseDetailScreen}
      />
      <Stack.Screen name={ROUTER_NAME.CHAT} component={ChatScreen} />
      <Stack.Screen
        name={ROUTER_NAME.ConsultScheduleScreen}
        component={ConsultScheduleScreen}
      />
      <Stack.Screen
        name={ROUTER_NAME.SetupConsultScreen}
        component={SetupConsultScreen}
      />
      <Stack.Screen
        name={ROUTER_NAME.NewFavoriteScreen}
        component={FavoriteScreen}
      />
      <Stack.Screen
        name={ROUTER_NAME.NewsScreen}
        component={NewsScreen}
      />
      <Stack.Screen
        name={ROUTER_NAME.CreatePostScreen}
        component={CreatePostScreen}
      />
      <Stack.Screen
        name={ROUTER_NAME.CmtScreen}
        component={CmtScreen}
      />
      <Stack.Screen
        name={ROUTER_NAME.QuestionScreen}
        component={QuestionScreen}
      />
      <Stack.Screen
        name={ROUTER_NAME.DetailQuestionScreen}
        component={DetailQuestionScreen}
      />
      <Stack.Screen
        name={ROUTER_NAME.PrivacyScreen}
        component={PrivacyScreen}
      />
      <Stack.Screen
        name={ROUTER_NAME.FilterNewsScreen}
        component={FilterNewsScreen}
      />
      <Stack.Screen
        name={ROUTER_NAME.QuizTestResultScreen}
        component={QuizTestResultScreen}
      />
      <Stack.Screen
        name={ROUTER_NAME.CheckInScreen}
        component={CheckInScreen}
      />
      <Stack.Screen
        name={ROUTER_NAME.CallScreen}
        component={CallScreen}
      />
    </Stack.Navigator>
  );
};

export default MainStackNavigator;
