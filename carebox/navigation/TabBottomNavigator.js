import React, { useEffect, useContext } from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import HomeContainer from '../../carebox/pages/home';
import TrainingContainer from '../../carebox/pages/training';
import AccountView from '../../carebox/pages/account';
import NewsPage from '../../carebox/pages/news';
import AppButton from '../../carebox/components/app-button';
import {Text, View, StyleSheet, Image} from 'react-native';
import { MainContext } from '../store/MainProvider';
import {
  IconAward,
  IconCoffee,
  IconHome,
  IconProfile,
  IconHomePng
} from '../../carebox/assets/icons';
import {COLORS} from '../../carebox/utils/colors';
import {SafeAreaView} from 'react-native-safe-area-context';
import {screenOptions} from './NavigationConfig';
import {TYPE_ICON_TAB} from './types';
import {setFontFamily} from '../../carebox/utils/functions';
import ForegroundMessage from '../../carebox/screens/call/foreground_message'

const RenderIcon = (index, color) => {
  switch (index) {
    case TYPE_ICON_TAB.HOME:
      if (color === COLORS.white) {
        return <IconHome fill={color} />;
      } else {
        return (
         <Image source={IconHomePng} style={{height: 24, width: 24}}/>
        )
      }
    case TYPE_ICON_TAB.COFFEE:
      return <IconCoffee fill={color} />;
    case TYPE_ICON_TAB.AWARD:
      return <IconAward fill={color} />;
    case TYPE_ICON_TAB.PROFILE:
      return <IconProfile fill={color} />;
    default:
      break;
  }
};

const handleShowBackground = isFocused => {
  return {backgroundColor: isFocused ? COLORS.tabActive : COLORS.white};
};
function MyTabBar({state, descriptors, navigation}) {
  return (
    <View style={styles.viewTab}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            // The `merge: true` option makes sure that the params inside the tab screen are preserved
            navigation.navigate({name: route.name, merge: true});
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <AppButton
            accessibilityRole="button"
            accessibilityState={isFocused ? {selected: true} : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            key={label}
            style={[styles.tabBarStyle, handleShowBackground(isFocused)]}>
            {RenderIcon(index, isFocused ? COLORS.white : COLORS.mughalGreen)}
            {isFocused && <Text style={styles.title}>{label}</Text>}
          </AppButton>
        );
      })}
    </View>
  );
}

const Tab = createBottomTabNavigator();

const TabBottomNavigator = () => {

  const forgroundMsg = new ForegroundMessage({ isInit: true });
  const { initialize } = useContext(MainContext);

  useEffect(() => {
    forgroundMsg.init()
    initialize();
  }, [])

  useEffect(() => {
    return () => {
      forgroundMsg.dispose();
    }
  }, [])

  return (
    <SafeAreaView style={styles.container}>
      <Tab.Navigator
        tabBar={props => <MyTabBar {...props} />}
        screenOptions={screenOptions}>
        <Tab.Screen name="Trang chủ" component={HomeContainer} />
        <Tab.Screen name="Tin mới" component={NewsPage} />
        <Tab.Screen name="Rèn luyện" component={TrainingContainer} />
        <Tab.Screen name="Tài khoản" component={AccountView} />
      </Tab.Navigator>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  title: {
    color: COLORS.white,
    fontFamily: setFontFamily('GilroyMedium'),
    fontSize: 12,
    marginTop: 2
  },
  tabBarStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    marginVertical: 4,
    paddingVertical: 4,
  },
  container: {
    flex: 1,
    backgroundColor: COLORS.white,
  },
  viewTab: {
    flexDirection: 'row',
    paddingHorizontal: 4,
  },
});
export default TabBottomNavigator;
