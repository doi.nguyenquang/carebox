import { CategoryModel } from '../../carebox/models/category_model'

class NewsModel {
  newsId;
  categoryId;
  newsTitle;
  newsContent;
  newsImages;
  newsTime;
  newsViews;
  favorited;
  likesCount;
  createdAt;
  updatedAt;
  category: CategoryModel;
  showFavorite;

  isFavorite() {
    return this.favorited != 0;
  }

  constructor({
    newsId,
    categoryId,
    newsTitle,
    newsContent,
    newsImages,
    newsTime,
    newsViews,
    createdAt,
    updatedAt,
    category,
    favorited,
    likesCount,
    showFavorite,
  }) {
    this.newsId = newsId;
    this.categoryId = categoryId;
    this.newsTitle = newsTitle;
    this.newsContent = newsContent;
    this.newsImages = newsImages;
    this.newsTime = newsTime;
    this.newsViews = newsViews;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.category = category;
    this.favorited = favorited;
    this.likesCount = likesCount;
    this.showFavorite = showFavorite ?? false;
  }

  static fromJson(json) {
    return new NewsModel(
      {
        newsId: json['news_id'],
        categoryId: json['category_id'],
        newsTitle: json['news_title'],
        newsContent: json['news_content'],
        newsImages: json['news_images'],
        newsTime: json['news_time'],
        newsViews: json['news_views'],
        createdAt: json['created_at'],
        updatedAt: json['updated_at'],
        favorited: json['favorited'] ?? 0,
        likesCount: json['likes_count'] ?? 0,
        category: json['category'] == null
          ? null
          : CategoryModel.fromJson(json['category']),
      }
    );
  }
}


class NewsModelList {
  list;

  constructor({ list }) {
    this.list = list;
  }

  static fromJson = (datas) => {
    var list = [];
    if (datas) {
      list = datas.map((f) => new NewsModel.fromJson(f));
    }
    return new NewsModelList({ list });
  }
}

export { NewsModel, NewsModelList }