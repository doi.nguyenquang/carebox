import {useState} from 'react';
import {GiftedChat} from 'react-native-gifted-chat';

const ChatModel = () => {
  const [listMessages, setListMessages] = useState([
    {
      _id: 0,
      text: 'Hello, What is CareBox can help you',
      createdAt: new Date(),
      user: {
        _id: 0,
      },
    },
  ]);
  const [messages, setMessages] = useState('');
  const onChangeText = text => {
    setMessages(text);
  };
  const addListMessages = newMessage => {
    setListMessages(previousMessages =>
      GiftedChat.append(previousMessages, newMessage),
    );
    setMessages('');
  };
  return {
    listMessages,
    messages,
    onChangeText,
    addListMessages,
  };
};

export default ChatModel;
