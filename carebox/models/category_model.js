
class CategoryModel {
  categorId;
  categoryTitle;
  categoryType;
  categoryIcon;
  createdAt;
  updatedAt;
  isSelected;
  subList = [];

  constructor({
    categorId,
    categoryTitle,
    categoryType,
    categoryIcon,
    createdAt,
    updatedAt,
  }) {
    this.categorId = categorId;
    this.categoryTitle = categoryTitle;
    this.categoryType = categoryType;
    this.categoryIcon = categoryIcon;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }

  static fromJson(json) {
    return new CategoryModel(
      {
        categorId: json['category_id'] ?? json['id'],
        categoryTitle: json['category_title'] ?? json['name'],
        categoryType: json['category_type'] ,
        categoryIcon: json['category_icon'] ?? json['icon'],
        createdAt: json['created_at'] ?? json['createdDate'],
        updatedAt: json['updated_at'] ?? json['lastUpdated'],
      }
    );
  }
}

class CategoryModelList {
  list;

  constructor({ list }){
    this.list = list;
  }

  static fromJson(datas) {
    var values = [];
    if (datas) {
      values = datas.map((f) => new CategoryModel.fromJson(f));
    }
    return new CategoryModelList({ list: values });
  }
}

export { CategoryModel, CategoryModelList }
