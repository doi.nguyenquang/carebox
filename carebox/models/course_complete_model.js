import { CourseModel } from '../../carebox/screens/course/course_model'

class CourseCompleteModel {
  createdAt;
  updatedAt;
  courseId;
  userd;
  courseCompleteId;
  course;


  constructor({
    createdAt,
    updatedAt,
    courseId,
    userd,
    course,
    courseCompleteId,
  }) {
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.courseId = courseId;
    this.userd = userd;
    this.course = course;
    this.courseCompleteId = courseCompleteId;
  }

  static fromJson(json) {
    return new CourseCompleteModel(
      {
        createdAt: json['created_at'],
        updatedAt: json['updated_at'],
        courseId: json['course_id'],
        userd: json['user_id'],
        courseCompleteId: json['user_course_complete_id'],
        course: json['course'] == null
          ? null
          : CourseModel.fromJson(json['course']),
      }
    );
  }
}



class CourseCompleteModelList {
  list;

  constructor({ list }) {
    this.list = list;
  }

  static fromJson(datas) {
    var list = [];
    if (datas) {
      list = datas.map((f) => CourseCompleteModel.fromJson(f));
    }
    return new CourseCompleteModelList({ list: list });
  }
}

export { CourseCompleteModel, CourseCompleteModelList }