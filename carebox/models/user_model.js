class UserModel {
  constructor({
    userId,
    userName,
    userFullName,
    userEmail,
    userPhone,
    userAvatar,
    userWallpage,
    userPosition,
    userAddress,
    userRole,
    userCode,
    createdAt,
    updatedAt,
    userToken,
    refreshToken,
    identityNumber,
    userMarriage,
    des,
  }) {
    this.userId = userId;
    this.userName = userName;
    this.userFullName = userFullName;
    this.userEmail = userEmail;
    this.userPhone = userPhone;
    this.userAvatar = userAvatar;
    this.userWallpage = userWallpage;
    this.userPosition = userPosition;
    this.userAddress = userAddress;
    this.userRole = userRole;
    this.userCode = userCode;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.userToken = userToken;
    this.refreshToken = refreshToken;
    this.identityNumber = identityNumber;
    this.userMarriage = userMarriage;
    this.des = des;
  }

  
  userId;
  userName;
  userFullName;
  userEmail;
  userPhone;
  userAvatar;
  userWallpage;
  userPosition;
  userAddress;
  userRole;
  userCode;
  createdAt;
  updatedAt;
  userToken;
  refreshToken;
  identityNumber;
  userMarriage;
  des;

  static fromJson(json) {
    return new UserModel(
      {
        userId: json['id'],
        userName: json['preferredUsername'],
        userFullName: json['fullName'],
        userEmail: json['email'],
        userPhone: json['user_phone'],
        userAvatar: json['pictureUrl'],
        userWallpage: json['user_wallpage'],
        userPosition: json['user_position'],
        userAddress: json['user_address'],
        userRole: json['user_role'],
        userCode: json['user_code'],
        createdAt: json['created_at'],
        updatedAt: json['updated_at'],
        userToken: json['user_token'],
        refreshToken: json['refresh_token'],
        identityNumber: json['identity_number'],
        userMarriage: json['user_marriage'],
        des: json['des'],
      }
    );
  }


}

export { UserModel }