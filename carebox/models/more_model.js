
class MoreModel {
  moreId;
  moreTitle;
  moreContent;
  createdAt;
  updatedAt;
  moreType;

  constructor({
    moreId,
    moreTitle,
    moreContent,
    createdAt,
    updatedAt,
    moreType,
  }) {
    this.moreId = moreId;
    this.moreTitle = moreTitle;
    this.moreContent = moreContent;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.moreType = moreType;
  }

  static fromJson(json) {
    return new MoreModel(
      {
        moreId: json['more_id'],
        moreTitle: json['more_title'],
        moreContent: json['more_content'],
        createdAt: json['created_at'],
        updatedAt: json['updated_at'],
        moreType: json['more_type'],
      }
    );
  }
}

class MoreModelList {
  list;

  constructor({ list }) {
    this.list = list;
  }

  static fromJson(datas) {
    var list = [];
    if (datas) {
      list = datas.map((f) => MoreModel.fromJson(f));
    }
    return new MoreModelList({list: list});
  }

  static fromJson2(json) {
    var list = [];
    if (json['values']) {
      json['values'].map((v) => {
        list.push(MoreModel.fromJson(v));
      });
    }
    return new MoreModelList({list: list});
  }
}

export { MoreModel, MoreModelList }