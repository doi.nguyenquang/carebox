import { UserModel } from "./user_model";
import {strftime} from '../../carebox/utils/times'

class BookModelList {
  list;

  constructor({ list }) {
    this.list = list;
  }

  static fromJson(datas) {
    var list = [];
    if (datas) {
      list = datas.map((f) => new BookModel.fromJson(f));
    }
    return new BookModelList({ list: list });
  }

}


class BookModel {
  bookId;
  receiverId;
  userId;
  bookCategories;
  bookPosition;
  bookTimeStart;
  bookTimeEnd;
  bookRejectReson;
  //comment: '0: reject, 1: active, 2: success, 3: fail',
  bookStatus;
  // 0 call bằng app, 1 trực tiếp
  bookType;
  createdAt;
  updatedAt;
  user;
  receiver;
  json;

  jsonStr() {
    return JSON.stringify(this.json);
  }


  isCall() {
    return this.bookType == '0';
  }

  bookTime() {
    return Date.parse(this.bookTimeStart);
  }

  timeStart() {
    return strftime('%A %k:%M %e %B %Y', this.bookTime());
  }

  diffentNow() {
    return Date.now() - this.bookTime();
  }

  isOverTimeCall() {
    return Date.now() > this.bookTime();
  }

  isCallCanJoin() {
    return !this.isOverTimeCall() && this.isCall() && this.receiverId;
  }

  status (){
    if(this.isOverTimeCall()){
      return 'Đã quá thời hạn';
    }
    return 'Còn thời gian nữa';
  }


  // isShowCancel() {
  //   return diffentNow.inDays > 1 && bookStatus == '1' && isCall;
  // }
  // isShowJoin() {
  //   return (diffentNow.inMinutes < 10 && diffentNow.inMinutes > 0) &&
  //     bookStatus == '1' &&
  //     isCall;
  // }


  constructor({
    bookId,
    receiverId,
    userId,
    bookCategories,
    bookPosition,
    bookTimeStart,
    bookTimeEnd,
    bookRejectReson,
    bookStatus,
    bookType,
    createdAt,
    updatedAt,
    user,
    json,
    receiver,
  }) {
    this.bookId = bookId;
    this.receiverId = receiverId;
    this.userId = userId;
    this.bookCategories = bookCategories;
    this.bookPosition = bookPosition;
    this.bookTimeStart = bookTimeStart;
    this.bookTimeEnd = bookTimeEnd;
    this.bookRejectReson = bookRejectReson;
    this.bookStatus = bookStatus;
    this.bookType = bookType;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.user = user;
    this.json = json;
    this.receiver = receiver;
  }

  static fromJson(json) {
    return new BookModel(
      {
        bookId: json['book_id'],
        receiverId: json['receiver_id'],
        userId: json['user_id'],
        bookCategories: json['book_categories'],
        bookPosition: json['book_position'],
        bookTimeStart: json['book_time_start'],
        bookTimeEnd: json['book_time_end'],
        bookRejectReson: json['book_reject_reson'],
        bookStatus: json['book_status'],
        bookType: json['book_type'],
        createdAt: json['created_at'],
        updatedAt: json['updated_at'],
        user: json['user'] == null ? null : UserModel.fromJson(json['user']),
        receiver: json['receiver'] == null ? null : UserModel.fromJson(json['receiver']),
        json: json,
      }
    );
  }
}

export { BookModelList, BookModel }