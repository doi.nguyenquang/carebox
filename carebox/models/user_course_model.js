import { CourseModel } from '../../carebox/screens/course/course_model'

class UserCourseModel {
  userCourseListId;
  courseListId;
  userId;
  courseId;
  createdAt;
  updatedAt;
  courseIndexCount;
  courseUserCount;
  course: CourseModel;

  constructor({
    userCourseListId,
    courseListId,
    userId,
    courseId,
    createdAt,
    updatedAt,
    courseIndexCount,
    courseUserCount,
    course,
  }) {
    this.userCourseListId = userCourseListId;
    this.courseListId = courseListId;
    this.userId = userId;
    this.courseId = courseId;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.courseIndexCount = courseIndexCount;
    this.courseUserCount = courseUserCount;
    this.course = course;
  }

  static fromJson(json) {
    return new UserCourseModel(
      {
        userCourseListId: json['user_course_list_id'],
        courseListId: json['course_list_id'],
        userId: json['user_id'],
        courseId: json['course_id'],
        createdAt: json['created_at'],
        updatedAt: json['updated_at'],
        courseIndexCount: json['course_index_count'],
        courseUserCount: json['course_user_count'],
        course: CourseModel.fromJson(json['course']),
      }
    );
  }
}


class UserCourseModelList {
  list;

  constructor({ list }) {
    this.list = list;
  }

  static fromJson(datas) {
    var list = [];
    if (datas) {
      list = datas.map((f) => UserCourseModel.fromJson(f));
    }
    return new UserCourseModelList({ list: list });
  }
}

export { UserCourseModel, UserCourseModelList }