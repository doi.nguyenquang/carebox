import React, { useState } from 'react';
import { Alert } from 'react-native';
import {
  mediaDevices,
} from 'react-native-webrtc';

import Peer from 'react-native-peerjs';
import Sound from 'react-native-sound'
import { NavigationService } from '../../carebox/navigation/NavigationService';

// Enable playback in silence mode
Sound.setCategory('Playback');

const initialValues = {
  peerId: '',
  username: '',
  setUsername: () => { },
  localStream: null,
  remoteStream: null,
  initialize: () => { },
  call: () => { },
  switchCamera: () => { },
  toggleMute: () => { },
  isMuted: false,
  isMutedCamera: false,
  closeCall: () => { },
  reset: () => { },
  initAudio: () => { },
  stopAudio: () => { },
  activeCall: null,
};

export const MainContext = React.createContext(initialValues);

const MainContextProvider = ({ children }) => {
  const [username, setUsername] = useState(initialValues.username);
  const [peerId, setPeerId] = useState(initialValues.peerId);
  const [localStream, setLocalStream] = useState(
    initialValues.localStream,
  );
  const [remoteStream, setRemoteStream] = useState(
    initialValues.remoteStream,
  );
  const [peerServer, setPeerServer] = useState(null);
  const [isMuted, setIsMuted] = useState(initialValues.isMuted);
  const [isMutedCamera, setIsMutedCamera] = useState(initialValues.isMutedCamera);
  const [activeCall, setActiveCall] = useState(null);
  const [track, setTrack] = useState(null);

  const initialize = async () => {
    console.log("MainContextProvider");
    const isFrontCamera = true;
    const devices = await mediaDevices.enumerateDevices();

    const facing = isFrontCamera ? 'front' : 'environment';
    const videoSourceId = devices.find(
      (device) => device.kind === 'videoinput' && device.facing === facing,
    );
    const facingMode = isFrontCamera ? 'user' : 'environment';
    const constraints = {
      audio: true,
      video: {
        mandatory: {
          minWidth: 1280,
          minHeight: 720,
          minFrameRate: 30,
        },
        facingMode,
        optional: videoSourceId ? [{ sourceId: videoSourceId }] : [],
      },
    };

    const newStream = await mediaDevices.getUserMedia(constraints);

    setLocalStream(newStream);

    const peerServer = new Peer(undefined, {
      host: '167.179.72.201',
      path: '/peerjs',
      secure: false,
      port: 9000,
      config: {
        iceServers: [
          {
            urls: [
              'stun:stun1.l.google.com:19302',
              'stun:stun2.l.google.com:19302',
            ],
          },
        ],
      },
    });

    peerServer.on('error', (err) => {
      console.log('Peer server error', err);
      Alert.alert(`Đã có lỗi xảy ra ${err.message}`);
      NavigationService.goBack();
    });

    peerServer.on('open', (peerId) => {
      console.log("peerId", peerId)
      setPeerServer(peerServer);
      setPeerId(peerId);
    });


    peerServer.on('call', (call) => {
      console.log("peerServercall")
      call.answer(newStream);
      setActiveCall(call);

      call.on('stream', (stream) => {
        setRemoteStream(stream);
        stopAudio();
        console.log("setRemoteStream", remoteStream?.toURL());

      });

      call.on('close', () => {
        closeCall();
      });

      call.on('error', (e) => { 
        Alert.alert(`Đã có lỗi xảy ra ${e.message}`);
        NavigationService.goBack();
      });
    });
  };


  const call = (peerID) => {
    if (!peerServer) {
      Alert.alert('Peer server connection not found');
      return;
    }

    try {
      //TODO: lấy peerID từ người nhận
      const call = peerServer.call(peerID, localStream);

      call.on(
        'stream',
        (stream) => {
          setActiveCall(call);
          setRemoteStream(stream);
          stopAudio();
        },
        (err) => {
          console.error('Failed to get call stream', err);
          Alert.alert(`Đã có lỗi xảy ra ${err.message}`);
          NavigationService.goBack();
        },
      );
    } catch (error) {
      console.log('Calling error', error);
    }
  };

  const switchCamera = () => {
    if (localStream) {
      localStream.getVideoTracks().forEach((track) => track._switchCamera());
    }
  };

  const toggleMute = () => {
    if (localStream)
      localStream.getAudioTracks().forEach((track) => {
        track.enabled = !track.enabled;
        setIsMuted(!track.enabled);
      });
  };

  const closeCall = () => {
    activeCall?.close();
    setActiveCall(null);
    stopAudio();
    reset();
  };

  const reset = async () => {
    peerServer?.destroy();
    setActiveCall(null);
    setLocalStream(null);
    setRemoteStream(null);
    setPeerId('');
    initialize();
  };

  const initAudio = (isReceiver) => {
    const sourceAudio = !isReceiver ? 'waiting.mp3' : 'iphone.mp3';
    const tra = new Sound(sourceAudio, Sound.MAIN_BUNDLE, (e) => {
      if (e) {
        console.log('error loading track:', e)
      } else {
        tra.play();
        setTrack(tra)
      }
    });
  }

  const stopAudio = () => {
    if (track) {
      track.stop();
      track.release();
      setTrack(null);
    }
  }

  return (
    <MainContext.Provider
      value={{
        username,
        setUsername,
        peerId,
        setPeerId,
        localStream,
        setLocalStream,
        remoteStream,
        setRemoteStream,
        initialize,
        call,
        switchCamera,
        toggleMute,
        isMuted,
        isMutedCamera,
        closeCall,
        reset,
        activeCall,
        initAudio,
        stopAudio,
      }}>
      {children}
    </MainContext.Provider>
  );
};

export default MainContextProvider;
