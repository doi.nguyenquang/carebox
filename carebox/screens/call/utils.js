import { mediaDevices } from 'react-native-webrtc';
import DeviceInfo from 'react-native-device-info';
import Sound from 'react-native-sound'

export default class Utils {
  static async getStream() {
    let isFront = true;
    const sourceInfos = await mediaDevices.enumerateDevices();
    let videoSourceId;

    for (let i = 0; i < sourceInfos.length; i++) {
      const sourceInfo = sourceInfos[i];
      if (
        sourceInfo.kind === 'videoinput' &&
        sourceInfo.facing == (isFront ? 'front' : 'environment')
      ) {
        videoSourceId = sourceInfo.deviceId;
      }
    }
    const stream = await mediaDevices.getUserMedia({
      audio: true,
      video: {
        width: 640,
        height: 480,
        frameRate: 30,
        facingMode: isFront ? 'user' : 'environment',
        deviceId: videoSourceId,
      },
    });

    return stream;
  }

  static async deviceId() {
    const id = await DeviceInfo.getUniqueId();
    return id;
  }

  static async playWaiting() {
    const tra = new Sound('waiting.mp3', null, (e) => {
      if (e) {
        console.log('error loading track:', e)
      } else {
        tra.play();
      }
    });
  }


}
