import React, { useState } from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import Pulse from "./pulse";
import {
  IconAcceptCall,
  IconEnCall,
  IconHungThinh,
} from '../../../../carebox/assets/icons'
import AppButton from '../../../../carebox/components/app-button'
import { IconImage } from '../../../../carebox/components/app-image'

const WaitingCallView = (props) => {

  const { enCall, avatar, isReceiver, acceptCall } = props;
  const sourceAvatar = avatar ? { uri: avatar } : IconHungThinh;
  const [connecting, setConnnecting] = useState(false);
  const [title, setTitle] = useState(isReceiver ? 'Cuộc gọi đến...' : 'Đang gọi...');

  const answerCall = () => {
    setConnnecting(true);
    setTitle('Đang thiết lập kết nối...');
    acceptCall();
  }

  return (
    <View style={styles.app}>
      <View style={styles.app}>
        <Pulse
          color="orange"
          numPulses={3}
          diameter={400}
          speed={20}
          duration={800}
        />
        <Image
          source={sourceAvatar}
          resizeMode="contain"
          style={styles.logo}
        />


      </View>
      <View style={styles.vBottom}>
        <Text style={styles.userName}>CareBox</Text>
        <Text style={styles.status}>{title}</Text>
        <View style={styles.vBottomBt}>

          {!connecting && !isReceiver && <AppButton onPress={enCall}>
            <IconImage source={IconEnCall} style={styles.btnEnd} />
          </AppButton>}

          {!connecting && isReceiver && <View style={styles.vAction}>
            <AppButton onPress={enCall}>
              <IconImage source={IconEnCall} style={styles.btnEnd} />
            </AppButton>

            <AppButton onPress={answerCall}>
              <IconImage source={IconAcceptCall} style={styles.btnEnd} />
            </AppButton>


          </View>}


        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  app: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
  },
  logo: {
    height: 80,
    borderRadius: 80,
    width: 80,
  },
  vBottom: {
    flex: 1,
    alignItems: "center",
    width: '100%'
  },
  vBottomBt: {
    flex: 1,
    alignItems: "center",
    justifyContent: 'flex-end'
  },
  btnEnd: {
    height: 50,
    marginBottom: 30,
    borderRadius: 50,
    width: 50,
  },
  userName: {
    fontWeight: 'bold'
  },
  status: {
    marginTop: 10,
  },
  vAction: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-evenly',
  }

});


export default WaitingCallView;
