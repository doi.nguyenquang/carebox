import React from 'react';
import {
    KeyboardAvoidingView,
    SafeAreaView,
    StyleSheet,
    View,
} from 'react-native';
import {
    RTCView,
} from 'react-native-webrtc';
import CallModel from '../call_model';
import {
    IconCamera,
    IconMuteCamera,
    IconSound,
    IconMuteSound,
    IconMic,
    IconMuteMic,
    IconChangeCam,
    IconEnCall,
} from '../../../../carebox/assets/icons'
import AppButton from '../../../../carebox/components/app-button'
import { IconImage } from '../../../../carebox/components/app-image'


const InCallView = ({ navigation, route }) => {
    const { userId, receiveId, isReceiver, idRoom} = route.params;
    const {
        localStream,
        remoteStream,
        muteCamera,
        muteSound,
        muteMic,
        turnOffCamera,
        turnOffSound,
        turnOffMic,
        rotateCam,
        waitingCall,
        enCall,
    } = CallModel({ userId: userId, receiveId: receiveId, isReceiver: isReceiver, idRoom: idRoom });

    const stopCall = () => {
        enCall();
        navigation.goBack(null);
    }

   
    return (
        <>
            {/* {waitingCall && <InCommingView enCall = {stopCall}/>} */}
            {waitingCall && <KeyboardAvoidingView style={styles.body}>
                <SafeAreaView>
                    <View style={styles.video}>
                        {localStream && (
                            <RTCView
                                streamURL={localStream?.toURL()}
                                style={styles.local}
                                objectFit="cover"
                                mirror
                            />
                        )}

                        {remoteStream && (
                            <RTCView
                                streamURL={remoteStream?.toURL()}
                                style={styles.stream}
                                objectFit="cover"
                                mirror
                            />
                        )}
                    </View>
                    <View style={styles.buttons}>
                        <AppButton onPress={turnOffCamera}>
                            <IconImage source={muteCamera ? IconMuteCamera : IconCamera} style={styles.icon} />
                        </AppButton>

                        <AppButton onPress={turnOffSound}>
                            <IconImage source={muteSound ? IconMuteSound : IconSound} style={styles.icon} />
                        </AppButton>

                        <View></View>

                        <AppButton onPress={turnOffMic}>
                            <IconImage source={muteMic ? IconMuteMic : IconMic} style={styles.icon} />
                        </AppButton>

                        <AppButton onPress={rotateCam}>
                            <IconImage source={IconChangeCam} style={styles.icon} />
                        </AppButton>

                    </View>

                    <View style={styles.btnEnCall}>
                        <AppButton onPress={stopCall}>
                            <IconImage source={IconEnCall} style={styles.iconEncall} />
                        </AppButton>
                    </View>
                </SafeAreaView>
            </KeyboardAvoidingView>}
        </>
    );
};

const styles = StyleSheet.create({
    body: {
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
        ...StyleSheet.absoluteFill,
    },
    local: {
        width: 90,
        height: 120,
        position: 'absolute',
        top: 10,
        right: 10,
    },
    stream: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    video: {
        flex: 1,
    },
    buttons: {
        justifyContent: 'space-evenly',
        flexDirection: 'row',
        paddingBottom: 10,
        paddingTop: 5,
        width: '100%',
        backgroundColor: '#fff',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    icon: {
        height: 36,
        width: 36,
    },
    btnEnCall: {
        position: 'absolute',
        bottom: 30,
        justifyContent: 'center',
        alignItems: 'center',
        right: 0,
        left: 0,
    },
    iconEncall: {
        height: 36,
        width: 36,
    }
});

export default InCallView;
