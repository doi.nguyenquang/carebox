import { useRef, useEffect, useState } from 'react';
import { Platform } from 'react-native';
import RNCallKeep from 'react-native-callkeep';
import BackgroundTimer from 'react-native-background-timer';
import CallConst from './call_const.js';
import { NavigationService } from '../../../carebox/navigation/NavigationService';
import { ROUTER_NAME } from '../../../carebox/navigation/RouterName';
import { DeviceEventEmitter } from 'react-native';

const isIOS = Platform.OS === 'ios';

BackgroundTimer.start();

export const CallKeep = () => {

  const [inCall, setInCall] = useState(false);

  const initializeCallKeep = async () => {
    console.log('initializeCallKeep');

    try {
      RNCallKeep.setup({
        ios: {
          appName: 'Hưng thịnh Carebox',
        },
        android: {
          alertTitle: 'Yêu cầu quyền',
          alertDescription: 'Ứng dụng này cần được cấp quyền quản lý cuộc gọi',
          cancelButton: 'Hủy',
          okButton: 'Đồng ý',
        },
      });

      RNCallKeep.setAvailable(true);
    } catch (err) {
      console.error('initializeCallKeep error:', err.message);
    }

    // Add RNCallKit Events
    RNCallKeep.addEventListener('didReceiveStartCallAction', onNativeCall);
    RNCallKeep.addEventListener('answerCall', onAnswerCallAction);
    RNCallKeep.addEventListener('endCall', onEndCallAction);
    RNCallKeep.addEventListener('didDisplayIncomingCall', onIncomingCallDisplayed);
    RNCallKeep.addEventListener('didPerformSetMutedCallAction', onToggleMute);
    RNCallKeep.addEventListener('didPerformDTMFAction', onDTMF);
    RNCallKeep.addEventListener('didLoadWithEvents', didLoadWithEvents);
    RNCallKeep.addEventListener('showIncomingCallUi', showIncomingCallUi);


  };

  const onNativeCall = ({ handle }) => {
    console.log('onNativeCall', handle);
    // _onOutGoingCall on android is also called when making a call from the app
    // so we have to check in order to not making 2 calls
    if (inCall) {
      return;
    }
    // Called when performing call from native Contact app
    call(handle);
  };
  // only ios
  const didLoadWithEvents = async (data) => {
    console.log('didLoadWithEvents', data);
    const events = await RNCallKeep.getInitialEvents();
    console.log('events', events);
  };

  const showIncomingCallUi = async (data) => {
    console.log('showIncomingCallUi', data);
  };

  const call = async (number, video = false) => {
    // const session = await Wazo.Phone.call(number, video);
    // setupCallSession(session);

    // dispatch({ inCall: true, ringing: false });

  };

  // const answer = withVideo => {
  //   dispatch({ inCall: true, ringing: false });
  //   RNCallKeep.setCurrentCallActive();

  //   Wazo.Phone.accept(currentSession, withVideo);
  // };


  const onAnswerCallAction = ({ callUUID }) => {
    RNCallKeep.setCurrentCallActive(callUUID);

    // On Android display the app when answering a video call
    if (!isIOS) {
      RNCallKeep.backToForeground();
    }
    if (dataCall) {
      const userId = dataCall['user_id'];
      const receiveId = dataCall['receiver_id'];
      NavigationService.push(ROUTER_NAME.CallScreen, { userId, receiveId, idRoom: callUUID })
    }
  };

  const callBack = () => {
    console.log("callBack")
    setInCall(false);
  }

  const onEndCallAction = ({ callUUID }) => {
    RNCallKeep.endCall(callUUID);
    console.log('onEndCallAction', callUUID);
    NavigationService.goBack();
  };

  const onIncomingCallDisplayed = ({ error, callUUID, handle, localizedCallerName, hasVideo, fromPushKit, payload }) => {
    // Incoming call displayed (used for pushkit on iOS)
    console.log('onIncomingCallDisplayed', callUUID);
    console.log('onIncomingCallDisplayed', handle);
    console.log('onIncomingCallDisplayed', payload);
    console.log('onIncomingCallDisplayed', localizedCallerName);

  };

  const onToggleMute = (muted) => {
    // Called when the system or the user mutes a call
    // Wazo.Phone[muted ? 'mute' : 'unmute'](currentSession);
    console.log('onToggleMute', muted);
  };

  const onDTMF = (action) => {
    console.log('onDTMF', action);
  };

  return { initializeCallKeep }

}

let dataCall;


