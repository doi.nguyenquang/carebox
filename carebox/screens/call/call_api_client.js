import CallConst from './call_const.js';
import * as api from '../../../carebox/services/Request'
import ApiConfig from '../../../carebox/services/api_config'
import Utils from './utils'
import { store } from '../../../carebox/redux/store';

export default class CallApiClient {
  constructor(userId, receiverId, idRoom, isReceiver) {
    this.userId = userId;
    this.receiverId = receiverId;
    this.idRoom = idRoom;
    this.isReceiver = isReceiver;
  }
  // người gọi
  async startCall() {
    const payload = {
      user_id: this.userId,
      caller_id: this.userId,
      receiver_id: this.receiverId,
      user_full_name: 'user_full_name',
      caller_name: 'user_full_name',
      type: 1,
      message: CallConst.startCall,
      idRoom: this.idRoom,
      session_id: this.idRoom,
    };
    const body = {
      user_id: this.receiverId, // id người nhận
      payload: payload,
      type: 1, // 1 để bắn apn token, 0 là ko bắn
    };

    const res = await api.methodPost({ api: ApiConfig.call, body: body });
    console.log('startCall', res);
    if (res['success']) {
      return true;
    }
    return false;
  }

  // người gọi tắt cuộc gọi
  async endCall() {
    const payload = {
      'user_id': this.userId, // id người nhận
      'receiver_id': this.receiverId,
    };
    payload['type'] = 0;
    payload['message'] = CallConst.endCall;
    payload['idRoom'] = this.idRoom;
    const id = this.isReceiver ? this.userId : this.receiverId;
    const body = {
      "user_id": id,
      "payload": payload,
      "type": 0, // 1 để bắn apn token, 0 là ko bắn
    };

    const res = await api.methodPost({ api: ApiConfig.call, body: body });
    console.log('startCall', res);
    if (res['success']) {
      return true;
    }
    return false;
  }

  // test send token
  async userDevices({ token, type }) {
    const deviceId = await Utils.deviceId();
    if (deviceId == null || token == null) {
      return false;
    }
    const body = {
      "appBuild": "string",
      "appName": "string",
      "appVer": "string",
      "devBrand": "string",
      "devIMEI": "string",
      "devIMS": "string",
      "devModel": "string",
      "devToken": token,
      "devUDID": deviceId,
      "manufacturerName": "string",
      "osBuild": "string",
      "osName": "string",
      "osVersion": "string",
      "serialID": "string",
      "userId": store.getState().profileReducer?.profileData?.id,
    }
    const res = await api.methodPost({ api: ApiConfig.userDevices, body: body });
    console.log('userDevices', res);
    if (res['success']) {
      return true;
    }
    return false;
  }
}
