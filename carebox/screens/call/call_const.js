class CallConst {
  static startCall = 'startCall'; // sv to call apn cho ios, firebase cho android
  static endCall = 'endCall'; // only firebase (dùng apn crash ios)
  static tokenFirebase = '0';
  static tokenApn = '1';
  static isStartCall = '1';
  static isEndCall = '0';
}

export default CallConst;