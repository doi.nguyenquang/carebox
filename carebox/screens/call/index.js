import React, { useContext, useEffect, useState } from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { RTCView } from 'react-native-webrtc';
import { MainContext } from '../../../carebox/store/MainProvider';
import WaitingCallView from './views/waiting_call_view';
import {
  IconCamera,
  IconMuteCamera,
  IconSound,
  IconMuteSound,
  IconMic,
  IconMuteMic,
  IconChangeCam,
  IconEnCall,
} from '../../../carebox/assets/icons'
import AppButton from '../../../carebox/components/app-button'
import { IconImage } from '../../../carebox/components/app-image'
import CallApiClient from './call_api_client';
import { NavigationService } from '../../../carebox/navigation/NavigationService';
import { setFontFamily } from '../../../carebox/utils/functions';

const { width, height } = Dimensions.get('window');


const Call = ({ route }) => {
  const {
    localStream,
    remoteStream,
    activeCall,
    isMuted,
    isMutedCamera,
    closeCall,
    toggleMute,
    switchCamera,
    peerId,
    call,
    initAudio,
    stopAudio,
  } = useContext(MainContext);
  const { userId, receiverId, idRoom, callBack, name, avatar } = route.params
  const isReceiver = idRoom;
  const roomId = isReceiver ? idRoom : peerId;
  const _apiClient = new CallApiClient(userId, receiverId, roomId, isReceiver);
  const [timer, setTimer] = useState(null);

  useEffect(() => {
    initAudio(isReceiver);
    return () => {stopAudio()}
}, [])

  useEffect(() => {
   
    if (isReceiver) {

    } else {
      console.log('_apiClient startCall');
      _apiClient.startCall();
    }
  }, [])


  const acceptCall = () => {
    call(roomId);
  }

  const endCall = () => {
    console.log("endCall")
    closeCall();
    _apiClient.endCall();
    if (callBack) {
      callBack();
    }
    NavigationService.goBack();
  }

  return (
    <>
      {!activeCall && (
        <WaitingCallView
          enCall={endCall}
          acceptCall={acceptCall}
          isReceiver={isReceiver}
          avatar={avatar}
        />
      )}
      {activeCall && <SafeAreaView style={styles.container}>
        {remoteStream && (
          <RTCView
            style={styles.remoteStream}
            streamURL={remoteStream.toURL()}
            objectFit="cover"
          />
        )}
        {localStream && (
          <View style={styles.myStreamWrapper}>
            <RTCView
              style={styles.myStream}
              objectFit="cover"
              streamURL={localStream.toURL()}
              zOrder={1}
            />
          </View>
        )}

        {/* <View style={styles.iconsWrapper}>
          <IconButton
            icon={icons.CHANGE_CAMERA}
            onPress={switchCamera}
            iconColor={'#341EFF'}
            backgroundColor="#fff"
          />
          {isMuted ? (
            <IconButton
              icon={icons.UNMUTE}
              onPress={toggleMute}
              iconColor={'#fff'}
              backgroundColor="red"
            />
          ) : (
            <IconButton
              icon={icons.MUTE}
              onPress={toggleMute}
              iconColor={'#341EFF'}
              backgroundColor="#fff"
            />
          )}
          <IconButton
            icon={icons.END_CALL}
            onPress={closeCall}
            iconColor={'#fff'}
            backgroundColor="red"
          />
        </View> */}

        <View style={styles.buttons}>
          <AppButton onPress={toggleMute}>
            <IconImage source={isMutedCamera ? IconMuteCamera : IconCamera} style={styles.icon} />
          </AppButton>

          <AppButton onPress={toggleMute}>
            <IconImage source={isMuted ? IconMuteSound : IconSound} style={styles.icon} />
          </AppButton>

          <View></View>

          <AppButton onPress={toggleMute}>
            <IconImage source={isMuted ? IconMuteMic : IconMic} style={styles.icon} />
          </AppButton>

          <AppButton onPress={switchCamera}>
            <IconImage source={IconChangeCam} style={styles.icon} />
          </AppButton>

        </View>

        <View style={styles.btnEnCall}>
          <AppButton onPress={endCall}>
            <IconImage source={IconEnCall} style={styles.iconEncall} />
          </AppButton>
        </View>
      </SafeAreaView>}
    </>
  );
};

export default Call;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#0f0f0f',
    flex: 1,
    position: 'relative',
  },
  myStream: {
    height: width * 0.5,
    width: width * 0.35,
  },
  myStreamWrapper: {
    position: 'absolute',
    top: 20,
    right: 20,
    height: width * 0.5,
    width: width * 0.35,
    backgroundColor: '#333',
    borderRadius: 12,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
  },
  remoteStreamWrapper: {},
  remoteStream: {
    width: '100%',
    height: '100%',
  },
  spinnerWrapper: {
    top: height * 0.3,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  callingText: {
    fontSize: 26,
    color: '#fff',
    fontFamily: setFontFamily('GilroyRegular'),
  },
  iconsWrapper: {
    position: 'absolute',
    bottom: 20,
    left: 20,
  },
  buttons: {
    justifyContent: 'space-evenly',
    flexDirection: 'row',
    paddingBottom: 10,
    paddingTop: 5,
    width: '100%',
    position: 'absolute',
    bottom: 0,
    backgroundColor: '#fff',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  icon: {
    height: 36,
    width: 36,
  },
  btnEnCall: {
    position: 'absolute',
    bottom: 30,
    justifyContent: 'center',
    alignItems: 'center',
    right: 0,
    left: 0,
  },
  iconEncall: {
    height: 36,
    width: 36,
  }
});
