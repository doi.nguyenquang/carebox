import { Platform } from 'react-native';
import messaging from '@react-native-firebase/messaging';
import CallConst from './call_const.js';
import { requestNotifications, request, PERMISSIONS } from 'react-native-permissions';
import VoiceCallApiClient from '../../../carebox/screens/call/call_api_client'
const isIOS = Platform.OS === 'ios';
import { NavigationService } from '../../../carebox/navigation/NavigationService';
import { ROUTER_NAME } from '../../../carebox/navigation/RouterName';

export default class ForegroundMessage {

    async init() {
        console.log('ForegroundMessage');
        this.unsubscribe = messaging().onMessage(async remoteMessage => {
            console.log('ForegroundMessage', remoteMessage);
            this.showCall(remoteMessage['data']);
        });
        this.api = new VoiceCallApiClient();
        this.isCall = false;
        this.regisToken();
        await requestNotifications(['alert', 'sound']);

        if (isIOS) {
            await request(PERMISSIONS.IOS.MICROPHONE);
            await request(PERMISSIONS.IOS.CAMERA);
        } else {
            await request(PERMISSIONS.ANDROID.READ_PHONE_STATE);
            await request(PERMISSIONS.ANDROID.CALL_PHONE);
            await request(PERMISSIONS.ANDROID.RECORD_AUDIO);
            await request(PERMISSIONS.ANDROID.CAMERA);
        }

    }



    async requestUserPermission() {
        const authStatus = await messaging().requestPermission();
        const enabled =
            authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
            authStatus === messaging.AuthorizationStatus.PROVISIONAL;

        return enabled;
    }

    async regisToken() {
        console.log('regisToken');
        const enabled = await this.requestUserPermission();
        console.log(enabled, 'enabled');
        await messaging().registerDeviceForRemoteMessages();
        const fcmToken = await messaging().getToken();
        console.log(fcmToken, 'fcmToken');
        this.api.userDevices({ token: fcmToken, type: CallConst.tokenFirebase });
        messaging().onTokenRefresh((token) => {
            console.log(token, 'onTokenRefresh');
            this.api.userDevices({ token: token, type: CallConst.tokenFirebase });
        });
    }

    
    // --- unsubscribe event listeners
    dispose() {
        if (this.unsubscribe) {
            this.unsubscribe();
        }

    }

    showCall(data) {
        const isStartCall = data['type'] == CallConst.isStartCall;
        const isEndCall = data['type'] == CallConst.isEndCall;
        if (isStartCall) {
            this.isCall = true;
            const idRoom = data['idRoom'];
            const name = data['user_full_name'];
            const avatar = data['avatar'];
            const userId = data['user_id'];
            const receiverId = data['receiver_id'];
            NavigationService.push(ROUTER_NAME.CallScreen,
                {
                    userId,
                    receiverId,
                    callBack: this.callBack,
                    name,
                    avatar,
                    idRoom,
                    isReceiver: true
                })
        } else if (isEndCall) {
            this.callback();
            NavigationService.goBack();
        }
    }

    callBack() {
        this.isCall = false;
    }



}

