import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
} from 'react-native';
import { AppImage } from '../../../../carebox/components/app-image'
import { COLORS } from '../../../../carebox/utils/colors';
import { CourseModel } from '../course_model'
import AppPeriodView from '../../../../carebox/components/app-period-view'
import AppCateView from '../../../../carebox/components/app-cate'
import AppButton from '../../../../carebox/components/app-button'
import { IconCourse } from '../../../../carebox/assets/icons'

const ItemQuizView = ({ item, onClick }) => {
    const model: CourseModel = item;
    return (
        <AppButton
            onPress={() => onClick(item)}
        >
            <View style={styles.item}>
                <AppImage
                    uri={model.courseImage}
                    style={styles.image}
                />
                <Text style={styles.title}>{model.courseTitle}</Text>
                <AppPeriodView
                    value={model.courseListCount}
                />
                <AppCateView
                    value={model.category}
                />

                <Image
                    source={IconCourse}
                    style={styles.icon}
                />
            </View>
        </AppButton>
    );
};


const styles = StyleSheet.create({
    title: {
        color: COLORS.black,
        fontWeight: 'bold',
        marginVertical: 10,
    },
    image: {
        width: 'auto',
        height: 200,
        borderRadius: 15
    },
    icon: {
        width: 36,
        height: 36,
        position: 'absolute',
        top: 6,
        right: 10,
    },
    item: {
        paddingTop: 15,
        paddingHorizontal: 15,
        backgroundColor: '#fff',
        marginVertical: 8,
        marginHorizontal: 10,
        elevation: 1,
        borderRadius: 10,
    },

});
export default ItemQuizView;