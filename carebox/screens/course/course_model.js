import { CategoryModel } from '../../../carebox/models/category_model'

class CourseModel {
  createdAt;
  updatedAt;
  courseId;
  courseTime;
  courseTarget;
  courseIntro;
  courseTitle;
  courseContent;
  courseImage;
  category: CategoryModel;
  categoryId;
  courseListCount;

  constructor({
    createdAt,
    updatedAt,
    courseId,
    courseTarget,
    courseIntro,
    categoryId,
    courseTitle,
    courseContent,
    courseImage,
    category,
    courseListCount,
  }) {
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.courseId = courseId;
    this.courseTarget = courseTarget;
    this.courseIntro = courseIntro;
    this.categoryId = categoryId;
    this.courseTitle = courseTitle;
    this.courseContent = courseContent;
    this.courseImage = courseImage;
    this.category = category;
    this.courseListCount = courseListCount;
  }

  targets() {
    if (this.courseTarget) {
      return JSON.parse(this.courseTarget)
    }
    return []
  }

  static fromJson = (json) => {
    return new CourseModel(
      {
        createdAt: json['created_at'],
        updatedAt: json['updated_at'],
        courseId: json['course_id'],
        courseTarget: json['course_target'],
        courseIntro: json['course_intro'],
        categoryId: json['category_id'],
        courseTitle: json['course_title'],
        courseContent: json['course_content'],
        courseImage: json['course_image'],
        courseListCount: json['course_list_count'],
        category: json['category'] == null
          ? null
          : CategoryModel.fromJson(json['category'])

      }
    );
  }
}

class CourseModelList {
  list;

  constructor({ list }) {
    this.list = list;
  }

  static fromJson = (datas) => {
    var list = [];
    if (datas) {
      list = datas.map((f) => new CourseModel.fromJson(f));
    }
    return new CourseModelList({ list });
  }
}

export { CourseModel, CourseModelList }