import React, { useState, useEffect } from 'react';
import {
  Text,
  View,
  Image,
  StatusBar,
  StyleSheet,
  ScrollView,
  SafeAreaView,
  TouchableOpacity,
  useWindowDimensions,
  Platform
} from 'react-native';
import { AppImage } from '../../../../carebox/components/app-image';
import AppLine from '../../../../carebox/components/app-line';
import AppHeader from '../../../../carebox/components/app-header';
import { IconCase, IconImgPDF } from '../../../../carebox/assets/icons';
import { COLORS } from '../../../../carebox/utils/colors';
import { fontPixel, SIZE } from '../../../../carebox/utils/sizes';
import { setFontFamily } from '../../../../carebox/utils/functions';
import RenderHtml from 'react-native-render-html';
import AppPeriodView from '../../../../carebox/components/app-period-view';
import AppLoading from '../../../../carebox/components/app-loading';
import { quizActions } from '../../../../carebox/redux/slices/quiz-slice';
import { useDispatch, useSelector } from 'react-redux';
import { courseSelector } from '../../../../carebox/redux/selectors';
import { CourseIndexItem } from './course_index_item';
import { isEmpty } from 'lodash';
import { downLoadAndSaveFile, showToastMessage } from '../../../../carebox/utils/utils';
import AppModal from '../../../../carebox/components/app-modal';
import Share from 'react-native-share';

let isNeedShowPopupDLSuccess = false;

const CourseDetailScreen = ({ route }) => {
  const { width } = useWindowDimensions();
  const dispatch = useDispatch();
  const model = route.params.model;

  const [documentList, setDocumentList] = useState([])
  const [excutingFile, setExcutingFile] = useState(false)
  const [showPopupDLSuccess, setShowPopupDLSuccess] = useState(false)

  const getCourseDetailLoading = useSelector(courseSelector.getCourseDetailLoading)
  const submitSaveDocumentLoading = useSelector(courseSelector.submitSaveDocumentLoading)
  const courseDetailData = useSelector(courseSelector.courseDetailData)
  const submitSaveDocumentSuccess = useSelector(courseSelector.submitSaveDocumentSuccess)

  useEffect(() => {
    const query = { 'course_id': model.courseId };
    dispatch(quizActions.getCourseDetailRequest(query));
  }, [])

  useEffect(() => {
    if (submitSaveDocumentSuccess) {
      isNeedShowPopupDLSuccess = true
    }
  }, [submitSaveDocumentSuccess])

  useEffect(() => {
    if (courseDetailData?.course_indexs && courseDetailData?.course_indexs.length > 0) {
      const listData = courseDetailData?.course_indexs?.map((item, index) => {
        return {
          ...item,
          isSelected: false,
        }
      })
      setDocumentList(listData)
    }
  }, [courseDetailData])

  const documentItemSelect = (documentId) => {
    const documentListcheck = documentList.map(item => {
      return { ...item, isSelected: item.course_list_id === documentId }
    })
    setDocumentList(documentListcheck)
  }

  const onDownLoadDocument = async () => {
    const documentItemSelect = documentList.find(document => document.isSelected)
    if (isEmpty(documentItemSelect)) {
      showToastMessage('Vui lòng chọn tài liệu muốn tải')
    } else {
      setExcutingFile(true);
      const dlResult = await downLoadAndSaveFile(documentItemSelect.youtube_id)
      if (dlResult.success) {
        const bodyData = {
          course_id: model.courseId,
          course_list_id: documentItemSelect.course_list_id,
        }
        dispatch(quizActions.submitSaveDocumentRequest({ bodyData }));
        await onShareDocument(dlResult);
        setExcutingFile(false);
      }
    }
  }

  const onShareDocument = async (dlResult) => {
    try {
      let options = {
        title: 'Chia sẻ từ ứng dụng Carebox',
        urls: [dlResult.data],
      };
      await Share.open(options);
    } catch (err) {
      console.log('ShareDocument error: ', err);
    }
  }

  return (
    <View style={styles.body}>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.body}>
        <ScrollView
          style={styles.scrollView}
          contentInsetAdjustmentBehavior="automatic"
          showsVerticalScrollIndicator={false}
        >
          <AppHeader />
          <AppImage
            uri={model.courseImage}
            style={styles.image}
          />
          <Text style={styles.title}>{model.courseTitle}</Text>
          <AppPeriodView value={model.courseListCount} />

          <AppLine style={styles.line_1} />
          <Text style={styles.titleContent}>Nội dung khoá học</Text>
          <RenderHtml
            contentWidth={width}
            source={{ html: model.courseContent }}
            tagsStyles={styles.tagsStyles}
          />
          <AppLine />
          <Text style={styles.titleContent}>Bạn sẽ nhận được</Text>
          <View style={styles.prContainer}>
            <Image style={styles.prIconStyles} source={IconImgPDF} />
            <View style={styles.prContentRight}>
              <Text style={styles.prTitleContent}>{model.courseListCount} Học liệu</Text>
              <Text style={styles.prContentStyles}>
                Bao gồm video, infographics, podcast... được chuẩn bị kỹ lưỡng từ đội ngũ chuyên gia của YTV.
              </Text>
            </View>
          </View>
          <AppLine />
          <Text style={styles.titleContent}>Mục tiêu khóa học</Text>
          {
            model.targets().map((target, index) => {
              return <View
                key={index}
                style={styles.target}>
                <Image style={styles.icTarget} source={IconCase} />
                <Text style={styles.targetValue}>{target['value']}</Text>
              </View>
            })
          }
          <AppLine style={{ marginTop: 16 }} />
          <Text style={styles.titleContent}>Mục lục</Text>
          <Text style={styles.courseIntroText}>{model.courseIntro}</Text>
          {
            documentList && documentList.length > 0 &&
            <CourseIndexItem
              documentList={documentList}
              documentItemSelect={documentItemSelect}
            />
          }
        </ScrollView>
        <View style={styles.vButton} >
          <TouchableOpacity
            style={styles.styleBtnDownload}
            onPress={onDownLoadDocument}
          >
            <Text style={styles.btnTextStyles}>TẢI TÀI LIỆU VỀ</Text>
          </TouchableOpacity>
        </View>
        <AppModal
          isVisible={showPopupDLSuccess}
          style={styles.modalContainer}
        >
          <View style={{
            alignItems: 'center',
            justifyContent: 'center'
          }}>
            <View style={styles.modalBoxStyles}>
              <View style={styles.modalContent}>
                <Text style={styles.textModalTitle}>Thông báo</Text>
                <Text style={styles.textModalContent}>File đã được tải thành công. Vui lòng kiểm tra ở thư mục download hoặc công cụ quản lý file</Text>
              </View>
              <View style={styles.actionModalcontainer}>
                <TouchableOpacity
                  style={styles.btnAcceptStyle}
                onPress={() => setShowPopupDLSuccess(false)}
                >
                  <Text style={styles.btnAcceptTextStyles}>Đồng ý</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </AppModal>
        <AppLoading
          isVisible={getCourseDetailLoading || submitSaveDocumentLoading || excutingFile}
          // onModalHide={() => {
          //   isNeedShowPopupDLSuccess && setShowPopupDLSuccess(true)
          //   isNeedShowPopupDLSuccess = false
          // }}
        />
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  body: {
    backgroundColor: COLORS.bg,
    height: '100%',
    width: '100%',
  },
  scrollView: {
    paddingHorizontal: 16,
    paddingBottom: 100
  },
  line_1: {
    marginTop: 10
  },
  image: {
    width: 'auto',
    height: 200,
    borderRadius: 15
  },
  title: {
    color: COLORS.main,
    marginVertical: 16,
    lineHeight: 24,
    fontSize: fontPixel(16),
    fontFamily: setFontFamily('GilroyBold')
  },
  titleContent: {
    color: COLORS.black303030,
    marginTop: 16,
    fontSize: fontPixel(16),
    fontFamily: setFontFamily('GilroyBold')
  },
  target: {
    flex: 1,
    width: '100%',
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  targetValue: {
    color: COLORS.black303030,
    width: '100%',
    flexShrink: 1,
    flexWrap: 'wrap',
    lineHeight: 20,
    fontSize: fontPixel(14),
    fontFamily: setFontFamily('GilroyMedium')
  },
  icTarget: {
    width: 24,
    height: 24,
    marginRight: 10,
    tintColor: COLORS.orangeF58220
  },
  vButton: {
    backgroundColor: COLORS.white,
    padding: 10,
  },
  tagsStyles: {
    body: {
      whiteSpace: 'normal',
      color: COLORS.black303030,
      textAlign: 'justify',
      fontSize: fontPixel(14),
      fontFamily: setFontFamily('GilroyMedium'),
      marginBottom: 16
    },
  },
  styleBtnDownload: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    backgroundColor: COLORS.main,
    paddingVertical: 6,
  },
  btnTextStyles: {
    lineHeight: 24,
    color: COLORS.white,
    fontSize: fontPixel(16),
    fontFamily: setFontFamily('GilroySemiBold')
  },
  courseIntroText: {
    marginTop: 16,
    color: COLORS.black303030,
    textAlign: 'justify',
    lineHeight: 20,
    fontSize: fontPixel(14),
    fontFamily: setFontFamily('GilroyMedium')
  },
  prContainer: {
    borderRadius: 16,
    borderWidth: 1,
    marginVertical: 16,
    padding: 16,
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: COLORS.white,
    borderColor: COLORS.greyE9E9E9,
  },
  prIconStyles: {
    width: 40,
    height: 40,
  },
  prContentStyles: {
    color: COLORS.black303030,
    textAlign: 'justify',
    lineHeight: 20,
    fontSize: fontPixel(14),
    fontFamily: setFontFamily('GilroyMedium')
  },
  prContentRight: {
    flex: 1,
    marginLeft: 16
  },
  prTitleContent: {
    color: COLORS.black303030,
    fontSize: fontPixel(16),
    fontFamily: setFontFamily('GilroyBold')
  },
  modalContainer: {
    justifyContent: 'center'
  },
  modalBoxStyles: {
    borderRadius: 16,
    backgroundColor: COLORS.white,
    width: SIZE.SCREEN_WIDTH - 32,
  },
  modalResultScore: {
    borderRadius: 16,
    backgroundColor: COLORS.white,
    width: SIZE.SCREEN_WIDTH - 32,
    height: SIZE.SCREEN_HEIGHT * 0.46,
  },
  modalContent: {
    height: 'auto',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 20,
    paddingHorizontal: 16,
  },
  textModalTitle: {
    fontSize: fontPixel(16),
    color: COLORS.black303030,
    fontFamily: setFontFamily('GilroyBold')
  },
  textModalContent: {
    marginTop: 16,
    textAlign: 'center',
    fontSize: fontPixel(16),
    color: COLORS.black303030,
    fontFamily: setFontFamily('GilroyMedium')
  },
  actionModalcontainer: {
    padding: 16,
    borderTopWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderTopColor: COLORS.greyE9E9E9,
  },
  btnAcceptTextStyles: {
    color: COLORS.white,
    paddingVertical: 8,
    paddingHorizontal: 24,

    fontSize: fontPixel(16),
    fontFamily: setFontFamily('GilroySemiBold')
  },
  btnAcceptStyle: {
    width: 'auto',
    borderRadius: 8,
    backgroundColor: COLORS.main
  }
});
export default CourseDetailScreen;
