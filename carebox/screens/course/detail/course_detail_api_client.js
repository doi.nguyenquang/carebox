import ApiConfig from '../../../../carebox/services/api_config'
import * as api from '../../../../carebox/services/Request'
import { CourseModel } from '../course_model'

export default class CourseDetailApiClient {

  model: CourseModel;

  constructor(model) {
    this.model = model;
  }

  async detail() {
    const query = { 'course_id': this.model.courseId };
    const res = await api.methodGet({ api: ApiConfig.courseDetail, queries: query });
    if (res['success']) {
      return res['data'] ?? 0.0;
    }
    return 0;
  }
}

