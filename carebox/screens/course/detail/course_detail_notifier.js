import { CourseModel } from '../course_model'
import CourseDetailApiClient from './course_detail_api_client'

export default class CourseDetailNotifier {

  constructor(model: CourseModel) {
    // console.log('model: ', model);
    this.model = model;
    this._apiClient = new CourseDetailApiClient(model);
    this.percent = 0;
    this.showTaget = false;
  }

  _apiClient: CourseDetailApiClient;
  model: CourseModel;
  percent;
  showTaget;


  async loadData() {
    this.pecent = await this._apiClient.detail();
  }

  onStart() {
    // Utils.navigatePage(context, QuizTestScreen(model: model));
  }

  showHideTaget() {
    // state = state.copyWith(showTaget: !state.showTaget);
    this.showTaget = !this.showTaget;
  }
}
