import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { COLORS } from '../../../../carebox/utils/colors';
import { fontPixel } from '../../../../carebox/utils/sizes';
import { IconPlayCircle, IconNext, IconChecked } from '../../../../carebox/assets/icons'
import { setFontFamily } from '../../../../carebox/utils/functions';
import { convertSecondToTime } from '../../../../carebox/utils/times';

export const CourseIndexItem = (props) => {
  const { documentList, documentItemSelect } = props;

  const getBackgroundItem = (item) => {
    if (item?.is_learned !== 0) {
      return {
        backgroundColor: COLORS.white
      }
    }
  }

  const getBorderColor = (isSelected) => {
    if (isSelected) {
      return {
        backgroundColor: COLORS.white,
        borderColor: COLORS.green
      }
    }
  }

  const getColorTitle = (item) => {
    return {
      color: (item?.is_learned !== 0 || item.isSelected) ?  COLORS.black303030 : COLORS.grey787878
    }
  }

  const disabledItem = (index) => {
    let lastIndex = documentList.map(el => el?.is_learned).lastIndexOf(1);
    return index > lastIndex + 1;
  }

  return (
    <View style={styles.container}>
      {
        documentList?.map((item, index) => {
          return (
            <TouchableOpacity
              key={index.toString()}
              disabled={disabledItem(index)}
              style={[styles.rowCheckbox, getBackgroundItem(item), getBorderColor(item?.isSelected)]}
              onPress={() => documentItemSelect(item.course_list_id)}
            >
              <Image source={IconPlayCircle} style={styles.playCircleStyles} />
              <View style={styles.documentInfoView}>
                <Text style={[styles.documentTitle, getColorTitle(item)]} numberOfLines={1}>{item.course_list_title}</Text>
                <Text style={styles.timeTextStyles} numberOfLines={1}>{convertSecondToTime(item.course_list_time)}</Text>
              </View>
              <Image source={item?.is_learned !== 0 ? IconChecked : IconNext} style={styles.rightIconStyles} />
            </TouchableOpacity>
          )
        })
      }
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    marginBottom: 20
  },
  rowCheckbox: {
    flexDirection: 'row',
    marginTop: 16,
    padding: 16,
    backgroundColor: COLORS.greyE9E9E9,
    borderRadius: 16,
    alignItems: 'center',
    borderWidth: 1,
    borderColor: COLORS.greyE9E9E9
  },
  playCircleStyles: {
    height: 32,
    width: 32,
  },
  documentInfoView: {
    flex: 1,
    paddingHorizontal: 16
  },
  rightIconStyles: {
    height: 24,
    width: 24,
  },
  documentTitle: {
    lineHeight: 20,
    fontSize: fontPixel(14),
    fontFamily: setFontFamily('GilroyBold')
  },
  timeTextStyles: {
    lineHeight: 20,
    color: COLORS.black303030,
    fontSize: fontPixel(12),
    fontFamily: setFontFamily('GilroyMedium')
  }
})
