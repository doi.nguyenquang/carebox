import { QuizModel } from '../quiz_model'
import { QuizTestModel } from './quiz_test_model'
import QuizTestTypeApiClient from './quiz_test_api_client'

export default class QuizTestNotifier {
  constructor(model: QuizModel) {
    this.model = model;
    this._apiClient = new QuizTestTypeApiClient(model);
  };

  _apiClient: QuizTestTypeApiClient;
  // final String _getQuestionFunc = 'get_question_func';
  // final String _sendResultFunc = 'send_question_func';
  model: QuizModel;
  current: QuizTestModel;
  index = 0;
  list = [];

  get btnTitle() {
    if (state.index < state.list.length) {
      return 'TIẾP TỤC';
    } else {
      return 'HOÀN THÀNH';
    }
  }

  async loadData(){
    const result = await this._apiClient.question();
    if (result.length > 0) {
      this.current = result[0];
    }
    this.list = result;
    console.log('QuizTestNotifier', this.current);
  }

   

  // get enable =>
  //     current?.userAnswer != null ||
  // (current != null && current.userAnswers.isNotEmpty);

  

  // async onContinue() {
  //   if (current.userAnswer == null && current.userAnswers.isEmpty) {
  //     return;
  //   }
  //   if (state.index < state.list.length - 1) {
  //     int index = state.index + 1;
  //     current = state.list[index];
  //     state = state.copyWith(index: index);
  //   } else {
  //     final result = await onSubmit(_sendResultFunc);
  //     if (result == true) {
  //       AppUtils.gotoRoot(context);
  //       Utils.navigatePage(context, const QuizResultScreen());
  //     }
  //   }
  // }

// function onAnswer(value) {
//   current?.userAnswer = value;
//   state = state.copyWith(update: !state.update);
// }

// function onAnswer2(value) {
//   if (!current!.userAnswers.contains(value)) {
//     current!.userAnswers.add(value);
//   } else {
//     current!.userAnswers.remove(value);
//   }
//   state = state.copyWith(update: !state.update);
// }

// async onBackPress({ value })  {
//   if (_apiClient.ans.isNotEmpty) {
//       final agree = await DialogHelper.instance.showConfirm(
//     context: context,
//     title: 'Bạn muốn rời khỏi?',
//     content:
//     'Bạn có thể tiếp tục trắc nghiệm nhanh chóng tại Trang chủ > Tiếp tục hoạt động.',
//     tvNo: 'Lưu và thoát',
//     tvYes: 'Tiếp tục');
//     if (agree == true) {
//     } else {
//       await onSubmit(_sendResultFunc);
//       NavigationService.goBack();
//     }
//   } else {
//     NavigationService.goBack();
//   }
// }
}
