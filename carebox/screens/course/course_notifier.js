import { CategoryModel } from '../../../carebox/models/category_model'
import CourseApiClient from './course_api_client'

export default class CourseNotifier {
  constructor() {
    this._apiClient = new CourseApiClient();
  }

  _apiClient: CourseApiClient;
  currentCate: CategoryModel;
  categories = [];
  list = [];
  popular = [];
  isFirst = true;
  hashNextPage = true;
  txSearch = null;


  async loadCate() {
    this.categories = await this._apiClient.categoriesQuiz();
  }

  async loadData({ isFirst }) {
    this.list = await this._apiClient.quiz({ isFirst: isFirst, cate: this.currentCate, search: this.txSearch, });
    this.hashNextPage = this._apiClient.hashNextPage;
    this.isFirst = false;
    if (this.popular.length == 0) {
      this.popular = [...this.list]
    }
    console.log('QuizNotifier', this.list)
  }

  async loadMore() {
    if (this.hashNextPage) {
      const _list = await this._apiClient.quiz({ isFirst: false, cate: this.currentCate, search: this.txSearch, });
      this.hashNextPage = this._apiClient.hashNextPage;
      if(_list.length > 0) {
        this.list = [this.list, ..._list];
      }
    }
  }

  async changeCate(value) {
    this.currentCate = value;
    this.list = [];
    this.isFirst = true;
    await this.loadData({ isFirst: true })
  }

  async onSubmitEditing(value) {
    this.txSearch = value;
    this.list = [];
    this.isFirst = true;
    await this.loadData({ isFirst: true })
  }



}
