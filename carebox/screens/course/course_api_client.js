import { CategoryModel, CategoryModelList } from '../../../carebox/models/category_model'
import * as api from '../../../carebox/services/Request'
import ApiConfig from '../../../carebox/services/api_config'
import { CourseModelList } from './course_model'

export default class CourseApiClient {
  hashNextPage = true;
  page = 0;
  limit = 40;
  constructor() {

  }

  async quiz({ isFirst, cate, search }) {
    if (isFirst) {
      this.page = 0;
      this.hashNextPage = true;
    }
    if (!this.hashNextPage) return [];
    const query = {
      'offset': this.page,
      'limit': this.limit
    };
    if (cate) {
      query['category_id'] = cate.categorId;
    }
    if (search) {
      query['tx_search'] = search;
    }
    this.page = this.page + this.limit;
    const res = await api.methodGet({ api: ApiConfig.course, queries: query })
    if (res['success']) {
      const list = CourseModelList.fromJson(res['data']['values']).list;
      if (list.length < this.limit) {
        this.hashNextPage = false;
      }
      return list;
    }
    return [];
  }

  async categoriesQuiz() {
    const res = await api
        .methodGet({api: ApiConfig.categoryAll, queries: {'type': ApiConfig.courseQuiz}});
    if (res['success']) {
      return CategoryModelList.fromJson(res['data']).list;
    } else {
      return [];
    }
  }

}
