import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  StatusBar,
  ScrollView,
  useWindowDimensions,
  ImageBackground,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import AppHeader from '../../../carebox/components/app-header';
import {COLORS} from '../../../carebox/utils/colors';
import {IconImage} from '../../../carebox/components/app-image';
import RenderHtml , { defaultSystemFonts }from 'react-native-render-html';
import AppButton from '../../../carebox/components/app-button';
import {IconArchive, IconUnFavorite} from '../../../carebox/assets/icons';
import FavoriteTimeView from '../../../carebox/components/app-favorite-time';
import NewsNotifier from './news_notifier';
import {setFontFamily, EnumFontFamily } from '../../../carebox/utils/functions';
const systemFonts = [setFontFamily(EnumFontFamily.GilroyMedium), setFontFamily(EnumFontFamily.GilroySemiBold)];

const NewsScreen = props => {
  const {route} = props;
  const model = route.params.model;
  const callBack = route.params.callBack;
  const showFavorite = route.params.favorite;

  console.log(model);
  const {width} = useWindowDimensions();
  const {isFavorite, favorite, favoriteClick} = NewsNotifier(model);

  const onFavorite = async () => {
    await favoriteClick();
    if (callBack) {
      callBack(model);
    }
  };
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <ScrollView>
        <View>
          <ImageBackground
            source={{uri: model.newsImages}}
            style={styles.image}>
            <AppHeader style={styles.header}>
            {showFavorite != 0 && (
               <AppButton onPress={onFavorite} style={styles.tagBtnContainer}>
               <IconImage
                 source={!isFavorite ? IconUnFavorite : IconArchive}
                 style={styles.iconF}
               />
             </AppButton>
            )}
            </AppHeader>
          </ImageBackground>
          <View style={styles.body}>
            <Text style={styles.title}>{model.newsTitle}</Text>
            {showFavorite != 0 && (
              <FavoriteTimeView favorite={favorite} time={model.newsTime} />
            )}
            <RenderHtml
              contentWidth={width}
              source={{html: model.newsContent}}
              styles={styles.html}
              systemFonts={systemFonts}
            />
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.white,
  },
  header: {
    marginHorizontal: 10,
    elevation: 5,
    justifyContent: 'space-between',
  },
  image: {
    width: 'auto',
    height: 200,
  },
  body: {
    paddingHorizontal: 15,
  },
  tvNoData: {
    textAlign: 'center',
  },
  title: {
    color: COLORS.black,
    fontWeight: 'bold',
    marginVertical: 10,
    fontFamily: setFontFamily('GilroyMedium'),
  },
  html: {
    color: COLORS.black,
    fontFamily: setFontFamily('GilroyMedium'),
  },
  iconF: {
    width: 30,
    height: 30,
  },
  tagBtnContainer: {
    height: 40,
    width: 40,
    borderRadius: 6,
    marginRight: 20,
    backgroundColor: COLORS.white,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default NewsScreen;
