import FavoriteApiClient from '../../../carebox/screens/favorite/favorite_api_client';
import {useState, useEffect} from 'react';
import {NewsModel} from '../../../carebox/models/news_model';
import {useDispatch} from 'react-redux';
import {homeActions} from '../../../carebox/pages/home/home-redux-saga/home-slice';

const NewsNotifier = (model: NewsModel) => {
  const _apiClient = new FavoriteApiClient();
  const dispatch = useDispatch();

  const [isFavorite, setFavorite] = useState(false);
  const [favorite, setNumFavorite] = useState(0);

  useEffect(() => {
    loadData();
  }, []);

  const favoriteClick = async () => {
    const datas = await _apiClient.favorite(model);
    if (datas) {
      dispatch(homeActions.updateFavoriteItemFastNewSuccess(model.newsId));
      loadData();
    }
  };

  const loadData = () => {
    setFavorite(model.isFavorite());
    setNumFavorite(model.likesCount);
  };

  return {
    isFavorite,
    favorite,
    favoriteClick,
  };
};
export default NewsNotifier;
