import ApiConfig from '../../../../carebox/services/api_config'
import * as api from '../../../../carebox/services/Request'
import { QuizModel } from '../quiz_model'

export default class QuizDetailApiClient {

  model: QuizModel;

  constructor(model) {
    this.model = model;
  }

  async pecent() {
    const query = { 'quiz_id': this.model.quizId };
    const res = await api.methodGet({ api: ApiConfig.quizPecent, queries: query });
    console.log('pecent', res)
    if (res['success']) {
      return res['data'] ?? 0.0;
    }
    return 0;
  }
}

