import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  TouchableOpacity
} from 'react-native';
import QuizDetailNotifier from './quiz_detail_notifier';
import { AppImage } from '../../../../carebox/components/app-image';
import AppLine from '../../../../carebox/components/app-line';
import AppHeader from '../../../../carebox/components/app-header';
import { IconCase, IconLock } from '../../../../carebox/assets/icons';
import { COLORS } from '../../../../carebox/utils/colors';
import { NavigationService } from '../../../../carebox/navigation/NavigationService';
import { ROUTER_NAME } from '../../../../carebox/navigation/RouterName';
import { fontPixel } from '../../../../carebox/utils/sizes';
import { setFontFamily } from '../../../../carebox/utils/functions';

const QuizDetailScreen = ({ route }) => {
  const [updateState, setUpState] = useState(false);
  const _presenter = new QuizDetailNotifier(route.params.model);
  const model = route.params.model;

  useEffect(() => {
    console.log(model.quizImage);

    const loadData = async () => {
      const _percent = await _presenter.loadData();
      setUpState(!updateState);
    };
    loadData();
  }, []);

  const onStart = () => {
    NavigationService.push(ROUTER_NAME.QuizTestScreen, { model: model });
  };

  return (
    <SafeAreaView style={styles.body}>
      <StatusBar barStyle="dark-content" />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        contentContainerStyle={styles.contentContainerStyle}
        showsVerticalScrollIndicator={false}
        style={styles.scrollView}>
        <AppHeader />
        <AppImage uri={model.quizImage} style={styles.image} />

        <Text style={styles.title}>{model.quizTitle}</Text>
        <View style={styles.timeCountContainer}>
          <Image style={styles.icTime} source={IconLock} />
          <Text style={styles.timeValueStyles}>{model.quizTime} phút</Text>
        </View>

        <AppLine style={styles.line_1} />
        <Text style={styles.intro}>Giới thiệu</Text>
        <Text style={styles.introValue}>{model.quizIntro}</Text>
        <AppLine />
        <Text style={styles.intro}>Mục tiêu trắc nghiệm</Text>
        {model.targets().map((target, index) => {
          return (
            <View key={index} style={styles.target}>
              <Image style={styles.icTarget} source={IconCase} />
              <Text style={styles.targetValue}>{target['value']}</Text>
            </View>
          );
        })}
      </ScrollView>
      <View style={styles.vButton}>
        <TouchableOpacity
          style={styles.styleBtn}
          onPress={onStart}>
          <Text style={styles.buttonText}>Bắt đầu</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  body: {
    backgroundColor: COLORS.bg,
    height: '100%',
    width: '100%',
  },
  scrollView: {
    paddingHorizontal: 15,
    paddingBottom: 60,
  },
  line_1: {
    marginTop: 10,
  },
  image: {
    width: 'auto',
    height: 200,
    borderRadius: 15,
  },
  title: {
    color: COLORS.main,
    marginVertical: 16,
    fontSize: fontPixel(16),
    fontFamily: setFontFamily('GilroyBold')
  },
  intro: {
    color: COLORS.black303030,
    marginTop: 16,
    fontSize: fontPixel(16),
    fontFamily: setFontFamily('GilroyBold')
  },
  introValue: {
    marginTop: 16,
    color: COLORS.black303030,
    textAlign: 'justify',
    lineHeight: 20,
    fontSize: fontPixel(14),
    fontFamily: setFontFamily('GilroyMedium')
  },
  target: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
    flex: 1,
    width: '100%',
  },
  targetValue: {
    color: COLORS.black303030,
    width: '100%',
    flexShrink: 1,
    flexWrap: 'wrap',
    lineHeight: 20,
    fontSize: fontPixel(14),
    fontFamily: setFontFamily('GilroyMedium')
  },
  icTarget: {
    width: 24,
    height: 24,
    marginRight: 10,
    tintColor: COLORS.orangeF58220
  },
  vButton: {
    backgroundColor: COLORS.white,
    padding: 15,
    width: 'auto',
  },
  styleBtn: {
    height: fontPixel(45),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 16,
    backgroundColor: COLORS.main
  },
  buttonText: {
    lineHeight: 24,
    color: COLORS.white,
    fontSize: fontPixel(16),
    fontFamily: setFontFamily('GilroySemiBold')
  },
  contentContainerStyle: {
    paddingBottom: 16
  },
  icTime: {
    width: 24,
    height: 24,
    marginRight: 10,
  },
  timeCountContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  timeValueStyles: {
    color: COLORS.black303030,
    fontSize: fontPixel(14),
    fontFamily: setFontFamily('GilroyMedium')
  }
});
export default QuizDetailScreen;
