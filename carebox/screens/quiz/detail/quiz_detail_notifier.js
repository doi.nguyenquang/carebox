import { QuizModel } from '../quiz_model'
import QuizDetailApiClient from './quiz_detail_api_client'

export default class QuizDetailNotifier {

  constructor(model: QuizModel) {
    // console.log('model: ', model);
    this.model = model;
    this._apiClient = new QuizDetailApiClient(model);
    this.percent = 0;
    this.showTaget = false;
  }

  _apiClient: QuizDetailApiClient;
  model: QuizModel;
  percent;
  showTaget;


  async loadData() {
    this.pecent = await this._apiClient.pecent();
  }

  onStart() {
    // Utils.navigatePage(context, QuizTestScreen(model: model));
  }

  showHideTaget() {
    // state = state.copyWith(showTaget: !state.showTaget);
    this.showTaget = !this.showTaget;
  }
}
