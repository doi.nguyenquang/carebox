import { CategoryModel } from '../../../carebox/models/category_model'

class QuizModel {
  createdAt;
  updatedAt;
  quizId;
  quizTime;
  quizTarget;
  quizIntro;
  quizTitle;
  quizContent;
  quizImage;
  category: CategoryModel;
  categoryId;

  constructor({
    createdAt,
    updatedAt,
    quizId,
    quizTarget,
    quizIntro,
    categoryId,
    quizTitle,
    quizContent,
    quizImage,
    category,
    quizTime,
  }) {
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.quizId = quizId;
    this.quizTarget = quizTarget;
    this.quizIntro = quizIntro;
    this.categoryId = categoryId;
    this.quizTitle = quizTitle;
    this.quizContent = quizContent;
    this.quizImage = quizImage;
    this.category = category;
    this.quizTime = quizTime;
  }

  targets() {
    if (this.quizTarget) {
      return JSON.parse(this.quizTarget)
    }
    return []
  }

  static fromJson = (json) => {
    return new QuizModel(
      {
        createdAt: json['created_at'],
        updatedAt: json['updated_at'],
        quizId: json['quiz_id'],
        quizTarget: json['quiz_target'],
        quizIntro: json['quiz_intro'],
        categoryId: json['category_id'],
        quizTitle: json['quiz_title'],
        quizContent: json['quiz_content'],
        quizImage: json['quiz_image'],
        quizTime: json['quiz_time'],
        category: json['category'] == null
          ? null
          : CategoryModel.fromJson(json['category'])

      }
    );
  }
}

class QuizModelList {
  list;

  constructor({ list }) {
    this.list = list;
  }

  static fromJson = (datas) => {
    var list = [];
    if (datas) {
      list = datas.map((f) => new QuizModel.fromJson(f));
    }
    return new QuizModelList({ list });
  }
}

export { QuizModel, QuizModelList }