import { CategoryModel } from '../../../carebox/models/category_model'
import QuizApiClient from './quiz_api_client'

export default class QuizNotifier {
  constructor() {
    this._apiClient = new QuizApiClient();
  }

  _apiClient: QuizApiClient;
  // final StreamController streamController = StreamController.broadcast();
  // late TabController tabController;
  // final textSearchControler = TextEditingController();
  // final searchKey = GlobalKey();
  // int _currentTab = 0;
  // QuizModel? currentModel;
  // List popular = [];
  // final String _loadCateFun = 'load_cate_func';
  // final String _searchQuiz = 'search_quiz';
  // late TickerProvider vsync;
  currentCate: CategoryModel;
  categories = [];
  list = [];
  popular = [];
  isFirst = true;
  hashNextPage = true;
  txSearch = null;


  // function initState() {
  //   loadData();
  //   loadCate();
  // }

  // function _handleTabSelection() {
  //   if (tabController.indexIsChanging) {
  //     final _tab = tabController.index;
  //     switch (_tab) {
  //       case 0:
  //         currentCate = null;
  //         break;
  //       default:
  //         currentCate = state.categories[_tab - 1];
  //         break;
  //     }
  //     if (_currentTab != _tab) {
  //       _currentTab = _tab;
  //       list.clear();
  //       state = state.copyWith(list: []);
  //       loadData();
  //     }
  //   }
  // }



  async loadCate() {
    this.categories = await this._apiClient.categoriesQuiz();
    // console.log('QuizNotifier', this.categories)
    // if (_cate != null) {
    //   state = state.copyWith(categories: _cate);
    //   tabController =
    //       TabController(length: state.categories.length + 1, vsync: vsync);
    //   controller.addListener(_handleScroll);
    //   tabController.addListener(_handleTabSelection);
    // }
  }

  async loadData({ isFirst }) {
    this.list = await this._apiClient.quiz({ isFirst: isFirst, cate: this.currentCate, search: this.txSearch, });
    this.hashNextPage = this._apiClient.hashNextPage;
    this.isFirst = false;
    if (this.popular.length == 0) {
      this.popular = [...this.list]
    }
    console.log('QuizNotifier', this.list)
  }

  async loadMore() {
    if (this.hashNextPage) {
      const _list = await this._apiClient.quiz({ isFirst: false, cate: this.currentCate, search: this.txSearch, });
      this.hashNextPage = this._apiClient.hashNextPage;
      if(_list.length > 0) {
        this.list = [this.list, ..._list];
      }
    }


  }

  async changeCate(value) {
    this.currentCate = value;
    this.list = [];
    this.isFirst = true;
    await this.loadData({ isFirst: true })
  }

  async onSubmitEditing(value) {
    this.txSearch = value;
    this.list = [];
    this.isFirst = true;
    await this.loadData({ isFirst: true })
  }


  // void _handleScroll() {
  //   streamController.sink.add(controller.offset);
  // }

  // void updateState(List datas) {
  //   list = datas;
  //   if (popular.isEmpty) {
  //     popular = List.from(datas);
  //     if (popular.isNotEmpty) {
  //       currentModel = popular.first;
  //     }
  //   }
  //   state = state.copyWith(list: datas);
  // }

  // Widget itemBuild(BuildContext context, int index) {
  //   if (index < list.length) {
  //     return ItemQuizScreenView(
  //       model: list[index],
  //       onTap: _onDetail,
  //     );
  //   }
  //   return Visibility(
  //     visible: _apiClient.hashNextPage,
  //     child: loadingView(),
  //   );
  // }

  // Widget itemBuilder2(BuildContext context, int index) {
  //   return ItemQuizPopularView(
  //     model: popular[index],
  //     onTap: _onDetail,
  //   );
  // }

  // void onIndexChanged(int value) {
  //   currentModel = popular[value];
  //   state = state.copyWith(currentSlide: value);
  // }

  // void _onDetail(QuizModel value) {
  //   Utils.navigatePage(
  //       context,
  //       QuizDetailScreen(
  //         model: value,
  //       ));
  // }



  // void search() async {
  //   var result = await Utils.navigatePage(context, SearchCourseScreen(text: state.search));
  //   if (result == null) return;
  //   if (state.search == result) return;
  //   state = state.copyWith(search: result);
  //   loadData();
  // }



  // @override
  // void dispose() {
  //   super.dispose();
  //   controller.removeListener(_handleScroll);
  //   onDestroy();
  //   tabController.removeListener(_handleTabSelection);
  //   tabController.dispose();
  // }

}
