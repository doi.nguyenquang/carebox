/* eslint-disable no-shadow */
/* eslint-disable radix */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect, useMemo} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import AppHeader from '../../../../carebox/components/app-header';
import {COLORS} from '../../../../carebox/utils/colors';
import {NavigationService} from '../../../../carebox/navigation/NavigationService';
import {ROUTER_NAME} from '../../../../carebox/navigation/RouterName';
import {useDispatch, useSelector} from 'react-redux';
import {quizActions} from '../../../../carebox/redux/slices/quiz-slice';
import {quizSelector} from '../../../../carebox/redux/selectors';
import CanView from '../../../../carebox/components/can-view';
import {fontPixel, SIZE} from '../../../../carebox/utils/sizes';
import {setFontFamily} from '../../../../carebox/utils/functions';
import {QuizAnswerRadioButton} from './quiz-answer-radio-button';
import AppModal from '../../../../carebox/components/app-modal';
import AppLoading from '../../../../carebox/components/app-loading';
import _ from 'lodash';
import {TestResultImg} from '../../../../carebox/assets/icons';

let isClickSaveAndExitModal = false;
let isNeedShowConfirm = false;

const QuizTestScreen = ({route}) => {
  const model = route.params.model;
  const dispatch = useDispatch();

  const getListQuizLoading = useSelector(quizSelector.getListQuizLoading);
  const submitQuizTestLoading = useSelector(quizSelector.submitQuizTestLoading);
  const listQuizData = useSelector(quizSelector.listQuizData) || [];
  const submitQuizTestSuccess = useSelector(quizSelector.submitQuizTestSuccess);

  const [showPopupConfirmExit, setShowPopupConfirmExit] = useState(false);
  const [showPopupFinish, setShowPopupFinish] = useState(false);
  const [step, setStep] = useState(0);
  const [listQuizDataParse, setListQuizDataParse] = useState([]);
  const [totalScore, setTotalScore] = useState(0);
  const [totalScoreAnswer, setTotalScoreAnswer] = useState(0);

  useEffect(() => {
    setListQuizDataParse(listQuizData);
  }, [listQuizData]);

  useEffect(() => {
    const query = {quiz_id: model.quizId};
    dispatch(quizActions.getListQuizRequest(query));
    return () => {
      dispatch(quizActions.resetQuizReducerAction());
    };
  }, []);

  useEffect(() => {
    if (submitQuizTestSuccess.status) {
      if (submitQuizTestSuccess.isFinished) {
        isNeedShowConfirm = true;
      } else {
        NavigationService.goBack();
      }
    }
  }, [submitQuizTestSuccess.status]);

  const onContinue = () => {
    const newStep = step + 1;
    if (newStep <= listQuizData?.length - 1) {
      setStep(newStep);
    } else {
      onSubmit(true);
    }
  };

  const currentStep = useMemo(() => {
    return step;
  }, [step]);

  const onBackPress = () => {
    if (listQuizDataParse.length === 0) {
      NavigationService.goBack();
      return;
    }
    if (step > 0) {
      setShowPopupConfirmExit(true);
    } else {
      const isCurrentAnswer = listQuizDataParse[step].quiz_test_answer.find(
        item => item.isSelected,
      );
      if (_.isEmpty(isCurrentAnswer)) {
        NavigationService.goBack();
      } else {
        setShowPopupConfirmExit(true);
      }
    }
  };

  const getQuiztestAnswerCheck = (listCurrentAnswer, answerId) => {
    const listQuizItemChecked = listCurrentAnswer.map(item => {
      return {...item, isSelected: item.id === answerId};
    });
    return listQuizItemChecked;
  };

  const answerItemSelect = (quizIndex, answerId) => {
    const listAnswerOfQuizCheck = listQuizDataParse.map((quizItem, index) => {
      return {
        ...quizItem,
        quiz_test_answer:
          index === quizIndex
            ? getQuiztestAnswerCheck(quizItem.quiz_test_answer, answerId)
            : quizItem.quiz_test_answer,
      };
    });
    setListQuizDataParse(listAnswerOfQuizCheck);
  };

  const renderQuizItemView = (quizItem, quizIndex) => {
    return (
      <View>
        <Text style={styles.quizTitleStyles}>{quizItem?.quiz?.quiz_title}</Text>
        <QuizAnswerRadioButton
          listAnswer={quizItem?.quiz_test_answer}
          quizId={quizItem?.quiz_test_id}
          quizIndex={quizIndex}
          answerItemSelect={answerItemSelect}
        />
      </View>
    );
  };

  const onModalHide = () => {
    isClickSaveAndExitModal = false;
    onSubmit(false);
  };

  const btnSaveAndExitOnClick = () => {
    setShowPopupConfirmExit(false);
    isClickSaveAndExitModal = true;
  };

  const btnNextOnclick = () => {
    setShowPopupConfirmExit(false);
  };

  const getProgressPercentBg = () => {
    const listAnswer = [];
    listQuizDataParse.forEach(quizItem => {
      const isCurrentAnswer = quizItem.quiz_test_answer.find(
        item => item.isSelected,
      );
      if (!_.isEmpty(isCurrentAnswer)) {
        listAnswer.push({
          test_detail_id: quizItem.quiz_test_id,
          user_answer: isCurrentAnswer.id,
        });
      }
    });
    if (listQuizData.length > 0) {
      const percent = (listAnswer?.length / listQuizData.length) * 100;
      return {
        width: percent + '%',
      };
    } else {
      return {
        width: 0,
      };
    }
  };

  const disabledBtnNext = useMemo(() => {
    if (listQuizDataParse.length > 0) {
      const isCurrentAnswer = listQuizDataParse[step].quiz_test_answer.find(
        item => item.isSelected,
      );
      return _.isEmpty(isCurrentAnswer);
    }
    return true;
  }, [step, listQuizDataParse]);

  const getStyleButton = useMemo(() => {
    return {
      backgroundColor: disabledBtnNext ? COLORS.greyE9E9E9 : COLORS.main,
    };
  }, [disabledBtnNext]);

  const getStyleTextButton = useMemo(() => {
    return {
      color: disabledBtnNext ? COLORS.grey787878 : COLORS.white,
    };
  }, [disabledBtnNext]);

  const getTextButton = useMemo(() => {
    return step === listQuizDataParse.length - 1 ? 'Hoàn thành' : 'Tiếp tục';
  }, [step]);

  const onSubmit = isFinished => {
    const listAnswer = [];
    listQuizDataParse.forEach(quizItem => {
      const isCurrentAnswer = quizItem.quiz_test_answer.find(
        item => item.isSelected,
      );
      if (!_.isEmpty(isCurrentAnswer)) {
        listAnswer.push({
          test_detail_id: quizItem.quiz_test_id,
          user_answer: isCurrentAnswer.id,
        });
      }
    });
    const bodyData = {
      test_id: model.quizId,
      answer: JSON.stringify(listAnswer),
    };
    let totalScore = 0;
    let totalScoreAnswer = 0;
    listQuizDataParse.forEach(quizItem => {
      const isCurrentAnswer = quizItem.quiz_test_answer.find(
        item => item.isSelected,
      );
      const totalScoreQuizItem = quizItem.quiz_test_answer.reduce(
        (total, currentValue) => {
          return total + parseInt(currentValue.point);
        },
        0,
      );

      totalScore += totalScoreQuizItem;
      if (!_.isEmpty(isCurrentAnswer)) {
        totalScoreAnswer += parseInt(isCurrentAnswer.point);
      }
    });
    setTotalScore(totalScore);
    setTotalScoreAnswer(totalScoreAnswer);
    dispatch(
      quizActions.submitQuizTestRequest({
        bodyData: bodyData,
        isFinished: isFinished,
      }),
    );
  };

  const onCertificateOnclick = () => {
    setShowPopupFinish(false);
    NavigationService.goBack();
    NavigationService.navigate(ROUTER_NAME.QuizTestResultScreen, {
      quizName: model.quizTitle,
    });
  };

  return (
    <View style={styles.body}>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.body}>
        <AppHeader
          title={model.quizTitle}
          style={{paddingHorizontal: 15}}
          onBack={onBackPress}
        />
        <View style={styles.container}>
          <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            style={styles.scrollView}
            showsVerticalScrollIndicator={false}>
            {listQuizDataParse?.map((quizItem, index) => {
              return (
                <CanView condition={index === currentStep} key={index}>
                  {renderQuizItemView(quizItem, index)}
                </CanView>
              );
            })}
          </ScrollView>
        </View>
        <View style={styles.vButton}>
          <Text style={styles.progressTextStyles}>{`Tiến độ ${step + 1}/${
            listQuizData.length
          }`}</Text>
          <View style={styles.progressBarStyles}>
            <View style={[styles.percentProgressBar, getProgressPercentBg()]} />
          </View>
          <TouchableOpacity
            style={[styles.styleBtn, getStyleButton]}
            disabled={disabledBtnNext}
            onPress={onContinue}>
            <Text style={[styles.buttonText, getStyleTextButton]}>
              {getTextButton}
            </Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
      <AppModal
        isVisible={showPopupConfirmExit}
        style={styles.modalContainer}
        onModalHide={isClickSaveAndExitModal && onModalHide}>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <View style={styles.modalBoxStyles}>
            <View style={styles.modalContent}>
              <Text style={styles.textModalTitle}>Bạn muốn rời khỏi?</Text>
              <Text style={styles.textModalContent}>
                Bạn có thể tiếp tục trắc nghiệm nhanh chóng tại trang chủ &gt;
                Tiếp tục hoạt động.
              </Text>
            </View>
            <View style={styles.actionModalcontainer}>
              <TouchableOpacity
                style={styles.btnBookCancelStyle}
                onPress={btnSaveAndExitOnClick}>
                <Text style={styles.btnTextStyles}>Lưu và thoát</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.btnBookStyle}
                onPress={btnNextOnclick}>
                <Text style={styles.btnTextStyles}>Tiếp tục</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </AppModal>
      <AppModal
        isVisible={showPopupFinish}
        style={styles.modalContainer}
        onModalHide={isClickSaveAndExitModal && onModalHide}>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <ImageBackground
            style={styles.modalResultScore}
            source={TestResultImg}
            imageStyle={{borderRadius: 16}}>
            <View style={{flex: 0.2}} />
            <View style={styles.contentResultPopup}>
              <Text style={styles.congratulationStyle}>CHÚC MỪNG</Text>
              <Text style={styles.resultTitleStyles}>
                {'Bạn đã hoàn thành \n khóa học với số điểm'}
              </Text>
              <View style={styles.scoreContainer}>
                <Text style={styles.scoreTextStyles}>
                  {totalScoreAnswer}{' '}
                  <Text style={styles.scoreTarget}>/ {totalScore}</Text>
                </Text>
              </View>
              <TouchableOpacity
                style={styles.btnReceiveCertificateStyle}
                onPress={onCertificateOnclick}>
                <Text style={styles.btnTextStyles}>Nhận chứng chỉ</Text>
              </TouchableOpacity>
            </View>
          </ImageBackground>
        </View>
      </AppModal>
      <AppLoading
        isVisible={getListQuizLoading || submitQuizTestLoading}
        onModalHide={() => {
          isNeedShowConfirm && setShowPopupFinish(true);
          isNeedShowConfirm = false;
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.bg,
  },
  body: {
    height: '100%',
    width: '100%',
    backgroundColor: COLORS.white,
  },
  scrollView: {
    paddingHorizontal: 15,
  },
  vButton: {
    backgroundColor: COLORS.white,
    padding: 15,
    width: 'auto',
  },
  quizTitleStyles: {
    fontSize: fontPixel(16),
    color: COLORS.black303030,
    fontFamily: setFontFamily('GilroyBold'),
    marginTop: 16,
  },
  modalContainer: {
    justifyContent: 'center',
  },
  modalBoxStyles: {
    borderRadius: 16,
    backgroundColor: COLORS.white,
    width: SIZE.SCREEN_WIDTH - 32,
  },
  modalResultScore: {
    borderRadius: 16,
    backgroundColor: COLORS.white,
    width: SIZE.SCREEN_WIDTH - 32,
    height: SIZE.SCREEN_HEIGHT * 0.46,
  },
  actionModalcontainer: {
    padding: 16,
    borderTopWidth: 1,
    flexDirection: 'row',
    borderTopColor: COLORS.greyE9E9E9,
  },
  btnBookStyle: {
    flex: 1,
    marginLeft: 10,
    borderRadius: 12,
    alignItems: 'center',
    height: fontPixel(40),
    justifyContent: 'center',
    backgroundColor: COLORS.main,
  },
  btnTextStyles: {
    color: COLORS.white,
    fontSize: fontPixel(16),
    fontFamily: setFontFamily('GilroySemiBold'),
  },
  modalContent: {
    height: 'auto',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 20,
    paddingHorizontal: 16,
  },
  btnBookCancelStyle: {
    flex: 1,
    marginRight: 10,
    borderRadius: 12,
    alignItems: 'center',
    height: fontPixel(40),
    justifyContent: 'center',
    backgroundColor: COLORS.orangeF58220,
  },
  textModalTitle: {
    fontSize: fontPixel(16),
    color: COLORS.black303030,
    fontFamily: setFontFamily('GilroyBold'),
  },
  textModalContent: {
    marginTop: 16,
    textAlign: 'center',
    fontSize: fontPixel(16),
    color: COLORS.black303030,
    fontFamily: setFontFamily('GilroyMedium'),
  },
  styleBtn: {
    height: fontPixel(45),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 12,
    backgroundColor: COLORS.main,
  },
  buttonText: {
    fontSize: fontPixel(16),
    fontWeight: 'bold',
    fontFamily: setFontFamily('GilroyMedium'),
  },
  progressBarStyles: {
    height: 6,
    borderRadius: 4,
    marginVertical: 15,
    backgroundColor: COLORS.greyE9E9E9,
  },
  percentProgressBar: {
    height: 6,
    borderRadius: 4,
    backgroundColor: COLORS.main,
  },
  progressTextStyles: {
    fontSize: fontPixel(14),
    fontFamily: setFontFamily('GilroyRegular'),
  },
  congratulationStyle: {
    color: COLORS.grey8F9BB3,
    fontSize: fontPixel(16),
    fontFamily: setFontFamily('GilroySemiBold'),
  },
  resultTitleStyles: {
    color: COLORS.black222B45,
    marginHorizontal: 20,
    marginTop: 16,
    textAlign: 'center',
    fontSize: fontPixel(20),
    fontFamily: setFontFamily('GilroySemiBold'),
  },
  btnReceiveCertificateStyle: {
    width: '85%',
    borderRadius: 12,
    marginHorizontal: 20,
    height: fontPixel(36),
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    backgroundColor: COLORS.main,
  },
  scoreContainer: {
    flex: 1,
  },
  scoreTextStyles: {
    marginTop: 20,
    fontSize: fontPixel(42),
    fontFamily: setFontFamily('GilroySemiBold'),
    color: COLORS.orangeF58220,
  },
  scoreTarget: {
    color: COLORS.green2,
  },
  contentResultPopup: {
    flex: 0.8,
    alignItems: 'center',
  },
});
export default QuizTestScreen;
