import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  ImageBackground,
  SafeAreaView,
  TouchableOpacity,
  Platform,
  StatusBar
} from 'react-native';
import { isEmpty } from 'lodash'
import { useSelector } from 'react-redux';
import { COLORS } from '../../../../carebox/utils/colors';
import { fontPixel } from '../../../../carebox/utils/sizes';
import { setFontFamily } from '../../../../carebox/utils/functions';
import { profileSelector } from '../../../../carebox/redux/selectors';
import { Bg3, IconExit, UserAvatarDefault } from '../../../../carebox/assets/icons';
import { useNavigation } from "@react-navigation/core"

const QuizTestResultScreen = ({ route }) => {
  const navigation = useNavigation()
  const quizName = route.params.quizName
  const profileData = useSelector(profileSelector.profileData)

  const getHeaderContainer = () => {
    return {
      marginTop: Platform.OS === 'android' ? StatusBar.currentHeight + 16 : 0
    }
  }

  return (
    <ImageBackground style={styles.container} source={Bg3}>
      <StatusBar translucent/>
      <SafeAreaView style={styles.container}>
        <View style={[styles.headerContainer, getHeaderContainer()]}>
          <TouchableOpacity style={styles.btnCloseStyles} onPress={() => navigation.pop(2)}>
            <Image style={styles.icCloseStyles} source={IconExit} />
          </TouchableOpacity>
        </View>
        <View style={styles.profileContainer}>
          <Image style={styles.avatarContainer} source={!isEmpty(profileData.pictureUrl) ? { uri: profileData.pictureUrl } : UserAvatarDefault} />
        </View>
        <View style={styles.resultContainer}>
          <View style={styles.contentResultContainer}>
            <Text style={styles.titleResult}>{`CHỨNG NHẬN HOÀN THÀNH \n BÀI TRẮC NGHIỆM TÂM LÝ`}</Text>
            <Text style={styles.quizNameStyles}>{quizName.toUpperCase()}</Text>
          </View>
          <TouchableOpacity
            style={styles.btnBackHomeScreen}
            onPress={() => navigation.pop(2)}
          >
            <Text style={styles.btnTextStyles}>ĐÓNG</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  icCloseStyles: {
    width: 30,
    height: 30
  },
  btnCloseStyles: {
    backgroundColor: 'red',
    marginRight: 20,
    borderRadius: 12,
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLORS.white,
  },
  profileContainer: {
    flex: 0.8,
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatarContainer: {
    width: 165,
    height: 165,
    borderRadius: 90,
    backgroundColor: 'white'
  },
  resultContainer: {
    flex: 1,
    paddingHorizontal: 20,
  },
  contentResultContainer: {
    flex: 1,
    alignItems: 'center',
  },
  btnBackHomeScreen: {
    borderRadius: 12,
    height: fontPixel(40),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLORS.white,
    marginBottom: Platform.OS === 'android' ? 16 : 0
  },
  btnTextStyles: {
    color: COLORS.main,
    lineHeight: 24,
    fontSize: fontPixel(16),
    fontFamily: setFontFamily('GilroySemiBold')
  },
  titleResult: {
    textAlign: 'center',
    color: COLORS.white,
    lineHeight: 24,
    fontSize: fontPixel(16),
  },
  quizNameStyles: {
    textAlign: 'center',
    color: COLORS.white,
    lineHeight: 28,
    marginTop: 8,
    fontSize: fontPixel(20),
    fontFamily: setFontFamily('GilroySemiBold')
  }
})

export default QuizTestResultScreen