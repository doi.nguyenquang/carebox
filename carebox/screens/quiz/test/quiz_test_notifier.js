import { QuizModel } from '../quiz_model'
import { QuizTestModel } from './quiz_test_model'
import QuizTestTypeApiClient from './quiz_test_api_client'

export default class QuizTestNotifier {
  constructor(model: QuizModel) {
    this.model = model;
    this._apiClient = new QuizTestTypeApiClient(model);
  };

  _apiClient: QuizTestTypeApiClient;
  // final String _getQuestionFunc = 'get_question_func';
  // final String _sendResultFunc = 'send_question_func';
  model: QuizModel;
  current: QuizTestModel;
  index = 0;
  list = [];

  get btnTitle() {
    if (state.index < state.list.length) {
      return 'TIẾP TỤC';
    } else {
      return 'HOÀN THÀNH';
    }
  }

  async loadData(){
    const result = await this._apiClient.question();
    console.log('AAAAAAAA result: ', result)
    if (result.length > 0) {
      this.current = result[0];
    }
    this.list = result;
  }

}
