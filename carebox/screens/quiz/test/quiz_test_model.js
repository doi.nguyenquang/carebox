class QuizTestModel {
  quizTestId;
  quizId;
  quizTestType;
  quizTestTime;
  quizTestContent;
  quizTestAnswer;
  createdAt;
  updatedAt;
  userAnswer;
  userAnswers = [];
  // oneChoice => quizTestType == '1';

  constructor({
    quizTestId,
    quizId,
    quizTestType,
    quizTestTime,
    quizTestContent,
    quizTestAnswer,
    createdAt,
    updatedAt,
  }) {
    this.quizTestId = quizTestId;
    this.quizId = quizId;
    this.quizTestType = quizTestType;
    this.quizTestTime = quizTestTime;
    this.quizTestContent = quizTestContent;
    this.quizTestAnswer = quizTestAnswer;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }

  static fromJson = (json) => {
    return new QuizTestModel(
      {
        quizTestId: json['quiz_test_id'],
        quizId: json['quiz_id'],
        quizTestType: json['quiz_test_type'],
        quizTestTime: json['quiz_test_time'],
        quizTestContent: json['quiz_test_content'],
        quizTestAnswer: json['quiz_test_answer'],
        createdAt: json['created_at'],
        updatedAt: json['updated_at'],
      }
    );
  }

  //   get userAn {
  //   if (oneChoice) {
  //     if (userAnswer == null) {
  //       return null;
  //     }
  //     return { 'quiz_test_id': quizTestId, 'user_answer': userAnswer };
  //   } else {
  //     if (userAnswers.isEmpty) return null;
  //     return { 'quiz_test_id': quizTestId, 'user_answer': userAnswers };
  //   }
  // }
}


class QuizTestModelList {
  list;

  constructor({ list }){
    this.list = list;
  }

  static fromJson (datas){
    var list = [];
    if (datas) {
      list = datas.map((f) => new QuizTestModel.fromJson(f));
    }
    return new QuizTestModelList({list: list});
  }
}

export {QuizTestModel, QuizTestModelList}