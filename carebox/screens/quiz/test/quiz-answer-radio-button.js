import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { IconImage } from '../../../../carebox/components/app-image'
import { COLORS } from '../../../../carebox/utils/colors';
import { fontPixel } from '../../../../carebox/utils/sizes';
import { IconUnSelected, IconSelected } from '../../../../carebox/assets/icons'
import { setFontFamily } from '../../../../carebox/utils/functions';

export const QuizAnswerRadioButton = (props) => {
  const { listAnswer, answerItemSelect, quizIndex } = props;

  const getStylesRowItem = (isSelected) => {
    return {
      backgroundColor: isSelected ? COLORS.white : COLORS.greyE9E9E9,
      borderColor: COLORS.main,
      borderWidth: isSelected ? 1 : 0,
    }
  }

  const getTitleStyles = (isSelected) => {
    return {
      color: isSelected ? COLORS.black303030 : COLORS.grey787878,
    }
  }

  return (
    <View>
      {
        listAnswer?.map((item, index) => {
          return (
            <TouchableOpacity
              key={index}
              onPress={() => answerItemSelect(quizIndex, item.id)}
              style={[styles.rowCheckbox, getStylesRowItem(item.isSelected)]}
            >
              <IconImage source={item.isSelected ? IconSelected : IconUnSelected} style={styles.radioIconStyles} />
              <View style={styles.radioBoxContent}>
                <Text numberOfLines={2} style={[styles.subTitleStyles, getTitleStyles(item?.isSelected)]}>{item.answer}</Text>
              </View>
            </TouchableOpacity>
          )
        })
      }
    </View>
  )
}

const styles = StyleSheet.create({
  rowCheckbox: {
    flexDirection: 'row',
    marginTop: 20,
    alignItems: 'center',
    padding: 16,
    borderRadius: 16,
  },
  radioIconStyles: {
    width: 24,
    height: 24,
  },
  radioBoxContent: {
    flex: 1,
    marginLeft: 16
  },
  subTitleStyles: {
    fontSize: fontPixel(14),
    fontFamily: setFontFamily('GilroyMedium'),
  }
})