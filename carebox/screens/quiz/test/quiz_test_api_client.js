import { QuizModel } from '../quiz_model'
import { QuizTestModelList } from './quiz_test_model'
import ApiConfig from '../../../../carebox/services/api_config'
import * as api from '../../../../carebox/services/Request'

export default class QuizTestTypeApiClient {
  constructor(model: QuizModel) {
    this.model = model;
  }

  model: QuizModel;
  datas = [];

  async question() {
    const query = { 'quiz_id': this.model.quizId };
    const res = await api.methodGet({api: ApiConfig.quizTest, queries: query});
    console.log('QuizTestTypeApiClient', res);
    if (res['success']) {
      this.datas = QuizTestModelList.fromJson(res['data']['values']).list;
      return this.datas;
    }
    return [];
  }

  async sendResult() {
    const query = { 'quiz_id': this.model.quizId, 'answer': JSON.stringify(ans) };
    const res = await api.methodPost({api: ApiConfig.quizResult, body: query});
    if (res['success']) {
      return true;
    }
    return false;
  }

  get ans() {
    const answer = [];
    for (var element in this.datas) {
      if (element.userAn) {
        answer.add(element.userAn);
      }
    }
    return answer;
  }
}
