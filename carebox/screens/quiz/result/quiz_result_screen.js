import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
} from 'react-native';
import { QuizModel } from '../quiz_model'
import QuizTestNotifier from './quiz_test_notifier'
import AppButton from '../../../../carebox/components/app-button'
import AppLine from '../../../../carebox/components/app-line'
import AppHeader from '../../../../carebox/components/app-header'
import { COLORS } from '../../../../carebox/utils/colors';

const QuizResultScreen = ({ route }) => {

    const [currentQues, setCurrentQues] = useState(null);
    const _presenter = new QuizTestNotifier(route.params.model);
    const model: QuizModel = route.params.model;

    useEffect(() => {
        const loadData = async () => {
            const _percent = await _presenter.loadData();
            setCurrentQues(_presenter.current)
            console.log('currentQues', currentQues)
            
        }
        loadData();

    }, [])




    const onContinue = () => {

    }

    return (
        <View style={styles.body}>
            <StatusBar barStyle="dark-content" />
            <SafeAreaView style={styles.body}>
                <ScrollView
                    contentInsetAdjustmentBehavior="automatic"
                    style={styles.scrollView}>

                    <AppHeader
                        title={model.quizTitle}
                    />

                    {currentQues && currentQues.quizTestContent
                        && <Text style={styles.introValue}>{currentQues.quizTestContent}</Text>}
                    <AppLine />


                </ScrollView>
                <View style={styles.vButton} >
                    <AppButton
                        title='Tiếp tục'
                        onPress={onContinue}
                    >
                    </AppButton>
                </View>

            </SafeAreaView>


        </View>
    );
};

const styles = StyleSheet.create({
    body: {
        backgroundColor: COLORS.bg,
        height: '100%',
        width: '100%',
    },
    scrollView: {
        paddingHorizontal: 15,
        paddingBottom: 60
    },
    line_1: {
        marginTop: 10
    },
    image: {
        width: 'auto',
        height: 200,
        borderRadius: 15
    },
    title: {
        color: COLORS.main,
        fontWeight: 'bold',
        fontSize: 16,
        marginVertical: 15,
    },
    intro: {
        color: COLORS.black303030,
        fontWeight: 'bold',
        fontSize: 16,
        marginTop: 15
    },
    introValue: {
        color: COLORS.black303030,
        marginVertical: 15,
    },
    target: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        flex: 1,
        width: '100%',
        // justifyContent: 'center',
    },
    targetValue: {
        color: COLORS.black303030,
        width: '100%',
        flexShrink: 1,
        flexWrap: 'wrap'
    },
    icTarget: {
        color: COLORS.orangeF58220,
        width: 18,
        height: 18,
        marginRight: 10,
    },
    vButton: {
        backgroundColor: COLORS.white,
        padding: 15,
        width: 'auto',
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
    },

});
export default QuizResultScreen;