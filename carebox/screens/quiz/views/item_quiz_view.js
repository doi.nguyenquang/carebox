import React from 'react';
import {
    StyleSheet,
    View,
    Text,
    Image,
} from 'react-native';
import { AppImage } from '../../../../carebox/components/app-image'
import { COLORS } from '../../../../carebox/utils/colors';
import { QuizModel } from '../quiz_model'
import AppTimeView from '../../../../carebox/components/app-time-view'
import AppCateView from '../../../../carebox/components/app-cate'
import AppButton from '../../../../carebox/components/app-button'
import { IconQuiz } from '../../../../carebox/assets/icons'

const ItemQuizView = ({ item, onClick }) => {
    const model: QuizModel = item;
    return (
        <AppButton
            onPress={() => onClick(item)}
        >
            <View style={styles.item}>
                <AppImage
                    uri={model.quizImage}
                    style={styles.image}
                />
                <Text style={styles.title}>{model.quizTitle}</Text>
                <AppTimeView
                    value={model.quizTime}
                />
                <AppCateView
                    value={model.category}
                />

                <Image
                    source={IconQuiz}
                    style={styles.icon}
                />
            </View>
        </AppButton>
    );
};


const styles = StyleSheet.create({
    title: {
        color: COLORS.black,
        fontWeight: 'bold',
        marginVertical: 10,
    },
    image: {
        width: 'auto',
        height: 200,
        borderRadius: 15
    },
    icon: {
        width: 36,
        height: 36,
        position: 'absolute',
        top: 6,
        right: 10,
    },
    item: {
        paddingTop: 15,
        paddingHorizontal: 15,
        backgroundColor: '#fff',
        marginVertical: 8,
        marginHorizontal: 10,
        elevation: 1,
        borderRadius: 10,
    },

});
export default ItemQuizView;