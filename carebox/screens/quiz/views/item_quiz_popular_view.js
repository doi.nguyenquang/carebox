import React, { useState, useEffect } from 'react';
import {
    StyleSheet,
    View,
    Text,
} from 'react-native';
import { COLORS } from '../../../../carebox/utils/colors';
import AppButton from '../../../../carebox/components/app-button'
import Swiper from 'react-native-swiper'
import { AppImage } from '../../../../carebox/components/app-image'
import { QuizModel } from '../quiz_model'
import AppTimeView from '../../../../carebox/components/app-time-view'
import { SIZE} from '../../../../carebox/utils/sizes';

const ItemQuizPopularView = ({ items, onClick }) => {

    const [currentCate, setCurrentCate] = useState(null);

    useEffect(() => {
        if (items.length > 0) {
            setCurrentCate(items[0])
        }
    }, [])

    const onIndexChanged = (index) => {
        setCurrentCate(items[index])
    }

    return (
        <View style={styles.item}>
            <AppImage
                uri={currentCate?.quizImage}
                style={styles.image}
            />
            <View style={styles.swiper}>
                <Swiper style={styles.wrapper}
                    onIndexChanged={onIndexChanged}
                    showsPagination = {true}
                    paginationStyle = {styles.paginationStyle}
                    autoplay = {true}
                >
                    {
                        items.map((target, index) => {
                            const value: QuizModel = target;
                            return <View
                                key={index}
                                style={styles.slide1}>
                                <Text style={styles.text}>{value.quizTitle}</Text>

                                <View style={styles.bottom}>
                                    <AppTimeView
                                        value={value.quizTime}
                                        style = {styles.timeView}
                                    />

                                    <View style={styles.vButton} >
                                        <AppButton
                                            title='Chi tiết'
                                            onPress={() => onClick(value)}
                                        >
                                        </AppButton>
                                    </View>
                                </View>
                            </View>
                        })
                    }
                </Swiper>
            </View>

        </View>
    );
};


const styles = StyleSheet.create({
    paginationStyle: {
        marginBottom: 0,
        marginTop: 40,
    },
    title: {
    },
   
    item: {
        height: 300,
        width: SIZE.SCREEN_WIDTH,
    },
    swiper: {
        position: 'absolute',
        bottom: -20,
        height: 170,
        marginBottom: 10
    },
    wrapper: {
       
    },
    image: {
        flex: 1,
        width: SIZE.SCREEN_WIDTH,
        marginBottom: 30,
        height: 300,
    },
    slide1: {
        elevation: 3,
        borderRadius: 10,
        padding: 15,
        marginHorizontal: 20,
        height: 120,
        backgroundColor: COLORS.white,
    },
    text: {
        color: COLORS.main,
        fontWeight: 'bold'
    },
    bottom: {
       flexDirection: 'row',
       justifyContent: 'space-between',
       width: 'auto',
    },
    vButton: {  
        marginTop: 15,
        width: 100
    },

});
export default ItemQuizPopularView;