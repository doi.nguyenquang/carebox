import React from 'react';
import {
    StyleSheet,
    View,
    Text,
} from 'react-native';
import { COLORS } from '../../../../carebox/utils/colors';
import { CategoryModel } from '../../../../carebox/models/category_model'
import AppButton from '../../../../carebox/components/app-button'

const ItemCateView = ({ item, current, onClick }) => {
    const model: CategoryModel = item;
    const currentCate: CategoryModel = current;
    const selected = () => {
        if (currentCate) {
            return model && currentCate.categorId == model.categorId
        }
        return model == null;
    }
    return (
        <AppButton
            onPress={() => onClick(item)}
        >
            <View style={styles.item}>
                <Text style={[styles.title, { color: selected() ? COLORS.orangeF58220 : COLORS.black303030 }]}>
                    {model == null ? 'Tất cả' : model.categoryTitle}
                </Text>
                {selected() && <View style={styles.bottom} />}
            </View>
        </AppButton>
    );
};


const styles =  StyleSheet.create({
    title: {
    },
    item: {
        marginVertical: 5,
        paddingHorizontal: 10,
    },
    bottom: {
        backgroundColor: COLORS.orangeF58220,
        height: 3,
        width: 'auto',
        borderTopLeftRadius: 5,
        marginTop: 5,
        borderTopRightRadius: 5,
    }

});
export default ItemCateView;