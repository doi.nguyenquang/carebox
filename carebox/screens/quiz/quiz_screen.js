import React, { useState, useEffect } from 'react';
import {
    StyleSheet,
    View,
    StatusBar,
    VirtualizedList,
    FlatList,
    RefreshControl
} from 'react-native';
import AppHeader from '../../../carebox/components/app-header'
import LoadingView from '../../../carebox/components/loading-view'
import AppText from '../../../carebox/components/app-text'
import { COLORS } from '../../../carebox/utils/colors';
import { NavigationService } from '../../../carebox/navigation/NavigationService';
import { ROUTER_NAME } from '../../../carebox/navigation/RouterName';
import ItemQuizView from './views/item_quiz_view'
import ItemCateView from './views/item_cate_view'
import ItemQuizPopularView from './views/item_quiz_popular_view'
import QuizNotifier from './quiz_notifier'

const QuizScreen = ({ route }) => {

    const [list, setList] = useState([]);
    const [currentCate, setCurrentCate] = useState(null);
    const [hashNextPage, setHashNextPage] = useState(true);
    const [isFirst, setIsFirst] = useState(true);
    const [categories, setCategories] = useState([]);
    const [popular, setPopular] = useState([]);
    const _presenter = new QuizNotifier();
    const [refreshing, setRefreshing] = useState(false);
    const onRefresh = async () => {
        setRefreshing(true);
        await loadData();
        setRefreshing(false);
    }

    const loadData = async () => {
        await _presenter.loadCate();
        await _presenter.loadData({ isFirst: true });
        setCategories([null, ..._presenter.categories])
        setPopular(_presenter.popular)
        reload();

    }

    useEffect(() => {

        loadData();

    }, [])

    const reload = () => {
        setList(_presenter.list)
        setHashNextPage(_presenter.hashNextPage)
        setIsFirst(_presenter.isFirst)
    }

    const resetState = () => {
        setList([]);
        setHashNextPage(true);
        setIsFirst(true);
    }

    const onClickCate = async (value) => {
        setCurrentCate(value);
        resetState();
        await _presenter.changeCate(value);
        reload();
    }

    const onSubmitEditing = async (value) => {
        resetState();
        await _presenter.onSubmitEditing(value);
        reload();
    }

    const scrollToEnd = async () => {
        console.log('scrollToEnd');
        if (hashNextPage) {
            await _presenter.loadMore();
            if (_presenter.list.length != list.length) {
                setList(_presenter.list)
            }
            setHashNextPage(_presenter.hashNextPage)
        }


    }

    const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
        const paddingToBottom = 20;
        return layoutMeasurement.height + contentOffset.y >=
            contentSize.height - paddingToBottom;
    };

    const onQuizDetail = (value) => {
        NavigationService.push(ROUTER_NAME.QuizDetailScreen, { model: value });
    }

    const itemPopular = () => {
        return <ItemQuizPopularView
            items={popular}
            onClick={onQuizDetail}
        />
    }
    const itemNoData = () => {
        return <View style={styles.notData}>
            <AppText
                title='Không có dữ liệu'
                style={styles.tvNoData}
            />
        </View>
    }

    const itemCate = () => {
        return <View style={styles.cate}>
            <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={categories}
                renderItem={renderCateItem}
                keyExtractor={(item, index) => index}
            />
        </View>
    }

    const renderItem = ({ item }) => {
        if (isFirst) {
            if (item === 0) {
                return itemPopular()
            } else if (item === 1) {
                return itemCate()
            }
            return <LoadingView />
        } else {
            if (item === 0) {
                return itemPopular()
            } else if (item === 1) {
                return itemCate()
            } else if (item === 2) {
                if (list.length == 0) {
                    return itemNoData();
                }
                return (hashNextPage && <LoadingView />);
            } else {
                return <ItemQuizView
                    item={item}
                    onClick={onQuizDetail}
                />
            }

        }

    }

    const renderCateItem = ({ item }) => (
        <ItemCateView
            item={item}
            current={currentCate}
            onClick={onClickCate}
        />
    );

    const getItem = (data, index) => {
        if (index < 2 || data.length === 0) {
            return index;
        } else if (index < (data.length + 2)) {
            return data[index - 2];
        } else {
            return 2;
        }
    }

    const getItemCount = (data) => {
        if (data.length == 0) {
            return 3;
        }
        return data.length + 2;
    }

    return (
        <View style={styles.container}>
            <StatusBar barStyle="dark-content" />
            <VirtualizedList
                data={list}
                renderItem={renderItem}
                keyExtractor={(item, index) => `content${index}`}
                getItemCount={getItemCount}
                getItem={getItem}
                keyboardShouldPersistTaps='handled'
                onEndReachedThreshold={400}
                onScroll={({ nativeEvent }) => {
                    if (isCloseToBottom(nativeEvent)) {
                        scrollToEnd();
                    }
                }}

                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={onRefresh}
                    />}
            />

            <AppHeader
                style={styles.header}
                search
                onSubmitEditing={onSubmitEditing}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.bg,
    },
    header: {
        marginHorizontal: 10,
        position: 'absolute',
        elevation: 5,
        width: '100%',
        left: 0,
        right: 0,
        top: 0,
    },
    cate: {
        marginTop: 0
    },
    tvNoData: {
        textAlign: 'center'
    },
    notData: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        height: 200
    },


});
export default QuizScreen;