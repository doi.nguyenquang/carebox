import React from 'react';
import {
  Text,
  Image,
  StyleSheet,
} from 'react-native';
import { COLORS } from '../../../carebox/utils/colors';
import AppButton from '../../../carebox/components/app-button';
import { IconNext } from '../../../carebox/assets/icons';
import { setFontFamily } from '../../../carebox/utils/functions';
import { fontPixel } from '../../../carebox/utils/sizes';

const ItemQuestionView = ({ item, onClick }) => {
  const model = item;
  return (
    <AppButton style={styles.item} onPress={() => onClick(item)}>
      <Text style={styles.contentStyles}>{model.categoryTitle}</Text>
      <Image
        source={IconNext}
        style={styles.icon}
      />
    </AppButton>
  );
};

const styles = StyleSheet.create({
  title: {
    color: COLORS.black,
    fontWeight: 'bold',
    marginVertical: 10,
  },
  icon: {
    width: 18,
    height: 18,
  },
  item: {
    paddingVertical: 15,
    paddingHorizontal: 15,
    backgroundColor: COLORS.white,
    marginVertical: 8,
    marginHorizontal: 10,
    elevation: 1,
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  contentStyles: {
    fontSize: fontPixel(14),
    fontFamily: setFontFamily('GilroyMedium')
  }
});
export default ItemQuestionView;