import React from 'react';
import {
  StyleSheet,
  View,
  StatusBar,
  VirtualizedList,
  SafeAreaView
} from 'react-native';
import AppHeader from '../../../carebox/components/app-header'
import LoadingView from '../../../carebox/components/loading-view'
import AppText from '../../../carebox/components/app-text'
import { COLORS } from '../../../carebox/utils/colors';
import { NavigationService } from '../../../carebox/navigation/NavigationService';
import { ROUTER_NAME } from '../../../carebox/navigation/RouterName';
import ItemQuestionView from './item_question_view'
import QuestionNotifier from './question_notifier'

const QuestionScreen = ({ route }) => {

  const {
    list,
    isFirst,
  } = QuestionNotifier();

  const onDetail = (value) => {
    NavigationService.push(ROUTER_NAME.DetailQuestionScreen, { model: value });
  }

  const itemNoData = () => {
    return <View style={styles.notData}>
      <AppText
        title='Không có dữ liệu'
        style={styles.tvNoData}
      />
    </View>
  }

  const renderItem = ({ item, }) => {
    if (isFirst) {
      return <LoadingView />
    } else {
      if (item === 2) {
        if (list.length == 0) {
          return itemNoData();
        }
        return <View />;
      } else {
        return <ItemQuestionView
          item={item}
          onClick={onDetail}
        />
      }
    }
  }

  const getItem = (data, index) => {
    if (index < (data.length)) {
      return data[index];
    } else {
      return 2;
    }
  }

  const getItemCount = (data) => {
    if (data.length == 0) {
      return 1;
    }
    return data.length;
  }

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <AppHeader
        title={'Câu hỏi thường gặp'}
        style={styles.header}
      />
      <VirtualizedList
        data={list}
        renderItem={renderItem}
        keyExtractor={(item, index) => `content${index}`}
        getItemCount={getItemCount}
        getItem={getItem}
        keyboardShouldPersistTaps='handled'
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.bg,
  },
  header: {
    paddingHorizontal: 10,
    backgroundColor: COLORS.white,
  },
  tvNoData: {
    textAlign: 'center'
  },
  notData: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    height: 200
  },
});
export default QuestionScreen;