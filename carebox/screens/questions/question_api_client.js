import * as api from '../../../carebox/services/Request'
import ApiConfig from '../../../carebox/services/api_config'
import { FrequentlyQuestionModelList } from './question_model'

export default class QuestionApiClient {

  constructor() {

  }

  async getQuestion() {
    const res = await api.methodGet({ api: ApiConfig.question })
    if (res['success']) {
      return FrequentlyQuestionModelList.fromJson(res['data']).list;
    }
    return [];
  }




}
