class QuestionModel {
  questionId;
  categoryId;
  questionContent;
  questionAnswer;
  isMore = false;

  constructor({
    questionId,
    categoryId,
    questionContent,
    questionAnswer,
  }) {
    this.questionId = questionId;
    this.categoryId = categoryId;
    this.questionContent = questionContent;
    this.questionAnswer = questionAnswer;

  }

  static fromJson(json) {
    return new QuestionModel(
      {
        questionId: json['question_id'],
        categoryId: json['category_id'],
        questionContent: json['question_content'],
        questionAnswer: json['question_answer'],
      }
    );
  }
}

class QuestionModelList {
  list;

  constructor({ list }) {
    this.list = list;
  }

  static fromJson = (datas) => {
    var list = [];
    if (datas) {
      list = datas.map((f) => new QuestionModel.fromJson(f));
    }
    return new QuestionModelList({ list });
  }
}



class FrequentlyQuestionModel {
  categoryId;
  categoryTitle;
  categoryType;
  questions;

  constructor({
    categoryId,
    categoryTitle,
    categoryType,
    questions,
  }) {
    this.categoryId = categoryId;
    this.categoryTitle = categoryTitle;
    this.categoryType = categoryType;
    this.questions = questions;
  }

  static fromJson(json) {
    return new FrequentlyQuestionModel(
      {
        categoryId: json['category_id'],
        categoryTitle: json['category_title'],
        categoryType: json['category_type'],
        questions: QuestionModelList.fromJson(json['questions']).list,
      }
    );
  }
}

class FrequentlyQuestionModelList {
  list;

  constructor({ list }) {
    this.list = list;
  }

  static fromJson = (datas) => {
    var list = [];
    if (datas) {
      list = datas.map((f) => new FrequentlyQuestionModel.fromJson(f));
    }
    return new FrequentlyQuestionModelList({ list });
  }
}

export { QuestionModel, QuestionModelList, FrequentlyQuestionModel, FrequentlyQuestionModelList }