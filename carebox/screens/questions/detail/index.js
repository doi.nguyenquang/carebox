import React from 'react';
import {
  StyleSheet,
  View,
  StatusBar,
  FlatList,
  SafeAreaView
} from 'react-native';
import AppHeader from '../../../../carebox/components/app-header'
import AppText from '../../../../carebox/components/app-text'
import { COLORS } from '../../../../carebox/utils/colors';
import { NavigationService } from '../../../../carebox/navigation/NavigationService';
import { ROUTER_NAME } from '../../../../carebox/navigation/RouterName';
import ItemDetailQuestionView from './item_detail_view'

const DetailQuestionScreen = ({ route }) => {

  const model = route.params.model;

  const itemNoData = () => {
    return <View style={styles.notData}>
      <AppText
        title='Không có dữ liệu'
        style={styles.tvNoData}
      />
    </View>
  }

  const renderItem = ({ item, }) => {
    return <ItemDetailQuestionView item={item}/>
  }

  const onCreate = () => {
    NavigationService.push(ROUTER_NAME.CreatePostScreen);
  }

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <AppHeader
        title={model.categoryTitle}
        style={styles.header}
      />
      {model.questions.length == 0 && itemNoData()}
      <FlatList
        data={model.questions}
        renderItem={renderItem}
        keyExtractor={(item, index) => `${index}`}
      />
    </SafeAreaView>

  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.bg,
  },
  header: {
    paddingHorizontal: 10,
    backgroundColor: COLORS.white,
  },
  tvNoData: {
    textAlign: 'center'
  },
  notData: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    height: 200
  },
});
export default DetailQuestionScreen;