import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  useWindowDimensions,
} from 'react-native';
import { COLORS } from '../../../../carebox/utils/colors';
import RenderHtml from 'react-native-render-html';

const ItemDetailQuestionView = ({ item, onClick }) => {
  const { width } = useWindowDimensions();
  const model = item;
  return (
    <View style={styles.item}>
      <Text style={styles.title}>Câu hỏi:</Text>
      {/* <Text style={styles.content}>{model.questionContent}</Text> */}
      <RenderHtml
        contentWidth={width}
        source={{ html: model.questionContent }}
      />
      <Text style={styles.title}>Trả lời:</Text>
      {/* <Text style={styles.content}>{model.questionContent}</Text> */}
      <RenderHtml
        contentWidth={width}
        source={{ html: model.questionAnswer }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    color: COLORS.black,
    fontWeight: 'bold',
    marginVertical: 10,
  },
  icon: {
    width: 18,
    height: 18,
  },
  item: {
    paddingVertical: 15,
    paddingHorizontal: 15,
    marginVertical: 8,
    backgroundColor: COLORS.white,
  },
});
export default ItemDetailQuestionView;