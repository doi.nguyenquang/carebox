import QuestionApiClient from './question_api_client'
import { useState, useEffect } from 'react';

const QuestionNotifier = () => {
  const _apiClient = new QuestionApiClient();

  const [list, setList] = useState([]);
  const [isFirst, setIsFirst] = useState(true);
  
  useEffect(() => {
    loadData();
  }, [])

  const loadData = async () => {
    const datas = await _apiClient.getQuestion();
    setIsFirst(false);
    setList(datas);
  }


  return {
    list,
    isFirst,
    loadData,
  }


}
export default QuestionNotifier;