import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Button,
    Image,
    FlatList,
} from 'react-native';
import { AppImage } from '../../../carebox/components/app-image'
import { COLORS } from '../../../carebox/utils/colors';
import { NewFavoriteModel } from './favorite_model'
import AppButton from '../../../carebox/components/app-button'
import { NewsModel } from '../../../carebox/models/news_model'
import { IconTag, IconArchive } from '../../../carebox/assets/icons'
import NewsFavoriteHelper from '../../../carebox/helper/news_favorite_helper';

const ItemFavoriteView = ({ item }) => {
    const value: NewFavoriteModel = item;
    let model: NewsModel;
    if (item != null) {
        model = value.news;
    }
    const {_onFavorite, onDetail, favorite  } = NewsFavoriteHelper(model);

    return (
        <AppButton
            onPress={() => onDetail(model)}
        >
            <View style={styles.item}>
                <AppImage
                    uri={model.newsImages}
                    style={styles.image}
                />
                <Text style={styles.title}>{model.newsTitle}</Text>
                {/* <AppPeriodView
                    value={model.courseListCount}
                /> */}
                {/* <AppCateView
                    value={model.category}
                /> */}

                <AppButton onPress={_onFavorite} style={styles.icon}>
                    <Image
                        source={favorite ? IconArchive : IconTag}
                        style={styles.ficon}
                    />
                </AppButton>

            </View>
        </AppButton>
    );
};


const styles = StyleSheet.create({
    title: {
        color: COLORS.black,
        fontWeight: 'bold',
        marginVertical: 10,
    },
    image: {
        width: 'auto',
        height: 200,
        borderRadius: 15
    },
    icon: {
       
        position: 'absolute',
        top: 6,
        right: 10,
    },
    ficon:{
        width: 24,
        height: 24,
    },
    item: {
        paddingTop: 15,
        paddingHorizontal: 15,
        backgroundColor: '#fff',
        marginVertical: 8,
        marginHorizontal: 10,
        elevation: 1,
        borderRadius: 10,
    },

});
export default ItemFavoriteView;