import { NewsModel } from '../../../carebox/models/news_model'

class NewFavoriteModel {
  newsFavoritesId;
  newsId;
  userId;
  createdAt;
  updatedAt;
  news;

  constructor({
    newsFavoritesId,
    newsId,
    userId,
    createdAt,
    updatedAt,
    news,
  }) {
    this.newsFavoritesId = newsFavoritesId;
    this.newsId = newsId;
    this.userId = userId;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.news = news;
  }

  static fromJson(json) {
    var model: NewsModel;
    if(json['news']){
      model = NewsModel.fromJson(json['news']);
      model.favorited = 1;
    }
    return new NewFavoriteModel(
      {
        newsFavoritesId: json['news_favorites_id'],
        newsId: json['news_id'],
        userId: json['user_id'],
        createdAt: json['created_at'],
        updatedAt: json['updated_at'],
        news: model,
      }
    );
  }
}


class NewFavoriteModelList {
  list;

  constructor({ list }) {
    this.list = list;
  }

  static fromJson = (datas) => {
    var list = [];
    if (datas) {
      list = datas.map((f) => new NewFavoriteModel.fromJson(f));
    }
    return new NewFavoriteModelList({ list });
  }
}

export { NewFavoriteModel, NewFavoriteModelList }