import * as api from '../../../carebox/services/Request'
import ApiConfig from '../../../carebox/services/api_config'
import { NewFavoriteModelList } from './favorite_model'
import { NewsModel } from '../../../carebox/models/news_model'

export default class FavoriteApiClient {
  hashNextPage = true;
  page = 0;
  limit = 40;
  constructor() {

  }

  async favorites({ isFirst, search }) {
    if (isFirst) {
      this.page = 0;
      this.hashNextPage = true;
    }
    if (!this.hashNextPage) return [];
    const query = {
      'offset': this.page,
      'limit': this.limit
    };

    if (search) {
      query['tx_search'] = search;
    }
    this.page = this.page + this.limit;
    const res = await api.methodGet({ api: ApiConfig.bookMarket, queries: query })
    if (res['success']) {
      const list = NewFavoriteModelList.fromJson(res['data']['values']).list;
      if (list.length < this.limit) {
        this.hashNextPage = false;
      }
      return list;
    }
    return [];
  }

  async favorite(model: NewsModel) {
    const body = { 'news_id': model.newsId };
    const res = await api.methodPost({ api: ApiConfig.newsFavorite, body: body });
    if (model.isFavorite()) {
      model.favorited = 0;
    } else {
      model.favorited = 1;
    }
    if (res['success']) {
      return res['data'];
    }
    return null;
  }




}
