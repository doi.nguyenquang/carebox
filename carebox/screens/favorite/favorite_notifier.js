import FavoriteApiClient from './favorite_api_client'
import { useState, useEffect } from 'react';

const FavoriteNotifier = () => {
  const _apiClient = new FavoriteApiClient();

  const [list, setList] = useState([]);
  const [hashNextPage, setHashNextPage] = useState(true);
  const [isFirst, setIsFirst] = useState(true);
  let txSearch = null;

  
  useEffect(() => {
    loadData();
  }, [])

  const loadData = async () => {
    const datas = await _apiClient.favorites({ isFirst: true, search: txSearch, });
    setIsFirst(false);
    setHashNextPage(_apiClient.hashNextPage);
    setList(datas);

  }

  const loadMore = async () => {
    if (hashNextPage) {
      const _list = await _apiClient.favorites({ isFirst: false, search: txSearch, });
      setHashNextPage(_apiClient.hashNextPage);
      if (_list.length > 0) {
        const datas = [list, ..._list];
        setList(datas);
      }
    }


  }

  const onSubmitEditing = async (value) => {
    txSearch = value;
    setList([]);
    setIsFirst(true);
    await loadData({ isFirst: true })
  }


  const [refreshing, setRefreshing] = useState(false);
  const onRefresh = async () => {
    setRefreshing(true);
    await loadData();
    setRefreshing(false);
  }


  return {
    onRefresh,
    refreshing,
    list,
    hashNextPage,
    isFirst,
    onSubmitEditing,
    loadData,
    loadMore,
  }


}
export default FavoriteNotifier;