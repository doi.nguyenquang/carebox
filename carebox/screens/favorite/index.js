import React from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  Image,
  VirtualizedList,
  FlatList,
  SafeAreaView,
  RefreshControl,
} from 'react-native';
import AppHeader from '../../../carebox/components/app-header'
import LoadingView from '../../../carebox/components/loading-view'
import AppText from '../../../carebox/components/app-text'
import { COLORS } from '../../../carebox/utils/colors';
import { setFontFamily } from '../../../carebox/utils/functions';
import ItemFavoriteView from './item_favorite_view'
import FavoriteNotifier from './favorite_notifier'

const FavoriteScreen = ({ route }) => {

  const {
    list,
    hashNextPage,
    isFirst,
    onSubmitEditing,
    loadData,
    loadMore,
    onRefresh,
    refreshing,
  } = FavoriteNotifier();

  const scrollToEnd = async () => {
    console.log('scrollToEnd');
    if (hashNextPage) {
      loadMore();
    }
  }

  const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = 20;
    return layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom;
  };

  const itemNoData = () => {
    return <View style={styles.notData}>
      <AppText
        title='Không có dữ liệu'
        style={styles.tvNoData}
      />
    </View>
  }

  const renderItem = ({ item }) => {
    if (isFirst) {
      return <LoadingView />
    } else {
      if (item === 2) {
        if (list.length == 0) {
          return itemNoData();
        }
        return (hashNextPage && <LoadingView />);
      } else {
        return <ItemFavoriteView
          item={item}
        />
      }

    }

  }

  const getItem = (data, index) => {
    if (index < (data.length)) {
      return data[index];
    } else {
      return 2;
    }
  }

  const getItemCount = (data) => {
    if (data.length == 0) {
      return 1;
    }
    return data.length + 1;
  }

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <AppHeader
        style={styles.header}
        search
        onSubmitEditing={onSubmitEditing}
      />
      <VirtualizedList
        data={list}
        renderItem={renderItem}
        keyExtractor={(item, index) => `content${index}`}
        getItemCount={getItemCount}
        getItem={getItem}
        keyboardShouldPersistTaps='handled'
        onEndReachedThreshold={400}
        onScroll={({ nativeEvent }) => {
          if (isCloseToBottom(nativeEvent)) {
            scrollToEnd();
          }
        }}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
          />}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.bg,
  },
  header: {
    marginHorizontal: 10,
    elevation: 5,
  },
  cate: {
    marginTop: 0
  },
  tvNoData: {
    textAlign: 'center',
    fontFamily: setFontFamily('GilroyMedium')
  },
  notData: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    height: 200
  },


});
export default FavoriteScreen;