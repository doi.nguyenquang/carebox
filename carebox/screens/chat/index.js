import ChatNotifier from './chat_notifier';
import React from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import ItemMessage from './components/item_message';
import styles from './styles';
import { GiftedChat } from 'react-native-gifted-chat';
import FormInput from './components/form_input';
import { Platform } from 'react-native';
import { isIphoneX } from 'react-native-iphone-x-helper';
import AppHeader from '../../../carebox/components/app-header'

const ChatScreen = () => {
  const {
    listMessages,
    onSendMsg,
  } = ChatNotifier();

  const renderItem = messageObject => {
    return (
      <ItemMessage key={messageObject.currentMessage.createdAt.toString()} messageObject={messageObject} />
    );
  };
  const renderInputToolbar = () => {
    return (
      <FormInput
        addListMessages={onSendMsg}
        listMessages={listMessages}
      />
    );
  };
  return (
    <SafeAreaView style={styles.container}>
      <AppHeader
        title={'CareBOX Mess'}
        style={styles.header}
      />
      <GiftedChat
        renderMessage={renderItem}
        wrapInSafeArea={true}
        isLoadingEarlier={true}
        messages={listMessages}
        style={styles.body}
        keyboardShouldPersistTaps={'handled'}
        placeholder={'Soạn tin nhắn...'}
        user={{ _id: 19 }}
        renderInputToolbar={renderInputToolbar}
        multiline={false}
        minInputToolbarHeight={isIphoneX() ? 0 : Platform.OS === 'android' ? 54 : 48}
      />
    </SafeAreaView>
  );
};

export default ChatScreen;
