class ChatModel {
  chatId;
  userId;
  message;
  audio;
  isAdmin;
  createdAt;
  updatedAt;
  isPlaying = false;
  isSending = false;
  timeTT = Date.now();

  //   bool get admin => isAdmin == '0';

  //   DateFormat get f => DateFormat('HH:mm, dd/MM/yyyy');
  //   DateFormat get fUtc => DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

  //   DateTime get cTime {
  //   return cTime1 ?? DateTime.now().toUtc().toLocal();
  // }

  //   String get createdAt {
  //   return createdAt1 ?? f.format(cTime);
  // }

  constructor({
    chatId,
    cTime1,
    userId,
    message,
    audio,
    isAdmin,
    createdAt,
    updatedAt,
    isSending = false,
  }) {
    this.chatId = chatId;
    this.cTime1 = cTime1;
    this.userId = userId;
    this.message = message;
    this.audio = audio;
    this.isAdmin = isAdmin;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.isSending = false;
  }

  static fromJson(json) {
    let user = json['user'];
    let idUser = json['user_id']
    if(user){
      idUser = user['user_id'];
    }
    return new ChatModel({
      chatId: json['chat_id'],
      userId: idUser,
      message: json['message'],
      audio: json['audio'],
      isAdmin: json['is_admin'],
      createdAt: json['created_at'],
      updatedAt: json['updated_at'],
    })
  }

  messenger() {
    return {
      _id: this.chatId,
      text: this.message,
      audio: this.audio,
      createdAt: Date.parse(this.createdAt),
      user: {
        _id: this.isAdmin === '0' ? 0 : this.userId,
      },
    };
  }
}

class ChatModelList {
  list;

  constructor({ list }) {
    this.list = list;
  }

  static fromJson = (datas) => {
    let list = [];
    if (datas) {
      list = datas.map((f) => ChatModel.fromJson(f).messenger());
    }
    return new ChatModelList({ list });
  }
}

export { ChatModel, ChatModelList }
