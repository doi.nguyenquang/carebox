import React, { useCallback, useState } from 'react';
import { Platform } from 'react-native';
import AudioRecorderPlayer, {
    AudioEncoderAndroidType,
    AudioSet,
    AudioSourceAndroidType, AVEncoderAudioQualityIOSType, AVEncodingOption,
    OutputFormatAndroidType,
} from 'react-native-audio-recorder-player';
import styles from '../styles';
import RNFetchBlob from 'rn-fetch-blob';

const audioRecorderPlayer = new AudioRecorderPlayer();
audioRecorderPlayer.setSubscriptionDuration(0.1);

const FormInputNotifier = ({ addListMessages }) => {
    const [isFocused, setIsFocused] = useState(false);
    const [isRecord, setRecord] = useState(false);
    const [recordSecs, setRecordSecs] = useState(0);
    const [currentPath, setPath] = useState(null);
    const [messages, setMessages] = useState('');

    const onChangeText = text => {
        setMessages(text);
    };

    const handleStyleFocusedView = useCallback(() => {
        return isFocused
          ? styles.viewTextInputActive
          : styles.viewTextInputNotActive;
    }, [isFocused]);

    const onBlur = () => {
        if (isFocused) {
            setIsFocused(false);
        }
    };
    const onFocus = () => {
        if (!isFocused) {
            setIsFocused(true);
        }
    };

    const handleAddMessages = () => {
        if (messages?.length) {
            addListMessages(messages, null);
            setMessages('')
        } else {
            setRecord(true);
            _startRecording();
        }
    };

    const removeRecord = async() => {
        setPath(null)
        setRecord(false);
        await _stopRecording();
    }

    const sendRecord = async() => {
        await removeRecord();
        addListMessages('.', currentPath);
    }

    const _startRecording = async () => {
        const path = Platform.select({
            ios: 'carebox.m4a',
            android: `${RNFetchBlob.fs.dirs.CacheDir}/carebox.mp3`,
        });
        const audioSet: AudioSet = {
            AudioEncoderAndroid: AudioEncoderAndroidType.AAC,
            AudioSourceAndroid: AudioSourceAndroidType.MIC,
            AVEncoderAudioQualityKeyIOS: AVEncoderAudioQualityIOSType.high,
            AVNumberOfChannelsKeyIOS: 2,
            AVFormatIDKeyIOS: AVEncodingOption.aac,
            OutputFormatAndroid: OutputFormatAndroidType.AAC_ADTS,
        };
        const result = await audioRecorderPlayer.startRecorder(path, audioSet);
        audioRecorderPlayer.addRecordBackListener((e) => {
            setRecordSecs(e.currentPosition);
        });
        setPath(result);
    }

    const _stopRecording = async () => {
        const result = await audioRecorderPlayer.stopRecorder();
        audioRecorderPlayer.removeRecordBackListener();
        setRecordSecs(0);
        console.log('_stopRecording: ', result);
    }

    return {
        handleAddMessages,
        removeRecord,
        onBlur,
        onFocus,
        isRecord,
        isFocused,
        handleStyleFocusedView,
        sendRecord,
        recordSecs,
        onChangeText,
        messages,
    };
};

export default FormInputNotifier;
