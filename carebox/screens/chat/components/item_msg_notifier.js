import React, { useCallback, useState, useEffect } from 'react';
import Sound from 'react-native-sound'

const ItemMsgNotifier = (messageObject) => {
    const { currentMessage, previousMessage } = messageObject;
    const [isPLay, setPlay] = useState(false);
    const [track, setTrack] = useState(null);
    const [duration, setDuration] = useState(0)
    const [currentTime, setCurrentTime] = useState(0)
    const [timer, setTimer] = useState(null)

    useEffect(() => {
        let isMounted = true;
        const initSound = () => {
            if (currentMessage?.audio) {
                const tra = new Sound(currentMessage?.audio, null, (e) => {
                    if (e) {
                        console.log('error loading track:', e)
                    } else {
                        if (isMounted) {
                            setTrack(tra)
                            setDuration(tra.getDuration())
                        }

                    }
                });
            }
        }
        initSound();
        return () => { isMounted = false };
    }, [])


    useEffect(() => {
        const interval = setInterval(() => {

            if (track && duration <= 0) {
                setDuration(track.getDuration())
            }
            if (track && isPLay) {
                console.log('ItemMsgNotifier', currentTime)
                track.getCurrentTime((seconds: number, isPlaying: boolean) => {
                    if (isPlaying) {
                        setCurrentTime(seconds)
                    }

                });
            }
        }, 1000)
        return () => clearInterval(interval)
    })

    const playTrack = () => {
        if (isPLay) {
            if (track) {
                track.pause((end) => {
                    setPlay(false);
                });
                // _stopTimer();
            }

        } else {
            if (track) {
                track.play((end) => {
                    setPlay(false);
                });
                setPlay(true);
                // _startTimer();
            }

        }

    }

    return {
        duration,
        currentTime,
        isPLay,
        playTrack
    }

}

export default ItemMsgNotifier;