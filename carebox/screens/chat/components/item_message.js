import AppText from '../../../../carebox/components/app-text';
import { formatDate, HHmmDDMMYYYY, isSameDate } from '../../../../carebox/utils/times';
import isEqual from 'react-fast-compare';
import React, { useCallback, useState } from 'react';
import { View } from 'react-native';
import styles from '../styles';
import AppButton from '../../../../carebox/components/app-button';
import { IconImage } from '../../../../carebox/components/app-image';
import {
  IconPlay,
  IconPause,
} from '../../../../carebox/assets/icons'
import Sound from 'react-native-sound'
import ItemMsgNotifier from './item_msg_notifier'
import {getMMSSFromMillis} from '../../../../carebox/utils/times'

const ItemMessage = ({ messageObject }) => {
  const { currentMessage, previousMessage, key } = messageObject;
  const {
    duration,
    currentTime,
    isPLay,
    playTrack
  } = ItemMsgNotifier(messageObject);

  const handleStyle = () => {
    return currentMessage?.user?._id
      ? styles.viewMessage
      : styles.viewMessageLeft;
  };

  const tvStyle = () => {
    return currentMessage?.user?._id
      ? styles.textMessage
      : styles.textMessageRight;
  };

  const handleDateTime = () => {
    return !isSameDate(currentMessage?.createdAt, previousMessage?.createdAt) &&
      previousMessage?._id &&
      previousMessage?.user?._id !== 0
      ? null
      : formatDate(currentMessage?.createdAt, HHmmDDMMYYYY);
  };


  return (
    <View key={key} style={styles.viewParentMessage}>
      <AppText style={styles.textTime} title={handleDateTime()} />
      <View style={styles.containerMessage}>
        {currentMessage?.user?._id !== 0 && <View style={styles.box} />}
        <View style={handleStyle()}>
          {!currentMessage?.audio && <AppText
            title={currentMessage?.text}
            style={tvStyle()}
            numberOfLines={15}
          />}

          {currentMessage?.audio && <View>
            <AppButton
              onPress={playTrack}
              style={styles.vAudio}>
              <IconImage source={isPLay ? IconPause : IconPlay} />
              <AppText
                title={`${getMMSSFromMillis(currentTime*1000)}/${getMMSSFromMillis(duration*1000)}`}
                style={[tvStyle(), styles.txAudio]}
                numberOfLines={15}
              />
            </AppButton>
          </View>}

        </View>
      </View>
    </View>
  );
};

export default React.memo(ItemMessage, (prevProps, nextProps) =>
  isEqual(prevProps.currentMessage, nextProps.currentMessage),
);
