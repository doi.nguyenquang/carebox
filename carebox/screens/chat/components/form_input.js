import AppButton from '../../../../carebox/components/app-button';
import React, { useCallback, useState } from 'react';
import { TextInput, View, Text, } from 'react-native';
import { IconImage } from '../../../../carebox/components/app-image';
import {
  IconSend,
  IconVoice,
  IconDel,
} from '../../../../carebox/assets/icons'
import styles from '../styles';
import FormInputNotifier from './form_input_notifier'
import { getMMSSFromMillis } from '../../../../carebox/utils/times'

const FormInput = ({ addListMessages, listMessages }) => {
  const {
    handleAddMessages,
    removeRecord,
    onBlur,
    onFocus,
    isRecord,
    isFocused,
    handleStyleFocusedView,
    sendRecord,
    recordSecs,
    messages,
    onChangeText,

  } = FormInputNotifier({ addListMessages })


  return (
    <View style={styles.vInput}>
      {!isRecord && <View style={[styles.viewTextInput, handleStyleFocusedView()]}>
        <TextInput
          onBlur={onBlur}
          onFocus={onFocus}
          style={styles.textInput}
          placeholder="Soạn tin nhắn..."
          value={messages}
          onChangeText={onChangeText}
        />
        <AppButton onPress={handleAddMessages}>
          <IconImage
            source={
              messages?.length
                ? IconSend
                : IconVoice
            }
          />
        </AppButton>
      </View>}

      {isRecord && <View style={[styles.viewTextInput, styles.vRecord]}>
        <AppButton onPress={removeRecord}>
          <IconImage source={IconDel} />
        </AppButton>

        <View style={styles.vRecording}>
          <Text style={styles.tvRecord}>Đang ghi âm {getMMSSFromMillis(recordSecs)} </Text>
          <AppButton onPress={sendRecord}>
            <IconImage source={IconSend} />
          </AppButton>
        </View>
      </View>}
    </View>
  );
};

export default FormInput;
