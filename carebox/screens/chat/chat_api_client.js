import * as api from '../../../carebox/services/Request'
import ApiConfig from '../../../carebox/services/api_config'
import { ChatModelList, ChatModel } from './chat_model'
import mime from 'mime';

class ChatApiClient {
    hashNextPage = true;
    page = 0;
    limit = 40;

    async sendMessage({ message, filePath }) {
        let filename;
        let name;
        let file;

        const body = new FormData();
        if (message) {
            body.append('message', message.trim());
        }
        if (filePath) {
            filename = filePath.replace(/^.*[\\\/]/, '');
            name = filename.split('.').slice(0, -1).join('.')
            file = filePath.replace('////', '///');
            body.append('file', {
                name: name,
                uri: file,
                type: mime.getType(file),
            });
        }

        const res = await api.methodPostFormData({ api: ApiConfig.chat, body });
        if (res['success']) {
            return ChatModel.fromJson(res['data']).messenger();
        }
        return null;
    }


    async getMsg({ isFirst }) {
        if (isFirst) {
            this.page = 0;
            this.hashNextPage = true;
        }
        if (!this.hashNextPage) return [];
        const query = {
            'offset': this.page,
            'limit': this.limit
        };

        this.page = this.page + this.limit;
        const res = await api.methodGet({ api: ApiConfig.chat, queries: query })
        if (res['success']) {
            const list = ChatModelList.fromJson(res['data']['values']).list;
            if (list.length < this.limit) {
                this.hashNextPage = false;
            }
            return list;
        }
        return [];
    }
}

export default ChatApiClient;
