import {COLORS} from '../../../carebox/utils/colors';
import {heightPixel, widthPixel} from '../../../carebox/utils/sizes';
import {EnumFontFamily, setFontFamily} from '../../../carebox/utils/functions';

const {StyleSheet} = require('react-native');

const styles = StyleSheet.create({
  vInput: {
    backgroundColor: COLORS.white,
  },
  header: {
    paddingHorizontal: 10,
    elevation: 1,
    width: '100%',
    backgroundColor: COLORS.white,
  },
  viewTextInput: {
    borderRadius: 10,
    borderWidth: 1,
    paddingVertical: 8,
    paddingHorizontal: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: COLORS.white,
    marginHorizontal: 10,
    marginTop:3,
  },
  viewTextInputNotActive: {
    borderColor: COLORS.gray1,
  },
  viewTextInputActive: {
    borderColor: COLORS.green2,
  },
  textInput: {
    width: '90%',
    paddingVertical: 0,
  },
  viewMessage: {
    backgroundColor: COLORS.green,
    borderRadius: 10,
    marginRight: 10,
    paddingVertical: heightPixel(8),
    paddingHorizontal: widthPixel(16),
    alignItems: 'flex-start',
    maxWidth: '80%',
  },
  viewMessageLeft: {
    backgroundColor: COLORS.white,
    borderRadius: 10,
    paddingVertical: heightPixel(8),
    paddingHorizontal: widthPixel(16),
    maxWidth: '80%',
    borderWidth: 1,
    borderColor: COLORS.gray1,
    alignItems: 'flex-start',
  },
  textMessage: {
    color: COLORS.white,
    fontFamily: setFontFamily(EnumFontFamily.GilroyRegular),
    flexShrink: 3,
  },
  textMessageLeft: {
    color: COLORS.black303030,
    fontSize: 16,
    fontFamily: setFontFamily(EnumFontFamily.GilroyRegular),
    flexShrink: 3,
  },
  box: {
    width: '20%',
  },
  containerMessage: {
    flexDirection: 'row',
    marginTop: heightPixel(8),
    justifyContent: 'space-between',
  },
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: COLORS.bg,
  },
  body:{
    backgroundColor: COLORS.bg,
    flex: 1,
  },
  viewListMessages: {
    flex: 1,
    paddingHorizontal: widthPixel(16),
    // paddingBottom: 10,
  },
  footerComponentStyle: {
    marginBottom: heightPixel(16),
  },
  textTime: {
    color: COLORS.gray1,
    fontSize: 12,
    textAlign: 'center',
    fontFamily: setFontFamily(EnumFontFamily.GilroyMedium),
  },
  viewParentMessage: {
    marginBottom: heightPixel(8),
    marginHorizontal: 10,
  },
  vRecord:{
    borderWidth: 0,
    borderTopWidth: 1,
    width: '100%',
    borderColor: COLORS.gray1,
    marginHorizontal: 0,
  },
  vRecording: {
    borderRadius: 30,
    paddingVertical: 8,
    paddingHorizontal: 16,
    flexDirection: 'row',
    flex: 1,
    marginLeft: 10,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: COLORS.blue54BADE,
  },
  tvRecord: {
    color: COLORS.white,
    textAlign: 'center',
    flex: 1,
    fontFamily: setFontFamily(EnumFontFamily.GilroyMedium),
  },
  vAudio:{
    flexDirection: 'row',
    alignItems: 'center',
  },
  txAudio:{
    marginRight: 10,
    paddingTop: 2,
  }
});
export default styles;
