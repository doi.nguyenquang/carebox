import { useState, useEffect } from 'react';
import { GiftedChat } from 'react-native-gifted-chat';
import ChatApiClient from './chat_api_client';
import messaging from '@react-native-firebase/messaging';

const ChatNotifier = () => {
  const _apiClient = new ChatApiClient();
  const [listMessages, setListMessages] = useState([
    {
      _id: 0,
      text: 'Xin chào, CareBOX có thể giúp gì cho bạn?',
      createdAt: new Date(),
      // audio: "http://167.179.72.201:3680/public/files/41398244-28e2-47da-b3f2-5948ceca39a5.mp3",
      user: {
        _id: 0,
      },
    },
  ]);


  const _addListMessages = newMessage => {
    setListMessages(previousMessages =>
      GiftedChat.append(previousMessages, newMessage),
    );
  };


  useEffect(() => {
    const loadData = async () => {
      const value = await _apiClient.getMsg({ isFirst: true });
      setListMessages([...value, ...listMessages]);
    }
    loadData();
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      console.log('ForegroundMessage', remoteMessage);
      _listenMsg(remoteMessage);
    });
    return () => {
      console.log("componentWillUnmount");
      unsubscribe();
    }
  }, [])

  const _listenMsg = (message) => {
    const data = message['data'];
    const notiChat = data['type'] == 1000;
    if (notiChat) {
      _addListMessages({
        _id: data['chat_id'],
        text: data['message'],
        audio: data['audio'],
        createdAt: new Date(),
        user: { _id: 0 },
      });
    }
  }

  const onSendMsg = async (messages, audioPath) => {
    if (messages.length > 0 || audioPath != null) {
      const res = await _apiClient.sendMessage({ message: messages, filePath: audioPath });
      const newMessage = {
        _id: Math.random(),
        text: messages,
        createdAt: new Date(),
        user: { _id: 1 },
      }
      if (res && res.audio) {
        newMessage.audio = res.audio;
      }
      _addListMessages(newMessage);
    }
  }

  return {
    listMessages,
    onSendMsg,
  };
};

export default ChatNotifier;
