/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-fallthrough */
/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useState, useMemo, useCallback} from 'react';
import {
  View,
  Text,
  Image,
  StatusBar,
  ScrollView,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import HeaderBgImage from '../components/header-bg-image';
import {NavigationService} from '../../../../carebox/navigation/NavigationService';
import {COLORS} from '../../../../carebox/utils/colors';
import {fontPixel, SIZE} from '../../../../carebox/utils/sizes';
import {setFontFamily} from '../../../../carebox/utils/functions';
import {IconImage} from '../../../../carebox/components/app-image';
import {useDispatch, useSelector} from 'react-redux';
import {homeSelectors, moreSelector} from '../../../../carebox/redux/selectors';
import {IssueSelectView} from '../../../../carebox/pages/home/component/consult-formality/issue-select-view';
import {homeActions} from '../../../../carebox/pages/home/home-redux-saga/home-slice';
import {consultMethods} from '../../../../carebox/utils/constants';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment';
import {convertDayVi, showToastMessage} from '../../../../carebox/utils/utils';
import {
  IconBack,
  IconCalendar,
  IconConsultMes,
  IconConsultLive,
  IconConsultVideo,
  IconDefaultAvatar,
} from '../../../../carebox/assets/icons';
import {RadioAddressComponent} from '../../../../carebox/pages/home/component/consult-formality/radio-address';
import {ROUTER_NAME} from '../../../../carebox/navigation/RouterName';
import {formatDate, YYYYMMDD} from '../../../../carebox/utils/times';

const SetupConsultScreen = props => {
  const dispatch = useDispatch();
  const listMoreData = useSelector(moreSelector.listMoreData);
  const listIssue = useSelector(homeSelectors.listIssueSelect);
  const [date, setDate] = useState(null);
  const [visibleDateTimePicker, setVisibleDateTimePicker] = useState(false);
  const [currentConsultMethod, setCurrentConsultMethod] = useState('Mes247');
  const [listAddressSelect, setListAddressSelect] = useState([]);

  const onIssueItemOnClick = id => {
    const listIssueTemp = listIssue.map(item => {
      if (item.id === id) {
        return {...item, isSelected: !item.isSelected};
      } else {
        return {...item};
      }
    });
    dispatch(homeActions.updateListIssueSelect(listIssueTemp));
  };

  useEffect(() => {
    if (listMoreData) {
      const moreObject = listMoreData.find(item => item.more_id === 8);
      if (moreObject) {
        const listAddressParse = JSON.parse(moreObject?.more_content) || [];
        const listAddressRadio = listAddressParse.map((item, index) => {
          return {
            id: index + 1,
            title: item.title,
            subTitle: item.sub_title,
            isSelected: false,
          };
        });
        setListAddressSelect(listAddressRadio);
      }
    }
  }, [listMoreData]);

  const addressItemSelect = id => {
    const addressListcheck = listAddressSelect.map(item => {
      return {...item, isSelected: item.id === id};
    });
    setListAddressSelect(addressListcheck);
  };

  const onGoBack = () => {
    NavigationService.goBack();
  };

  const showDateTimePicker = () => {
    setVisibleDateTimePicker(true);
  };

  const hideDateTimePicker = () => {
    setVisibleDateTimePicker(false);
  };

  const handleDatePicked = day => {
    setDate(day);
    hideDateTimePicker();
  };

  const widthConsulFormalityItem = useMemo(() => {
    return (SIZE.SCREEN_WIDTH - 64) / consultMethods.length;
  }, [consultMethods]);

  const getReSourceItem = (item, key) => {
    switch (item) {
      case 'Mes247':
        return key === 'Icon' ? IconConsultMes : 'Nhắn tin 24/7';
      case 'VideoCall':
        return key === 'Icon' ? IconConsultVideo : 'Video call';
      case 'Live':
        return key === 'Icon' ? IconConsultLive : 'Trực tiếp';
      default:
        return null;
    }
  };

  const getBottomBoxBoder = item => {
    switch (item) {
      case 'Mes247':
        if (item === currentConsultMethod) {
          return {
            borderColor: COLORS.green,
          };
        }
      case 'VideoCall':
        if (item === currentConsultMethod) {
          return {
            borderColor: COLORS.main,
          };
        }
      case 'Live':
        if (item === currentConsultMethod) {
          return {
            borderColor: COLORS.redCE3737,
          };
        }
      default:
        return {
          borderColor: COLORS.greyE9E9E9,
        };
    }
  };

  const getTitleConsultFormalityItemStyle = item => {
    switch (item) {
      case 'Mes247':
        if (item === currentConsultMethod) {
          return {
            color: COLORS.green,
            fontWeight: 'bold',
          };
        }
      case 'VideoCall':
        if (item === currentConsultMethod) {
          return {
            color: COLORS.main,
            fontWeight: 'bold',
          };
        }
      case 'Live':
        if (item === currentConsultMethod) {
          return {
            color: COLORS.redCE3737,
            fontWeight: 'bold',
          };
        }
      default:
        return {};
    }
  };

  const getDateContent = useMemo(() => {
    if (date) {
      return (
        'Thời gian tư vấn: ' +
        convertDayVi(date) +
        ', ' +
        moment(date).format('l')
      );
    }
    return 'Chọn thời gian tư vấn phù hợp';
  }, [currentConsultMethod, date]);

  const getColorDate = useMemo(() => {
    if (date) {
      return {
        color: COLORS.main,
      };
    } else {
      return {
        color: COLORS.black303030,
      };
    }
  }, [currentConsultMethod, date]);

  const renderDateTimePickerView = () => {
    return (
      <TouchableOpacity
        style={styles.dateTimeView}
        onPress={showDateTimePicker}>
        <Text style={[styles.timeValueStyles, getColorDate]} numberOfLines={1}>
          {getDateContent}
        </Text>
        <IconImage source={IconCalendar} style={styles.icCalendarStyles} />
      </TouchableOpacity>
    );
  };

  const renderConsultFormalityContent = useCallback(() => {
    switch (currentConsultMethod) {
      case 'Mes247':
        return (
          <Text style={styles.contentMes247Style}>
            Nhận tư vấn khẩn cấp từ các chuyên gia hàng đầu. Nội dung tin nhắn
            sẽ tự động xoá sau khi thoát.
          </Text>
        );
      case 'VideoCall':
        return (
          <View>
            <Text style={styles.contentMes247Style}>
              Nhận tư vấn trực tiếp 1-1 thông qua video call. Hoàn toàn riêng tư
              và bảo mật.
            </Text>
            {renderDateTimePickerView()}
          </View>
        );
      case 'Live':
        return (
          <View>
            {renderDateTimePickerView()}
            <Text style={styles.addressTitleStyle}>Địa điểm tư vấn</Text>
            <RadioAddressComponent
              listAddress={listAddressSelect}
              addressItemSelect={addressItemSelect}
            />
          </View>
        );
      default:
        return null;
    }
  }, [currentConsultMethod, date, listAddressSelect]);

  const nextBtnOnClick = () => {
    switch (currentConsultMethod) {
      case 'Mes247':
        NavigationService.push(ROUTER_NAME.CHAT);
        break;
      case 'VideoCall':
        if (date !== null) {
          const bookDay = formatDate(date, YYYYMMDD);
          NavigationService.push(ROUTER_NAME.ConsultScheduleScreen, {
            bookDay,
            bookType: 0,
          });
        } else {
          showToastMessage('Bạn chưa chọn thời gian tư vấn');
        }
        break;
      case 'Live':
        const addressSelect = listAddressSelect.find(
          address => address.isSelected,
        );
        if (!date) {
          showToastMessage('Bạn chưa chọn thời gian tư vấn');
          return;
        }
        if (!addressSelect) {
          showToastMessage('Bạn chưa chọn địa điểm tư vấn');
          return;
        }
        const bookDay = formatDate(date, YYYYMMDD);
        NavigationService.push(ROUTER_NAME.ConsultScheduleScreen, {
          addressSelect,
          bookDay,
          bookType: 1,
        });
        break;
    }
  };

  return (
    <SafeAreaProvider style={styles.container}>
      <StatusBar translucent backgroundColor="transparent" />
      <HeaderBgImage headerHeight={80}>
        <View style={styles.headerContainer}>
          <TouchableOpacity onPress={onGoBack}>
            <IconImage source={IconBack} style={styles.iconHeaderSize} />
          </TouchableOpacity>
          <View style={styles.headerContentStyles}>
            <Text style={styles.titleStyles}>CareBox luôn bên bạn!</Text>
            <Text style={styles.sloganStyles}>Vì một Hưng Thịnh bền vững.</Text>
          </View>
          {/* <View style={styles.avatarContainer}>
            <Image style={styles.avatar} source={IconDefaultAvatar} />
          </View> */}
        </View>
      </HeaderBgImage>
      <DateTimePicker
        isVisible={visibleDateTimePicker}
        onConfirm={handleDatePicked}
        onCancel={hideDateTimePicker}
        date={date || new Date()}
        mode="date"
        minimumDate={new Date()}
      />
      <SafeAreaView style={styles.container}>
        <ScrollView
          style={styles.contentContainer}
          showsVerticalScrollIndicator={false}>
          <IssueSelectView
            listIssue={listIssue}
            onCheckBoxItemOnClick={onIssueItemOnClick}
          />
          <Text style={styles.titleStyleConsultMethod}>Hình thức tư vấn</Text>
          <View style={styles.consultFormalityView}>
            {consultMethods.map((item, index) => {
              return (
                <TouchableOpacity
                  key={index}
                  style={[
                    styles.consultFormalityContainer,
                    {
                      width: widthConsulFormalityItem,
                      marginRight: index === consultMethods.length - 1 ? 0 : 16,
                    },
                  ]}
                  onPress={() => setCurrentConsultMethod(item)}>
                  <Image
                    resizeMode={'contain'}
                    source={getReSourceItem(item, 'Icon')}
                    style={styles.consultFormalityIcon}
                  />
                  <View style={[styles.bottomBox, getBottomBoxBoder(item)]}>
                    <Text
                      style={[
                        styles.titleConsultFormalityItem,
                        getTitleConsultFormalityItemStyle(item),
                      ]}
                      numberOfLines={1}>
                      {getReSourceItem(item, 'Text')}
                    </Text>
                  </View>
                </TouchableOpacity>
              );
            })}
          </View>
          {renderConsultFormalityContent()}
        </ScrollView>
        <TouchableOpacity style={[styles.styleBtn]} onPress={nextBtnOnClick}>
          <Text style={[styles.buttonText]}>Tiếp tục</Text>
        </TouchableOpacity>
      </SafeAreaView>
    </SafeAreaProvider>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contentContainer: {
    paddingTop: 20,
    paddingHorizontal: 16,
  },
  headerContainer: {
    flex: 1,
    paddingHorizontal: 16,
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerContentStyles: {
    flex: 1,
    height: '100%',
    marginLeft: 20,
    justifyContent: 'center',
  },
  iconHeaderSize: {
    width: 40,
    height: 40,
  },
  avatarContainer: {
    height: 40,
    width: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    backgroundColor: 'gray',
  },
  avatar: {
    height: 40,
    width: 40,
    borderRadius: 20,
  },
  titleStyles: {
    fontSize: fontPixel(16),
    fontFamily: setFontFamily('GilroyXBold'),
    color: COLORS.black303030,
  },
  sloganStyles: {
    fontSize: fontPixel(16),
    fontFamily: setFontFamily('GilroyRegular'),
    color: COLORS.black303030,
  },
  titleStyleConsultMethod: {
    color: COLORS.black303030,
    fontWeight: 'bold',
    marginTop: 20,
    fontSize: fontPixel(16),
    fontFamily: setFontFamily('GilroyRegular'),
  },
  consultFormalityContainer: {
    height: fontPixel(100),
    alignItems: 'center',
  },
  consultFormalityIcon: {
    height: fontPixel(70),
    width: fontPixel(58),
    zIndex: 1,
  },
  bottomBox: {
    width: '100%',
    height: fontPixel(70),
    borderWidth: 1,
    bottom: 0,
    position: 'absolute',
    borderRadius: 16,
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingBottom: 8,
    backgroundColor: COLORS.white,
  },
  consultFormalityView: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 20,
  },
  titleConsultFormalityItem: {
    fontSize: fontPixel(14),
    fontFamily: setFontFamily('GilroyLight'),
  },
  styleBtn: {
    marginTop: 20,
    marginBottom: 20,
    height: fontPixel(50),
    marginHorizontal: 16,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 16,
    backgroundColor: COLORS.main,
  },
  buttonText: {
    fontSize: fontPixel(16),
    fontWeight: 'bold',
    color: COLORS.white,
  },
  contentMes247Style: {
    width: '100%',
    fontSize: fontPixel(14),
    marginTop: 16,
    fontFamily: setFontFamily('GilroyLight'),
  },
  addressTitleStyle: {
    color: COLORS.black303030,
    fontWeight: 'bold',
    marginTop: 20,
    fontSize: fontPixel(16),
    fontFamily: setFontFamily('GilroyMedium')
  },
  dateTimeView: {
    flexDirection: 'row',
    height: fontPixel(45),
    borderRadius: 16,
    backgroundColor: COLORS.white,
    marginTop: 20,
    borderColor: COLORS.main,
    borderWidth: 1,
    alignItems: 'center',
    paddingHorizontal: 16,
  },
  icCalendarStyles: {
    width: 24,
    height: 24,
  },
  timeValueStyles: {
    flex: 1,
    fontSize: fontPixel(14),
    fontFamily: setFontFamily('GilroyRegular'),
  },
});

export default SetupConsultScreen;
