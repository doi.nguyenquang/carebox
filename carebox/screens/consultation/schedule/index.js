import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  StatusBar,
  ScrollView,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import HeaderBgImage from '../components/header-bg-image';
import AppModal from '../../../../carebox/components/app-modal';
import { COLORS } from '../../../../carebox/utils/colors';
import { fontPixel, SIZE } from '../../../../carebox/utils/sizes';
import { IconBack, IconCareBox } from '../../../../carebox/assets/icons'
import { setFontFamily } from '../../../../carebox/utils/functions';
import { IconImage } from '../../../../carebox/components/app-image';
import { useDispatch, useSelector } from 'react-redux';
import { IssueSelectView } from '../../../../carebox/pages/home/component/consult-formality/issue-select-view';
import { NavigationService } from '../../../../carebox/navigation/NavigationService';
import { moreSelector, homeSelectors } from '../../../../carebox/redux/selectors';
import { homeActions } from '../../../../carebox/pages/home/home-redux-saga/home-slice';
import AppLoading from '../../../../carebox/components/app-loading';
import Toast from "react-native-root-toast";
import { ROUTER_NAME } from '../../../../carebox/navigation/RouterName';
import { useNavigation } from "@react-navigation/native"
import { showToastMessage } from "../../../../carebox/utils/utils";

let isClickBookingModal = false;

const ConsultScheduleScreen = (props) => {
  const { params } = props.route;
  const dispatch = useDispatch();
  const navigation = useNavigation()
  const listIssue = useSelector(homeSelectors.listIssueSelect)

  const listMoreData = useSelector(moreSelector.listMoreData)
  const isBookingLoading = useSelector(homeSelectors.isBookingLoading)
  const bookScheduleConsultSuccess = useSelector(homeSelectors.bookScheduleConsultSuccess)
  const [listFrameTime, setListFrameTime] = useState([])
  const [showConfirmModal, setShowConfirmModal] = useState(false)

  useEffect(() => {
    dispatch(homeActions.resetBookingData())
    return function cleanup() {
      dispatch(homeActions.resetBookingData())
    }
  }, [])

  useEffect(() => {
    if (listMoreData) {
      const moreObject = listMoreData.find(item => item.more_id === 5)
      if (moreObject) {
        const listFramTimeParse = JSON.parse(moreObject?.more_content) || []
        const listFramTimeTemp = listFramTimeParse?.map(item => {
          return { ...item, isSelected: false }
        })
        setListFrameTime(listFramTimeTemp)
      }
    }
  }, [listMoreData])

  useEffect(() => {
    if (bookScheduleConsultSuccess) {
      const listIssueTemp = listIssue.map(item => {
        return { ...item, isSelected: false }
      })
      dispatch(homeActions.updateListIssueSelect(listIssueTemp))
      showToastMessage("Đặt lịch tư vấn thành công!")
      NavigationService.goBack();
      // navigation.popToTop()
    }
  }, [bookScheduleConsultSuccess])

  const onIssueItemOnClick = (id) => {
    const listIssueTemp = listIssue.map(item => {
      if (item.id === id) {
        return { ...item, isSelected: !item.isSelected }
      } else {
        return { ...item }
      }
    })
    dispatch(homeActions.updateListIssueSelect(listIssueTemp))
  }

  const onGoBack = () => {
    NavigationService.goBack();
  }

  const getItemStyles = (item, index) => {
    return {
      marginRight: (index + 1) % 3 === 0 ? 0 : 8,
      backgroundColor: item.isSelected ? COLORS.orangeF58220 : COLORS.white,
      borderWidth: item.isSelected ? 0 : 1,
    }
  }

  const getColorTime = (item) => {
    return {
      color: item.isSelected ? COLORS.white : COLORS.black32302F
    }
  }

  const itemFrameTimeonClick = (id) => {
    const listFrameTimeChecked = listFrameTime.map(item => {
      return { ...item, isSelected: item.id === id }
    })
    setListFrameTime(listFrameTimeChecked)
  }

  const disabledBookButton = React.useMemo(() => {
    const frameTimeSelected = listFrameTime.find(item => item.isSelected)
    if (frameTimeSelected) {
      return false
    } else {
      return true
    }
  }, [listFrameTime])

  const btnBookingOnclick = () => {
    const listIssueSelected = listIssue.filter(item => item.isSelected) || []
   if (!validationTimeBooking()) {
    return
   } 
    if (listIssueSelected.length === 0) {
      showToastMessage("Bạn chưa chọn vấn đề cần tư vấn")
      return
    }
    setShowConfirmModal(true)

  }

  const validationTimeBooking = () => {
    const timeSelected = listFrameTime.filter(item => item.isSelected)
    if (!isToday(new Date(params?.bookDay ?? ""))) {
      return true
    } else if (timeSelected.length > 0) {
      const hoursNow = new Date().getHours();
      const hoursNowFormatted = hoursNow.toString().replace(/(^|-)0+/g, "$1");
      const timeStartBooking = timeSelected[0]?.timeStart?.replace(/(^|-)0+/g, "$1");
      const hoursNowNumber = parseInt(hoursNowFormatted)
      const timeStartBookingNumber = parseInt(timeStartBooking)
      const isInvalid = hoursNowNumber >= 8 && hoursNowNumber < timeStartBookingNumber
      return isInvalid
    } else {
      return false
    }
  }

  function isToday(date) {
    const today = new Date();
    if (today.toDateString() === date.toDateString()) {
      return true;
    }
    return false;
  }

  const btnBookOnclick = () => {
    setShowConfirmModal(false)
    isClickBookingModal = true
  }

  const onModalHide = () => {
    isClickBookingModal = false
    const bookType = params?.bookType
    const frameTimeSelected = listFrameTime.find(item => item.isSelected)
    const listIssueSelected = listIssue.filter(item => item.isSelected) || []
    const bookPosition = params?.addressSelect ? params.addressSelect?.subTitle : ''
    const categoriesId = []
    if (listIssueSelected.length > 0) {
      listIssueSelected?.map(item => {
        categoriesId.push(item.id)
      })
    }
    let bookStartDate = '';
    let bookEndDate = '';
    if (frameTimeSelected) {
      bookStartDate = params?.bookDay + ' ' + frameTimeSelected?.timeStart + ':00';
      bookEndDate = params?.bookDay + ' ' + frameTimeSelected?.timeEnd + ':00'
    }
    const bodyData = {
      book_time_start: bookStartDate,
      book_time_end: bookEndDate,
      book_categories: categoriesId,
      book_type: bookType,
      book_position: bookType === 1 ? bookPosition : ''
    }
    dispatch(homeActions.bookConsultScheduleRequest(bodyData))
  }

  return (
    <SafeAreaProvider style={styles.container}>
      <StatusBar translucent backgroundColor='transparent' />
      <HeaderBgImage>
        <View style={styles.headerContainer}>
          <TouchableOpacity onPress={onGoBack}>
            <IconImage source={IconBack} style={styles.backIconStyles} />
          </TouchableOpacity>
          <IconImage source={IconCareBox} style={styles.careboxIcon} />
          <Text style={styles.textHeaderStyle} numberOfLines={1}>CareBOX Live</Text>
        </View>
      </HeaderBgImage>
      <SafeAreaView style={styles.container}>
        <ScrollView
          style={styles.contentContainer}
          showsVerticalScrollIndicator={false}
        >
          <IssueSelectView
            listIssue={listIssue}
            onCheckBoxItemOnClick={onIssueItemOnClick}
          />
          <Text style={styles.titleStyle} numberOfLines={1}>Chọn thời gian tư vấn phù hợp với bạn</Text>
          <Text style={styles.subTitleStyle}>Bạn có thể huỷ lịch hoặc dừng cuộc tư vấn bất kỳ lúc nào.</Text>
          <View style={styles.frameTimeContainer}>
            {
              listFrameTime?.map((frameTimeItem, index) => {
                return (
                  <TouchableOpacity
                    key={index}
                    onPress={() => itemFrameTimeonClick(frameTimeItem?.id)}
                    style={[styles.frameTimeItemView, getItemStyles(frameTimeItem, index)]}
                  >
                    <Text
                      style={[
                        styles.itemTextStyles,
                        getColorTime(frameTimeItem)
                      ]}
                    >
                      {`${frameTimeItem?.timeStart} - ${frameTimeItem?.timeEnd}`}
                    </Text>
                  </TouchableOpacity>
                )
              })
            }
          </View>
        </ScrollView>
        <View style={{ backgroundColor: COLORS.white }}>
          <TouchableOpacity
            disabled={disabledBookButton}
            style={[styles.styleBtn]}
            onPress={btnBookingOnclick}
          >
            <Text style={[styles.buttonText]}>Đặt lịch tư vấn</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
      <AppModal
        isVisible={showConfirmModal}
        style={styles.modalContainer}
        onModalHide={isClickBookingModal && onModalHide}
      >
        <View style={{
          alignItems: 'center',
          justifyContent: 'center'
        }}>
          <View style={styles.modalBoxStyles}>
            <View style={styles.modalContent}>
              <Text style={styles.textModalTitle}>Xác nhận đặt lịch</Text>
              <Text style={styles.textModalContent}>Tham vấn viên sẽ có mặt và đợi bạn 10 phút, vui lòng xuất hiện đúng giờ.</Text>
            </View>
            <View style={styles.actionModalcontainer}>
              <TouchableOpacity
                style={styles.btnBookCancelStyle}
                onPress={() => setShowConfirmModal(false)}
              >
                <Text style={styles.btnTextStyles}>Huỷ lịch</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.btnBookStyle}
                onPress={btnBookOnclick}
              >
                <Text style={styles.btnTextStyles}>Đặt lịch</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </AppModal>
      <AppLoading isVisible={isBookingLoading} />
    </SafeAreaProvider>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    flex: 1,
    paddingHorizontal: 16,
    flexDirection: 'row',
    alignItems: 'center',
  },
  backIconStyles: {
    width: 40,
    height: 40
  },
  careboxIcon: {
    width: 40,
    height: 40,
    marginLeft: 20,
    marginRight: 10
  },
  textHeaderStyle: {
    flex: 1,
    fontSize: fontPixel(20),
    fontFamily: setFontFamily('GilroyXBold'),
    color: COLORS.black303030,
  },
  contentContainer: {
    paddingTop: 20,
    paddingHorizontal: 16,
  },
  styleBtn: {
    marginTop: 8,
    marginHorizontal: 16,
    height: fontPixel(50),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 16,
    backgroundColor: COLORS.main,
    marginBottom: 20,
  },
  buttonText: {
    fontSize: fontPixel(16),
    fontFamily: setFontFamily('GilroyXBold'),
    color: COLORS.white,
  },
  titleStyle: {
    marginTop: 20,
    fontSize: fontPixel(16),
    color: COLORS.black303030,
    fontFamily: setFontFamily('GilroyBold')
  },
  subTitleStyle: {
    opacity: 0.5,
    marginTop: 5,
    fontSize: fontPixel(14),
    color: COLORS.black303030,
    fontFamily: setFontFamily('GilroyMedium')
  },
  frameTimeContainer: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
  },
  frameTimeItemView: {
    marginTop: 20,
    borderRadius: 12,
    height: fontPixel(45),
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: COLORS.greyE9E9E9,
    width: (SIZE.SCREEN_WIDTH - 48) / 3,
  },
  itemTextStyles: {
    fontSize: fontPixel(14),
    color: COLORS.black32302F,
    fontFamily: setFontFamily('GilroyMedium'),
  },
  modalContainer: {
    justifyContent: 'center'
  },
  modalBoxStyles: {
    borderRadius: 16,
    backgroundColor: COLORS.white,
    width: SIZE.SCREEN_WIDTH - 32,
  },
  actionModalcontainer: {
    padding: 16,
    borderTopWidth: 1,
    flexDirection: 'row',
    borderTopColor: COLORS.greyE9E9E9,
  },
  btnBookCancelStyle: {
    flex: 1,
    marginRight: 10,
    borderRadius: 12,
    alignItems: 'center',
    height: fontPixel(40),
    justifyContent: 'center',
    backgroundColor: COLORS.orangeF58220
  },
  btnBookStyle: {
    flex: 1,
    marginLeft: 10,
    borderRadius: 12,
    alignItems: 'center',
    height: fontPixel(36),
    justifyContent: 'center',
    backgroundColor: COLORS.main
  },
  btnTextStyles: {
    color: COLORS.white,
    fontSize: fontPixel(16),
    fontFamily: setFontFamily('GilroySemiBold')
  },
  modalContent: {
    height: 'auto',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 20,
    paddingHorizontal: 16,
  },
  textModalTitle: {
    fontSize: fontPixel(16),
    color: COLORS.black303030,
    fontFamily: setFontFamily('GilroyBold')
  },
  textModalContent: {
    marginTop: 16,
    textAlign: 'center',
    fontSize: fontPixel(16),
    color: COLORS.black303030,
    fontFamily: setFontFamily('GilroyMedium')
  }
})

export default ConsultScheduleScreen
