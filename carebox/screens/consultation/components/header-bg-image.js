import React from "react";
import { View, StyleSheet, ImageBackground, Platform, StatusBar } from "react-native";
import { BgImageConsult } from '../../../../carebox/assets/icons'
import { useSafeAreaInsets } from 'react-native-safe-area-context';

const HeaderBgImage = (props) => {
  const { headerHeight = 56, children } = props
  const insets = useSafeAreaInsets();
  const statusBarHeight = insets.top
  const height = statusBarHeight + headerHeight
  return (
    <ImageBackground
      style={[styles.container, { height }]}
      source={BgImageConsult}
      resizeMode={'stretch'}
    >
      <View style={{ flex: 1, height: statusBarHeight }} />
      {children}
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
  }
})

export default HeaderBgImage;