import {COLORS} from '../../../carebox/utils/colors';
import {setFontFamily} from '../../../carebox/utils/functions';
import {fontPixel, SIZE} from '../../../carebox/utils/sizes';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    flex: 1,
    paddingHorizontal: 16,
    flexDirection: 'row',
    alignItems: 'center',
  },
  backIconStyles: {
    width: 36,
    height: 36,
  },
  textHeaderStyle: {
    flex: 1,
    alignItems: 'center',
    textAlign: 'center',
    fontSize: fontPixel(18),
    color: COLORS.black303030,
    fontFamily: setFontFamily('GilroyBold'),
  },
  checkinContainer: {
    padding: 16,
    backgroundColor: COLORS.white,
  },
  btnCheckInStyles: {
    height: fontPixel(45),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 12,
    backgroundColor: COLORS.main,
  },
  btnCheckInText: {
    fontSize: fontPixel(16),
    color: COLORS.white,
    fontFamily: setFontFamily('GilroySemiBold'),
  },
  greetingStyles: {
    textAlign: 'center',
    fontSize: fontPixel(16),
    color: COLORS.black303030,
    fontFamily: setFontFamily('GilroySemiBold'),
  },
  checkInItemStyles: {
    height: (SIZE.SCREEN_WIDTH - 64) / 3,
    width: (SIZE.SCREEN_WIDTH - 64) / 3,
    marginRight: 16,
    marginBottom: 16,
    backgroundColor: COLORS.white,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 12,
  },
  checkInTitle: {
    marginTop: 8,
    fontSize: fontPixel(14),
    fontFamily: setFontFamily('GilroyMedium'),
  },
  checkInIconStyles: {
    height: fontPixel(36),
    width: fontPixel(36),
  },
  greetingContainer: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 16,
    marginVertical: 16,
  },
  checkInMonthItemContainer: {
    height: fontPixel(45),
    borderRadius: 12,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: fontPixel(16),
    backgroundColor: COLORS.orangeF58220,
    marginBottom: fontPixel(10),
  },
  checkInMonthContent: {
    flex: 1,
    height: '100%',
    marginLeft: fontPixel(16),
    alignItems: 'center',
    justifyContent: 'center',
  },
  checkInOfMonthcontainer: {
    marginHorizontal: 16,
  },
  checkInOfMonthTitleStyles: {
    fontSize: fontPixel(16),
    color: COLORS.white,
    fontFamily: setFontFamily('GilroySemiBold'),
  },
});
