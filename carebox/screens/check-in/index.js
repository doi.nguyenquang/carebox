/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect, useMemo} from 'react';
import {
  Text,
  View,
  FlatList,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import HeaderBgImage from '../consultation/components/header-bg-image';
import {styles} from './styles';
import {IconBack} from '../../../carebox/assets/icons';
import {IconImage} from '../../../carebox/components/app-image';
import {NavigationService} from '../../../carebox/navigation/NavigationService';
import {useDispatch, useSelector} from 'react-redux';
import {homeActions} from '../../../carebox/pages/home/home-redux-saga/home-slice';
import {homeSelectors} from '../../../carebox/redux/selectors';
import AppLoading from '../../../carebox/components/app-loading';
import {COLORS} from '../../../carebox/utils/colors';
import {ScrollView} from 'react-native-virtualized-view';
import {
  getFirstDayOfMonth,
  getGreetingText,
  getLastDayOfMonth,
} from '../../../carebox/utils/utils';
import {profileSelector} from '../../../carebox/redux/selectors';
import {setFontFamily} from '../../../carebox/utils/functions';
import FastImage from 'react-native-fast-image';
import {groupBy} from 'lodash';

const CheckInScreen = () => {
  const dispatch = useDispatch();

  const [currentListCheckIn, setCurrentListCheckIn] = useState([]);
  const profileData = useSelector(profileSelector.profileData);
  const getListCheckInLoading = useSelector(
    homeSelectors.getListCheckInLoading,
  );
  const listCheckInData = useSelector(homeSelectors.listCheckInData) || [];

  const checkInLoading = useSelector(homeSelectors.checkInLoading);
  const checkInResult = useSelector(homeSelectors.checkInResult);

  const listUserCheckInOfMonthData = useSelector(
    homeSelectors.listUserCheckInOfMonthData,
  );

  const currentListCheckInMonth = useMemo(() => {
    if (listUserCheckInOfMonthData && listUserCheckInOfMonthData?.length > 0) {
      const groupData = groupBy(listUserCheckInOfMonthData, 'checkin_id');
      const dataConvert = Object.keys(groupData).map(key => groupData[key]);
      return dataConvert;
    }
    return [];
  }, [listUserCheckInOfMonthData]);

  const lastName =
    profileData?.fullName?.split(' ')[
      profileData?.fullName?.split(' ').length - 1
    ];

  useEffect(() => {
    const params = {
      startDate: getFirstDayOfMonth(),
      endDate: getLastDayOfMonth(),
    };
    dispatch(homeActions.getListCheckInRequest());
    dispatch(homeActions.getUserCheckInOfMonth(params));
    return () => {
      dispatch(homeActions.resetCheckInState());
    };
  }, []);

  useEffect(() => {
    if (checkInResult.isSuccess && checkInResult.statusCode === 200) {
      NavigationService.goBack();
    }
  }, [checkInResult]);

  useEffect(() => {
    const currentListCheckInTamp = listCheckInData.map((item, index) => {
      return {
        ...item,
        isChecked: false,
      };
    });
    setCurrentListCheckIn(currentListCheckInTamp);
  }, [listCheckInData]);

  const getBorderColor = item => {
    return {
      borderColor: COLORS.main,
      borderWidth: item?.isChecked ? 1 : 0,
    };
  };

  const getTextStyles = isChecked => {
    if (isChecked) {
      return {
        color: COLORS.main,
        fontFamily: setFontFamily('GilroySemiBold'),
      };
    }
  };

  const onItemCheckInSelected = checkInId => {
    const listCheckInCheck = currentListCheckIn.map((item, index) => {
      return {
        ...item,
        isChecked: item?.checkin_id === checkInId,
      };
    });
    setCurrentListCheckIn(listCheckInCheck);
  };

  const percentStatus = item => {
    if (listCheckInData && listCheckInData.length > 0) {
      const percent = (item.length / listCheckInData.length) * 100;
      return `(${percent.toFixed(2)}%)`;
    }
    return '';
  };

  const getOpcity = item => {
    if (listCheckInData && listCheckInData.length > 0) {
      const percent = (item.length / listCheckInData.length) * 50;
      return {
        opacity: parseInt(percent, 10) / 10,
      };
    }
    return {
      opacity: 1,
    };
  };

  const renderUserCheckInOfMonthItem = (item, index) => {
    return (
      <View style={[styles.checkInMonthItemContainer, getOpcity(item)]}>
        <FastImage
          source={{uri: item[0]?.checkin?.checkin_icon}}
          style={styles.checkInIconStyles}
          resizeMode={'stretch'}
        />
        <View style={styles.checkInMonthContent}>
          <Text style={styles.checkInOfMonthTitleStyles} numberOfLines={1}>
            {`${item[0]?.checkin?.checkin_title} ${percentStatus(item)}`}
          </Text>
        </View>
        <View style={styles.checkInIconStyles} />
      </View>
    );
  };

  const renderCheckInItem = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => onItemCheckInSelected(item?.checkin_id)}
        style={[styles.checkInItemStyles, getBorderColor(item)]}>
        <FastImage
          style={styles.checkInIconStyles}
          resizeMode={'stretch'}
          source={{uri: item.checkin_icon}}
        />
        <Text style={[styles.checkInTitle, getTextStyles(item?.isChecked)]}>
          {item.checkin_title}
        </Text>
      </TouchableOpacity>
    );
  };

  const checkInOnPress = () => {
    const isCurrentCheckInItem = currentListCheckIn.find(
      item => item.isChecked,
    );
    const bodyData = {
      checkin_id: isCurrentCheckInItem.checkin_id,
    };
    dispatch(homeActions.userCheckInRequest(bodyData));
  };

  return (
    <SafeAreaProvider style={styles.container}>
      <StatusBar translucent backgroundColor="transparent" />
      <HeaderBgImage>
        <View style={styles.headerContainer}>
          <TouchableOpacity onPress={() => NavigationService.goBack()}>
            <IconImage source={IconBack} style={styles.backIconStyles} />
          </TouchableOpacity>
          <Text style={styles.textHeaderStyle} numberOfLines={1}>
            Daily Check-in
          </Text>
          <View style={styles.backIconStyles} />
        </View>
      </HeaderBgImage>
      <SafeAreaView style={styles.container}>
        <ScrollView
          nestedScrollEnabled={true}
          showsVerticalScrollIndicator={false}>
          <View style={styles.container}>
            <View style={styles.greetingContainer}>
              <Text style={styles.greetingStyles}>Cảm xúc của tháng</Text>
            </View>
            <View style={styles.checkInOfMonthcontainer}>
              {currentListCheckInMonth?.map((item, index) =>
                renderUserCheckInOfMonthItem(item, index),
              )}
            </View>
            <View style={styles.greetingContainer}>
              <Text
                style={
                  styles.greetingStyles
                }>{`${getGreetingText()}, ${lastName} ơi!\nHôm nay của bạn thế nào?`}</Text>
            </View>
            <View style={styles.container}>
              <FlatList
                style={{
                  paddingHorizontal: 16,
                }}
                scrollEnabled={false}
                data={currentListCheckIn}
                numColumns={3}
                keyExtractor={(_, index) => `key-${index}`}
                renderItem={renderCheckInItem}
              />
            </View>
          </View>
        </ScrollView>
        <View style={styles.checkinContainer}>
          <TouchableOpacity
            style={styles.btnCheckInStyles}
            onPress={checkInOnPress}>
            <Text style={styles.btnCheckInText}>CHECK-IN</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
      <AppLoading isVisible={getListCheckInLoading || checkInLoading} />
    </SafeAreaProvider>
  );
};

export default CheckInScreen;
