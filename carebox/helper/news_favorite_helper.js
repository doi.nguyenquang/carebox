import React, { useState, useEffect } from 'react';
import { NewsModel } from '../../carebox/models/news_model'
import { NavigationService } from '../../carebox/navigation/NavigationService';
import { ROUTER_NAME } from '../../carebox/navigation/RouterName';
import FavoriteApiClient from '../../carebox/screens/favorite/favorite_api_client';

const NewsFavoriteHelper = (model: NewsModel) => {
    const _apiClient = new FavoriteApiClient();

    const [favorite, setFavorite] = useState(model?.isFavorite());

    const onDetail = (value) => {
        NavigationService.push(ROUTER_NAME.NewsScreen, { model: value, callBack: callBack });
    }

    const callBack = (value) => {
        setFavorite(value.isFavorite());
    }

    const _onFavorite = () => {
        if (favorite) {
            setFavorite(false);
        } else {
            setFavorite(true);
        }
        _apiClient.favorite(model);
    }

    return { favorite, onDetail, _onFavorite }
}


export default NewsFavoriteHelper;