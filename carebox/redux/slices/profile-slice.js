import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  getProfileError: {
    status: false,
    error: {}
  },
  profileData: {},
  roles: [],
  getProfileLoading: false,
}

const profileSlice = createSlice({
  name: 'profile',
  initialState,
  reducers: {
    getProfileRequest: (state, action) => {
      return {
        ...state,
        getProfileLoading: true,
      }
    },
    handleGetProfileError: (state, action) => {
      return {
        ...state,
        getProfileLoading: false,
        getMoreError: {
          status: true,
          error: action.payload
        },
        profileData: {},
      }
    },
    getProfileSuccess: (state, action) => {
      return {
        ...state,
        getProfileLoading: false,
        getProfileError: {
          status: false,
          error: {}
        },
        profileData: action.payload,
      }
    },
    saveRoles: (state, action) => {
      return {
        ...state,
        roles: action.payload,
      }
    }
  }
})

export const profileActions = profileSlice.actions
export default profileSlice