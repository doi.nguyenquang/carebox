import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  getCategoryError: {
    status: false,
    error: {}
  },
  listSuggestionIssue: []
}

const categorySlice = createSlice({
  name: 'category',
  initialState,
  reducers: {
    getAllRequest: (state, action) => {
      return {
        ...state,
      }
    },
    handleAllCategoryError: (state, action) => {
      return {
        ...state,
        getCategoryError: {
          status: true,
          error: action.payload
        },
        listSuggestionIssue: [],
      }
    },
    getlistSuggestionIssueData: (state, action) => {
      return {
        ...state,
        getCategoryError: {
          status: false,
          error: {}
        },
        listSuggestionIssue: action.payload
      }
    }
  }
})

export const categoryActions = categorySlice.actions
export default categorySlice