import {createSlice} from '@reduxjs/toolkit';
// export interface LoginStateProps {
//   isLogin: boolean;
//   user: undefined;
//   error: boolean;
// }

const initialState = {
  isLogin: false,
  user: undefined,
  error: false,
};
const UserSlice = createSlice({
  name: 'user',
  initialState: initialState,
  reducers: {},
});

export default UserSlice;
