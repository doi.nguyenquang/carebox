import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  getMoreError: {
    status: false,
    error: {}
  },
  listMoreData: []
}

const moreSlice = createSlice({
  name: 'more',
  initialState,
  reducers: {
    getMoreRequest: (state, action) => {
      return {
        ...state,
      }
    },
    handleGetMoreError: (state, action) => {
      return {
        ...state,
        getMoreError: {
          status: true,
          error: action.payload
        },
        listMoreData: [],
      }
    },
    getMoreSuccess: (state, action) => {
      return {
        ...state,
        getCategoryError: {
          status: false,
          error: {}
        },
        listMoreData: action.payload
      }
    }
  }
})

export const moreActions = moreSlice.actions
export default moreSlice