import { createSlice } from '@reduxjs/toolkit';
import { findIndex, cloneDeep, set } from 'lodash';

const initialState = {
  getListQuizError: {
    status: false,
    error: {}
  },
  getListQuizLoading: false,
  listQuizData: [],

  submitQuizTestLoading: false,
  submitQuizTestSuccess: {
    status: false,
    isFinished: false,
  },
  submitQuizTestError: false,

  getCourseDetailLoading: false,
  courseDetailData: {},
  getCourseDetailError: {
    status: false,
    error: {}
  },
  submitSaveDocumentLoading: false,
  submitSaveDocumentSuccess: false,
  submitSaveDocumentFailure: false
}

const quizSlice = createSlice({
  name: 'quiz',
  initialState,
  reducers: {
    getListQuizRequest: (state, action) => {
      return {
        ...state,
        getListQuizLoading: true,
        getListQuizError: {
          status: false,
          error: {}
        },
        listQuizData: []
      }
    },
    getListQuizError: (state, action) => {
      return {
        ...state,
        getListQuizLoading: false,
        getListQuizError: {
          status: true,
          error: action.payload
        },
        listQuizData: []
      }
    },
    getListQuizSuccess: (state, action) => {
      return {
        ...state,
        getListQuizLoading: false,
        getListQuizError: {
          status: false,
          error: {}
        },
        listQuizData: action.payload
      }
    },

    submitQuizTestRequest: (state, action) => {
      return {
        ...state,
        submitQuizTestLoading: true,
        submitQuizTestSuccess: {
          status: false,
          isFinished: false,
        },
        submitQuizTestError: false,
      }
    },
    submitQuizTestSuccess: (state, action) => {
      return {
        ...state,
        submitQuizTestLoading: false,
        submitQuizTestSuccess: {
          status: true,
          isFinished: action.payload,
        },
        submitQuizTestError: false
      }
    },
    submitQuizTestFailure: (state) => {
      return {
        ...state,
        submitQuizTestLoading: false,
        submitQuizTestSuccess: {
          status: false,
          isFinished: false,
        },
        submitQuizTestError: true
      }
    },
    resetQuizReducerAction: (state) => {
      return {
        ...state,
        getListQuizError: {
          status: false,
          error: {}
        },
        getListQuizLoading: false,
        listQuizData: [],

        submitQuizTestLoading: false,
        submitQuizTestSuccess: {
          status: false,
          isFinished: false,
        },
        submitQuizTestError: false
      }
    },

    getCourseDetailRequest: (state) => {
      return {
        ...state,
        getCourseDetailLoading: true,
        courseDetailData: {},
        getCourseDetailError: {
          status: false,
          error: {}
        }
      }
    },
    getCourseDetailSuccess: (state, action) => {
      return {
        ...state,
        getCourseDetailLoading: false,
        courseDetailData: action.payload,
        getCourseDetailError: {
          status: false,
          error: {}
        }
      }
    },
    getCourseDetailFailure: (state, action) => {
      return {
        ...state,
        getCourseDetailLoading: false,
        courseDetailData: {},
        getCourseDetailError: {
          status: true,
          error: action.payload
        }
      }
    },
    submitSaveDocumentRequest: (state) => {
      return {
        ...state,
        submitSaveDocumentLoading: true,
        submitSaveDocumentSuccess: false,
        submitSaveDocumentFailure: false
      }
    },
    submitSaveDocumentSuccess: (state, action) => {
      const courseListId = action.payload
      const newState = cloneDeep(state)
      const course = state.courseDetailData.course_indexs.map((item) => {
        if (item.course_list_id === courseListId) {
          return { ...item, is_learned: 1 }
        }
        return item
      })
      set(newState, 'courseDetailData.course_indexs', course)
      return {
        ...newState,
        submitSaveDocumentLoading: false,
        submitSaveDocumentSuccess: true,
        submitSaveDocumentFailure: false
      }
    },
    submitSaveDocumentFailure: (state) => {
      return {
        ...state,
        submitSaveDocumentLoading: false,
        submitSaveDocumentSuccess: false,
        submitSaveDocumentFailure: true
      }
    }
  }
})

export const quizActions = quizSlice.actions
export default quizSlice