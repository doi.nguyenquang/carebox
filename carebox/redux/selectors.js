import {createSelector} from '@reduxjs/toolkit';

export const loginSelector = {
  loginLoading: state => state.loginReducer.loginLoading,
  loginResult: state => state.loginReducer.loginResult,
};

export const homeSelectors = {
  getHomeDataLoading: state => state.homeReducer.getHomeDataLoading,
  homeData: state => state.homeReducer.homeData,
  listStaff: state => state.homeReducer.listStaff,
  listIssueSelect: state => state.homeReducer.listIssueSelect,
  isBookingLoading: state => state.homeReducer.isBookingLoading,
  bookScheduleConsultSuccess: state =>
    state.homeReducer.bookScheduleConsultSuccess,
  bookScheduleConsultError: state => state.homeReducer.bookScheduleConsultError,
  getListCheckInLoading: state => state.homeReducer.getListCheckInLoading,
  listCheckInData: state => state.homeReducer.listCheckInData,
  getListCheckInError: state => state.homeReducer.getListCheckInError,
  checkInLoading: state => state.homeReducer.checkInLoading,
  checkInResult: state => state.homeReducer.checkInResult,
  getUserCheckInLoading: state => state.homeReducer.getUserCheckInLoading,
  userCheckInStatus: state => state.homeReducer.userCheckInStatus,
  getStaffDetailLoading: state => state.homeReducer.getStaffDetailLoading,
  staffDetailData: state => state.homeReducer.staffDetailData,
  getUserCheckInOfMonthLoading: state =>
    state.homeReducer.getUserCheckInOfMonthLoading,
  listUserCheckInOfMonthData: state =>
    state.homeReducer.listUserCheckInOfMonthData,
  getUserCheckInOfMonthError: state =>
    state.homeReducer.getUserCheckInOfMonthError,
};

export const categorySelector = {
  getCategoryError: state => state.categoryReducer.getCategoryError,
  listSuggestionIssue: state => state.categoryReducer.listSuggestionIssue,
};

export const moreSelector = {
  getMoreError: state => state.moreReducer.getMoreError,
  listMoreData: state => state.moreReducer.listMoreData,
};

export const profileSelector = {
  getProfileLoading: state => state.profileReducer.getProfileLoading,
  profileData: state => state.profileReducer.profileData,
  roles: state => state.profileReducer.roles,
  getProfileError: state => state.profileReducer.getProfileError,
};

export const quizSelector = {
  getListQuizLoading: state => state.quizReducer.getListQuizLoading,
  listQuizData: state => state.quizReducer.listQuizData,
  getListQuizError: state => state.quizReducer.getListQuizError,
  submitQuizTestLoading: state => state.quizReducer.submitQuizTestLoading,
  submitQuizTestSuccess: state => state.quizReducer.submitQuizTestSuccess,
  submitQuizTestError: state => state.quizReducer.submitQuizTestError,
};

export const courseSelector = {
  getCourseDetailLoading: state => state.quizReducer.getCourseDetailLoading,
  courseDetailData: state => state.quizReducer.courseDetailData,
  submitSaveDocumentLoading: state =>
    state.quizReducer.submitSaveDocumentLoading,
  submitSaveDocumentSuccess: state =>
    state.quizReducer.submitSaveDocumentSuccess,
};
