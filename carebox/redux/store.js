import {configureStore} from '@reduxjs/toolkit';
import { createLogger } from 'redux-logger'
import createSagaMiddleware from 'redux-saga'
import {
  FLUSH,
  PAUSE,
  PERSIST,
  persistStore,
  PURGE,
  REGISTER,
  REHYDRATE,
} from 'redux-persist';
import persistedReducer from './rootReducer';
import rootSaga from './rootSaga';
const sagaMiddleware = createSagaMiddleware()
const logger = createLogger({
//   // ...options
})
export const store = configureStore({
  reducer: persistedReducer,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },

    }).concat(sagaMiddleware, logger),
});

sagaMiddleware.run(rootSaga)

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export const persistor = persistStore(store);
