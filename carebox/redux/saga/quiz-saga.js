import { all, call, put, takeLatest } from 'redux-saga/effects';
import { quizActions } from '../slices/quiz-slice';
import * as api from '../../../carebox/services/Request';
import ApiConfig from '../../../carebox/services/api_config';

const parseQuizAnswerList = (quiz_test_answer) => {
  const listAnswerParsed = JSON.parse(quiz_test_answer)?.map((item, index) => {
    return { ...item, isSelected: false, id: index };
  })
  return listAnswerParsed
}

function* handleGetListQuizRequest(action) {
  const query = action.payload
  try {
    const response = yield api.methodGet({ api: ApiConfig.quizTest, queries: query })
    if (response.code === 200 && response.success) {
      const data = response?.data?.values?.map(quiz => {
        return {
          ...quiz,
          quiz_test_answer: parseQuizAnswerList(quiz.quiz_test_answer)
        }
      })
      yield put(quizActions.getListQuizSuccess(data))
    } else {
      yield put(quizActions.getListQuizError(response))
    }
  } catch (error) {
    yield put(quizActions.getListQuizError(error))
  }
}

function* handleSubmitQuizTestRequest(action) {
  const { bodyData, isFinished } = action.payload
  try {
    const response = yield api.methodPost({ api: ApiConfig.testResult, body: bodyData })
    if (response.code === 200 && response.success) {
      yield put(quizActions.submitQuizTestSuccess(isFinished))
    } else {
      yield put(quizActions.submitQuizTestFailure())
    }
  } catch (error) {
    yield put(quizActions.submitQuizTestFailure())
  }
}

function* handleGetCourseDetailRequest(action) {
  const query = action.payload
  try {
    const response = yield api.methodGet({ api: ApiConfig.courseDetail, queries: query });
    if (response.code === 200 && response.success) {
      yield put(quizActions.getCourseDetailSuccess(response?.data))
    } else {
      yield put(quizActions.getCourseDetailFailure(response))
    }
  } catch (error) {
    yield put(quizActions.getCourseDetailFailure(error))
  }
}

function* handleSaveDocumentRequest(action) {
  const { bodyData } = action.payload
  try {
    const response = yield api.methodPost({ api: ApiConfig.userCourseList, body: bodyData })
    if (response.code === 200 && response.success) {
      yield put(quizActions.submitSaveDocumentSuccess(bodyData?.course_list_id))
    } else {
      yield put(quizActions.submitSaveDocumentFailure())
    }
  } catch (error) {
    yield put(quizActions.submitSaveDocumentFailure())
  }
}

export default function* watchQuizSaga() {
  yield all([
    takeLatest(quizActions.getListQuizRequest, handleGetListQuizRequest),
    takeLatest(quizActions.submitQuizTestRequest, handleSubmitQuizTestRequest),
    takeLatest(quizActions.getCourseDetailRequest, handleGetCourseDetailRequest),
    takeLatest(quizActions.submitSaveDocumentRequest, handleSaveDocumentRequest),
  ])
}