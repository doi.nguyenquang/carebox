import { all, call, put, takeLatest } from 'redux-saga/effects';
import { categoryActions } from '../slices/category-slice';
import * as api from '../../../carebox/services/Request';
import ApiConfig from '../../../carebox/services/api_config';
import { categoryType } from '../../../carebox/utils/constants';

function* handleAllCategory(action) {
  const params = action.payload
  try {
    const response = yield api.methodGet({ api: ApiConfig.categoryAll, queries: params })
    if (response.code === 200 && response.success) {
      switch (params.type) {
        case categoryType.new: {
          // put category data for new
          break;
        }
        case categoryType.courseAndQuiz:
          yield put(categoryActions.getlistSuggestionIssueData(response?.data))
          break;
        default: {
          // put category data for newsMentality
          break;
        }
      }
      if (params.type === categoryType.courseAndQuiz) {
        yield put(categoryActions.getlistSuggestionIssueData(response?.data))
      }
    } else {
      yield put(categoryActions.handleAllCategoryError(response))
    }
  } catch (error) {
    yield put(categoryActions.handleAllCategoryError(error))
  }
}

export default function* watchCategorySaga() {
  yield all([
    takeLatest(categoryActions.getAllRequest, handleAllCategory),
  ])
}