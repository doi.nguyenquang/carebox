import { all, put, takeLatest } from 'redux-saga/effects';
import { profileActions } from '../slices/profile-slice';
import * as api from '../../../carebox/services/Request';
import ApiConfig from '../../../carebox/services/api_config';

function* handleGetProfileRequest(action) {

  const params = action.payload
  try {
    const response = yield api.methodGet({ api: ApiConfig.userProfile, queries: params})
    if (response.code === 200 && response.success) {
      if(!params.user_id || params.user_id === '') {
        yield put(profileActions.getProfileSuccess(response?.data))
      }
    } else {
      yield put(profileActions.handleGetProfileError(response))
    }
  } catch (error) {
    yield put(profileActions.handleGetProfileError(error))
  }
}

export default function* watchProfileSaga() {
  yield all([
    takeLatest(profileActions.getProfileRequest, handleGetProfileRequest),
  ])
}