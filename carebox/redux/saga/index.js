import watchCategorySaga from './category-saga';
import watchMoreSaga from './more-saga';
import watchProfileSaga from './profile-saga';
import watchHomeSaga from '../../../carebox/pages/home/home-redux-saga/home-saga';
import watchQuizSaga from './quiz-saga';
import watchLoginSaga from './login-saga'

export {
  watchHomeSaga,
  watchCategorySaga,
  watchMoreSaga,
  watchProfileSaga,
  watchQuizSaga,
  watchLoginSaga
}