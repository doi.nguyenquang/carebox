import { all, call, put, takeLatest } from 'redux-saga/effects';
import { moreActions } from '../slices/more-slice';
import * as api from '../../../carebox/services/Request';
import ApiConfig from '../../../carebox/services/api_config';

function* handleGetMoreRequest() {
  try {
    const response = yield api.methodGet({ api: ApiConfig.more })
    if (response.code === 200 && response.success) {
     yield put(moreActions.getMoreSuccess(response?.data?.values))
    } else {
      yield put(moreActions.handleGetMoreError(response))
    }
  } catch (error) {
    yield put(moreActions.handleGetMoreError(error))
  }
}

export default function* watchMoreSaga() {
  yield all([
    takeLatest(moreActions.getMoreRequest, handleGetMoreRequest),
  ])
}