import { all, put, takeLatest } from 'redux-saga/effects';
import { loginActions } from '../../../carebox/redux/slices/LoginSlice';
import * as api from '../../../carebox/services/Request';
import ApiConfig from '../../../carebox/services/api_config';
import { profileActions } from '../../../carebox/redux/slices/profile-slice';

function* handleLoginRequest(action) {
    const { bodyData } = action.payload
  try {
    const response = yield api.methodPost({ api: ApiConfig.loginAdmin, body: bodyData })
    if (response.code === 200 && response.success) {
        yield put(profileActions.getProfileSuccess(response?.data?.userInfo))
        yield put(loginActions.loginSuccess(response?.data?.accessToken))
        yield put(profileActions.saveRoles(response?.data?.roles))
    } else {
      yield put(loginActions.loginFailure())
    }
  } catch (error) {
    yield put(loginActions.loginFailure())
  }
}

export default function* watchLoginSaga() {
  yield all([
    takeLatest(loginActions.loginRequest, handleLoginRequest),
  ])
}