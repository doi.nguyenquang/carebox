import {combineReducers} from 'redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {persistReducer} from 'redux-persist';
import LoginSlice from './slices/LoginSlice';
import UserSlice from './slices/UserSlice';
import homeSlice from '../../carebox/pages/home/home-redux-saga/home-slice'
import categorySlice from './slices/category-slice';
import moreSlice from './slices/more-slice';
import profileSlice from './slices/profile-slice'
import quizSlice from './slices/quiz-slice';

const rootReducers = combineReducers({
  loginReducer: LoginSlice.reducer,
  userReducer: UserSlice.reducer,
  homeReducer: homeSlice.reducer,
  categoryReducer: categorySlice.reducer,
  moreReducer: moreSlice.reducer,
  profileReducer: profileSlice.reducer,
  quizReducer: quizSlice.reducer
});
const persistedReducer = persistReducer(
  {
    key: 'root',
    debug: true,
    storage: AsyncStorage,
    version: 1.0,
    whitelist: [],
  },
  rootReducers,
);
export default persistedReducer;
