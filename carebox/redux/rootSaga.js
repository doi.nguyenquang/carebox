import { all, fork } from 'redux-saga/effects';
import {
  watchHomeSaga,
  watchCategorySaga,
  watchMoreSaga,
  watchProfileSaga,
  watchQuizSaga,
  watchLoginSaga
} from './saga'

export default function* rootSaga() {
  console.log('root saga');
  yield all([
    fork(watchHomeSaga),
    fork(watchCategorySaga),
    fork(watchMoreSaga),
    fork(watchProfileSaga),
    fork(watchQuizSaga),
    fork(watchLoginSaga)
  ])
}