import {Dimensions, Platform} from 'react-native';

const getDefaultHeaderHeight = () => {
  if (Platform.OS === 'ios') {
    return 44;
  } else if (Platform.OS === 'android') {
    return 56;
  } else {
    return 64;
  }
};
const {width: widthScreen, height: heightScreen} = Dimensions.get('window');
const BASE_WIDTH = 375;
const BASE_HEIGHT = 812;
const widthBaseScale = widthScreen / BASE_WIDTH;
const heightBaseScale = heightScreen / BASE_HEIGHT;

export default {
  screenSideEdge: 16,
  defaultHeaderHeight: getDefaultHeaderHeight(),
  widthScreen,
  heightScreen,
  widthBaseScale,
  heightBaseScale,
};
