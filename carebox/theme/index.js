import colors from './colors.js';
import dimensions from './dimensions';
import fontWeights from './fontWeights';
import spacing from './spacing';

export default {
  colors,
  dimensions,
  spacing,
  fontWeights,
};
