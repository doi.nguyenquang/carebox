import {Platform} from 'react-native';

export default {
  normal: 'normal',
  bold: 'bold',
  extraBold: Platform.select({
    ios: '900',
    android: 'bold',
  }),
};
