const BASE_DIMENSION = 8;

const spacing = (value) => value * BASE_DIMENSION;
export default spacing;
