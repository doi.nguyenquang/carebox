import React from 'react';
import { Text, View, StyleSheet, StatusBar, ScrollView, useWindowDimensions } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { MoreModel } from '../../../../carebox/models/more_model'
import AppHeader from '../../../../carebox/components/app-header'
import { COLORS } from '../../../../carebox/utils/colors';
import { AppImage, IconImage } from '../../../../carebox/components/app-image'
import RenderHtml from 'react-native-render-html';
import AppButton from '../../../../carebox/components/app-button'

const PrivacyScreen = (props) => {
  const { route } = props;
  const { width } = useWindowDimensions();
  const model: MoreModel = route.params.model;

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <ScrollView>
        <View>
          <AppHeader
            style={styles.header}
            title={model.moreTitle}
          />
          <View style={styles.vContent}>
            <RenderHtml
              contentWidth={width}
              source={{ html: model.moreContent }}
            />
          </View>
        </View>
      </ScrollView>
    </SafeAreaView >
  );
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.white,
  },
  header: {
    marginHorizontal: 10,
    elevation: 5,
    justifyContent: 'space-between',
  },
  image: {
    width: 'auto',
    height: 200
  },
  body: {
    paddingHorizontal: 15
  },
  tvNoData: {
    textAlign: 'center'
  },
  title: {
    color: COLORS.black,
    fontWeight: 'bold',
    marginVertical: 10,
  },
  iconF: {
    width: 30,
    height: 30
  },
  vContent: {
    paddingHorizontal: 15,
  }


});

export default PrivacyScreen;
