import AccountApiClient from './account_api_client';
import { useState, useEffect } from 'react';
import { BookModel } from '../../../carebox/models/book_model'
import { NavigationService } from '../../../carebox/navigation/NavigationService';
import { ROUTER_NAME } from '../../../carebox/navigation/RouterName';
import { profileSelector } from '../../../carebox/redux/selectors';
import { useSelector } from 'react-redux';
import ApiConfig from '../../../carebox/services/api_config'

const AccountNotifier = () => {

  const _apiClient = new AccountApiClient();
  const [mores, setMores] = useState([]);
  const [books, setBooks] = useState([]);
  const [courses, setCourses] = useState([]);

  useEffect(() => {

    loadData();
  }, [])

  const loadData = async () => {
    const value = await _apiClient.getMore();
    setMores(value);
    const valueBook = await _apiClient.getBook(isAdmin);
    setBooks(valueBook);
    const valueCourse = await _apiClient.getCourseCompleted();
    setCourses(valueCourse);
  }


  const [refreshing, setRefreshing] = useState(false);
  const onRefresh = async () => {
    setRefreshing(true);
    await loadData();
    setRefreshing(false);
  }
  const profileData = useSelector(profileSelector.profileData)
  const roles = useSelector(profileSelector.roles)
  const isAdmin = roles.includes(ApiConfig.isAdmin) || roles.includes(ApiConfig.isDoctor)

  const onJoin = async (model: BookModel) => {
    const userId = isAdmin ? model.receiverId : model.userId;
    const receiverId = isAdmin ? model.userId : model.receiverId;
    NavigationService.push(ROUTER_NAME.CallScreen, { userId, receiverId, isReceiver: false  })
  }


  return {
    onRefresh,
    refreshing,
    mores,
    books,
    courses,
    onJoin,
  }

}
export default AccountNotifier;