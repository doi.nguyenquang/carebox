import React from 'react';
import { Text, View, Button, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { IconMore } from '../../../../carebox/assets/icons'
import AppButton from '../../../../carebox/components/app-button';
import { COLORS } from '../../../../carebox/utils/colors';
import { setFontFamily } from '../../../../carebox/utils/functions';

const ItemView = (props) => {
    const { onClick, title, subTitle, icon } = props;
    return (
        <AppButton onPress={onClick} style={styles.container} >
            <Image style={styles.icon} source={icon} />
            <View style={styles.vInfo}>
                <Text style={styles.fullname}>{title}</Text>
                <Text style={styles.username}>{subTitle}</Text>
            </View>
            <Image style={styles.icBack} source={IconMore} />
        </AppButton>
    );
};


const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 15,
        marginTop:15,
    },
    vInfo: {
        width: 'auto',
        flex: 1,
    },
    fullname: {
        fontWeight: 'bold',
        color: COLORS.black,
        marginBottom: 5,
        fontFamily: setFontFamily('GilroySemiBold'),
    },
    username: {
        color: COLORS.grey949494,
        fontFamily: setFontFamily('GilroyMedium')
    },
    icon: {
        width: 24,
        height: 24,
        marginRight: 15,
    },
    icBack: {
        width: 24,
        height: 24,
    },
});

export default ItemView;
