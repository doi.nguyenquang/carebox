import React from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import { UserAvatarDefault, ImageDefault } from '../../../../carebox/assets/icons'
import { COLORS } from '../../../../carebox/utils/colors';
import { useSelector } from 'react-redux';
import { profileSelector } from '../../../../carebox/redux/selectors';
import { setFontFamily } from '../../../../carebox/utils/functions';

const HeaderView = (props) => {

  const profileData = useSelector(profileSelector.profileData)

  return (
    <View style={styles.container} >
      <Image style={styles.wallpage} source={ImageDefault} />
      <View style={styles.bottom}>
        <Image style={styles.avatar} source={UserAvatarDefault} />
        <Text style={styles.fullname}>{profileData?.fullName}</Text>
        <Text style={styles.username}>@{profileData?.preferredUsername}</Text>
      </View>
    </View>
  );
};


const styles = StyleSheet.create({
  container: {
    marginBottom: 120,
  },
  wallpage: {
    width: '100%',
    height: 200,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
  },
  icBack: {
    width: 50,
    height: 50,
  },
  vBack: {
    position: 'absolute',
    top: 10,
    left: 10,
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 100,
  },
  bottom: {
    width: '100%',
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    top: 150,
  },
  fullname: {
    fontWeight: 'bold',
    color: COLORS.black,
    fontSize: 18,
    marginBottom: 5,
    fontFamily: setFontFamily('GilroySemiBold'),
  },
  username: {
    color: COLORS.main,
    fontFamily: setFontFamily('GilroyMedium')
  }
});

export default HeaderView;
