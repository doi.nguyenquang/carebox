import React from 'react';
import { Text, View, Button, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { IconCourse1, IconCourse0 } from '../../../../carebox/assets/icons'
import AppButton from '../../../../carebox/components/app-button';
import { COLORS } from '../../../../carebox/utils/colors';
import { CourseCompleteModel } from '../../../../carebox/models/course_complete_model'
import { CourseModel } from '../../../../carebox/screens/course/course_model'
import { setFontFamily } from '../../../../carebox/utils/functions';

const ItemCourseView = ({ item, index }) => {
    const value: CourseCompleteModel = item;
    const model: CourseModel = item.course;

    return (
        <View style={styles.container} >
            <View style={styles.vIm}>
                <Image
                    style={styles.icon}
                    resizeMode='contain'
                    source={index % 2 == 0 ? IconCourse0 : IconCourse1}
                />
            </View>
            <View style={styles.vInfo}>
                <Text style={styles.fullname}>{model.courseTitle}</Text>
            </View>
        </View>
    );
};


const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        borderRadius: 10,
        alignItems: 'center',
    },
    vInfo: {
        width: 200,
        position: 'absolute',
        alignItems: 'center',
        left: 0,
        right: 0,
        top: 30,
    },
    icon: {
        resizeMode: 'contain',
        width: 200,
    },
    vIm: {
        marginRight: 10,
    },
    fullname: {
        fontWeight: 'bold',
        color: COLORS.white,
        textAlign: 'center',
        fontFamily: setFontFamily('GilroySemiBold'),
    },

});

export default ItemCourseView;
