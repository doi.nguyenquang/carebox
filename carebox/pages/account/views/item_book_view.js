import React from 'react';
import { Text, View, Button, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { IconConsultVideo, IconConsultLive } from '../../../../carebox/assets/icons'
import AppButton from '../../../../carebox/components/app-button';
import { COLORS } from '../../../../carebox/utils/colors';
import { BookModel } from '../../../../carebox/models/book_model'
import { setFontFamily } from '../../../../carebox/utils/functions';
import { profileSelector } from '../../../../carebox/redux/selectors';
import { useSelector } from 'react-redux';

const ItemBookView = ({ item, onJoin }) => {
    const model: BookModel = item;
     
    return (
        <View style={styles.container} >
            <View style={styles.vIm}>
                <Image
                    style={styles.icon}
                    resizeMode='contain'
                    source={model.isCall() ? IconConsultVideo : IconConsultLive}
                />
            </View>
            <View style={styles.vInfo}>
                <Text style={styles.fullname}>Lịch tư vấn của bạn</Text>
                <Text style={styles.time}>{model.timeStart()}</Text>
                <Text style={styles.username}>{model.status()}</Text>
                {model.isCallCanJoin() &&  <AppButton
                onPress = {() => onJoin(model)}
                title='Tham gia' />}
            </View>
        </View>
    );
};


const styles = StyleSheet.create({
    container: {
        width: 320,
        flexDirection: 'row',
        borderColor: COLORS.blue54BADE,
        borderWidth: 1,
        borderRadius: 10,
        paddingHorizontal: 15,
        alignItems: 'center',
        marginRight: 15,
    },
    vInfo: {
        width: 'auto',
        marginLeft: 15,
    },
    fullname: {
        fontWeight: 'bold',
        color: COLORS.black,
        marginBottom: 5,
        fontFamily: setFontFamily('GilroySemiBold'),
    },
    username: {
        color: COLORS.grey949494,
        marginBottom: 10,
        fontFamily: setFontFamily('GilroyMedium')
    },
    time: {
        color: COLORS.grey949494,
        marginBottom: 5,
        color: COLORS.blue54BADE,
        fontSize: 12,
        fontFamily: setFontFamily('GilroyMedium')
    },
    icon: {
        resizeMode: 'contain',
        width: 80,
        height: 150,
    },
    vIm: {
        marginLeft: 10,
    }

});

export default ItemBookView;
