import * as api from '../../../carebox/services/Request'
import ApiConfig from '../../../carebox/services/api_config'
import { MoreModelList } from '../../../carebox/models/more_model'
import { BookModelList } from '../../../carebox/models/book_model'
import { CourseCompleteModelList } from '../../../carebox/models/course_complete_model'

export default class AccountApiClient {

  async getMore()  {
    const res = await api.methodGet({api: ApiConfig.more});
    if (res['success']) {
      return MoreModelList.fromJson(res['data']['values']).list;
    }
    return null;
  }


  async getBook(isAdmin)  {
    const query = {}
    if(isAdmin) {
      query['user_role'] = '1'; 
    }
    const res = await api.methodGet({api: ApiConfig.book, queries: query });
    if (res['success']) {
      return BookModelList.fromJson(res['data']['values']).list;
    }
    return null;
  }

  async getCourseCompleted()  {
    const res = await api.methodGet({api: ApiConfig.courseComplete});
    if (res['success']) {
      return CourseCompleteModelList.fromJson(res['data']).list;
    }
    return null;
  }


}
