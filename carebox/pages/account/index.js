import React from 'react';
import {
  Text,
  View,
  Platform,
  ScrollView,
  StyleSheet,
  Linking,
  FlatList,
  SafeAreaView,
  RefreshControl
} from 'react-native';
import ItemView from './views/item_view'
import HeaderView from './views/header_view'
import ItemBookView from './views/item_book_view'
import ItemCourseView from './views/item_course_view'
import { COLORS } from '../../../carebox/utils/colors';
import { setFontFamily } from '../../../carebox/utils/functions';
import {
  IconArchive,
  IconMess,
  Icon99,
  IconGuard,
  IconHotline,
} from '../../../carebox/assets/icons'
import { NavigationService } from '../../../carebox/navigation/NavigationService';
import { ROUTER_NAME } from '../../../carebox/navigation/RouterName';
import AccountNotifier from './account_notifier'
import ApiConfig from '../../../carebox/services/api_config'
import { profileSelector } from '../../../carebox/redux/selectors';
import { useSelector } from 'react-redux';

const AccountView = () => {
  const { 
    mores, 
    books, 
    courses,
    onRefresh,
    refreshing,
    onJoin,
  } = AccountNotifier();

  const gotoNewFavorite = () => {
    NavigationService.push(ROUTER_NAME.NewFavoriteScreen);
  }
  const gotoQuestion = () => {
    NavigationService.push(ROUTER_NAME.QuestionScreen);
  }

  const gotoPrivacy = () => {
    NavigationService.push(ROUTER_NAME.PrivacyScreen, { model: privacy() });
  }

  const gotoTerm = () => {
    NavigationService.push(ROUTER_NAME.PrivacyScreen, { model: term() });
  }

  const phoneContact = () => {
    const data = mores.filter((item) => {
      return item.moreType == ApiConfig.hotline;
    })
    if (data.length > 0) {
      return data[0].moreContent;
    }
    return '';
  }

  const term = () => {
    const data = mores.filter((item) => {
      return item.moreType == ApiConfig.term;
    })
    if (data.length > 0) {
      return data[0];
    }
    return null;
  }

  const privacy = () => {
    const data = mores.filter((item) => {
      return item.moreType == ApiConfig.privacy;
    })
    if (data.length > 0) {
      return data[0];
    }
    return null;
  }

  const callContact = () => {
    const phone = phoneContact();
    let phoneNumber = phone;
    if (Platform.OS !== 'android') {
      phoneNumber = `telprompt:${phone}`;
    }
    else {
      phoneNumber = `tel:${phone}`;
    }
    Linking.openURL(phoneNumber)
  }

  const renderItemBook = ({ item, index }) => {
    return <ItemBookView
      item={item}
      onJoin = {onJoin}
    />
  }

  const renderItemCourse = ({ item, index }) => {
    return <ItemCourseView
      item={item}
      index={index}
    />
  }

  return (
    <SafeAreaView style={styles.body}>
      <ScrollView
       refreshControl={
        <RefreshControl
          refreshing={refreshing}
          onRefresh={onRefresh}
        />}
      
      >
        <HeaderView />

        {courses.length > 0 && <View style={styles.vCourse}>
          <Text style={styles.titleCourse}>Chứng nhận khóa học hoàn thành</Text>
          <FlatList
            data={courses}
            renderItem={renderItemCourse}
            horizontal
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item, index) => `${index}`}
          />
        </View>}

        {books.length > 0 && <View style={styles.vbook}>
          <FlatList
            data={books}
            renderItem={renderItemBook}
            horizontal
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item, index) => `${index}`}
          />
        </View>}

        <ItemView
          title='Bài viết đã lưu'
          icon={IconArchive}
          subTitle='Tin tức, Tin nội bộ...'
          onClick={gotoNewFavorite}
        />

        <ItemView
          title='Câu hỏi thường gặp'
          icon={IconMess}
          subTitle='Hướng dẫn sử dụng'
          onClick={gotoQuestion} />

        <ItemView
          title='Điều khoản sử dụng'
          icon={Icon99}
          subTitle='Quy định và Chính sách của Công ty'
          onClick={gotoTerm} />

        <ItemView
          title='Chính sách bảo mật'
          icon={IconGuard}
          subTitle='Bảo mật và Kiến nghị'
          onClick={gotoPrivacy} />
        <ItemView
          title='Hotline'
          icon={IconHotline}
          onClick={callContact}
          subTitle={phoneContact()} />

        <View style={styles.vBottom} />
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  body: {
    backgroundColor: COLORS.white,
  },
  vBottom: {
    height: 50,
  },
  vbook: {
    width: '100%',
    height: 150,
    marginLeft: 15,
    paddingRight: 15,
  },
  vCourse: {
    width: '100%',
    height: 180,
    marginLeft: 15,
    paddingRight: 15,
  },
  titleCourse: {
    color: COLORS.black,
    fontWeight: 'bold',
  }
});

export default AccountView;
