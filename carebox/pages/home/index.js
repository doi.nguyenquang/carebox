/* eslint-disable react-hooks/exhaustive-deps */
import {NavigationService} from '../../../carebox/navigation/NavigationService';
import {ROUTER_NAME} from '../../../carebox/navigation/RouterName';
import React, {useMemo, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  StatusBar,
  ScrollView,
  RefreshControl,
  TouchableOpacity,
} from 'react-native';
import styles from './styles';
import {
  ProfileHeader,
  ConsultFormalityComponent,
  SliderBannerComponent,
  StaffComponent,
  FastNewsComponent,
  CourseComponent,
} from './component';
import {IconCreate} from '../../../carebox/assets/icons';
import {useDispatch, useSelector} from 'react-redux';
import {homeActions} from './home-redux-saga/home-slice';
import {categoryActions} from '../../../carebox/redux/slices/category-slice';
import {moreActions} from '../../../carebox/redux/slices/more-slice';
import {homeSelectors} from '../../../carebox/redux/selectors';
import {profileSelector} from '../../../carebox/redux/selectors';
import {categoryType} from '../../../carebox/utils/constants';
import AppLoading from '../../../carebox/components/app-loading';
import {isEmpty} from 'lodash';

const HomeContainer = () => {
  const dispatch = useDispatch();

  const getHomeDataLoading = useSelector(homeSelectors.getHomeDataLoading);
  const homeData = useSelector(homeSelectors.homeData);
  const listStaff = useSelector(homeSelectors.listStaff);
  const userCheckInStatus = useSelector(homeSelectors.userCheckInStatus);
  const profileData = useSelector(profileSelector.profileData);

  const lastName =
    profileData?.fullName?.split(' ')[
      profileData?.fullName?.split(' ')?.length - 1
    ];

  const getHomeData = () => {
    dispatch(homeActions.getHomeDataRequest());
    dispatch(homeActions.getStaffHomeDataRequest());
    dispatch(homeActions.getUserCheckInRequest());
  };

  const getConfigData = () => {
    const params = {
      type: categoryType.courseAndQuiz,
      ids: '',
      limit: '',
    };
    dispatch(categoryActions.getAllRequest(params));
    dispatch(moreActions.getMoreRequest());
  };

  useEffect(() => {
    getHomeData();
    getConfigData();
  }, []);

  const handleRefresh = () => {
    getHomeData();
  };

  const floatButtonOnClick = () => {
    NavigationService.push(ROUTER_NAME.SetupConsultScreen);
    // NavigationService.push(ROUTER_NAME.CallScreen, { userId: 19, receiverId: 18 })
  };

  const currentUserStatus = useMemo(() => {
    if (
      userCheckInStatus.statusCode === 200 &&
      userCheckInStatus.data.length > 0
    ) {
      return userCheckInStatus.data[0];
    } else {
      return null;
    }
  }, [userCheckInStatus]);

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="transparent" barStyle={'dark-content'} />
      <TouchableOpacity
        style={styles.floatButtonContainer}
        onPress={floatButtonOnClick}>
        <Image source={IconCreate} style={styles.floatButtonStyles} />
      </TouchableOpacity>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={false} onRefresh={handleRefresh} />
        }
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.contentContainer}>
        <ProfileHeader
          name={profileData?.fullName}
          avatar={profileData?.pictureUrl}
        />
        {!isEmpty(currentUserStatus) && (
          <View style={styles.checkInStatusContainer}>
            <View style={styles.checkInStatusContent}>
              <Image
                source={{uri: currentUserStatus.checkin.checkin_icon}}
                style={styles.checkInIconStyles}
                resizeMode={'stretch'}
              />
              <Text style={styles.userCheckInTitle} numberOfLines={1}>
                {`Hôm nay ${lastName} thấy `}
                <Text style={styles.statusText}>
                  {currentUserStatus.checkin.checkin_title}
                </Text>
              </Text>
            </View>
          </View>
        )}
        <ConsultFormalityComponent />
        <SliderBannerComponent data={homeData?.banners || []} />
        <StaffComponent data={listStaff || []} />
        <CourseComponent data={homeData?.course_indexs || []} />
        <FastNewsComponent data={homeData?.news || []} />
      </ScrollView>

      <AppLoading isVisible={getHomeDataLoading} />
    </View>
  );
};

export default HomeContainer;
