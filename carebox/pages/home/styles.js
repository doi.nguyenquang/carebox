import {StyleSheet} from 'react-native';
import {COLORS} from '../../../carebox/utils/colors';
import {fontPixel} from '../../../carebox/utils/sizes';
import {setFontFamily} from '../../../carebox/utils/functions';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.bg,
  },
  contentContainer: {
    paddingHorizontal: 16,
    paddingBottom: 32,
  },
  floatButtonStyles: {
    width: fontPixel(60),
    height: fontPixel(60),
  },
  floatButtonContainer: {
    position: 'absolute',
    bottom: 10,
    right: 20,
    zIndex: 2,
  },
  checkInStatusContainer: {
    height: fontPixel(45),
    borderWidth: 1,
    marginTop: 16,
    borderRadius: 12,
    backgroundColor: COLORS.white,
    borderColor: COLORS.greyE5E5E5,
    alignItems: 'center',
    paddingHorizontal: fontPixel(10),
  },
  checkInIconStyles: {
    height: fontPixel(36),
    width: fontPixel(36),
  },
  checkInStatusContent: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    width: '100%',
  },
  userCheckInTitle: {
    flex: 1,
    marginLeft: 10,
    fontSize: fontPixel(16),
    fontFamily: setFontFamily('GilroyMedium'),
  },
  statusText: {
    fontFamily: setFontFamily('GilroySemiBold'),
    color: COLORS.orangeF58220,
  },
});

export default styles;
