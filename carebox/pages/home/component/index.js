import { ProfileHeader } from './profile-header';
import { ConsultFormalityComponent } from './consult-formality';
import { SliderBannerComponent } from './slider-banner';
import { StaffComponent } from './staff';
import { FastNewsComponent } from './fast-news';
import { CourseComponent } from './course'

export {
  ProfileHeader,
  ConsultFormalityComponent,
  SliderBannerComponent,
  StaffComponent,
  CourseComponent,
  FastNewsComponent,
}