import React, { useCallback } from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity, FlatList } from 'react-native';
import { COLORS } from '../../../../../carebox/utils/colors';
import { fontPixel, SIZE } from '../../../../../carebox/utils/sizes';
import { setFontFamily } from '../../../../../carebox/utils/functions';
import CardView from 'react-native-cardview';
import { TitleCardComponent } from '../../../../../carebox/components/app-title-card';
import { NavigationService } from '../../../../../carebox/navigation/NavigationService';
import { ROUTER_NAME } from '../../../../../carebox/navigation/RouterName';
import { CourseModel } from '../../../../../carebox/screens/course/course_model'

export const CourseComponent = (props) => {
  const { data } = props;

  const getStylesItem = (index) => {
    if (data && data.length > 1) {
      return { marginRight: index === data.length - 1 ? 0 : 16 }
    } else {
      return { marginRight: 0 }
    }
  }

  const calPercentCourse = (courseItem) => {
    if (courseItem?.course_index_count > 0) {
      return Math.floor((courseItem?.course_user_count / courseItem?.course_index_count) * 100)
    } else {
      return 0
    }
  }

  const getPercentCourse = (courseItem) => {
    return calPercentCourse(courseItem) + '%'
  }

  const getPercentBacground = (courseItem) => {
    return {
      width: calPercentCourse(courseItem) + '%'
    }
  }

  const onCourseDetail = (item) => {
    console.log('onCourseDetail', item);
    NavigationService.push(ROUTER_NAME.CourseDetailScreen, { model: CourseModel.fromJson(item) });
  }

  const renderItem = (item, index) => {
    const courseData = item?.course
    return (
      <TouchableOpacity
        onPress={() => onCourseDetail(courseData)}
      >
        <CardView
          cardElevation={2}
          cardMaxElevation={2}
          cornerRadius={16}
          style={[styles.courseItemContainer, getStylesItem(index)]}
        >
          <Image
            style={styles.thumbnailStyles}
            source={{ uri: courseData?.course_image }}
          />
          <View style={styles.rightContainer}>
            <Text style={styles.titleTextStyles} numberOfLines={2}>{courseData?.course_title}</Text>
            <View style={styles.percentCourseContainer}>
              <View style={styles.percenView}>
                <View style={[styles.currentPercent, getPercentBacground(item)]} />
              </View>
              <Text style={styles.percentTextStyle}>{getPercentCourse(item)}</Text>
            </View>
          </View>
        </CardView>

      </TouchableOpacity>
    )
  }
  return (
    <>
      {
        data && data.length > 0 && <View style={styles.container}>
          <TitleCardComponent
            titleText={"Tiếp tục hoạt động " + `(${data.length})`}
            more={false}
          />
          <FlatList
            data={data}
            renderItem={({ item, index }) => renderItem(item, index)}
            horizontal={true}
            contentContainerStyle={{
              marginTop: 20,
              paddingBottom: 4,
            }}
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item) => item?.course_id.toString()}
          />
        </View>
      }</>
  )
}
const styles = StyleSheet.create({
  container: {
    marginTop: 20,
  },
  courseItemContainer: {
    width: SIZE.SCREEN_WIDTH * 0.8,
    height: 'auto',
    backgroundColor: COLORS.white,
    padding: 16,
    flexDirection: 'row',
    alignItems: 'center',
  },
  thumbnailStyles: {
    height: fontPixel(82),
    width: '40%',
    borderRadius: 8
  },
  rightContainer: {
    flex: 1,
    height: '100%',
    marginLeft: 16,
  },
  percentCourseContainer: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10
  },
  percenView: {
    flex: 1,
    height: 10,
    borderRadius: 5,
    backgroundColor: COLORS.greyE9E9E9
  },
  currentPercent: {
    height: '100%',
    borderRadius: 5,
    backgroundColor: COLORS.green3
  },
  percentTextStyle: {
    marginLeft: 10,
    fontSize: fontPixel(14),
    color: COLORS.black303030,
    fontFamily: setFontFamily('GilroyMedium'),
  },
  titleTextStyles: {
    fontSize: fontPixel(14),
    color: COLORS.main,
    fontFamily: setFontFamily('GilroyMedium'),
  }
})