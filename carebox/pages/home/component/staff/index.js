import React, { useState, useEffect, useMemo } from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native';
import { COLORS } from '../../../../../carebox/utils/colors';
import { fontPixel, SIZE } from '../../../../../carebox/utils/sizes';
import { setFontFamily } from '../../../../../carebox/utils/functions';
import CardView from 'react-native-cardview';
import { TitleCardComponent } from '../../../../../carebox/components/app-title-card';
import AppModal from '../../../../../carebox/components/app-modal';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { UserAvatarDefault } from '../../../../../carebox/assets/icons';
import { isEmpty } from 'lodash'
import { useDispatch, useSelector } from 'react-redux';
import { homeActions } from '../../home-redux-saga/home-slice';
import { homeSelectors } from '../../../../../carebox/redux/selectors';
import { UIActivityIndicator } from 'react-native-indicators';
import FastImage from 'react-native-fast-image';

export const StaffComponent = (props) => {
  const { data } = props;
  const dispatch = useDispatch();
  const [showStaffDetailModal, setShowStaffDetailModal] = useState(false);
  const [staffId, setStaffId] = useState('');
  const getStaffDetailLoading = useSelector(homeSelectors.getStaffDetailLoading);
  const staffDetailData = useSelector(homeSelectors.staffDetailData)

  const staffItemOnclick = (item) => {
    setStaffId(item?.id)
    setShowStaffDetailModal(true);
  }

  useEffect(() => {
    if (showStaffDetailModal) {
      dispatch(homeActions.getStaffDetailRequest(staffId))
    }
  }, [showStaffDetailModal, staffId])

  const staffDetail = useMemo(() => {
    if (staffDetailData.statusCode === 200) {
      return staffDetailData.data;
    }
  }, [staffDetailData])

  const onStaffDetailModalHide = () => {
    setShowStaffDetailModal(false);
  }

  const renderModalDetail = () => {
    return (
      <AppModal
        isVisible={showStaffDetailModal}
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onModalHide={onStaffDetailModalHide}
      >
        <SafeAreaProvider style={{
          flex: 1,
          width: SIZE.SCREEN_WIDTH,
          backgroundColor: COLORS.white,
        }}>
          {
            getStaffDetailLoading ? <View style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
              <View style={{ height: 50, width: 50 }}>
                <UIActivityIndicator color={COLORS.loading} />
              </View>
              <Text style={styles.textTitleLoading}>Đang tải...</Text>
            </View> :
              <>
                <FastImage
                  source={!isEmpty(staffDetail?.pictureUrl) ? { uri: staffDetail?.pictureUrl } : UserAvatarDefault}
                  style={{
                    height: SIZE.SCREEN_HEIGHT * 0.4,
                    width: '100%'
                  }} />
                <SafeAreaView style={{ flex: 1 }}>
                  <View style={{
                    flex: 1.2,
                    backgroundColor: COLORS.white,
                    borderTopLeftRadius: 25,
                    borderTopRightRadius: 25,
                    marginTop: -20,
                    paddingHorizontal: 16,
                    paddingTop: 20
                  }}>
                    <Text style={styles.staffNameStyles}>{staffDetail?.fullName}</Text>
                    <ScrollView showsVerticalScrollIndicator={false}>
                      <Text style={styles.desTextStyles}>{staffDetail?.note}</Text>
                    </ScrollView>
                  </View>
                  <TouchableOpacity
                    style={styles.styleBtn}
                    onPress={onStaffDetailModalHide}
                  >
                    <Text style={styles.buttonText}>Đóng</Text>
                  </TouchableOpacity>
                </SafeAreaView>
              </>
          }
        </SafeAreaProvider>
      </AppModal>
    )
  }

  const setTitleStaff = (item) => {
    if(item?.departments && item.departments.length > 0) {
      return item.departments[0]?.title
    } else {
      return ''
    }
  }

  return (
    <View style={styles.container}>
      <TitleCardComponent
        titleText={"Chuyên gia hàng đầu"}
        more={false}
      />
      <Text style={styles.subTitleStyle} numberOfLines={1}>Đội ngũ chuyên gia tại Hưng Thịnh CareBOX</Text>
      {
        data.map((item, index) => {
          if (index < 3) {
            return (
              <CardView
                cardElevation={2}
                cardMaxElevation={2}
                cornerRadius={16}
                key={index}
                style={styles.staffItemView}
              >
                <Image
                  style={styles.userAvatarStyles}
                  source={!isEmpty(item?.pictureUrl) ? { uri: item?.pictureUrl } : UserAvatarDefault} />
                <View style={styles.staffContentBox}>
                  <View style={{ flex: 1 }}>
                    <Text style={styles.staffNameStyles} numberOfLines={1}>{item?.fullName}</Text>
                    <Text style={styles.staffPositionStyles} numberOfLines={2}>{setTitleStaff(item)}</Text>
                  </View>
                  <TouchableOpacity
                    style={styles.btnViewDetailStyle}
                    onPress={() => staffItemOnclick(item)}
                  >
                    <Text style={styles.btnViewDetailText}>Xem chi tiết</Text>
                  </TouchableOpacity>
                </View>
              </CardView>
            )
          }
        })
      }
      {
        renderModalDetail()
      }

    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
  },
  subTitleStyle: {
    width: '100%',
    marginTop: 4,
    fontSize: fontPixel(14),
    fontFamily: setFontFamily('GilroyLight')
  },
  staffItemView: {
    width: '100%',
    // height: fontPixel(132),
    marginTop: 16,
    backgroundColor: COLORS.white,
    flexDirection: 'row',
    alignItems: 'center',
    padding: fontPixel(16),
  },
  userAvatarStyles: {
    height: fontPixel(100),
    width: fontPixel(100),
    borderRadius: 8
  },
  staffContentBox: {
    flex: 1,
    height: '100%',
    marginLeft: 16,
  },
  btnViewDetailStyle: {
    width: '100%',
    height: fontPixel(36),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 12,
    marginTop: 10,
    backgroundColor: COLORS.main
  },
  btnViewDetailText: {
    color: COLORS.white,
    fontSize: fontPixel(14),
    fontFamily: setFontFamily('GilroyMedium')
  },
  staffNameStyles: {
    color: COLORS.green2,
    fontSize: fontPixel(16),
    fontFamily: setFontFamily('GilroySemiBold')
  },
  staffPositionStyles: {
    color: COLORS.black303030,
    fontSize: fontPixel(14),
    fontFamily: setFontFamily('GilroyMedium')
  },
  styleBtn: {
    marginTop: 20,
    height: fontPixel(45),
    marginHorizontal: 16,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 12,
    marginBottom: 20,
    backgroundColor: COLORS.main
  },
  buttonText: {
    fontSize: fontPixel(16),
    fontWeight: 'bold',
    color: COLORS.white
  },
  desTextStyles: {
    textAlign: 'justify',
    marginTop: 16,
    color: COLORS.black303030,
    fontSize: fontPixel(14),
    fontFamily: setFontFamily('GilroyMedium')
  },
  textTitleLoading: {
    marginTop: 10,
    color: COLORS.orangeF58220,
    fontSize: fontPixel(16),
    fontFamily: setFontFamily('GilroyMedium')
  }
})
