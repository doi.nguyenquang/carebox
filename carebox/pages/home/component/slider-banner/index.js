import React, { useState } from 'react';
import { View, StyleSheet, Image, Text } from 'react-native';
import Carousel from 'react-native-snap-carousel';
import { COLORS } from '../../../../../carebox/utils/colors';
import { fontPixel, SIZE } from '../../../../../carebox/utils/sizes';
import { setFontFamily } from '../../../../../carebox/utils/functions';
import CardView from 'react-native-cardview';

export const SliderBannerComponent = (props) => {
  const { data } = props;
  
  const [activeSlideIndex, setActiveSlide] = useState(0);

  const renderItem = ({ item, index }) => {
    return (
      <CardView
        cardElevation={2}
        cardMaxElevation={2}
        cornerRadius={16}
        style={styles.cardSliderContainer}
      >
        <Image
          source={{ uri: item.banner_avatar }}
          resizeMode={'cover'}
          style={styles.imageSliderStyles}
        />
        <View style={styles.contentContainer}>
          <Text
            numberOfLines={2}
            style={styles.contentTextStyles}
          >
            {item.banner_des}
          </Text>
          <Text style={styles.authorTextStyles} numberOfLines={1}>{item.banner_title}</Text>
        </View>

      </CardView>
    )
  }

  const pagination = (activeSlideIndex) => {
    return (
      <View style={styles.pagingContainer}>
        {
          data.map((element, index) => {
            return (
              <View style={[styles.pagingtionStyles, { backgroundColor: activeSlideIndex === index ? COLORS.orangeF58220 : COLORS.gray1, }]} key={index} />
            )
          })
        }
      </View>
    );
  }

  return (
    <View style={styles.slideContainer}>
      <Carousel
        layout={'default'}
        data={data}
        loop={true}
        renderItem={renderItem}
        sliderWidth={SIZE.SCREEN_WIDTH - 32}
        itemWidth={SIZE.SCREEN_WIDTH - 32}
        inactiveSlideOpacity={1}
        onSnapToItem={(index) => setActiveSlide(index)}
      />
      {
        pagination(activeSlideIndex)
      }
    </View>
  )
}

const styles = StyleSheet.create({
  slideContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    marginTop: 20
  },
  pagingContainer: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  cardSliderContainer: {
    width: SIZE.SCREEN_WIDTH - 32,
    borderRadius: 16,
    backgroundColor: COLORS.white,
    marginBottom: 10
  },
  imageSliderStyles: {
    width: '100%',
    height: fontPixel(120),
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16
  },
  contentContainer: {
    flex: 1,
    padding: fontPixel(16),
  },
  contentTextStyles: {
    flex: 1,
    width: '100%',
    fontFamily: setFontFamily('GilroyBold')
  },
  pagingtionStyles: {
    width: 8,
    height: 8,
    marginHorizontal: 4,
    borderRadius: 8
  },
  authorTextStyles: {
    width: '100%',
    textAlign: 'right',
    fontFamily: setFontFamily('GilroyBold')
  }
})
