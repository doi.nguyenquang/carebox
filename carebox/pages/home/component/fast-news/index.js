/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {COLORS} from '../../../../../carebox/utils/colors';
import {fontPixel, SIZE} from '../../../../../carebox/utils/sizes';
import {setFontFamily} from '../../../../../carebox/utils/functions';
import {IconUnFavorite, IconArchive} from '../../../../../carebox/assets/icons';
import {IconImage} from '../../../../../carebox/components/app-image';
import {TitleCardComponent} from '../../../../../carebox/components/app-title-card';
import {NavigationService} from '../../../../../carebox/navigation/NavigationService';
import {ROUTER_NAME} from '../../../../../carebox/navigation/RouterName';
import {NewsModel} from '../../../../../carebox/models/news_model';
import {useDispatch} from 'react-redux';
import {homeActions} from '../../../../../carebox/pages/home/home-redux-saga/home-slice';

export const FastNewsComponent = props => {
  const {data} = props;
  const dispatch = useDispatch();

  const renderItem = (item, index) => {
    const model = NewsModel.fromJson(item);
    const _onDetail = () => {
      NavigationService.push(ROUTER_NAME.NewsScreen, {
        model,
      });
    };
    const _onFavorite = () => {
      dispatch(homeActions.updateFavoriteItemFastNew(item));
    };
    return (
      <TouchableOpacity
        onPress={_onDetail}
        style={[
          styles.newsItemContainer,
          {marginRight: index === data.length - 1 ? 0 : 16},
        ]}>
        <Image
          style={styles.thumbnailStyles}
          source={{uri: item.news_images}}
        />
        <View style={styles.newsTitleContainer}>
          <Text style={styles.titleTextStyles}>{item.news_title}</Text>
        </View>
        {item?.category?.category_title && (
          <View style={styles.categoryContainer}>
            <Text numberOfLines={1} style={styles.categoryTextStyles}>
              {item?.category?.category_title}
            </Text>
          </View>
        )}
        <TouchableOpacity style={styles.tagBtnContainer} onPress={_onFavorite}>
          <IconImage
            source={item?.favorited === 0 ? IconUnFavorite : IconArchive}
            style={styles.iconTagStyles}
          />
        </TouchableOpacity>
      </TouchableOpacity>
    );
  };
  return (
    <View style={styles.container}>
      <TitleCardComponent titleText={'Bản tin nhanh'} more={false} />
      <FlatList
        data={data}
        renderItem={({item, index}) => renderItem(item, index)}
        horizontal={true}
        contentContainerStyle={{
          marginTop: 20,
        }}
        showsHorizontalScrollIndicator={false}
        keyExtractor={item => item.news_id.toString()}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    marginTop: 16,
  },
  newsItemContainer: {
    width: SIZE.SCREEN_WIDTH - 60,
    backgroundColor: COLORS.white,
    borderRadius: 16,
    borderWidth: 1,
    borderColor: COLORS.greyE9E9E9,
    padding: 16,
    alignItems: 'center',
  },
  thumbnailStyles: {
    height: fontPixel(180),
    width: '100%',
    borderRadius: 8,
  },
  newsTitleContainer: {
    flex: 1,
    width: '100%',
    paddingTop: 16,
    marginBottom: 20,
  },
  titleTextStyles: {
    color: COLORS.black303030,
    fontSize: fontPixel(16),
    fontFamily: setFontFamily('GilroySemiBold'),
  },
  categoryContainer: {
    position: 'absolute',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: COLORS.blue54BADE,
    bottom: -30,
    borderRadius: 30,
    height: 60,
    paddingHorizontal: 15,
  },
  categoryTextStyles: {
    fontSize: fontPixel(12),
    fontFamily: setFontFamily('GilroyMedium'),
    color: COLORS.white,
    marginTop: 10,
  },
  tagBtnContainer: {
    height: 40,
    width: 40,
    borderRadius: 6,
    position: 'absolute',
    right: 0,
    backgroundColor: COLORS.white,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconTagStyles: {
    height: 32,
    width: 32,
  },
});
