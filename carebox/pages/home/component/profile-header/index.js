import React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import { COLORS } from '../../../../../carebox/utils/colors';
import { fontPixel } from '../../../../../carebox/utils/sizes';
import { IconHealthCheckin } from '../../../../../carebox/assets/icons';
import { setFontFamily } from '../../../../../carebox/utils/functions';
import { NavigationService } from '../../../../../carebox/navigation/NavigationService';
import { ROUTER_NAME } from '../../../../../carebox/navigation/RouterName';
import { getGreetingText } from '../../../../../carebox/utils/utils';

export const ProfileHeader = (props) => {

  const { name, avatar } = props;

  const healthCheckInOnclick = () => {
    NavigationService.navigate(ROUTER_NAME.CheckInScreen)
  }
  return (
    <View style={styles.container}>
      <View style={styles.infoContainer}>
        <Text style={styles.greetingTextStyle}>{getGreetingText()}</Text>
        <Text style={styles.nameStyle}>{name}</Text>
      </View>
      <TouchableOpacity
        style={styles.healthCheckin}
        onPress={healthCheckInOnclick}
      >
        <Image
          style={styles.healthCheckin}
          source={IconHealthCheckin}
          resizeMode={'stretch'}
        />
      </TouchableOpacity>
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: 16,
    alignItems: 'center',
  },
  infoContainer: {
    flex: 1,
    justifyContent: 'center',
    marginRight: 16
  },
  greetingTextStyle: {
    fontSize: fontPixel(16),
    color: COLORS.black303030,
    fontFamily: setFontFamily('GilroyMedium'),
  },
  nameStyle: {
    fontSize: fontPixel(16),
    color: COLORS.green2,
    fontFamily: setFontFamily('GilroyBold'),
  },
  healthCheckin: {
    height: 30,
    width: 30,
  },
  statusActive: {
    width: 11,
    height: 11,
    borderRadius: 5,
    position: 'absolute',
    zIndex: 1,
    top: 0,
    right: -2,
    borderColor: COLORS.white,
    borderWidth: 2
  }
});
