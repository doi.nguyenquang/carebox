import React from 'react';
import { View, Text, Platform, StyleSheet, TouchableOpacity } from 'react-native';
import { IconImage } from '../../../../../carebox/components/app-image'
import { COLORS } from '../../../../../carebox/utils/colors';
import { fontPixel } from '../../../../../carebox/utils/sizes';
import { IconCheckBoxSelected, IconCheckBoxUnSelected } from '../../../../../carebox/assets/icons'
import { setFontFamily } from '../../../../../carebox/utils/functions';

export const CheckBoxView = (props) => {
  const { listCheckBoxData, itemOnclick } = props;
  return (
    <View style={{minHeight: 200}}>
      {
        listCheckBoxData?.map((item, index) => {
          return (
            <TouchableOpacity
              style={styles.rowCheckbox}
              onPress={() => itemOnclick(item.id)}
              key={index}
            >
              <IconImage
                source={item.isSelected ? IconCheckBoxSelected : IconCheckBoxUnSelected}
                style={[styles.checkboxIconStyles]}
              />
              <Text numberOfLines={2} style={styles.titleStyles}>{item.title}</Text>
            </TouchableOpacity>
          )
        })
      }
    </View>
  )
}

const styles = StyleSheet.create({
  rowCheckbox: {
    flexDirection: 'row',
    marginTop: 20,
    alignItems: 'center',
  },
  checkboxIconStyles: {
    width: 24,
    height: 24,
  },
  titleStyles: {
    flex: 1,
    marginLeft: 16,
    fontSize: fontPixel(14),
    color: COLORS.black303030,
    fontFamily: setFontFamily('GilroyMedium'),
  },
})