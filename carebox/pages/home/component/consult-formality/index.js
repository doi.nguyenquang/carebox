/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-fallthrough */
/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useMemo, useCallback, useEffect} from 'react';
import {View, Image, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {
  IconCalendar,
  IconConsultMes,
  IconConsultLive,
  IconConsultVideo,
} from '../../../../../carebox/assets/icons';
import {COLORS} from '../../../../../carebox/utils/colors';
import {fontPixel, SIZE} from '../../../../../carebox/utils/sizes';
import {IconImage} from '../../../../../carebox/components/app-image';
import {setFontFamily} from '../../../../../carebox/utils/functions';
import DateTimePicker from 'react-native-modal-datetime-picker';
import {convertDayVi, showToastMessage} from '../../../../../carebox/utils/utils';
import moment from 'moment';
import {RadioAddressComponent} from './radio-address';
import {NavigationService} from '../../../../../carebox/navigation/NavigationService';
import {ROUTER_NAME} from '../../../../../carebox/navigation/RouterName';
import {useDispatch, useSelector} from 'react-redux';
import {
  moreSelector,
  homeSelectors,
  categorySelector,
} from '../../../../../carebox/redux/selectors';
import {IssueSelectView} from './issue-select-view';
import {homeActions} from '../../home-redux-saga/home-slice';
import {formatDate, YYYYMMDD} from '../../../../../carebox/utils/times';
import {consultMethods} from '../../../../../carebox/utils/constants';

export const ConsultFormalityComponent = () => {
  const dispatch = useDispatch();

  const listSuggestionIssue = useSelector(categorySelector.listSuggestionIssue);
  const listMoreData = useSelector(moreSelector.listMoreData);
  const listIssue = useSelector(homeSelectors.listIssueSelect);

  const [currentConsultMethod, setCurrentConsultMethod] = useState('');
  const [visibleDateTimePicker, setVisibleDateTimePicker] = useState(false);
  const [listAddressSelect, setListAddressSelect] = useState([]);
  const [date, setDate] = useState(null);

  useEffect(() => {
    if (listSuggestionIssue) {
      const listCheckboxData = listSuggestionIssue.map(item => {
        return {
          id: item.category_id,
          title: item.category_title,
          isSelected: false,
        };
      });
      dispatch(homeActions.updateListIssueSelect(listCheckboxData));
    }
  }, [listSuggestionIssue]);

  useEffect(() => {
    if (listMoreData) {
      const moreObject = listMoreData.find(item => item.more_id === 8);
      if (moreObject) {
        const listAddressParse = JSON.parse(moreObject?.more_content) || [];
        const listAddressRadio = listAddressParse.map((item, index) => {
          return {
            id: index + 1,
            title: item.title,
            subTitle: item.sub_title,
            isSelected: false,
          };
        });
        setListAddressSelect(listAddressRadio);
      }
    }
  }, [listMoreData]);

  const disabledBtnNext = useMemo(() => {
    if (currentConsultMethod.length > 0) {
      return false;
    }
    return true;
  }, [currentConsultMethod]);

  const getStyleTextButton = useMemo(() => {
    return {
      color: disabledBtnNext ? COLORS.grey787878 : COLORS.white,
    };
  }, [disabledBtnNext]);

  const getStyleButton = useMemo(() => {
    return {
      backgroundColor: disabledBtnNext ? COLORS.gray1 : COLORS.main,
    };
  }, [disabledBtnNext]);

  const nextBtnOnClick = () => {
    switch (currentConsultMethod) {
      case 'Mes247':
        NavigationService.push(ROUTER_NAME.CHAT);
        break;
      case 'VideoCall':
        if (date !== null) {
          const bookDay = formatDate(date, YYYYMMDD);
          NavigationService.push(ROUTER_NAME.ConsultScheduleScreen, {
            bookDay,
            bookType: 0,
          });
        } else {
          showToastMessage('Bạn chưa chọn thời gian tư vấn');
        }
        break;
      case 'Live':
        const addressSelect = listAddressSelect.find(
          address => address.isSelected,
        );
        if (!date) {
          showToastMessage('Bạn chưa chọn thời gian tư vấn');
          return;
        }
        if (!addressSelect) {
          showToastMessage('Bạn chưa chọn địa điểm tư vấn');
          return;
        }
        const bookDay = formatDate(date, YYYYMMDD);
        NavigationService.push(ROUTER_NAME.ConsultScheduleScreen, {
          addressSelect,
          bookDay,
          bookType: 1,
        });
        break;
    }
  };

  const widthConsulFormalityItem = useMemo(() => {
    return (SIZE.SCREEN_WIDTH - 64) / consultMethods.length;
  }, [consultMethods]);

  const getReSourceItem = (item, key) => {
    switch (item) {
      case 'Mes247':
        return key === 'Icon' ? IconConsultMes : 'Nhắn tin 24/7';
      case 'VideoCall':
        return key === 'Icon' ? IconConsultVideo : 'Video call';
      case 'Live':
        return key === 'Icon' ? IconConsultLive : 'Trực tiếp';
      default:
        return null;
    }
  };

  const getTitleConsultFormalityItemStyle = item => {
    switch (item) {
      case 'Mes247':
        if (item === currentConsultMethod) {
          return {
            color: COLORS.green,
            fontWeight: 'bold',
          };
        }
      case 'VideoCall':
        if (item === currentConsultMethod) {
          return {
            color: COLORS.main,
            fontWeight: 'bold',
          };
        }
      case 'Live':
        if (item === currentConsultMethod) {
          return {
            color: COLORS.redCE3737,
            fontWeight: 'bold',
          };
        }
      default:
        return {};
    }
  };

  const getBottomBoxBoder = item => {
    switch (item) {
      case 'Mes247':
        if (item === currentConsultMethod) {
          return {
            borderColor: COLORS.green,
          };
        }
      case 'VideoCall':
        if (item === currentConsultMethod) {
          return {
            borderColor: COLORS.main,
          };
        }
      case 'Live':
        if (item === currentConsultMethod) {
          return {
            borderColor: COLORS.redCE3737,
          };
        }
      default:
        return {
          borderColor: COLORS.greyE9E9E9,
        };
    }
  };

  const getDateContent = useMemo(() => {
    if (date) {
      return (
        'Thời gian tư vấn: ' +
        convertDayVi(date) +
        ', ' +
        moment(date).format('l')
      );
    }
    return 'Chọn thời gian tư vấn phù hợp';
  }, [currentConsultMethod, date]);

  const getColorDate = useMemo(() => {
    if (date) {
      return {
        color: COLORS.main,
      };
    } else {
      return {
        color: COLORS.black303030,
      };
    }
  }, [currentConsultMethod, date]);

  const renderDateTimePickerView = () => {
    return (
      <TouchableOpacity
        style={styles.dateTimeView}
        onPress={showDateTimePicker}>
        <Text style={[styles.timeValueStyles, getColorDate]} numberOfLines={1}>
          {getDateContent}
        </Text>
        <IconImage source={IconCalendar} style={styles.icCalendarStyles} />
      </TouchableOpacity>
    );
  };

  const addressItemSelect = id => {
    const addressListcheck = listAddressSelect.map(item => {
      return {...item, isSelected: item.id === id};
    });
    setListAddressSelect(addressListcheck);
  };

  const onIssueItemOnClick = id => {
    const listIssueTemp = listIssue.map(item => {
      if (item.id === id) {
        return {...item, isSelected: !item.isSelected};
      } else {
        return {...item};
      }
    });
    dispatch(homeActions.updateListIssueSelect(listIssueTemp));
  };

  const renderConsultFormalityContent = useCallback(() => {
    switch (currentConsultMethod) {
      case 'Mes247':
        return (
          <Text style={styles.contentMes247Style}>
            Nhận tư vấn khẩn cấp từ các chuyên gia hàng đầu. Nội dung tin nhắn
            sẽ tự động xoá sau khi thoát.
          </Text>
        );
      case 'VideoCall':
        return (
          <View>
            <Text style={styles.contentMes247Style}>
              Nhận tư vấn trực tiếp 1-1 thông qua video call. Hoàn toàn riêng tư
              và bảo mật.
            </Text>
            {renderDateTimePickerView()}
          </View>
        );
      case 'Live':
        return (
          <View>
            {renderDateTimePickerView()}
            <Text style={styles.titleStyle}>Địa điểm tư vấn</Text>
            <RadioAddressComponent
              listAddress={listAddressSelect}
              addressItemSelect={addressItemSelect}
            />
          </View>
        );
      default:
        return null;
    }
  }, [currentConsultMethod, date, listAddressSelect]);

  const showDateTimePicker = () => {
    setVisibleDateTimePicker(true);
  };

  const hideDateTimePicker = () => {
    setVisibleDateTimePicker(false);
  };

  const handleDatePicked = dateTime => {
    setDate(dateTime);
    hideDateTimePicker();
  };

  return (
    <View style={styles.container}>
      <DateTimePicker
        isVisible={visibleDateTimePicker}
        onConfirm={handleDatePicked}
        onCancel={hideDateTimePicker}
        date={date || new Date()}
        mode="date"
        minimumDate={new Date()}
      />

      <IssueSelectView
        listIssue={listIssue}
        onCheckBoxItemOnClick={onIssueItemOnClick}
      />

      <Text style={styles.titleStyle}>Hình thức tư vấn</Text>
      <Text style={styles.subTitleStyle}>
        Chọn phương thức tư vấn để tiếp tục
      </Text>
      <View style={styles.consultFormalityView}>
        {consultMethods.map((item, index) => {
          return (
            <TouchableOpacity
              key={index}
              style={[
                styles.consultFormalityContainer,
                {
                  width: widthConsulFormalityItem,
                  marginRight: index === consultMethods.length - 1 ? 0 : 16,
                },
              ]}
              onPress={() => setCurrentConsultMethod(item)}>
              <Image
                resizeMode={'contain'}
                source={getReSourceItem(item, 'Icon')}
                style={styles.consultFormalityIcon}
              />
              <View style={[styles.bottomBox, getBottomBoxBoder(item)]}>
                <Text
                  style={[
                    styles.titleConsultFormalityItem,
                    getTitleConsultFormalityItemStyle(item),
                  ]}
                  numberOfLines={1}>
                  {getReSourceItem(item, 'Text')}
                </Text>
              </View>
            </TouchableOpacity>
          );
        })}
      </View>
      {renderConsultFormalityContent()}
      <TouchableOpacity
        style={[styles.styleBtn, getStyleButton]}
        disabled={disabledBtnNext}
        onPress={nextBtnOnClick}>
        <Text style={[styles.buttonText, getStyleTextButton]}>Tiếp tục</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
  },
  titleStyle: {
    color: COLORS.black303030,
    fontWeight: 'bold',
    marginTop: 20,
    fontSize: fontPixel(16),
    fontFamily: setFontFamily('GilroyRegular'),
  },
  subTitleStyle: {
    fontSize: fontPixel(14),
    opacity: 0.5,
    fontFamily: setFontFamily('GilroyRegular'),
  },
  styleBtn: {
    marginTop: 20,
    height: fontPixel(44),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 12,
  },
  buttonText: {
    fontSize: fontPixel(16),
    fontWeight: 'bold',
    fontFamily: setFontFamily('GilroyRegular'),
  },
  consultFormalityView: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 20,
  },
  consultFormalityContainer: {
    height: fontPixel(100),
    alignItems: 'center',
  },
  bottomBox: {
    width: '100%',
    height: fontPixel(70),
    borderWidth: 1,
    bottom: 0,
    position: 'absolute',
    borderRadius: 12,
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingBottom: fontPixel(4),
    backgroundColor: COLORS.white,
  },
  consultFormalityIcon: {
    height: fontPixel(70),
    width: fontPixel(58),
    zIndex: 1,
  },
  titleConsultFormalityItem: {
    fontSize: fontPixel(14),
    lineHeight: 20,
    textAlign: 'center',
    fontFamily: setFontFamily('GilroyLight'),
  },
  contentMes247Style: {
    width: '100%',
    fontSize: fontPixel(14),
    marginTop: 16,
    fontFamily: setFontFamily('GilroyLight'),
  },
  dateTimeView: {
    flexDirection: 'row',
    height: fontPixel(45),
    borderRadius: 12,
    backgroundColor: COLORS.white,
    marginTop: 20,
    borderColor: COLORS.main,
    borderWidth: 1,
    alignItems: 'center',
    paddingHorizontal: 12,
  },
  icCalendarStyles: {
    width: 24,
    height: 24,
  },
  timeValueStyles: {
    flex: 1,
    fontSize: fontPixel(14),
    fontFamily: setFontFamily('GilroyRegular'),
  },
});
