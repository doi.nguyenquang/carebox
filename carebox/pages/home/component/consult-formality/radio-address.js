import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { IconImage } from '../../../../../carebox/components/app-image'
import { COLORS } from '../../../../../carebox/utils/colors';
import { fontPixel } from '../../../../../carebox/utils/sizes';
import { IconUnSelected, IconSelected } from '../../../../../carebox/assets/icons'
import { setFontFamily } from '../../../../../carebox/utils/functions';

export const RadioAddressComponent = (props) => {
  const { listAddress, addressItemSelect } = props;
  return (
    <View>
      {
        listAddress?.map((item, index) => {
          return (
            <TouchableOpacity style={styles.rowCheckbox} onPress={() => addressItemSelect(item.id)} key={index}>
              <IconImage source={item.isSelected ? IconSelected : IconUnSelected} style={styles.radioIconStyles} />
              <View style={styles.radioBoxContent}>
                <Text numberOfLines={1} style={styles.titleStyles}>{item.title}</Text>
                <Text numberOfLines={2} style={styles.subTitleStyles}>{item.subTitle}</Text>
              </View>
            </TouchableOpacity>
          )
        })
      }
    </View>
  )
}

const styles = StyleSheet.create({
  rowCheckbox: {
    flexDirection: 'row',
    marginTop: 20,
    alignItems: 'center',
  },
  radioIconStyles: {
    width: 24,
    height: 24,
  },
  radioBoxContent: {
    flex: 1,
    marginLeft: 16
  },
  titleStyles: {
    fontSize: fontPixel(14),
    color: COLORS.black303030,
    fontFamily: setFontFamily('GilroySemiBold'),
  },
  subTitleStyles: {
    marginTop: 8,
    fontSize: fontPixel(14),
    color: COLORS.black303030,
    fontFamily: setFontFamily('GilroyLight'),
  }
})