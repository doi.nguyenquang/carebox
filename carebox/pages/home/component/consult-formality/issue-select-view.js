import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { CheckBoxView } from './check-box-view';
import { IconNext } from '../../../../../carebox/assets/icons';
import { IconImage } from '../../../../../carebox/components/app-image';
import { fontPixel } from '../../../../../carebox/utils/sizes';
import { COLORS } from '../../../../../carebox/utils/colors';
import { setFontFamily } from '../../../../../carebox/utils/functions';

export const IssueSelectView = (props) => {
  const { listIssue, onCheckBoxItemOnClick } = props;

  const [openCbIssue, setShowBbIssue] = React.useState(false)

  const cbIssueOnClick = () => {
    setShowBbIssue(!openCbIssue)
  }

  const getTitleIssueButton = React.useMemo(() => {
    const listIssueSelected = listIssue?.filter(issue => issue.isSelected)
    if (listIssueSelected?.length > 0) {
      let title = ''
      listIssueSelected.map((item, index) => {
        title += (index === 0 ? '' : ', ') + item.title
      })
      return title
    } else {
      return 'Chọn vấn đề cần tư vấn'
    }
  }, [listIssue])

  const getTextSeclectionStyles = React.useMemo(() => {
    const listIssueSelected = listIssue?.filter(issue => issue.isSelected)
    if (listIssueSelected?.length > 0) {
      return {
        color: COLORS.green3, 
      }
    } else {
      return {
        color: COLORS.black, 
      }
    }
  }, [listIssue])

  return (
    <View>
      <TouchableOpacity style={styles.cbIssueContainer} onPress={cbIssueOnClick}>
        <Text style={[styles.textSelectionStyle, getTextSeclectionStyles]} numberOfLines={1}>{getTitleIssueButton}</Text>
        <IconImage source={IconNext} style={styles.iconStyles} />
      </TouchableOpacity>

      {
        openCbIssue &&
        <View style={styles.listIssueContainer}>
          <CheckBoxView
            listCheckBoxData={listIssue}
            itemOnclick={onCheckBoxItemOnClick}
          />
        </View>
      }
    </View>
  )
}

const styles = StyleSheet.create({
  cbIssueContainer: {
    height: fontPixel(45),
    width: '100%',
    borderColor: COLORS.green3,
    borderWidth: 1,
    borderRadius: 12,
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 16
  },
  textSelectionStyle: {
    flex: 1,
    marginRight: 16,
    fontSize: fontPixel(14),
    fontFamily: setFontFamily('GilroyMedium')
  },
  iconStyles: {
    transform: [{ rotate: '90deg' }],
    width: fontPixel(20),
    height: fontPixel(20)
  },
  listIssueContainer: {
    borderRadius: 16,
    borderColor: COLORS.main,
    borderWidth: 1,
    marginTop: 8,
    paddingBottom: 16,
    paddingHorizontal: 16,
  },
})