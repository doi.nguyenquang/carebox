import {createSlice} from '@reduxjs/toolkit';
import {cloneDeep, set} from 'lodash';

const initialState = {
  getHomeDataLoading: false,
  homeData: {},
  listStaff: [],
  listIssueSelect: [],

  isBookingLoading: false,
  bookScheduleConsultSuccess: false,
  bookScheduleConsultError: false,

  getListCheckInLoading: false,
  listCheckInData: [],
  getListCheckInError: {
    status: false,
    error: {},
  },
  getUserCheckInOfMonthLoading: false,
  listUserCheckInOfMonthData: [],
  getUserCheckInOfMonthError: {
    status: false,
    error: {},
  },

  checkInLoading: false,
  checkInResult: {
    isSuccess: false,
    statusCode: -1,
  },

  getUserCheckInLoading: false,
  userCheckInStatus: {
    data: [],
    statusCode: -1,
  },

  getStaffDetailLoading: false,
  staffDetailData: {
    data: null,
    statusCode: -1,
  },

  updateFavoriteLoading: false,
};

const homeSlice = createSlice({
  name: 'home',
  initialState,
  reducers: {
    getHomeDataRequest: (state, action) => {
      return {
        ...state,
        getHomeDataLoading: true,
        homeData: {},
      };
    },
    getHomeDataSuccess: (state, action) => {
      return {
        ...state,
        getHomeDataLoading: false,
        homeData: action.payload,
      };
    },
    getHomeDataError: (state, action) => {
      return {
        ...state,
        getHomeDataLoading: false,
        homeData: {},
      };
    },
    getStaffHomeDataRequest: (state, action) => {
      return {
        ...state,
        listStaff: [],
      };
    },
    getStaffDataSuccess: (state, action) => {
      return {
        ...state,
        listStaff: action.payload,
      };
    },
    updateListIssueSelect: (state, action) => {
      return {
        ...state,
        listIssueSelect: action.payload,
      };
    },
    bookConsultScheduleRequest: (state, action) => {
      return {
        ...state,
        isBookingLoading: true,
        bookScheduleConsultSuccess: false,
      };
    },
    bookConsultScheduleSuccess: (state, action) => {
      return {
        ...state,
        isBookingLoading: false,
        bookScheduleConsultSuccess: true,
        bookScheduleConsultFalse: false,
      };
    },
    bookConsultScheduleFailed: (state, action) => {
      return {
        ...state,
        isBookingLoading: false,
        bookScheduleConsultSuccess: false,
        bookScheduleConsultError: true,
      };
    },
    resetBookingData: (state, action) => {
      return {
        ...state,
        isBookingLoading: false,
        bookScheduleConsultSuccess: false,
        bookScheduleConsultError: false,
      };
    },
    getListCheckInRequest: state => {
      return {
        ...state,
        getListCheckInLoading: true,
        listCheckInData: [],
        getListCheckInError: {
          status: false,
          error: {},
        },
      };
    },
    getListCheckInSuccess: (state, action) => {
      return {
        ...state,
        getListCheckInLoading: false,
        listCheckInData: action.payload,
        getListCheckInError: {
          status: false,
          error: {},
        },
      };
    },
    getListCheckInFailure: (state, action) => {
      return {
        ...state,
        getListCheckInLoading: false,
        listCheckInData: [],
        getListCheckInError: {
          status: true,
          error: action.payload,
        },
      };
    },

    getUserCheckInOfMonth: state => {
      return {
        ...state,
        getUserCheckInOfMonthLoading: true,
        listUserCheckInOfMonthData: [],
        getUserCheckInOfMonthError: {
          status: false,
          error: {},
        },
      };
    },
    getUserCheckInOfMonthSuccess: (state, action) => {
      return {
        ...state,
        getUserCheckInOfMonthLoading: false,
        listUserCheckInOfMonthData: action.payload,
        getUserCheckInOfMonthError: {
          status: false,
          error: {},
        },
      };
    },
    getUserCheckInOfMonthFailure: (state, action) => {
      return {
        ...state,
        getUserCheckInOfMonthLoading: false,
        listUserCheckInOfMonthData: [],
        getUserCheckInOfMonthError: {
          status: true,
          error: action.payload,
        },
      };
    },

    userCheckInRequest: state => {
      return {
        ...state,
        checkInLoading: true,
        checkInResult: {
          isSuccess: false,
          statusCode: -1,
        },
      };
    },
    userCheckInSuccess: state => {
      return {
        ...state,
        checkInLoading: false,
        checkInResult: {
          isSuccess: true,
          statusCode: 200,
        },
      };
    },
    userCheckInFailure: state => {
      return {
        ...state,
        checkInLoading: false,
        checkInResult: {
          isSuccess: false,
          statusCode: 500,
        },
      };
    },
    resetCheckInState: state => {
      return {
        ...state,
        checkInLoading: false,
        checkInResult: {
          isSuccess: false,
          statusCode: -1,
        },
      };
    },
    getUserCheckInRequest: state => {
      return {
        ...state,
        getUserCheckInLoading: true,
        userCheckInStatus: {
          data: [],
          statusCode: -1,
        },
      };
    },
    getUserCheckInSuccess: (state, action) => {
      return {
        ...state,
        getUserCheckInLoading: true,
        userCheckInStatus: {
          data: action.payload,
          statusCode: 200,
        },
      };
    },
    getUserCheckInFailure: state => {
      return {
        ...state,
        getUserCheckInLoading: true,
        userCheckInStatus: {
          data: [],
          statusCode: 500,
        },
      };
    },
    getStaffDetailRequest: state => {
      return {
        ...state,
        getStaffDetailLoading: true,
        staffDetailData: {
          data: null,
          statusCode: -1,
        },
      };
    },
    getStaffDetailSuccess: (state, action) => {
      return {
        ...state,
        getStaffDetailLoading: false,
        staffDetailData: {
          data: action.payload,
          statusCode: 200,
        },
      };
    },
    getStaffDetailFailure: state => {
      return {
        ...state,
        getStaffDetailLoading: false,
        staffDetailData: {
          data: {},
          statusCode: 500,
        },
      };
    },
    updateFavoriteItemFastNew: state => {
      return {
        ...state,
        updateFavoriteLoading: true,
      };
    },
    updateFavoriteItemFastNewSuccess: (state, action) => {
      const newsId = action.payload;
      const newState = cloneDeep(state);
      const fastNew = state.homeData.news.map(item => {
        if (item.news_id === newsId) {
          return {...item, favorited: item.favorited === 0 ? 1 : 0};
        }
        return item;
      });
      set(newState, 'homeData.news', fastNew);
      return {
        ...newState,
        updateFavoriteLoading: false,
      };
    },
    updateFavoriteItemFastNewFailure: state => {
      return {
        ...state,
        updateFavoriteLoading: false,
      };
    },
  },
});

export const homeActions = homeSlice.actions;
export default homeSlice;
