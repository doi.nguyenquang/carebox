import {all, put, takeLatest} from 'redux-saga/effects';
import {homeActions} from './home-slice';
import * as api from '../../../../carebox/services/Request';
import ApiConfig from '../../../../carebox/services/api_config';

function* handleGetHomeData() {
  const response = yield api.methodGet({api: ApiConfig.home});
  try {
    if (response.code === 200 && response.success) {
      yield put(homeActions.getHomeDataSuccess(response?.data));
    } else {
      yield put(homeActions.getHomeDataError(response?.data));
    }
  } catch (error) {
    yield put(homeActions.getHomeDataError(response?.data));
  }
}

function* handleGetStaffData() {
  try {
    const response = yield api.methodGet({api: ApiConfig.getStaff});
    if (response.code === 200 && response.success) {
      yield put(homeActions.getStaffDataSuccess(response?.data?.data || []));
    } else {
      yield put(homeActions.getStaffDataSuccess([]));
    }
  } catch (error) {
    yield put(homeActions.getStaffDataSuccess([]));
  }
}

function* handleBookConsultSchedule(action) {
  const bodyData = action.payload;
  const body = {
    book_time_start: bodyData.book_time_start,
    book_time_end: bodyData.book_time_end,
    book_categories: JSON.stringify(bodyData.book_categories),
    book_type: `${bodyData.book_type}`,
    book_position: bodyData.book_position,
  };
  try {
    const response = yield api.methodPost({api: ApiConfig.book, body: body});
    if (response.code === 200 && response.success) {
      yield put(homeActions.bookConsultScheduleSuccess());
    } else {
      yield put(homeActions.bookConsultScheduleFailed());
    }
  } catch (error) {
    yield put(homeActions.bookConsultScheduleFailed());
  }
}

function* handleGetListCheckInRequest() {
  try {
    const response = yield api.methodGet({api: ApiConfig.checkin});
    if (response.code === 200 && response.success) {
      yield put(homeActions.getListCheckInSuccess(response?.data));
    } else {
      yield put(homeActions.getListCheckInFailure(response));
    }
  } catch (error) {
    yield put(homeActions.getListCheckInFailure(error));
  }
}

function* handleUserCheckInRequest(action) {
  const bodyData = action.payload;
  const body = {
    checkin_id: bodyData.checkin_id,
  };
  try {
    const response = yield api.methodPost({
      api: ApiConfig.checkinUser,
      body: body,
    });
    if (response.code === 200 && response.success) {
      yield put(homeActions.userCheckInSuccess());
      yield put(homeActions.getUserCheckInRequest());
    } else {
      yield put(homeActions.userCheckInFailure());
    }
  } catch (error) {
    yield put(homeActions.userCheckInFailure());
  }
}

function* handleGetUserCheckInRequest() {
  try {
    const response = yield api.methodGet({api: ApiConfig.checkinUser});
    if (response.code === 200 && response.success) {
      yield put(homeActions.getUserCheckInSuccess(response?.data));
    } else {
      yield put(homeActions.getUserCheckInFailure());
    }
  } catch (error) {
    yield put(homeActions.getUserCheckInFailure());
  }
}

function* handleGetStaffDetailRequest(action) {
  const staffId = action.payload;

  try {
    const response = yield api.methodGet({
      api: ApiConfig.getStaffDetail + `/${staffId}`,
    });
    if (response.code === 200 && response.success) {
      yield put(homeActions.getStaffDetailSuccess(response?.data));
    } else {
      yield put(homeActions.getStaffDetailFailure());
    }
  } catch (error) {
    yield put(homeActions.getStaffDetailFailure());
  }
}

function* handleGetUserCheckInOfMonth(action) {
  const params = action.payload;
  try {
    const response = yield api.methodGet({
      api: ApiConfig.checkinUser,
      queries: params,
    });
    if (response.code === 200 && response.success) {
      if (response?.data && response.data.length > 0) {
        yield put(homeActions.getUserCheckInOfMonthSuccess(response.data));
      } else {
        yield put(homeActions.getUserCheckInOfMonthSuccess([]));
      }
    } else {
      yield put(homeActions.getUserCheckInOfMonthFailure(response));
    }
  } catch (error) {
    yield put(homeActions.getUserCheckInOfMonthFailure(error));
  }
}

function* handleUpdateFavoriteItemFastNew(action) {
  const item = action.payload;
  const body = {news_id: item?.news_id};
  try {
    const response = yield api.methodPost({
      api: ApiConfig.newsFavorite,
      body: body,
    });
    if (response.code === 200 && response.success) {
      yield put(homeActions.updateFavoriteItemFastNewSuccess(item.news_id));
    } else {
      yield put(homeActions.updateFavoriteItemFastNewFailure());
    }
  } catch (error) {
    yield put(homeActions.updateFavoriteItemFastNewFailure());
  }
}

export default function* watchHomeSaga() {
  yield all([
    takeLatest(homeActions.getHomeDataRequest, handleGetHomeData),
    takeLatest(homeActions.getStaffHomeDataRequest, handleGetStaffData),
    takeLatest(
      homeActions.bookConsultScheduleRequest,
      handleBookConsultSchedule,
    ),
    takeLatest(homeActions.getListCheckInRequest, handleGetListCheckInRequest),
    takeLatest(homeActions.userCheckInRequest, handleUserCheckInRequest),
    takeLatest(homeActions.getUserCheckInRequest, handleGetUserCheckInRequest),
    takeLatest(homeActions.getStaffDetailRequest, handleGetStaffDetailRequest),
    takeLatest(homeActions.getUserCheckInOfMonth, handleGetUserCheckInOfMonth),
    takeLatest(
      homeActions.updateFavoriteItemFastNew,
      handleUpdateFavoriteItemFastNew,
    ),
  ]);
}
