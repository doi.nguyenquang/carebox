import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  Image,
  StatusBar,
  SafeAreaView,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
} from 'react-native';
import { isEmpty } from 'lodash';
import { useDispatch, useSelector } from 'react-redux';
import { loginActions } from '../../../carebox/redux/slices/LoginSlice';
import AppLoading from '../../../carebox/components/app-loading';
import { loginSelector } from '../../../carebox/redux/selectors';
import { ROUTER_NAME } from '../../../carebox/navigation/RouterName';
import { useNavigation } from "@react-navigation/native"
import { save } from '../../../carebox/utils/storage';
import { styles } from './styles'
import { AUTH_TOKEN } from '../../../carebox/utils/constants';

const LoginPage = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation()
  const [username, setUserName] = useState('chau.tranhoangphuong')
  const [password, setPassword] = useState('12345678')
  const loginLoading = useSelector(loginSelector.loginLoading)
  const loginResult = useSelector(loginSelector.loginResult)

  const onLoginPress = () => {
    if (!isEmpty(username) && !isEmpty(password)) {
      const bodyData = {
        username: username,
        password: password,
      }
      dispatch(loginActions.loginRequest({ bodyData: bodyData }))
    }
  }

  useEffect(() => {
    return () => {
      dispatch(loginActions.resetLoginData())
    }
  }, [])

  const saveAccessToken = async () => {
    try {
      await save(AUTH_TOKEN, loginResult.accessToken)
    } catch (err) {
      console.error(err)
    }
  }

  useEffect(() => {
    if (loginResult.isSuccess && loginResult.statusCode === 200) {
      saveAccessToken()
      navigation.replace(ROUTER_NAME.MAIN_TAB)
    }
  }, [loginResult])

  return (
    <View style={styles.container}>
      <StatusBar translucent backgroundColor="transparent" barStyle={"light-content"} />
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior={Platform.OS === 'ios' ? 'padding' : undefined}>
        <SafeAreaView style={styles.loginContainer}>
          <View style={styles.loginViewStyles}>
            <Text style={styles.loginTitleStyles}>CareBOX chào bạn</Text>
            <Text style={styles.loginContentStyles}>Vui lòng đăng nhập để tiếp tục</Text>
            <View style={styles.emptyView} />
            <View style={styles.inputContainer}>
              <TextInput
                value={username}
                onChangeText={(text) => setUserName(text)}
                style={styles.inputStyle}
                keyboardType={'email-address'}
                placeholder={"Username của bạn"}
              />
            </View>
            <View style={styles.emptyView} />
            <View style={styles.inputContainer}>
              <TextInput
                value={password}
                passwordReveal={true}
                autoCapitalize="none"
                autoCorrect={false}
                secureTextEntry={true}
                style={styles.inputStyle}
                placeholder={"Mật khẩu"}
                onChangeText={(text) => setPassword(text)}
              />
            </View>
          </View>
          <View style={styles.vButton}>
            <TouchableOpacity style={styles.loginBtnStyles} onPress={onLoginPress}>
              <Text style={styles.btnTextStyles}>ĐĂNG NHẬP</Text>
            </TouchableOpacity>
          </View>
        </SafeAreaView>
        <AppLoading isVisible={loginLoading} />
      </KeyboardAvoidingView>
    </View>
  )
}

export default LoginPage;
