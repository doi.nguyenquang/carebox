import { COLORS } from '../../../carebox/utils/colors';
import { fontPixel, SIZE } from '../../../carebox/utils/sizes';
import { setFontFamily } from '../../../carebox/utils/functions';
import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    backgroundImage: {
        height: SIZE.SCREEN_WIDTH * (1208 / 1500),
        width: SIZE.SCREEN_WIDTH,
        backgroundColor: COLORS.white
    },
    loginContainer: {
        flex: 1,
        backgroundColor: COLORS.white
    },
    emptyView: {
        height: 20
    },
    loginViewStyles: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 16,
        backgroundColor: COLORS.white,
    },
    loginTitleStyles: {
        color: COLORS.main,
        fontSize: fontPixel(24),
        fontFamily: setFontFamily('GilroySemiBold')
    },
    loginContentStyles: {
        color: COLORS.black303030,
        fontSize: fontPixel(16),
        marginTop: 8,
        fontFamily: setFontFamily('GilroyMedium')
    },
    inputContainer: {
        height: fontPixel(60),
        width: '100%',
        borderRadius: 12,
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: COLORS.greyE5E5E5,
    },
    inputStyle: {
        height: '100%',
        paddingHorizontal: 16
    },
    vButton: {
        paddingHorizontal: 16,
        paddingBottom: 20,
        backgroundColor: COLORS.white
    },
    loginBtnStyles: {
        height: fontPixel(45),
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderRadius: 12,
        borderColor: COLORS.main,
    },
    btnTextStyles: {
        color: COLORS.main,
        lineHeight: 24,
        fontSize: fontPixel(16),
        fontFamily: setFontFamily('GilroySemiBold')
    }
})