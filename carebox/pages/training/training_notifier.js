import TraningApiClient from './training_api_client';
import { useState, useEffect } from 'react';
const TraningNotifier = () => {

  const _apiClient = new TraningApiClient();
  const [happy, setHappy] = useState(null);
  const [isShowHappy, setShowHappy] = useState(false);
  const [quizs, setQuiz] = useState([]);
  const [courses, setCourse] = useState([]);


  useEffect(() => {

    loadHappy();
    loadData();


  }, [])

  const loadHappy = async () => {
    const value = await _apiClient.getHappy();
      setHappy(value);
  }

  const loadData = async () => {
    const value = await _apiClient.homePractise();
    setCourse(value[0]);
    setQuiz(value[1]);
  }

  const showHideHappy = () => {
    setShowHappy(!isShowHappy)
    console.log('showHideHappy', isShowHappy);

  }

  const [refreshing, setRefreshing] = useState(false);
  const onRefresh = async () => {
    setRefreshing(true);
    await loadHappy();
    await loadData();
    setRefreshing(false);
  }


  return {
    onRefresh,
    refreshing,
    happy,
    quizs,
    courses,
    isShowHappy,
    showHideHappy
  }

}


export default TraningNotifier;