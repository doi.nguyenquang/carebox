import { QuizModelList } from '../../../carebox/screens/quiz/quiz_model'
import { CourseModelList } from '../../../carebox/screens/course/course_model'
import { UserCourseModelList } from '../../../carebox/models/user_course_model'

export default class PractiseModel {
  quizs;
  courses;
  courseIndexs;

  constructor({
    quizs,
    courses,
    courseIndexs,
  }) {
    this.quizs = quizs;
    this.courses = courses;
    this.courseIndexs = courseIndexs;
  }

  static fromJson(json) {
    return new PractiseModel(
      {
        quizs: QuizModelList.fromJson(json['quizs']).list,
        courses: CourseModelList.fromJson(json['courses']).list,
        courseIndexs: UserCourseModelList.fromJson(json['course_indexs']).list,
      }
    );
  }
}
