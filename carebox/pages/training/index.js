import { NavigationService } from '../../../carebox/navigation/NavigationService';
import { ROUTER_NAME } from '../../../carebox/navigation/RouterName';
import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  FlatList,
  RefreshControl,
} from 'react-native';
import AppHeader from '../../../carebox/components/app-header';
import AppModal from '../../../carebox/components/app-modal';
import AppLine from '../../../carebox/components/app-line';
import AppButton from '../../../carebox/components/app-button';
import ItemBoxView from './views/item_box_view';
import { COLORS } from '../../../carebox/utils/colors';
import TraningNotifier from './training_notifier';
import ItemCourseView from './views/item_course_view';
import ItemQuizView from './views/item_quiz_view';
import { fontPixel } from '../../../carebox/utils/sizes';
import { TitleCardComponent } from '../../../carebox/components/app-title-card';
import { setFontFamily } from '../../../carebox/utils/functions';
import { IconMentality, IconSoftSkill, IconHappy } from '../../../carebox/assets/icons';

const TrainingContainer = () => {
  const gotoMentality = () => {
    NavigationService.push(ROUTER_NAME.CourseScreen);
  };

  const gotoSoftSkill = () => {
    NavigationService.push(ROUTER_NAME.QuizScreen);
  };

  const onCourseDetail = item => {
    NavigationService.push(ROUTER_NAME.CourseDetailScreen, { model: item });
  };

  const onQuizDetail = item => {
    NavigationService.push(ROUTER_NAME.QuizDetailScreen, { model: item });
  };

  const {
    onRefresh,
    refreshing,
    happy,
    quizs,
    courses,
    isShowHappy,
    showHideHappy,
  } = TraningNotifier();

  const renderQuizItem = ({ item }) => {
    return <ItemQuizView item={item} onClick={onQuizDetail} />;
  };

  const renderCourseItem = ({ item }) => {
    return <ItemCourseView item={item} onClick={onCourseDetail} />;
  };

  const onSubmitEditing = value => {
    console.log('onSubmitEditing', value);
  };

  return (
    <View style={styles.body}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        <View style={styles.container}>
          <AppHeader
            showBack={false}
            search
            onSubmitEditing={onSubmitEditing}
            searchStyle={styles.searchStyle}
          />
          <Text style={styles.titleStyle}>Thư viện CareBOX</Text>
          <Text style={styles.subTitleStyle}>
            Học hỏi khám phá bản thân mỗi ngày
          </Text>

          <View style={styles.vTop}>
            <ItemBoxView
              icon={IconMentality}
              title={'Bài tập\nTâm lý'}
              onClick={gotoMentality}
            />
            <View style={styles.space} />
            <ItemBoxView
              icon={IconSoftSkill}
              title={'Trò chơi\nTâm lý'}
              onClick={gotoSoftSkill}
            />
            <View style={styles.space} />
            <ItemBoxView
              icon={IconHappy}
              title={'Góc\nHạnh Phúc'}
              onClick={showHideHappy}
            />
          </View>


          {courses.length > 0 && (
            <TitleCardComponent
              titleText={'Khóa học phổ biến'}
              onPress={gotoMentality}
            />
          )}
          {courses.length > 0 && (
            <View style={styles.quiz}>
              <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={courses}
                renderItem={renderCourseItem}
                keyExtractor={(item, index) => index}
              />
            </View>
          )}



          {quizs.length > 0 && (
            <TitleCardComponent
              titleText={'Trắc nghiệm phổ biến'}
              onPress={gotoSoftSkill}
            />
          )}

          {quizs.length > 0 && (
            <View style={styles.quiz}>
              <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={quizs}
                renderItem={renderQuizItem}
                keyExtractor={(item, index) => index}
              />
            </View>
          )}
        </View>
      </ScrollView>

      <AppModal isVisible={isShowHappy} style={styles.happy}>
        <View style={styles.happyContent}>
          <Text style={[styles.title, { marginTop: 0, textAlign: 'center' }]}>
            Góc hạnh phúc
          </Text>
          <Text style={styles.happyValue}>{happy}</Text>
          <AppLine />
          <AppButton title="Đồng ý" onPress={showHideHappy} />
        </View>
      </AppModal>
    </View>
  );
};

const styles = StyleSheet.create({
  body: {
    flex: 1,
    backgroundColor: COLORS.bg,
  },
  container: {
    paddingHorizontal: 15,
  },
  searchStyle: {
    marginLeft: 0,
    marginRight: 0,
    elevation: 2,
    borderWidth: 1,
    borderColor: COLORS.greyE9E9E9,
  },
  titleStyle: {
    marginTop: 20,
    fontSize: fontPixel(16),
    color: COLORS.black303030,
    fontFamily: setFontFamily('GilroyBold'),
  },
  title: {
    marginTop: 20,
    fontSize: fontPixel(16),
    color: COLORS.black303030,
    fontFamily: setFontFamily('GilroyBold'),
  },
  subTitleStyle: {
    opacity: 0.6,
    fontSize: fontPixel(14),
    color: COLORS.black303030,
    marginTop: 4,
    fontFamily: setFontFamily('GilroyMedium'),
  },
  vTop: {
    flexDirection: 'row',
    marginTop: 20,
    width: '100%',
    marginBottom: 20,
  },
  space: {
    width: 10,
  },
  vList: {
    flexDirection: 'row',
    marginTop: 20,
    justifyContent: 'space-between',
  },
  happy: {
    justifyContent: 'center',
  },
  happyValue: {
    marginTop: 20,
    marginBottom: 20,
    textAlign: 'center',
    fontFamily: setFontFamily('GilroyMedium'),
    color: COLORS.black303030,
  },
  happyContent: {
    justifyContent: 'center',
    borderRadius: 10,
    backgroundColor: COLORS.white,
    margin: 20,
    padding: 20,
  },
  quiz: {
    marginTop: 10,
    marginBottom: 20,
  },
});

export default TrainingContainer;
