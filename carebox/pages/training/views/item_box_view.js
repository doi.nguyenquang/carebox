import { NavigationService } from '../../../../carebox/navigation/NavigationService';
import { ROUTER_NAME } from '../../../../carebox/navigation/RouterName';
import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet, Dimensions, Image } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { QuizModel } from '../../../../carebox/screens/quiz/quiz_model';
import { IconImage } from '../../../../carebox/components/app-image'
import AppButton from '../../../../carebox/components/app-button'
import { COLORS } from '../../../../carebox/utils/colors';
import { fontPixel, SIZE } from '../../../../carebox/utils/sizes';
import { setFontFamily } from '../../../../carebox/utils/functions';

const windowWidth = Dimensions.get('window').width;

const ItemBoxView = (props) => {
  const { title, icon, onClick, sub } = props;
  const gotoQuizDetail = () => {

  }
  return (
    <TouchableOpacity
      style={styles.itemContainer}
      onPress={onClick}
    >
      <Image
        resizeMode={'contain'}
        source={icon}
        style={styles.icon}
      />
      <View style={styles.bottomBox}>
        <Text
          style={styles.title}
          numberOfLines={2}>{title}</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  itemContainer: {
    height: fontPixel(120),
    alignItems: 'center',
    flex: 1,
  },
  bottomBox: {
    width: '100%',
    height: fontPixel(90),
    borderWidth: 1,
    bottom: 0,
    position: 'absolute',
    borderRadius: 12,
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingBottom: 8,
    backgroundColor: COLORS.white,
    borderColor: COLORS.greyE9E9E9,
  },
  icon: {
    height: fontPixel(70),
    width: fontPixel(58),
    zIndex: 1,
  },
  title: {
    fontSize: fontPixel(14),
    lineHeight: 20,
    textAlign: 'center',
    color: COLORS.black303030,
    fontFamily: setFontFamily('GilroyMedium')
  },
});

export default ItemBoxView;
