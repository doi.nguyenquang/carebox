import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import { AppImage } from '../../../../carebox/components/app-image';
import { COLORS } from '../../../../carebox/utils/colors';
import { QuizModel } from '../../../../carebox/screens/quiz/quiz_model';
import { IconQuiz, IconLock } from '../../../../carebox/assets/icons';
import { fontPixel, SIZE } from '../../../../carebox/utils/sizes';
import { setFontFamily } from '../../../../carebox/utils/functions';

const ItemQuizView = ({ item, onClick }) => {
  const model = item;
  return (
    <TouchableOpacity
      onPress={() => onClick(item)}
      style={styles.itemQuizContainer}
    >
      <AppImage
        uri={model.quizImage}
        style={styles.image}
      />
      <View style={{ flex: 1 }}>
        <Text numberOfLines={2} style={styles.titleTextStyles}>{model.quizTitle}</Text>
        <View style={styles.timeCountContainer}>
          <Image style={styles.icTime} source={IconLock} />
          <Text style={styles.timeValueStyles}>{model.quizTime} phút</Text>
        </View>
        <View style={styles.introContainer}>
          <Text numberOfLines={2} style={styles.introText}>{model.quizIntro}</Text>
        </View>
      </View>
      <Image
        source={IconQuiz}
        style={styles.icon}
      />
    </TouchableOpacity>
  );
};


const styles = StyleSheet.create({
  itemQuizContainer: {
    height: 285,
    padding: 16,
    borderWidth: 1,
    marginRight: 10,
    borderRadius: 12,
    backgroundColor: COLORS.white,
    borderColor: COLORS.greyE9E9E9,
    width: SIZE.SCREEN_WIDTH * 0.7,
  },
  titleTextStyles: {
    color: COLORS.black,
    flex: 1,
    fontSize: fontPixel(15),
    fontFamily: setFontFamily('GilroyBold'),
    alignItems: 'center',
  },
  introText: {
    color: COLORS.black,
    fontSize: fontPixel(14),
    fontFamily: setFontFamily('GilroyLight')
  },
  image: {
    width: 'auto',
    height: 130,
    borderRadius: 8,
    marginBottom: 10
  },
  icon: {
    width: 36,
    height: 36,
    position: 'absolute',
    top: 6,
    right: 10,
  },
  icTime: {
    width: 24,
    height: 24,
    marginRight: 10,
  },
  timeCountContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  introContainer: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  timeValueStyles: {
    color: COLORS.black303030,
    fontSize: fontPixel(14),
    fontFamily: setFontFamily('GilroyMedium')
  }
});
export default ItemQuizView;