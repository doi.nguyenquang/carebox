import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import { AppImage } from '../../../../carebox/components/app-image';
import { COLORS } from '../../../../carebox/utils/colors';
import { CourseModel } from '../../../../carebox/screens/course/course_model';
import { IconCourse, IconLesson } from '../../../../carebox/assets/icons';
import { fontPixel, SIZE } from '../../../../carebox/utils/sizes';
import { setFontFamily } from '../../../../carebox/utils/functions';

const ItemQuizView = ({ item, onClick }) => {
  const model: CourseModel = item;

  return (
    <TouchableOpacity
      onPress={() => onClick(item)}
      style={styles.itemCourseContainer}
    >
      <AppImage
        uri={model.courseImage}
        style={styles.image}
      />
      <View style={{ flex: 1 }}>
        <Text numberOfLines={2} style={styles.title}>{model.courseTitle}</Text>
        <View style={styles.courseCountContainer}>
          <Image style={styles.icTarget} source={IconLesson} />
          <Text style={styles.targetValue}>{model.courseListCount} tiết</Text>
        </View>
        <View style={styles.introContainer}>
          <Text numberOfLines={2} style={styles.introText}>{model.courseIntro}</Text>
        </View>
      </View>
      <Image
        source={IconCourse}
        style={styles.icon}
      />
    </TouchableOpacity>
  )
}


const styles = StyleSheet.create({
  itemCourseContainer: {
    height: 285,
    width: SIZE.SCREEN_WIDTH * 0.7,
    marginRight: 10,
    backgroundColor: COLORS.white,
    borderRadius: 12,
    borderWidth: 1,
    borderColor: COLORS.greyE9E9E9,
    padding: 16,
  },
  title: {
    color: COLORS.black,
    flex: 1,
    fontSize: fontPixel(15),
    fontFamily: setFontFamily('GilroyBold'),
    alignItems: 'center',
  },
  introContainer: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  introText: {
    color: COLORS.black,
    fontSize: fontPixel(14),
    fontFamily: setFontFamily('GilroyLight')
  },
  image: {
    width: 'auto',
    height: 130,
    borderRadius: 8,
    marginBottom: 10
  },
  icon: {
    width: 36,
    height: 36,
    position: 'absolute',
    top: 6,
    right: 10,
  },
  courseCountContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icTarget: {
    width: 24,
    height: 24,
    marginRight: 10,
  },
  targetValue: {
    color: COLORS.black303030,
    fontSize: fontPixel(14),
    fontFamily: setFontFamily('GilroyMedium')
  }

});
export default ItemQuizView;