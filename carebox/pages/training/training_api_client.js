import { CategoryModel, CategoryModelList } from '../../../carebox/models/category_model'
import * as api from '../../../carebox/services/Request'
import ApiConfig from '../../../carebox/services/api_config'
import { QuizModel, QuizModelList } from '../../../carebox/screens/quiz/quiz_model'
import PractiseModel from './traning_model'
import { MoreModelList } from '../../../carebox/models/more_model'

export default class TraningApiClient {

  async homePractise()  {
    const res = await api.methodGet({api: ApiConfig.homePractise});
    if (res['success']) {
      const model = PractiseModel.fromJson(res['data']);
      return [model.courses, model.quizs];
    }
    return null;
  }

  async getHappy()  {
    const res = await api.methodGet({api: ApiConfig.more});
    if (res['success']) {
      const value = MoreModelList.fromJson(res['data']['values']).list;
      var list = [...value];
      const data = list.filter((item) =>{
        return item.moreType == ApiConfig.happy;
     })
     if(data.length > 0){
      return data[0].moreContent;
     }
      return null;
    }
    return null;
  }
}
