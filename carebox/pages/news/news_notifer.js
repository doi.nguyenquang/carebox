import { useState, useEffect } from 'react';
import NewsApiClient  from './news_api_client'

export default NewsNotifier = () => {

  const [cates, setCates] = useState([]);
  const _apiClient = new NewsApiClient();

  useEffect(() => {
    const loadData = async () => {
        const value = await _apiClient.categories();
        setCates(value);
       
    }
    loadData();
  }, [])

  return { cates }

}
