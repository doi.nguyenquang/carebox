import { CategoryModel, CategoryModelList } from '../../../carebox/models/category_model'
import * as api from '../../../carebox/services/Request'
import ApiConfig from '../../../carebox/services/api_config'
import {CourseModelList} from '../../../carebox/models/category_model';

export default class NewsApiClient {
  hashNextPage = true;
  page = 0;
  limit = 40;
  constructor() {

  }
 
  async categories() {
    const res = await api
        .methodGet({api: ApiConfig.categoryAll, queries: {'type': ApiConfig.cateNEW}});
    if (res['success']) {
      return CategoryModelList.fromJson(res['data']).list;
    } else {
      return [];
    }
  }

}
