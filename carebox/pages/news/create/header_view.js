import React from 'react';
import {
  Text,
  View,
  StyleSheet,
} from 'react-native';
import { IconImage } from '../../../../carebox/components/app-image';
import { UserAvatarDefault } from '../../../../carebox/assets/icons';
import { COLORS } from '../../../../carebox/utils/colors';
import { profileSelector } from '../../../../carebox/redux/selectors';
import { useSelector } from 'react-redux';
import { isEmpty } from 'lodash'

const HeaderView = (props) => {

  const profileData = useSelector(profileSelector.profileData)
  const { style } = props;
  return (
    <View style={[styles.container, style]}>
      <IconImage
        style={styles.avatar}
        source={!isEmpty(profileData.pictureUrl) ? { uri: profileData.pictureUrl } : UserAvatarDefault}
      />
      <View>
        <Text style={styles.tvName}>{profileData?.fullName}</Text>
        <Text style={styles.tvUserName}>{profileData?.email}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  avatar: {
    width: 50,
    height: 50,
    marginRight: 10,
    borderRadius: 50,
  },
  tvName: {
    fontWeight: 'bold'
  },
  tvUserName: {
    color: COLORS.main,
    marginTop: 3
  }
});
export default HeaderView;