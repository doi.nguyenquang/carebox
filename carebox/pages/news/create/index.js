import React, { useState } from 'react';
import { StyleSheet, View, StatusBar, Image, TextInput, SafeAreaView, Keyboard, ScrollView, Text } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import AppButton from '../../../../carebox/components/app-button'
import AppHeader from '../../../../carebox/components/app-header'
import { IconGallery, IconVideo } from '../../../../carebox/assets/icons'
import { COLORS } from '../../../../carebox/utils/colors';
import CreateNotifier from './create_notifier'
import HeaderView from './header_view'
import ItemMediaView from './item_media_view'
import AppLoading from '../../../../carebox/components/app-loading';
import { IconBuilding, IconTimer, IconTwoUsers, IconArrowCircleDown } from '../../../../carebox/assets/icons'
import { AppImage, IconImage } from '../../../../carebox/components/app-image';
import { setFontFamily } from '../../../../carebox/utils/functions';
import AppModal from '../../../../carebox/components/app-modal';
import { SIZE } from '../../../../carebox/utils/sizes';

const scopePostContents = [
  {
    title: 'Xem bởi mọi người',
    name: 'Toàn công ty',
    icon: IconBuilding,
    type: '0',
  },
  {
    title: 'Xem bởi phòng ban',
    name: 'Phòng Ban',
    icon: IconTwoUsers,
    type: '1',
  },
  {
    title: 'Tự xoá sau 30p',
    name: 'Tin tạm thời',
    icon: IconTimer,
    type: '2',
  }
]

const CreatePostScreen = () => {
  const [showModal, setShowModal] = useState(false)
  const [typePost, setTypePost] = useState('0')

const itemPostTypeSelected = scopePostContents[parseInt(typePost)]

  const {
    chooseImage,
    chooseVideo,
    onChangeText,
    list,
    onRemove,
    loading,
    onCreatePost,
  } = CreateNotifier();

  const insets = useSafeAreaInsets();

  const renderItem = (item, index) => (
    <ItemMediaView
      key={index.toString()}
      item={item}
      onRemove={onRemove}
      index={index}
    />
  );

  const renderBottom = () => {
    const listVideo = list.filter(item => item.assets[0].type.includes('video'));
    const isShowChooseVideo = !(listVideo && listVideo.length);
    return list.length <= 9 && (
      <View style={[styles.vAction, { paddingBottom: insets.bottom }]}>
        <AppButton
          style={styles.vBtn}
          onPress={chooseImage}
        >
          <Image
            source={IconGallery}
            style={styles.icon}
          />
        </AppButton>
        {isShowChooseVideo ? (
          <AppButton
            style={styles.vBtn}
            onPress={chooseVideo}
          >
            <Image
              source={IconVideo}
              style={styles.icon}
            />
          </AppButton>
        ) : null}
      </View>
    );
  };

  const openModalChooseScope = () => {
    setShowModal(true)
  }

  const onItemSelected = (item) => {
    setShowModal(false)
    setTypePost(item.type)
  }

  const _renderModalChooseScope = () => {
    return (
      <AppModal
        isVisible={showModal}
        style={styles.modalContainer}
      >
        <View style={{
          alignItems: 'center',
          justifyContent: 'center',
        }}>
          <View style={styles.modalBoxStyles}>
            <Text style={styles.scopeTitle}>Quyền riêng tư bài viết</Text>

            <View style={styles.wrapperItem}>
              {
                scopePostContents.map((item, index) => (
                  <View key={index}>
                    <AppButton onPress={() => {onItemSelected(item)}} style={styles.itemScopeModal}>
                      <View>
                        <Text style={styles.nameItemScope}>{item.title}</Text>
                        <Text style={styles.nameDetailItemScope}>{item.name}</Text>
                      </View>
                      <IconImage source={item.icon} style={styles.iconScopeItem} />
                    </AppButton>
                    {index != scopePostContents.length - 1 && <View style={styles.border} />}
                  </View>
                ))
              }
            </View>
          </View>
        </View>
      </AppModal>
    )
  }
  return (
   <>
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <AppHeader style={styles.header}>
        <View style={styles.wrapperHeaderItem}>
          <AppButton onPress={openModalChooseScope} style={styles.scopeBtn}>
            <View style={styles.wrapperScope}>
              <Text style={styles.nameScope}>{itemPostTypeSelected.title}</Text>
              <IconImage
                source={IconArrowCircleDown}
                style={styles.iconArrowDown}
              />
            </View>
            <Text style={styles.nameDetailScope}>{itemPostTypeSelected.name}</Text>
          </AppButton>
          <AppButton
            title="Đăng"
            onPress={() => onCreatePost(typePost)}
          />
        </View>
      </AppHeader>
      <HeaderView style={styles.postHeader} />
      <ScrollView>
        <TextInput
          style={styles.input}
          onChangeText={onChangeText}
          placeholder="Nhập chia sẻ của bạn"
          underlineColorAndroid="transparent"
          multiline
          returnKeyType={'done'}
          onSubmitEditing={() => Keyboard.dismiss()}
        />
        {list.length > 0 && (
          <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
            {list.map(renderItem)}
          </View>
        )}
      </ScrollView>

      {renderBottom()}
      <AppLoading isVisible={loading} />
    </SafeAreaView>
      {_renderModalChooseScope()}
   </>
  );
};

const styles = StyleSheet.create({
  wrapperItem: {
    borderTopColor: COLORS.greyE9E9E9,
    borderTopWidth: 1,
    paddingHorizontal: 16,
  },
  border: {
    width: SIZE.SCREEN_WIDTH - 32,
    height: 1,
    backgroundColor: COLORS.greyE9E9E9,
    marginVertical: 2,
  },
  nameDetailItemScope: {
    fontFamily: setFontFamily('SVN-GilroyBold'),
    fontWeight: '500',
    fontSize: 14,
  },
  nameItemScope: {
    marginBottom: 5,
    fontFamily: setFontFamily('SVN-Gilroy'),
    fontSize: 12,
  },
  wrapperScope: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconArrowDown: {
    width: 24,
    height: 24,
  },
  scopeBtn: {
    marginLeft: 20,
  },
  wrapperHeaderItem: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  nameScope: {
    fontFamily: setFontFamily('SVN-GilroyBold'),
    fontWeight: '600',
    fontSize: 14,
  },
  nameDetailScope: {
    fontFamily: setFontFamily('SVN-Gilroy'),
    fontSize: 12,
  },
  modalContainer: {
    justifyContent: 'flex-end',
  },
  modalBoxStyles: {
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    backgroundColor: COLORS.white,
    width: SIZE.SCREEN_WIDTH,
          paddingBottom: 20,
  },
  scopeTitle: {
    fontFamily: setFontFamily('SVN-GilroyBold'),
    fontWeight: '600',
    fontSize: 16,
    paddingVertical: 20,
    textAlign: 'center',
  },
  iconScopeItem: {
    width: 24,
    height: 24,
  },
  itemScopeModal: {
    paddingVertical: 13,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  container: {
    flex: 1,
    backgroundColor: COLORS.bg,
  },
  header: {
    marginHorizontal: 10,
    elevation: 5,
    justifyContent: 'space-between',
  },
  input: {
    padding: 15,
  },
  postHeader: {
    padding: 15,
  },
  vAction: {
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0,
    justifyContent: 'flex-end',
    width: '100%',
  },
  vBtn: {
    marginRight: 15,
  },
  icon: {
    width: 24,
    height: 24
  },
  imageContainer: {
    marginVertical: 24,
    alignItems: 'center',
  },
  image: {
    width: 200,
    height: 200,
  },
  listMedia: {
    flexGrow: 1,
  },
});

export default CreatePostScreen;
