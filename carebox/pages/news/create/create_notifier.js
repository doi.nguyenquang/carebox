import CreatePostApiClient from './create_api_client'
import React, { useState } from 'react';
import { launchImageLibrary } from 'react-native-image-picker';
import { NavigationService } from '../../../../carebox/navigation/NavigationService';
import { showToastMessage } from '../../../../carebox/utils/utils'
import { EventRegister } from '../../../../carebox/utils/eventListener';
import { EVENTS } from '../../../../carebox/services/events';

const CreateNotifier = () => {
  const _apiClient = new CreatePostApiClient();

  const [list, setList] = useState([]);
  const [loading, setLoading] = useState(false);
  const [content, setContent] = useState('');

  const chooseImage = async () => {
    const result = await launchImageLibrary({ mediaType: 'photo' });
    console.log('CreateNotifierChooseImage', result);
    if (!result.assets || result.didCancel) {
      return;
    }
    setList([...list, result]);
  }

  const chooseVideo = async () => {
    const result = await launchImageLibrary({ mediaType: 'video' });
    console.log('CreateNotifierChooseVideo', result);
    if (!result.assets || result.didCancel) {
      return;
    }
    setList([...list, result]);
  }

  const onChangeText = (text) => setContent(text);

  const onRemove = (value) => {
    const newList = list.filter((_item, index) => index !== value);
    setList(newList);
  }

  const onCreatePost = async (typeScope = '0') => {
    setLoading(true);
    const result = await _apiClient.createNews({ content, assets: list, type: typeScope });
    setLoading(false);
    if (result) {
      EventRegister.emit(EVENTS.CREATE_SUCCESS);
      showToastMessage('Tạo bài viết thành công');
      NavigationService.goBack();
    } else {
      showToastMessage(`Lỗi ${result}`);
    }
  }

  return {
    list,
    chooseImage,
    chooseVideo,
    onChangeText,
    onRemove,
    loading,
    onCreatePost,
  }
}

export default CreateNotifier;
