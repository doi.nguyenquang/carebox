import * as api from '../../../../carebox/services/Request'
import ApiConfig from '../../../../carebox/services/api_config'
// import RNFS from 'react-native-fs';
// import { decode } from 'base64-arraybuffer';

export default class CreatePostApiClient {
  async createNews({ content, assets, type }) {
    const body = new FormData();
    if (content != null) {
      body.append('post_content', content.trim());
    }
    if (assets && assets.length) {
      for (let i = 0; i < assets.length; i++) {
        const assetEntity = assets[i].assets[0];
        // const base64 = await RNFS.readFile(assetEntity.uri, 'base64');
        // const arrayBuffer = decode(base64);

        const { type } = assetEntity;
        const ext = type.split('/').pop();
        const imageName = `${new Date().getTime()}.${ext}`;

        // body.append('post_medias', arrayBuffer);
        body.append('post_medias', { uri: assetEntity.uri, name: `${imageName}`, type });
        // body.append('post_medias', assetEntity, assetEntity.fileName);
        // body.append('post_medias', {
        //   uri: assetEntity.uri,
        //   type: assetEntity.type,
        //   name: assetEntity.fileName,
        //   type: type,
        // });
      }
    }
    const res = await api.methodPostFormData({ api: ApiConfig.postCreate, body });
    if (res['success']) {
      return true;
    }
    return res['message'];
  }
}
