import React from 'react';
import { StyleSheet, View, Image, Dimensions } from 'react-native';
import { IconImage } from '../../../../carebox/components/app-image'
import AppButton from '../../../../carebox/components/app-button'
import { IconClose } from '../../../../carebox/assets/icons'
import { COLORS } from '../../../../carebox/utils/colors';
import PostVideoView from '../../../../carebox/pages/news/post/post_video_view';

const { width } = Dimensions.get('window');
const SIZE_MEDIA = width - 30;

const ItemMediaView = ({ item, onRemove, index }) => {
  const uri = item.assets[0].uri;

  const renderMedia = () => {
    return item.assets[0].type.includes('image') ? renderImage() : renderVideo();
  };

  const renderImage = () => (
    <Image
      source={{ uri }}
      style={styles.image}
    />
  );

  const renderVideo = () => <PostVideoView uri={uri} size={SIZE_MEDIA / 2}/>;

  return (
    <View style={styles.item}>
      {renderMedia()}
      <AppButton
        onPress={() => onRemove(index)}
        style={styles.vClose}>
        <IconImage source={IconClose} style={styles.iconClose} />
      </AppButton>
    </View>
  );
};

const styles = StyleSheet.create({
  item: {
    marginLeft: 10,
    marginBottom: 10,
  },
  imageContainer: {
    marginVertical: 24,
    alignItems: 'center',
  },
  image: {
    width: SIZE_MEDIA / 2,
    height: SIZE_MEDIA / 2,
    resizeMode: 'cover',
  },
  vClose: {
    width: 50,
    height: 50,
    position: 'absolute',
    top: 5,
    left: 5,
  },
  iconClose: {
    tintColor: COLORS.white,
  },
});

export default ItemMediaView;
