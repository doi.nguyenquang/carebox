import InterApiClient from './inter_api_client'
import { useState, useEffect } from 'react';

const InterNotifier = () => {
  const _apiClient = new InterApiClient();

  const [list, setList] = useState([]);
  const [hashNextPage, setHashNextPage] = useState(true);
  const [isFirst, setIsFirst] = useState(true);
  const [cates, setCates] = useState([]);
  const [currentCate, setCurrentCate] = useState(null);

  useEffect(() => {
    const loadDataInit = async () => {
      await loadCateH();
      loadData();
    }
    loadDataInit();
  }, [])

  const loadData = async () => {
    const datas = await _apiClient.getNews({ isFirst: true, currentCate: currentCate });
    setIsFirst(false);
    setHashNextPage(_apiClient.hashNextPage);
    setList(datas);
  }

  const loadMore = async () => {
    const datas = await _apiClient.getNews({ isFirst: false, currentCate: currentCate });
    setHashNextPage(_apiClient.hashNextPage);
    setList(datas);
  }

  const loadCateH = async () => {
    const datas = await _apiClient.cateH();
    console.log('loadCateH', datas);
    setCates(datas);
    if (datas.length > 0) {
      setCurrentCate(datas[0])
    }
  }


  const onClickCate = async (value) => {
    setCurrentCate(value);
    setIsFirst(true);
    setHashNextPage(true);
    setList([]);
    loadData();
  }


  const [refreshing, setRefreshing] = useState(false);
  const onRefresh = async () => {
    setRefreshing(true);
    await loadData();
    setRefreshing(false);
  }


  return {
    onRefresh,
    refreshing,
    list,
    hashNextPage,
    isFirst,
    loadData,
    loadMore,
    cates,
    onClickCate,
    currentCate,
  }


}
export default InterNotifier;