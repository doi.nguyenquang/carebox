import { CategoryModel } from '../../../../carebox/models/category_model'
import { NewsModel } from '../../../../carebox/models/news_model'

class InterModel {
  id;
  createdDate;
  createdBy;
  lastUpdated;
  updatedBy;
  publicationDate;
  publishedBy;
  title;
  description;
  imageUrl;
  category: CategoryModel;


  constructor({
    id,
    createdDate,
    createdBy,
    lastUpdated,
    updatedBy,
    publicationDate,
    publishedBy,
    title,
    description,
    imageUrl,
    category,
  }) {
    this.id = id;
    this.createdDate = createdDate;
    this.createdBy = createdBy;
    this.lastUpdated = lastUpdated;
    this.updatedBy = updatedBy;
    this.publicationDate = publicationDate;
    this.publishedBy = publishedBy;
    this.title = title;
    this.description = description;
    this.imageUrl = imageUrl;
    this.category = category;
  }

  static fromJson(json) {
    return new InterModel(
      {
        id: json['id'],
        createdDate: json['createdDate'],
        createdBy: json['createdBy'],
        lastUpdated: json['lastUpdated'],
        updatedBy: json['updatedBy'],
        publicationDate: json['publicationDate'],
        publishedBy: json['publishedBy'],
        title: json['title'],
        description: json['description'],
        imageUrl: json['imageUrl'],
        category: json['category'] == null
          ? null
          : CategoryModel.fromJson(json['category']),
      }
    );
  }

  news() {
    return new NewsModel(
      {
        newsId: this.id,
        newsTitle: this.title,
        newsContent: this.description,
        newsImages: this.imageUrl,
        createdAt: this.createdBy,
        updatedAt: this.lastUpdated,
        category: this.category,
        showFavorite: false,
      }
    );
  }
}


class InterModelList {
  list;

  constructor({ list }) {
    this.list = list;
  }

  static fromJson = (datas) => {
    var list = [];
    if (datas) {
      list = datas.map((f) => new InterModel.fromJson(f));
    }
    return new InterModelList({ list });
  }
}

export { InterModel, InterModelList }