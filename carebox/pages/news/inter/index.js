import React from 'react';
import {
    StyleSheet,
    View,
    StatusBar,
    VirtualizedList,
    FlatList,
    RefreshControl
} from 'react-native';
import LoadingView from '../../../../carebox/components/loading-view'
import AppText from '../../../../carebox/components/app-text'
import { COLORS } from '../../../../carebox/utils/colors';
import ItemInterView from './views/item_inter_view'
import InterNotifier from './inter_notifier'
import ItemCateView from './views/item_cate_view'
import { fontPixel } from '../../../../carebox/utils/sizes';

const InterScreen = ({ route }) => {

    const {
        list,
        hashNextPage,
        isFirst,
        loadMore,
        cates,
        onClickCate,
        currentCate,
        onRefresh,
        refreshing,
    } = InterNotifier();


    const scrollToEnd = async () => {
        console.log('scrollToEnd');
        if (hashNextPage) {
            loadMore();
        }
    }

    const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
        const paddingToBottom = 20;
        return layoutMeasurement.height + contentOffset.y >=
            contentSize.height - paddingToBottom;
    };

    const itemNoData = () => {
        return <View style={styles.notData}>
            <AppText
                title='Không có dữ liệu'
                style={styles.tvNoData}
            />
        </View>
    }

    const renderItem = ({ item, index }) => {
        if (isFirst) {
            return <LoadingView />
        } else {
            if (item === 2) {
                if (list.length == 0) {
                    return itemNoData();
                }
                return (hashNextPage && <LoadingView />);
            } else {
                return <ItemInterView
                    item={item}
                    index={index}
                />
            }

        }

    }

    const getItem = (data, index) => {
        if (index < (data.length)) {
            return data[index];
        } else {
            return 2;
        }
    }

    const getItemCount = (data) => {
        if (data.length == 0) {
            return 1;
        }
        return data.length + 1;
    }

    const renderCateItem = ({ item }) => (
        <ItemCateView
            item={item}
            current={currentCate}
            onClick={onClickCate}
        />
    );


    return (

        <View style={styles.container}>
            <StatusBar barStyle="dark-content" />
            <View style={styles.cate}>
                <FlatList
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    data={cates}
                    renderItem={renderCateItem}
                    keyExtractor={(item, index) => index}
                />
            </View>
            <VirtualizedList
                data={list}
                renderItem={renderItem}
                keyExtractor={(item, index) => `content${index}`}
                getItemCount={getItemCount}
                getItem={getItem}
                keyboardShouldPersistTaps='handled'
                onEndReachedThreshold={400}
                onScroll={({ nativeEvent }) => {
                    if (isCloseToBottom(nativeEvent)) {
                        scrollToEnd();
                    }
                }}
                refreshControl={
                    <RefreshControl
                      refreshing={refreshing}
                      onRefresh={onRefresh}
                    />}
            />

        </View>

    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        marginHorizontal: 10,
        elevation: 5,
    },
    cate: {
        width: '100%',
        backgroundColor: COLORS.white,
        marginBottom: 10,
    },
    tvNoData: {
        textAlign: 'center'
    },
    notData: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        height: 200
    },
    floatButtonContainer: {
        position: 'absolute',
        bottom: 10,
        right: 20,
        zIndex: 2
    },
    icon: {
        width: fontPixel(60),
        height: fontPixel(60)
    },



});
export default InterScreen;