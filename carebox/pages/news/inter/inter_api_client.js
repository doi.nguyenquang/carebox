import * as api from '../../../../carebox/services/Request'
import ApiConfig from '../../../../carebox/services/api_config'
import { InterModelList } from './inter_model'
import { CategoryModelList, CategoryModel } from '../../../../carebox/models/category_model'

export default class InterApiClient {
  hashNextPage = true;
  page = 0;
  limit = 40;
  constructor() { }

  async getNews({ isFirst, currentCate }) {
    const model: CategoryModel = currentCate;
    if (isFirst) {
      this.page = 0;
      this.hashNextPage = true;
    }
    if (!this.hashNextPage) return [];
    const query = {
      'page': this.page,
      'size': this.limit,
      'type': 'NEWS',
      'categoryId': model?.categorId,
    };
    
    console.log('IntcurrentCate', currentCate);
    console.log('IerApiClient', query);

    this.page = this.page + 1;
    const res = await api.methodGet({ api: ApiConfig.getByCategory, queries: query })
    if (res['data']) {
      const list = InterModelList.fromJson(res['data']['data']).list;
      if (list.length < this.limit) {
        this.hashNextPage = false;
      }
      return list;
    }
    return [];
  }


  async cateH() {
    var res = await api.methodGet({ api: ApiConfig.cateH });
    if (res) {
      return CategoryModelList.fromJson(res['data']).list;;
    }
    return [];
  }

}
