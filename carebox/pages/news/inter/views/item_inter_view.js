import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Button,
    Image,
    TouchableOpacity,
} from 'react-native';
import { AppImage, IconImage } from '../../../../../carebox/components/app-image'
import { IconTag, IconArchive } from '../../../../../carebox/assets/icons'
import { COLORS } from '../../../../../carebox/utils/colors';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { InterModel } from '../inter_model'
import AppPeriodView from '../../../../../carebox/components/app-period-view'
import AppCateView from '../../../../../carebox/components/app-cate'
import AppButton from '../../../../../carebox/components/app-button'
import { fontPixel, SIZE } from '../../../../../carebox/utils/sizes';
import { setFontFamily } from '../../../../../carebox/utils/functions';
import { NavigationService } from '../../../../../carebox/navigation/NavigationService';
import { ROUTER_NAME } from '../../../../../carebox/navigation/RouterName';

const ItemInterView = ({ item, index }) => {
    const model: InterModel = item;
    const onDetail = () => {
        NavigationService.push(ROUTER_NAME.NewsScreen, { model: model.news(), favorite: 0 });
    }
    return (
        <TouchableOpacity
            onPress={onDetail}
            style={[styles.newsItemContainer]}
        >
            <Image style={styles.thumbnailStyles} source={{ uri: model.imageUrl }} />
            <View style={styles.newsTitleContainer}>
                <Text style={styles.titleTextStyles}>{model.title}</Text>
            </View>
            {model?.category?.categoryTitle && <View style={styles.categoryContainer}>
                <Text
                    numberOfLines={1}
                    style={styles.categoryTextStyles}
                >
                    {model?.category?.categoryTitle}
                </Text>
            </View>}

        </TouchableOpacity>
    );
};


const styles = StyleSheet.create({
    newsItemContainer: {
        width: 'auto',
        height: fontPixel(300),
        backgroundColor: COLORS.white,
        borderRadius: 16,
        borderWidth: 1,
        marginBottom: 15,
        marginHorizontal: 15,
        borderColor: COLORS.greyE9E9E9,
        padding: 16,
        alignItems: 'center',
    },
    thumbnailStyles: {
        height: fontPixel(180),
        width: '100%',
        borderRadius: 8,
        backgroundColor: COLORS.bg
    },
    newsTitleContainer: {
        flex: 1,
        width: '100%',
        paddingTop: 16,
        marginBottom: 20,
    },
    titleTextStyles: {
        color: COLORS.black303030,
        fontSize: fontPixel(16),
        fontFamily: setFontFamily('GilroySemiBold')
    },
    categoryContainer: {
        position: 'absolute',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: COLORS.blue54BADE,
        bottom: 0,
        borderTopEndRadius: 30,
        borderTopLeftRadius: 30,
        paddingBottom: 5,
        paddingHorizontal: 15,
    },
    categoryTextStyles: {
        fontSize: fontPixel(12),
        fontFamily: setFontFamily('GilroyMedium'),
        color: COLORS.white,
        marginTop: 10,
    },
    tagBtnContainer: {
        height: 40,
        width: 40,
        borderRadius: 6,
        position: 'absolute',
        right: 0,
        backgroundColor: COLORS.white,
        justifyContent: 'center',
        alignItems: 'center',
    },
    iconTagStyles: {
        height: 32,
        width: 32
    }

});
export default ItemInterView;