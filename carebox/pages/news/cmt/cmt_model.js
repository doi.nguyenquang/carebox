import { UserModel } from '../../../../carebox/models/user_model';

class CmtModel {
  commentId;
  postId;
  commentContent;
  userId;
  createdAt;
  updatedAt;
  user;

  constructor({
    commentId,
    postId,
    commentContent,
    userId,
    createdAt,
    updatedAt,
    user,
  }) {
    this.commentId = commentId;
    this.postId = postId;
    this.commentContent = commentContent;
    this.userId = userId;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.user = user;
  }

  static fromJson(json) {
    return new CmtModel(
      {
        commentId: json['comment_id'],
        postId: json['post_id'],
        commentContent: json['coment_content'],
        userId: json['user_id'],
        createdAt: json['created_at'],
        updatedAt: json['updated_at'],
        user: json['user'] ?  UserModel.fromJson(json['user']) : null,
      }
    );
  }
}

class CmtModelList {
  list;

  constructor({ list }) {
    this.list = list;
  }

  static fromJson = (datas) => {
    var list = [];
    if (datas) {
      list = datas.map((f) => CmtModel.fromJson(f));
    }
    return new CmtModelList({ list });
  }
}


export { CmtModelList, CmtModel }