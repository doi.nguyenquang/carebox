import * as api from '../../../../carebox/services/Request'
import ApiConfig from '../../../../carebox/services/api_config'
import { CmtModelList, CmtModel } from './cmt_model'

export default class CmtApiClient {
  hashNextPage = true;
  page = 0;
  limit = 40;
  postId;
  constructor(postId) {
    this.postId = postId;
  }

  async getCmt({ isFirst }) {
    if (isFirst) {
      this.page = 0;
      this.hashNextPage = true;
    }
    if (!this.hashNextPage) return [];
    const query = {
      'offset': this.page,
      'limit': this.limit,
      'post_id': this.postId,
    };

    this.page = this.page + this.limit;
    const res = await api.methodGet({ api: ApiConfig.comment, queries: query })
    if (res['success']) {
      const list = CmtModelList.fromJson(res['data']['values']).list;
      if (list.length < this.limit) {
        this.hashNextPage = false;
      }
      return list;
    }
    return [];
  }

  async sendComment(comment) {
    const body = {
      post_id: this.postId ,
      coment_content: comment
    }
    const res = await api.methodPost({ api: ApiConfig.comment, body: body });
    if (res['success']) {
      return CmtModel.fromJson(res['data']);
    }
    return null;
  }

  async deleteComment(commentId) {
    const body = { 'comment_id': commentId };
    const res = await api.methodDel({ api: ApiConfig.comment, body: body });
    if (res['success']) {
      return true;
    }
    return false;
  }




}
