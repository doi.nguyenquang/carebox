import React from 'react';
import {
  StyleSheet,
  View,
  StatusBar,
  Image,
  VirtualizedList,
  SafeAreaView,
  TextInput,
  RefreshControl,
  KeyboardAvoidingView,
} from 'react-native';
import AppButton from '../../../../carebox/components/app-button'
import AppHeader from '../../../../carebox/components/app-header'
import LoadingView from '../../../../carebox/components/loading-view'
import AppText from '../../../../carebox/components/app-text'
import { IconSend } from '../../../../carebox/assets/icons'
import { COLORS } from '../../../../carebox/utils/colors';
import ItemCmtView from './item_cmt_view'
import CmtNotifier from './cmt_notifier'
import { fontPixel } from '../../../../carebox/utils/sizes';
import { setFontFamily } from '../../../../carebox/utils/functions';

const CmtScreen = (props) => {

  const {
    list,
    hashNextPage,
    isFirst,
    loadData,
    loadMore,
    onChangeText,
    onSend,
    content,
    onRefresh,
    refreshing,
  } = CmtNotifier(props);


  const scrollToEnd = async () => {
    console.log('scrollToEnd');
    if (hashNextPage) {
      loadMore();
    }
  }

  const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = 20;
    return layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom;
  };


  const itemNoData = () => {
    return <View style={styles.notData}>
      <AppText
        title='Không có dữ liệu'
        style={styles.tvNoData}
      />
    </View>
  }

  const renderItem = ({ item }) => {
    if (isFirst) {
      return <LoadingView />
    } else {
      if (item === 2) {
        if (list.length == 0) {
          return itemNoData();
        }
        return (hashNextPage && <LoadingView />);
      } else {
        return <ItemCmtView
          item={item}
        />
      }

    }

  }

  const getItem = (data, index) => {
    if (index < (data.length)) {
      return data[index];
    } else {
      return 2;
    }
  }

  const getItemCount = (data) => {
    if (data.length == 0) {
      return 1;
    }
    return data.length + 1;
  }

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <AppHeader
        style={styles.header}
        title={'Bình luận'} />
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior={Platform.OS === 'ios' ? 'padding' : undefined}>
        <VirtualizedList
          data={list}
          renderItem={renderItem}
          keyExtractor={(item, index) => `content${index}`}
          getItemCount={getItemCount}
          getItem={getItem}
          keyboardShouldPersistTaps='handled'
          onEndReachedThreshold={400}
          onScroll={({ nativeEvent }) => {
            if (isCloseToBottom(nativeEvent)) {
              scrollToEnd();
            }
          }}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={onRefresh}
            />}
        />
        <View style={styles.vCmt} >
          <TextInput
            style={styles.input}
            value={content}
            onChangeText={onChangeText}
            placeholder="Nhập bình luận của bạn"
            underlineColorAndroid="transparent"
          />
          <AppButton
            onPress={onSend}>
            <Image
              source={IconSend}
              style={styles.iconSend}
            />
          </AppButton>
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.bg,
  },
  header: {
    marginHorizontal: 10,
    elevation: 5,
  },
  cate: {
    marginTop: 0
  },
  tvNoData: {
    textAlign: 'center'
  },
  notData: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    height: 200
  },
  iconSend: {
    width: fontPixel(24),
    height: fontPixel(24)
  },
  vCmt: {
    flexDirection: 'row',
    margin: 10,
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 15,
    borderWidth: 1,
    borderRadius: 10,
    paddingHorizontal: 10,
    borderColor: COLORS.greyE5E5E5,
    fontFamily: setFontFamily('GilroyMedium'),
  },
  input: {
    height: fontPixel(55)
  }


});
export default CmtScreen;