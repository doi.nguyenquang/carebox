import { CategoryModel } from '../../../../carebox/models/category_model'
import CmtApiClient from './cmt_api_client'
import { useState, useEffect } from 'react';
import { PostModel } from '../post/post_model';
import { EventRegister } from '../../../../carebox/utils/eventListener';
import { EVENTS } from '../../../../carebox/services/events';

const CmtNotifier = (props) => {
  const { route } = props;
  const model: PostModel = route.params.model;

  const _apiClient = new CmtApiClient(model.postId);

  const [list, setList] = useState([]);
  const [hashNextPage, setHashNextPage] = useState(true);
  const [isFirst, setIsFirst] = useState(true);
  const [content, setContent] = useState('');

  useEffect(() => {
    loadData();
  }, [])

  const loadData = async () => {
    const datas = await _apiClient.getCmt({ isFirst: true });
    setIsFirst(false);
    setHashNextPage(_apiClient.hashNextPage);
    setList(datas);

  }

  const loadMore = async () => {
    if (hashNextPage) {
      const _list = await _apiClient.favorites({ isFirst: false });
      setHashNextPage(_apiClient.hashNextPage);
      if (_list.length > 0) {
        const datas = [list, ..._list];
        setList(datas);
      }
    }
  }

  const onChangeText = (value) => {
    setContent(value);
  }

  const onSend = async () => {
    const value = await _apiClient.sendComment(content);
    console.log('AAAAAAA-----: ', value);
    EventRegister.emit(EVENTS.COMMENT_SUCCESS);
    if (value) {
      setList([...list, value]);
      setContent('');
    }
  }


  const [refreshing, setRefreshing] = useState(false);
  const onRefresh = async () => {
    setRefreshing(true);
    await loadData();
    setRefreshing(false);
  }


  return {
    onRefresh,
    refreshing,
    list,
    hashNextPage,
    isFirst,
    loadData,
    loadMore,
    onChangeText,
    onSend,
    content,
  }


}
export default CmtNotifier;