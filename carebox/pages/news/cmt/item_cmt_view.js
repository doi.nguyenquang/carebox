import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Button,
    Image,
    FlatList,
} from 'react-native';
import { AppImage } from '../../../../carebox/components/app-image'
import { IconCase } from '../../../../carebox/assets/icons'
import { COLORS } from '../../../../carebox/utils/colors';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import AppPeriodView from '../../../../carebox/components/app-period-view'
import AppCateView from '../../../../carebox/components/app-cate'
import AppButton from '../../../../carebox/components/app-button'
import { IconFavorite, IconComment, IconSend } from '../../../../carebox/assets/icons'
import PostHeaderView from '../../../../carebox/pages/news/post/post_header_view'
import { CmtModel } from './cmt_model'
import { setFontFamily } from '../../../../carebox/utils/functions';

const ItemCmtView = ({ item }) => {
    const model: CmtModel = item;
    return (

        <View style={styles.item}>
            <PostHeaderView 
                model = {model}
            />
            <Text style={styles.content}>{model.commentContent}</Text>
        </View>
    );
};


const styles = StyleSheet.create({
    content: {
        color: COLORS.black,
        marginVertical: 10,
        fontFamily: setFontFamily('GilroyMedium'),
    },
    item: {
        paddingTop: 15,
        paddingHorizontal: 15,
        backgroundColor: '#fff',
        marginVertical: 8,
        marginHorizontal: 10,
        elevation: 1,
        borderRadius: 10,
    },

});
export default ItemCmtView;