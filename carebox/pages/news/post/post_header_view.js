import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  ActionSheetIOS,
  Button
} from 'react-native';
import { IconImage } from '../../../../carebox/components/app-image';
import AppButton from '../../../../carebox/components/app-button';
import { UserAvatarDefault, IconMoreVer } from '../../../../carebox/assets/icons';
import { COLORS } from '../../../../carebox/utils/colors';
import { profileSelector } from '../../../../carebox/redux/selectors';
import { useSelector } from 'react-redux';
import { isEmpty } from 'lodash'
import { PostModel } from "./post_model";
import { UserModel } from "../../../../carebox/models/user_model";
import { setFontFamily } from '../../../../carebox/utils/functions';
import { SheetManager } from 'react-native-actions-sheet';

const PostHeaderView = ({model, onDel, style}) => {
  const user: UserModel = model?.user;
  const profileData = useSelector(profileSelector.profileData)
  const isMy = user?.userName === profileData.preferredUsername;
  const onMore = () => {
    console.log('profileData', profileData);
    console.log('isMy', isMy);
    SheetManager.show('more_action', { payload: {
      isMy
    }, }).then((result) => {
      console.log('result', result);
      if(result){
        if(isMy){
          if(onDel){
            onDel(model);
          }
        }else{
          SheetManager.show('report_action');
        }
        
      }
    })
  }
  return (
    <View style={[styles.container, style]}>
      <View style={styles.vcontainer}>
        <IconImage
          style={styles.avatar}
          source={user?.pictureUrl ? { uri: user?.pictureUrl } : UserAvatarDefault}
        />
        <View>
          <Text style={styles.tvName}>{user?.userFullName}</Text>
          <Text style={styles.tvUserName}>{user?.userName}</Text>
        </View>
      </View>
      <AppButton onPress={onMore}>
        <IconImage
          style={styles.ic_more}
          source={IconMoreVer}
        />
      </AppButton>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  vcontainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  avatar: {
    width: 50,
    height: 50,
    marginRight: 10,
    borderRadius: 50,
  },
  tvName: {
    fontWeight: 'bold',
    fontFamily: setFontFamily('GilroyMedium'),
  },
  tvUserName: {
    color: COLORS.main,
    marginTop: 3,
    fontFamily: setFontFamily('GilroyMedium'),
  },
  ic_more: {
    width: 24,
    height: 24,
  },
});
export default PostHeaderView;