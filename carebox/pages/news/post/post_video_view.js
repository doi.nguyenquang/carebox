import React, { useState } from 'react'
import Video from 'react-native-video';
import { Dimensions, StyleSheet, TouchableOpacity, View } from 'react-native';
import { IconImage } from '../../../../carebox/components/app-image';
import { IconPause, IconPlay } from '../../../../carebox/assets/icons';
import { COLORS } from '../../../../carebox/utils/colors';

const { width } = Dimensions.get('window');
const SIZE_MEDIA = width - 50;

const PostVideoView = ({ uri, size }) => {
  const [paused, setPaused] = useState(true);
  const sizeMedia = size || SIZE_MEDIA;

  return (
    <View style={{ width: sizeMedia, height: sizeMedia }}>
      <Video
        source={{ uri }}
        style={{ width: sizeMedia, height: sizeMedia }}
        controls={false}
        paused={paused}
        repeat
        resizeMode="cover"
        onEnd={() => setPaused(true)}
      />
      <View style={styles.vCustomControl}>
        <TouchableOpacity activeOpacity={0.7} style={styles.buttonControl} onPress={() => setPaused(!paused)}>
          <IconImage source={paused ? IconPlay : IconPause} style={styles.iconControl} />
        </TouchableOpacity>
      </View>
    </View>
  )
};

export default PostVideoView;

const styles = StyleSheet.create({
  vCustomControl: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonControl: {
    backgroundColor: COLORS.white,
    width: 50,
    height: 50,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconControl: {
    width: 40,
    height: 40,
    tintColor: COLORS.black,
  },
});
