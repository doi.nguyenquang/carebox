import * as api from '../../../../carebox/services/Request'
import ApiConfig from '../../../../carebox/services/api_config'
import { PostModelList } from './post_model'
import { NotiReModelList } from './regulation_model'

export default class PostApiClient {
  hashNextPage = true;
  page = 0;
  limit = 40;
  constructor() {

  }

  async getPost({ isFirst}) {
    if (isFirst) {
      this.page = 0;
      this.hashNextPage = true;
    }
    if (!this.hashNextPage) return [];
    const query = {
      'offset': this.page,
      'limit': this.limit
    };

    this.page = this.page + this.limit;
    const res = await api.methodGet({ api: ApiConfig.post, queries: query })
    if (res['success']) {
      const list = PostModelList.fromJson(res['data']['values']).list;
      if (list.length < this.limit) {
        this.hashNextPage = false;
      }
      return list;
    }
    return [];
  }

  async like(postID) {
    const body = { 'post_id': postID };
    const res = await api.methodPost({ api: ApiConfig.like, body: body });
    if (res['success']) {
      return res['data'];
    }
    return null;
  }

  async deletePost(postID) {
    const body = { 'post_id': postID };
    const res = await api.methodPost({ api: ApiConfig.postDel, body: body });
    if (res['success']) {
      return res['data'];
    }
    return null;
  }

  async getNotiRegular() {
    const res = await api.methodGet({ api: ApiConfig.notiRegular });
    console.log(res, 'getNotiRegular')
    if (res['success']) {
      return NotiReModelList.fromJson(res['data']['rows']).list;
    }
    return [];
  }




}
