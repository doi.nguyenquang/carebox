import React  from 'react';
import { StyleSheet, View, Image, Dimensions } from 'react-native';
import { SwiperFlatList } from 'react-native-swiper-flatlist';
import { COLORS } from '../../../../carebox/utils/colors';
import PostVideoView from '../../../../carebox/pages/news/post/post_video_view';

const { width } = Dimensions.get('window');
const SIZE_MEDIA = width - 50;

const PostMediaView = ({ postMedias }) => {
  const renderItem = ({ item }) => {
    return item.postMediaType === '0' ? renderImage(item) : renderVideo(item);
  };

  const renderImage = (item) => (
    <Image
      source={{ uri: item.postMediaLink }}
      style={styles.media}
    />
  );

  const renderVideo = (item) => (
    <PostVideoView uri={item.postMediaLink} size={SIZE_MEDIA}/>
  );

  return postMedias && postMedias.length ? (
    <View style={[styles.container, postMedias.length > 1 ? {} : { paddingBottom: 10 }]}>
      <SwiperFlatList
        index={0}
        showPagination={postMedias.length > 1}
        data={postMedias}
        paginationStyle={styles.paginationContainer}
        paginationStyleItemActive={styles.paginationItemActive}
        paginationStyleItemInactive={styles.paginationItemInactive}
        renderItem={renderItem}
      />
    </View>
  ) : null;
};

export default PostMediaView;

const styles = StyleSheet.create({
  container: {
    paddingBottom: 30,
  },
  paginationContainer: {
    height: 30,
    marginBottom: 0,
    alignItems: 'center',
  },
  paginationItemActive: {
    width: 12,
    height: 12,
    backgroundColor: COLORS.green2,
    marginHorizontal: 3,
  },
  paginationItemInactive: {
    width: 8,
    height: 8,
    backgroundColor: COLORS.gray1,
    marginHorizontal: 3,
  },
  media: {
    width: SIZE_MEDIA,
    height: SIZE_MEDIA,
    resizeMode: 'cover',
    backgroundColor: COLORS.bg,
  },
});
