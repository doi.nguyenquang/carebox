import React from 'react';
import {
  StyleSheet,
  View,
  StatusBar,
  VirtualizedList,
  RefreshControl,
  FlatList,
  Text
} from 'react-native';
import { IconImage } from '../../../../carebox/components/app-image'
import AppButton from '../../../../carebox/components/app-button'
import LoadingView from '../../../../carebox/components/loading-view'
import AppText from '../../../../carebox/components/app-text'
import { IconCreatePost, IconUp, IconDown } from '../../../../carebox/assets/icons'
import { NavigationService } from '../../../../carebox/navigation/NavigationService';
import { ROUTER_NAME } from '../../../../carebox/navigation/RouterName';
import ItemPostView from './item_post_view'
import ItemNotiReguView from './item_noti_regular'
import PostNotifier from './post_notifier'
import './post_notifier'
import { fontPixel } from '../../../../carebox/utils/sizes';
import { COLORS } from '../../../../carebox/utils/colors';
import { setFontFamily } from '../../../../carebox/utils/functions';
import AppLoading from '../../../../carebox/components/app-loading';

const PostScreen = ({ route }) => {
  const {
    list,
    hashNextPage,
    isFirst,
    onRefresh,
    loadMore,
    refreshing,
    onFavorite,
    onDel,
    notiRegulars,
    isShowNoti,
    showHideNoti,
    onClickNoti,
    excutingFile,

  } = PostNotifier();

  const scrollToEnd = async () => {
    if (hashNextPage) {
      loadMore();
    }
  }

  const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = 20;
    return layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom;
  };

  const onDetail = (value) => {
    NavigationService.push(ROUTER_NAME.CmtScreen, { model: value });
  }

  const itemNoData = () => {
    return <View style={styles.notData}>
      <AppText
        title='Không có dữ liệu'
        style={styles.tvNoData}
      />
    </View>
  }

  const renderItem = ({ item, index }) => {
    if (isFirst) {
      return <LoadingView />
    } else {
      if (item === 2) {
        if (!list.length) {
          return itemNoData();
        }
        return (hashNextPage && <LoadingView />);
      } else {
        return <ItemPostView
          item={item}
          onClick={onDetail}
          onFavorite={onFavorite}
          onDel={(model) => onDel(model, index)}
        />
      }
    }
  }

  const getItem = (data, index) => {
    if (index < (data.length)) {
      return data[index];
    } else {
      return 2;
    }
  }

  const getItemCount = (data) => {
    if (!data.length) {
      return 1;
    }
    return data.length + 1;
  }

  const onCreate = () => {
    NavigationService.push(ROUTER_NAME.CreatePostScreen);
  }

  const onScroll = ({ nativeEvent }) => {
    if (isCloseToBottom(nativeEvent)) {
      scrollToEnd();
    }
  };

  const renderItemNoti = ({ item, index }) => {
    return <ItemNotiReguView
      item={item}
      onClick={onClickNoti}
    />
  }

  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" />
      {notiRegulars.length > 0 && <View >
        <AppButton onPress={showHideNoti}>

          <View style={styles.vNoti}>
            <Text style={styles.title}>Thông báo/ Quy định</Text>

            <IconImage
              source={isShowNoti ? IconUp : IconDown}
              style={styles.v}
            />
          </View>
        </AppButton>
        {isShowNoti && <FlatList
          data={notiRegulars}
          renderItem={renderItemNoti}
          style={styles.notiRegu}
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item, index) => `${index}`}
        />}
      </View>}
      <VirtualizedList
        data={list}
        renderItem={renderItem}
        keyExtractor={(item, index) => `content${index}`}
        getItemCount={getItemCount}
        getItem={getItem}
        keyboardShouldPersistTaps='handled'
        onEndReachedThreshold={400}
        onScroll={onScroll}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
          />}
      />

      <AppButton
        onPress={onCreate}
        style={styles.floatButtonContainer}>
        <IconImage
          source={IconCreatePost}
          style={styles.icon}
        />
      </AppButton>

      <AppLoading
        isVisible={excutingFile}
      />
    </View>

  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
  },
  header: {
    marginHorizontal: 10,
    elevation: 5,
  },
  cate: {
    marginTop: 0
  },
  tvNoData: {
    textAlign: 'center'
  },
  notData: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    height: 200
  },
  floatButtonContainer: {
    position: 'absolute',
    bottom: 10,
    right: 20,
    zIndex: 2
  },
  icon: {
    width: fontPixel(60),
    height: fontPixel(60)
  },
  notiRegu: {
    height: 400,
    width: '100%',
  },
  title: {
    color: COLORS.black,
    fontFamily: setFontFamily('GilroyMedium'),
    fontWeight: 'bold',
  },
  iconNoti: {
    width: fontPixel(24),
    height: fontPixel(24)
  },
  vNoti: {
    width: '100%',
    paddingTop: 10,
    paddingBottom: 5,
    paddingHorizontal: 15,
    justifyContent: 'space-between',
    flexDirection: 'row',
  }
});
export default PostScreen;
