import React, { useState } from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import { COLORS } from '../../../../carebox/utils/colors';
import AppButton from '../../../../carebox/components/app-button';
import { IconFavorite, IconComment, IconSend, IconFavFull } from '../../../../carebox/assets/icons';
import { NotiRegularModel } from "./regulation_model";
import { setFontFamily } from '../../../../carebox/utils/functions';

const ItemNotiReguView = ({ item, onClick }) => {
    const model: NotiRegularModel = item;

    return (
        <AppButton onPress={() => onClick(model)}>
            <View style={styles.item}>
                <Text style={styles.title}>{model.notiRegulationTitle}</Text>
                <Text style={styles.content}>{model.notiRegulationContent}</Text>
                {/* <Text style={styles.content}>{model.notiRegulationFile}</Text> */}
            </View>
        </AppButton>
    );
};

const styles = StyleSheet.create({
    title: {
        color: COLORS.black,
        fontWeight: 'bold',
        marginTop: 15,
        fontFamily: setFontFamily('GilroyMedium'),
    },
    content: {
        color: COLORS.black,
        marginVertical: 10,
        fontFamily: setFontFamily('GilroyMedium'),
    },
    image: {
        width: 'auto',
        height: 200,
        borderRadius: 15
    },
    icon: {
        width: 18,
        height: 18,
    },
    vAction: {
        flexDirection: 'row',
    },
    tvNumLike: {
        paddingLeft: 5,
        paddingRight: 10,
    },
    item: {
        paddingHorizontal: 15,
        backgroundColor: '#fff',
        marginVertical: 5,
        marginHorizontal: 10,
        elevation: 1,
        borderRadius: 10,
    },

});

export default ItemNotiReguView;
