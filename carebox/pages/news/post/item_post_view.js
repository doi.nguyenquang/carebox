import React, { useState } from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import { COLORS } from '../../../../carebox/utils/colors';
import AppButton from '../../../../carebox/components/app-button';
import { IconFavorite, IconComment, IconSend, IconFavFull } from '../../../../carebox/assets/icons';
import PostHeaderView from '../../../../carebox/pages/news/post/post_header_view';
import PostMediaView from "../../../../carebox/pages/news/post/post_media_view";
import { PostModel } from "./post_model";

const ItemPostView = ({ item, onClick, onFavorite, onDel }) => {
  const model: PostModel = item;
  const [favorite, setFavorite] = useState(model?.isLike);
  const _onFavorite = () => {
    if(favorite){
      model.likesCount -=1;
    }else{
      model.likesCount +=1
    }
    setFavorite(!favorite)
    onFavorite(model)
    
  }

  return (
    <View style={styles.item}>
      <PostHeaderView
        model={model}
        onDel = {onDel}
      />
      <Text style={styles.content}>{model.postContent}</Text>
      <PostMediaView postMedias={model.postMedias} />
      <View style={styles.vAction}>
        <AppButton
          onPress={() => _onFavorite(item)}
          style={styles.vAction}>
          <Image
            source={favorite ? IconFavFull : IconFavorite}
            style={styles.icon}
          />
          <Text style={styles.tvNumLike}>{model.likesCount}</Text>
        </AppButton>

        <AppButton
          onPress={() => onClick(item)}
          style={styles.vAction}>
          <Image
            source={IconComment}
            style={styles.icon}
          />
          <Text style={styles.tvNumLike}>{model.commentsCount}</Text>
        </AppButton>

      </View>

      <AppButton style={styles.vCmt} onPress={() => onClick(item)}>
        <Text style={styles.tvNumLike}>Nhập bình luận</Text>
        <Image
          source={IconSend}
          style={styles.icon}
        />
      </AppButton>

    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    color: COLORS.black,
    fontWeight: 'bold',
    marginVertical: 10,
  },
  content: {
    color: COLORS.black,
    marginVertical: 10,
  },
  image: {
    width: 'auto',
    height: 200,
    borderRadius: 15
  },
  icon: {
    width: 18,
    height: 18,
  },
  vAction: {
    flexDirection: 'row',
  },
  tvNumLike: {
    paddingLeft: 5,
    paddingRight: 10,
  },
  item: {
    paddingTop: 15,
    paddingHorizontal: 15,
    backgroundColor: '#fff',
    marginVertical: 8,
    marginHorizontal: 10,
    elevation: 1,
    borderRadius: 10,
  },
  vCmt: {
    flexDirection: 'row',
    marginTop: 10,
    justifyContent: 'space-between',
    marginBottom: 15,
    borderWidth: 1,
    borderRadius: 10,
    padding: 10,
    borderColor: COLORS.greyE5E5E5,
  },
});

export default ItemPostView;
