import PostApiClient from './post_api_client'
import React, { useState, useEffect } from 'react';
import { EventRegister } from '../../../../carebox/utils/eventListener';
import { EVENTS } from '../../../../carebox/services/events';
import { PostModel } from "./post_model";
import {
  Text,
  View,
  StyleSheet,
  ActionSheetIOS,
  Button
} from 'react-native';

import ActionSheet, { SheetManager, registerSheet } from 'react-native-actions-sheet';
import { downLoadAndSaveFile, showToastMessage } from '../../../../carebox/utils/utils';
import Share from 'react-native-share';
import { NotiRegularModel } from "./regulation_model";

function MoreAction(props) {
  const payload = props.payload;
  const isMy = payload.isMy;
  return (
    <ActionSheet
      id={props.sheetId}
      statusBarTranslucent={false}
      drawUnderStatusBar={false}
      gestureEnabled={true}
      containerStyle={{
        paddingHorizontal: 12,
      }}
      springOffset={50}
      defaultOverlayOpacity={0.3}>
      <Button
        title={isMy ? 'Xóa' : 'Báo cáo'}
        onPress={() => {
          SheetManager.hide(props.sheetId, {
            payload: true,
          });
        }}
      />
      <View
        style={{
          height: 10,
        }}
      />
      <Button
        title="Đóng"
        onPress={() => {
          SheetManager.hide(props.sheetId, {
            payload: false,
          });
        }}
      />
      <View
        style={{
          height: 10,
        }}
      />
    </ActionSheet>
  );
}


function ReportAction(props) {
  return (
    <ActionSheet
      id={props.sheetId}
      statusBarTranslucent={false}
      drawUnderStatusBar={false}
      gestureEnabled={true}
      containerStyle={{
        paddingHorizontal: 12,
      }}
      springOffset={50}
      defaultOverlayOpacity={0.3}>

      <Text style={{
        fontSize: 16,
        fontWeight: 'bold',
      }}>Xác nhận</Text>
      <View
        style={{
          height: 10,
        }}
      />
      <Text>Bạn muốn báo xấu hoạt động này</Text>
      <View
        style={{
          height: 10,
        }}
      />
      <Button
        title='Nội dung nhạy cảm'
        onPress={() => {
          SheetManager.hide(props.sheetId, {
            payload: true,
          });
        }}
      />
      <View
        style={{
          height: 10,
        }}
      />
      <Button
        title='Làm phiền'
        onPress={() => {
          SheetManager.hide(props.sheetId, {
            payload: false,
          });
        }}
      />
      <View
        style={{
          height: 10,
        }}
      />
      <Button
        title='Lừa đảo'
        onPress={() => {
          SheetManager.hide(props.sheetId, {
            payload: false,
          });
        }}
      />
      <View
        style={{
          height: 10,
        }}
      />
      <Button
        title='Đóng'
        onPress={() => {
          SheetManager.hide(props.sheetId, {
            payload: false,
          });
        }}
      />
      <View
        style={{
          height: 10,
        }}
      />
    </ActionSheet>
  );
}

registerSheet('report_action', ReportAction);
registerSheet('more_action', MoreAction);
export { };

const PostNotifier = () => {
  const _apiClient = new PostApiClient();

  const [list, setList] = useState([]);
  const [hashNextPage, setHashNextPage] = useState(true);
  const [isFirst, setIsFirst] = useState(true);
  const [isShowNoti, showNotiRegu] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [notiRegulars, setNotiRegulars] = useState([]);
  const [excutingFile, setExcutingFile] = useState(false)

  useEffect(() => {
    loadData();
  }, []);

  useEffect(() => {
    // listen event create post success
    const listener = EventRegister.addEventListener(EVENTS.CREATE_SUCCESS, () => {
      loadData();
    });
    const listenerCommentSuccess = EventRegister.addEventListener(EVENTS.COMMENT_SUCCESS, () => {
      loadData();
    });
    // listen event comment success
    return () => {
      EventRegister.removeEventListener(listener);
      EventRegister.removeEventListener(listenerCommentSuccess);
    };
  }, []);

  const onRefresh = async () => {
    setRefreshing(true);
    await loadData();
    setRefreshing(false);
  }

  const loadData = async () => {
    loadNotiRegular();
    const datas = await _apiClient.getPost({ isFirst: true });
    setIsFirst(false);
    setHashNextPage(_apiClient.hashNextPage);
    setList(datas);
  }

  const loadMore = async () => {
    if (hashNextPage) {
      const _list = await _apiClient.getPost({ isFirst: false });
      setHashNextPage(_apiClient.hashNextPage);
      if (_list.length > 0) {
        const datas = [list, ..._list];
        setList(datas);
      }
    }
  }

  const onFavorite = (model: PostModel) => {
    _apiClient.like(model.postId)
  }

  const onDel = (model, index) => {
    console.log('_apiClient.deletePost', index);
    const filteredItems = list.filter(item => item !== model)
    setList(filteredItems);
    _apiClient.deletePost(model.postId)
  }

  const removeItem = (items, i) =>
    items.slice(0, i - 1).concat(items.slice(i, items.length))

  const loadNotiRegular = async () => {
    const datas = await _apiClient.getNotiRegular();
    setNotiRegulars(datas);
  }

  const showHideNoti = () => {
    showNotiRegu(!isShowNoti)
  }
  const onClickNoti = async (item: NotiRegularModel) => {
    if (item.notiRegulationFile) {
      setExcutingFile(true);
      const dlResult = await downLoadAndSaveFile(item.notiRegulationFile)
      if (dlResult?.success) {
        await onShareDocument(dlResult);
        setExcutingFile(false);
      }
    }
  }

  const onShareDocument = async (dlResult) => {
    try {
      let options = {
        title: 'Chia sẻ từ ứng dụng Carebox',
        urls: [dlResult.data],
      };
      await Share.open(options);
    } catch (err) {
      console.log('ShareDocument error: ', err);
    }
  }

  return {
    list,
    hashNextPage,
    isFirst,
    loadData,
    loadMore,
    refreshing,
    onRefresh,
    onFavorite,
    onDel,
    notiRegulars,
    isShowNoti,
    showHideNoti,
    onClickNoti,
    excutingFile,
  }
}
export default PostNotifier;
