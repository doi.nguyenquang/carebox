import { UserModel } from '../../../../carebox/models/user_model';
class PostModel {
  postId;
  postContent;
  userId;
  createdAt;
  updatedAt;
  commentsCount;
  likesCount;
  postMedias;
  user;
  isLike;

  constructor({
    postId,
    postContent,
    userId,
    createdAt,
    updatedAt,
    commentsCount,
    likesCount,
    postMedias,
    user,
    isLike,
  }) {
    this.postId = postId;
    this.postContent = postContent;
    this.userId = userId;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.commentsCount = commentsCount;
    this.likesCount = likesCount;
    this.postMedias = postMedias;
    this.isLike = isLike;
    this.user = user;
  }

  static fromJson(json) {
    return new PostModel(
      {
        postId: json['post_id'],
        postContent: json['post_content'],
        userId: json['user_id'],
        createdAt: json['created_at'],
        updatedAt: json['updated_at'],
        commentsCount: json['comments_count'],
        likesCount: json['likes_count'],
        postMedias: PostMediaModelList.fromJson(json['post_medias']).list,
        user: json['user'] ? UserModel.fromJson(json['user']) : null,
        isLike: json['isLike'] ?? 0,
      }
    );
  }
}

class PostModelList {
  list;

  constructor({ list }) {
    this.list = list;
  }

  static fromJson = (datas) => {
    var list = [];
    if (datas) {
      list = datas.map((f) => new PostModel.fromJson(f));
    }
    return new PostModelList({ list });
  }
}


class PostMediaModel {
  postMediaId;
  postId;
  userId;
  postMediaLink;
  postMediaType;
  createdAt;
  updatedAt;

  constructor({
    postMediaId,
    postId,
    userId,
    postMediaLink,
    postMediaType,
    createdAt,
    updatedAt,
  }) {
    this.postMediaId = postMediaId;
    this.postId = postId;
    this.userId = userId;
    this.postMediaLink = postMediaLink;
    this.postMediaType = postMediaType;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }

  static fromJson(json) {
    return new PostMediaModel(
      {
        postMediaId: json['post_media_id'],
        postId: json['post_id'],
        userId: json['user_id'],
        postMediaLink: json['post_media_link'],
        postMediaType: json['post_media_type'],
        createdAt: json['created_at'],
        updatedAt: json['updated_at'],
      }
    );
  }
}

class PostMediaModelList {
  list;

  constructor({ list }) {
    this.list = list;
  }

  static fromJson = (datas) => {
    var list = [];
    if (datas) {
      list = datas.map((f) => new PostMediaModel.fromJson(f));
    }
    return new PostMediaModelList({ list });
  }
}


export { PostModel, PostModelList, PostMediaModel, PostMediaModelList }