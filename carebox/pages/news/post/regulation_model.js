class NotiReModelList {
  list;

  constructor({ list }) {
    this.list = list;
  }

  static fromJson = (datas) => {
    var list = [];
    if (datas) {
      list = datas.map((f) => new NotiRegularModel.fromJson(f));
    }
    return new NotiReModelList({ list });
  }
}

class NotiRegularModel {
  notiRegulationId;
  notiRegulationDeparment;
  notiRegulationTitle;
  notiRegulationContent;
  notiRegulationStatus;
  createdAt;
  createdAtConvert;
  updatedAt;
  notiRegulationFile;
  isActive = false;

  constructor({
    notiRegulationId,
    notiRegulationDeparment,
    notiRegulationTitle,
    notiRegulationContent,
    notiRegulationStatus,
    createdAt,
    notiRegulationFile,
    updatedAt,
  }) {
      this.notiRegulationId = notiRegulationId;
      this.notiRegulationDeparment = notiRegulationDeparment;
      this.notiRegulationTitle = notiRegulationTitle;
      this.notiRegulationContent = notiRegulationContent;
      this.notiRegulationStatus = notiRegulationStatus;
      this.createdAt = createdAt;
      this.notiRegulationFile = notiRegulationFile;
      this.updatedAt = updatedAt;
  }

  static fromJson(json) {
    return new NotiRegularModel({
      notiRegulationId: json['noti_regulation_id'],
      notiRegulationDeparment: json['noti_regulation_deparment'],
      notiRegulationTitle: json['noti_regulation_title'],
      notiRegulationContent: json['noti_regulation_content'],
      notiRegulationStatus: json['noti_regulation_status'],
      notiRegulationFile: json['noti_regulation_file'],
      isActive: json['isActive'] || true,
      createdAt: json['created_at'],
      updatedAt: json['updated_at'],
    });

  }

}

export {NotiReModelList, NotiRegularModel}