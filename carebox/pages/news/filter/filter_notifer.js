import { useState, useEffect } from 'react';

export default FilterNewsNotifier = () => {

  const [status, setStatus] = useState(true);

  return { status, setStatus }

}
