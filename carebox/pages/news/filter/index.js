import React, { useEffect, useState } from 'react';
import { Text, View, StyleSheet, ScrollView } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import AppHeader from '../../../../carebox/components/app-header'
import { COLORS } from '../../../../carebox/utils/colors';
import AppButton from '../../../../carebox/components/app-button';
import { IconImage } from '../../../../carebox/components/app-image';
import { CategoryModel } from '../../../../carebox/models/category_model';
import { IconUnSelected, IconSelected, IconCheckWhite } from '../../../../carebox/assets/icons'
import { fontPixel } from "../../../../carebox/utils/sizes";
import { NavigationService } from "../../../../carebox/navigation/NavigationService";
import { EventRegister } from "../../../../carebox/utils/eventListener";
import { EVENTS} from "../../../../carebox/services/events";
import { setFontFamily } from '../../../../carebox/utils/functions';

const FilterNewsScreen = ({ route }) => {
  const { params } = route;
  const [cates, setCates] = useState([]);
  const [isNew, setIsNew] = useState(!params?.dataFilter?.order_name);

  useEffect(() => {
    const { dataFilter } = params;
    const newCates: CategoryModel[] = params?.cates.map(e => {
      return {
        ...e,
        isSelected: dataFilter.ids.some(id => id === e.categorId),
      };
    });
    setCates(newCates);
  }, [params]);

  const onFilter = () => {
    const catesSelected = cates.filter(cate => cate.isSelected);
    const data = {
      ids: catesSelected.map(cateSelected => cateSelected.categorId),
      order_name: isNew ? '' : 'news_views',
    };
    EventRegister.emit(EVENTS.FILTER_DONE, data);
    NavigationService.goBack();
  }

  const vRadio = ({ title, value }) => {
    return <AppButton onPress={() => setIsNew(value)}>
      <View style={styles.vRadio}>
        <IconImage
          source={value === isNew ? IconSelected : IconUnSelected}
        />
        <Text style={styles.titleRadio}>{title}</Text>
      </View>
    </AppButton>
  }

  const vSquare = (item: CategoryModel, index) => {
    return (
      <AppButton key={index.toString()} style={styles.vRadio} onPress={() => onSelectIds(item)}>
        <View style={[styles.vCheck, item.isSelected ? { backgroundColor: COLORS.main } : {}]}>
          {item.isSelected ? (
              <IconImage source={IconCheckWhite} style={styles.iconCheck} />
          ) : null}
        </View>
        <Text style={styles.titleRadio}>{item.categoryTitle}</Text>
      </AppButton>
    )
  };

  const onSelectIds = (item: CategoryModel) => {
    const updatedCates = cates.map(cate => {
      return {
        ...cate,
        isSelected: cate.categorId === item.categorId ? !cate.isSelected : cate.isSelected,
      };
    });
    setCates(updatedCates);
  }

  return (
    <SafeAreaView style={styles.container}>
      <View>
        <AppHeader
          style={styles.header}
          title={'Lọc nội dung'}
        />
        <ScrollView style={styles.body}>
          <View >
            <Text style={styles.title}>Sắp xếp theo</Text>
            {vRadio({
              title: 'Mới nhất',
              value: true,
            })}
            {vRadio({
              title: 'Quan tâm nhiều nhất',
              value: false,
            })}
            <Text style={styles.titleCd}>Chủ đề</Text>
            {cates.length ? cates.map(vSquare) : null}
            <AppButton
              onPress={onFilter}
              style={styles.filter}
              title={'Lọc nội dung'}
            />
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};


const styles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.white,
    flex: 1,
  },
  header: {
    marginHorizontal: 10,
    elevation: 5,
  },
  body: {
    paddingHorizontal: 15,
    paddingBottom: 100,
  },
  title: {
    color: COLORS.black,
    fontWeight: 'bold',
    marginBottom: 10,
    marginTop: 10,
  },
  titleCd: {
    color: COLORS.black,
    fontWeight: 'bold',
    marginBottom: 15,
    marginTop: 10,
  },
  space: {
    height: 'auto',
  },
  vRadio: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 8,
  },
  titleRadio: {
    marginLeft: 10,
    fontFamily: setFontFamily('GilroySemiBold'),
  },
  filter: {
    marginTop: 25,
  },
  vCheck: {
    width: fontPixel(22),
    height: fontPixel(22),
    borderColor: COLORS.main,
    borderWidth: 1,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconCheck: {
    width: fontPixel(16),
    height: fontPixel(16),
  },
});

export default FilterNewsScreen;
