import React from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  Image,
  VirtualizedList,
  FlatList,
  SafeAreaView
} from 'react-native';
import { AppImage, IconImage } from '../../../carebox/components/app-image'
import AppButton from '../../../carebox/components/app-button'
import AppLine from '../../../carebox/components/app-line'
import AppHeader from '../../../carebox/components/app-header'
import LoadingView from '../../../carebox/components/loading-view'
import AppText from '../../../carebox/components/app-text'
import { COLORS } from '../../../carebox/utils/colors';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { NavigationService } from '../../../carebox/navigation/NavigationService';
import { ROUTER_NAME } from '../../../carebox/navigation/RouterName';
import { getStatusBarHeight } from '../../../carebox/utils/sizes'
import { fontPixel } from '../../../carebox/utils/sizes';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import AllScreen from './all'
import PostScreen from './post'
import InterScreen from './inter'
import { IconFilter } from '../../../carebox/assets/icons'
import NewsNotifier from './news_notifer';
import AllNotifier from "../../../carebox/pages/news/all/all_notifier";
import {EventRegister} from "../../../carebox/utils/eventListener";
import {EVENTS} from "../../../carebox/services/events";
import { setFontFamily } from '../../../carebox/utils/functions';

const Tab = createMaterialTopTabNavigator();

const NewsPage = ({ route }) => {
  const { cates } = NewsNotifier();
  const { dataFilter } = AllNotifier();

  const goToFilter = () => {
    NavigationService.push(ROUTER_NAME.FilterNewsScreen, { cates, dataFilter });
  };

  const onSearch = (val) => {
    if (!val) {
      return;
    }
    EventRegister.emit(EVENTS.FILTER_DONE, { tx_search: val });
  };

  const renderBadge = () => {
    const { ids } = dataFilter;
    return ids && ids.length ? (
      <View style={styles.vBadge}>
        <Text style={styles.textBadge}>{ids.length}</Text>
      </View>
    ) : null
  };

  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <AppHeader
        style={styles.header}
        search
        onSubmitEditing={onSearch}
        searchStyle={styles.searchStyle}
        showBack = {false}
      >
        <AppButton
          onPress={goToFilter}
          style={styles.filter}>
          <IconImage
            source={IconFilter}
            style={styles.icon}
          />
          {renderBadge()}
        </AppButton>
      </AppHeader>
      <Tab.Navigator
        screenOptions={{
          // tabBarLabelStyle: { fontSize: 12 },
          // tabBarItemStyle: { width: 100 },
          // tabBarStyle: { backgroundColor: 'powderblue' },
          swipeEnabled: false,
          tabBarLabelStyle: { textTransform: 'none', fontSize: 12, fontFamily: setFontFamily('GilroyMedium'), },
        }}
      >
        <Tab.Screen name="B.tin tâm lý" component={AllScreen} />
        <Tab.Screen name="B.tin nội bộ" component={InterScreen} />
        <Tab.Screen name="Góc hỏi đáp" component={PostScreen} />
      </Tab.Navigator>

    </View>

  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.bg,
  },
  header: {
    marginHorizontal: 10,
    elevation: 5,
  },
  cate: {
    marginTop: 0
  },
  tvNoData: {
    textAlign: 'center'
  },
  notData: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    height: 200
  },
  filter: {

  },
  searchStyle: {
    marginRight: 10,
  },
  icon: {
    width: 36,
    height: 36
  },
  vBadge: {
    position: 'absolute',
    top: -5,
    right: -5,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.redCE3737,
    width: 20,
    height: 20,
    borderRadius: 10,
  },
  textBadge: {
    color: COLORS.white,
    fontSize: 12,
  },
});
export default NewsPage;
