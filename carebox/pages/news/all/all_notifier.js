import { CategoryModel } from '../../../../carebox/models/category_model'
import AllApiClient from './all_api_client'
import { useState, useEffect } from 'react';
import {  NewsModel } from '../../../../carebox/models/news_model'
import { EventRegister } from "../../../../carebox/utils/eventListener";
import { EVENTS } from "../../../../carebox/services/events";

const PostNotifier = () => {
  const _apiClient = new AllApiClient();

  const [list, setList] = useState([]);
  const [hashNextPage, setHashNextPage] = useState(true);
  const [isFirst, setIsFirst] = useState(true);
  const [dataFilter, setDataFilter] = useState({ ids: [] });
 

  useEffect(() => {
    loadData();
  }, [dataFilter]);

  useEffect(() => {
    // listen event filter
    const listenerFilterDone = EventRegister.addEventListener(EVENTS.FILTER_DONE, (data) => {
      setDataFilter({ ...dataFilter, ...data });
    });
    return () => {
      EventRegister.removeEventListener(listenerFilterDone);
    };
  }, [dataFilter]);

  const loadData = async () => {
    const datas = await _apiClient.getNews({ isFirst: true, ...dataFilter });
    setIsFirst(false);
    setHashNextPage(_apiClient.hashNextPage);
    setList(datas);

  }

  const loadMore = async () => {
    if (hashNextPage) {
      const _list = await _apiClient.getNews({ isFirst: false, ...dataFilter });
      setHashNextPage(_apiClient.hashNextPage);
      if (_list.length > 0) {
        const datas = [list, ..._list];
        setList(datas);
      }
    }
  }

  const favorite = (model: NewsModel) =>{
    _apiClient.favorite(model);
  }
  const [refreshing, setRefreshing] = useState(false);
  const onRefresh = async () => {
    setRefreshing(true);
    await loadData();
    setRefreshing(false);
  }


  return {
    onRefresh,
    refreshing,
    dataFilter,
    list,
    hashNextPage,
    isFirst,
    loadMore,
    favorite,
  }


}
export default PostNotifier;
