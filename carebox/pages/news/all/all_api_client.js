import * as api from '../../../../carebox/services/Request'
import ApiConfig from '../../../../carebox/services/api_config'
import { NewsModelList, NewsModel } from '../../../../carebox/models/news_model'

export default class AllApiClient {
  hashNextPage = true;
  page = 0;
  limit = 40;
  constructor() {}

  async getNews({ isFirst, ids, tx_search, order_name }) {
    if (isFirst) {
      this.page = 0;
      this.hashNextPage = true;
    }
    if (!this.hashNextPage) return [];
    const query = {
      'offset': this.page,
      'limit': this.limit,
    };

    if (ids && ids.length) {
      query['ids'] = `[${ids}]`;
    }

    if (tx_search) {
      query['tx_search'] = tx_search;
    }

    if (order_name) {
      query['order_name'] = order_name;
    }

    this.page = this.page + this.limit;
    const res = await api.methodGet({ api: ApiConfig.news, queries: query })
    if (res['success']) {
      const list = NewsModelList.fromJson(res['data']['values']).list;
      if (list.length < this.limit) {
        this.hashNextPage = false;
      }
      return list;
    }
    return [];
  }

  async favorite(model: NewsModel)  {
    const body = {'news_id': model.newsId};
    var res = await api.methodPost({api: ApiConfig.newsFavorite, body: body});
    if (model.isFavorite()) {
      model.favorited = 0;
    } else {
      model.favorited = 1;
    }
    if (res['success']) {
      return res['data'];
    }
    return false;
  }

  

}
