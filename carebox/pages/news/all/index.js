import React from 'react';
import {
    StyleSheet,
    View,
    StatusBar,
    VirtualizedList,
    RefreshControl,
} from 'react-native';

import LoadingView from '../../../../carebox/components/loading-view'
import AppText from '../../../../carebox/components/app-text'
import ItemAllView from './item_all_view'
import AllNotifier from './all_notifier'
import { fontPixel, SIZE } from '../../../../carebox/utils/sizes';
import { COLORS } from '../../../../carebox/utils/colors';

const AllScreen = ({ route }) => {

    const {
        list,
        hashNextPage,
        isFirst,
        onRefresh,
        loadMore,
        favorite,
        refreshing,
    } = AllNotifier();


    const scrollToEnd = async () => {
        console.log('scrollToEnd');
        if (hashNextPage) {
            loadMore();
        }
    }

    const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
        const paddingToBottom = 20;
        return layoutMeasurement.height + contentOffset.y >=
            contentSize.height - paddingToBottom;
    };

    const itemNoData = () => {
        return <View style={styles.notData}>
            <AppText
                title='Không có dữ liệu'
                style={styles.tvNoData}
            />
        </View>
    }

    const renderItem = ({ item, index }) => {
        if (isFirst) {
            return <LoadingView />
        } else {
            if (item === 2) {
                if (list.length == 0) {
                    return itemNoData();
                }
                return (hashNextPage && <LoadingView />);
            } else {
                return <ItemAllView
                    item={item}
                    onFavorite={favorite}
                    index={index}
                />
            }

        }

    }

    const getItem = (data, index) => {
        if (index < (data.length)) {
            return data[index];
        } else {
            return 2;
        }
    }

    const getItemCount = (data) => {
        if (data.length == 0) {
            return 1;
        }
        return data.length + 1;
    }


    return (

        <View style={styles.container}>
            <StatusBar barStyle="dark-content" />
            <VirtualizedList
                data={list}
                renderItem={renderItem}
                keyExtractor={(item, index) => `content${index}`}
                getItemCount={getItemCount}
                getItem={getItem}
                keyboardShouldPersistTaps='handled'
                onEndReachedThreshold={400}
                onScroll={({ nativeEvent }) => {
                    if (isCloseToBottom(nativeEvent)) {
                        scrollToEnd();
                    }
                }}
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={onRefresh}
                    />}
            />

        </View>

    );
};

const styles = StyleSheet.create({
    container: {
        marginTop: 15,
        flex: 1,
        backgroundColor: COLORS.bg,
    },
    header: {
        marginHorizontal: 10,
        elevation: 5,
    },
    cate: {
        marginTop: 0
    },
    tvNoData: {
        textAlign: 'center'
    },
    notData: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        height: 200
    },
    floatButtonContainer: {
        position: 'absolute',
        bottom: 10,
        right: 20,
        zIndex: 2
    },
    icon: {
        width: fontPixel(60),
        height: fontPixel(60)
    }


});
export default AllScreen;
